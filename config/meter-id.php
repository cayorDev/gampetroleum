<?php

return [
	'GAS' => [
		'1A' => 'GAS 1A',
		'1B' => 'GAS 1B',
		'2A' => 'GAS 2A',
		'2B' => 'GAS 2B',
	],
	'DPK' => [
		'1A' => 'DPK 1A',
		'1B' => 'DPK 1B',
	],
	'HFO' => [
		'1A' => 'HFO 1A',
		'1B' => 'HFO 1B',
		'2A' => 'HFO 2A',
		'2B' => 'HFO 2B',
		'3A' => 'HFO 3A',
		'3B' => 'HFO 3B',
	],
	'ADO' => [
		'1A' => 'ADO 1A',
		'1B' => 'ADO 1B',
		'2A' => 'ADO 2A',
		'2B' => 'ADO 2B',
		'3A' => 'ADO 3A',
		'3B' => 'ADO 3B',
	],
	'AGO' => [
		'1A' => 'AGO 1A',
		'1B' => 'AGO 1B',
		'2A' => 'AGO 2A',
		'2B' => 'AGO 2B',
		'3A' => 'AGO 3A',
		'3B' => 'AGO 3B',
	],
	'PMS' => [
		'1A' => 'PMS 1A',
		'1B' => 'PMS 1B',
		'2A' => 'PMS 2A',
		'2B' => 'PMS 2B',
		'3A' => 'PMS 3A',
		'3B' => 'PMS 3B',
	],
	'KEROSENE' => [
		'1A' => 'KEROSENE 1A',
		'1B' => 'KEROSENE 1B',
		'2A' => 'KEROSENE 2A',
		'2B' => 'KEROSENE 2B',
		'3A' => 'KEROSENE 3A',
		'3B' => 'KEROSENE 3B',
	],
	'JET' => [
		'1A' => 'JET 1A',
		'1B' => 'JET 1B',
		'2A' => 'JET 2A',
		'2B' => 'JET 2B',
		'3A' => 'JET 3A',
		'3B' => 'JET 3B',
	],
	'LPG' => [
		'1A' => 'LPG 1A',
		'1B' => 'LPG 1B',
		'2A' => 'LPG 2A',
		'2B' => 'LPG 2B',
		'3A' => 'LPG 3A',
		'3B' => 'LPG 3B',
	]
];