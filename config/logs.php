<?php

return [
	'1' => [
		'id'    => 1,
		'type'  => 'USERS',
		'model' => 'users',
		'placeholder' => 'user'
	],
	'2' => [
		'id'	=> 2,
		'type'	=> 'VEHICLES',
		'model' => 'vehicles',
		'placeholder' => 'vehicle'
	],
	'3' => [
		'id'	=> 3,
		'type'	=> 'DRIVERS',
		'model' => 'drivers',
		'placeholder' => 'driver'
	],
	'4' => [
		'id'	=> 4,
		'type'	=> 'OMC',
		'model' => 'omc',
		'placeholder' => 'omc'
	],
	'5' => [
		'id'	=> 5,
		'type'	=> 'IMPORTERS',
		'model' => 'importers',
		'placeholder' => 'importer'
	],
	'6' => [
		'id'	=> 6,
		'type'	=> 'INVENTORY_UPDATE',
		'model' => 'importer_product_stocks',
		'placeholder' => 'inventory for importer'
	],
	'7' => [
		'id'	=> 7,
		'type'	=> 'ORDER',
		'model' => 'orders',
		'placeholder' => 'order'
	],
	'8' => [
		'id'	=> 8,
		'type'	=> 'OMC_PRODUCTS',
		'model' => 'omc_product_stocks',
		'placeholder' => 'inventory for omc'
	],
	'9' => [
		'id'	=> 9,
		'type'	=> 'INVENTORY_TRANSACTION',
		'model' => 'importer_product_transactions',
		'placeholder' => 'inventory transcation for importer'
	],
	'10' => [
		'id'	=> 10,
		'type'	=> 'OMC_INVENTORY_TRANSACTION',
		'model' => 'omc_product_transactions',
		'placeholder' => 'inventory transcation for omc'
	],
	'11' => [
		'id'	=> 11,
		'type'	=> 'CHAMBER_ASSIGNMENT',
		'model' => 'chamber_assignment',
		'placeholder' => 'chamber assignment for order'
	],
	'12' => [
		'id'	=> 12,
		'type'	=> 'ORDER_PRODUCT_METER',
		'model' => 'order_product_meter',
		'placeholder' => 'meter reading for order product'
	],
	'13' => [
		'id'	=> 13,
		'type'	=> 'RELEASE',
		'model' => 'release',
		'placeholder' => 'release order'
	],
	'14' => [
		'id'	=> 13,
		'type'	=> 'LOADERS',
		'model' => 'loaders',
		'placeholder' => 'loader'
	],
];