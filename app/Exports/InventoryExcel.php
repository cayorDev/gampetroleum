<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Models\System\OMC\OmcProductTransactions;
use Carbon\Carbon;

class InventoryExcel implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithTitle
{
	public function __construct($data , $company)
	{
		$this->data = $data;
		$this->company = $company;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	return collect([$this->data]);	
    }

    /**
     * @return string
     */
    public function title(): string
    {
    	return 'Omc Inventory Report';
    }

    public function headings(): array
    {
    	$headings=[
    		["Company: ", $this->company],
    		[],
    		["","","DIESEL(AGO)","","","","","","PETROL(PMS)","","","","","","KEROSENE/JET"],
    		["DATE",
    		"BOUGHT(L)" ,
    		"ORDER NO.",
    		"DELIVERED(L)",
    		"BALANCE",
    		"",
    		"DATE",
    		"BOUGHT(L)" ,
    		"ORDER NO.",
    		"DELIVERED(L)",
    		"BALANCE",
    		"",
    		"DATE",
    		"BOUGHT(L)" ,
    		"ORDER NO.",
    		"DELIVERED(L)",
    		"BALANCE",],

    	];
    	return $headings;
    }

    public function map($inventorydata) :array
    {	
    	$data=[];
    	$largest= max([count($inventorydata['ADO']),count($inventorydata['PMS']),count($inventorydata['JET'])]);
    	$agoCounter =0;
    	$pmsCounter =0;
    	$jetCounter =0;
    	foreach($inventorydata['ADO'] as $ago){
    		$data[$agoCounter]['date'] = $ago['date'] !=""?Carbon::parse($ago['date'])->toDateString():"";
    		$data[$agoCounter]['bought'] = $ago['bought']?$ago['bought']:"0";
    		$data[$agoCounter]['orderNo'] = $ago['orderNo']?$ago['orderNo']:"0";
    		$data[$agoCounter]['delivered'] = $ago['delivered']?$ago['delivered']:"0";
    		$data[$agoCounter]['balance'] = $ago['balance']?$ago['balance']:"0";
    		$data[$agoCounter]['extra']="";
    		$agoCounter++;
    	}
    	while ($agoCounter<$largest) {
    		$data[$agoCounter]['date'] = "";
    		$data[$agoCounter]['bought'] = "";
    		$data[$agoCounter]['orderNo'] = "";
    		$data[$agoCounter]['delivered'] = "";
    		$data[$agoCounter]['balance'] = "";
    		$data[$agoCounter]['extra']="";
    		$agoCounter++;
    	}
    	foreach($inventorydata['PMS'] as $pms){
    		$data[$pmsCounter]['date1'] = $pms['date']!=""?Carbon::parse($pms['date'])->toDateString():"";
    		$data[$pmsCounter]['bought1'] = $pms['bought']?$pms['bought']:"0";
    		$data[$pmsCounter]['orderNo1'] = $pms['orderNo']?$pms['orderNo']:"0";
    		$data[$pmsCounter]['delivered1'] = $pms['delivered']?$pms['delivered']:"0";
    		$data[$pmsCounter]['balance1'] = $pms['balance']?$pms['balance']:"0";
    		$data[$pmsCounter]['extra1']="";
    		$pmsCounter++;
    	}
    	while ($pmsCounter<$largest) {
    		$data[$pmsCounter]['date1'] = "";
    		$data[$pmsCounter]['bought1'] = "";
    		$data[$pmsCounter]['orderNo1'] = "";
    		$data[$pmsCounter]['delivered1'] = "";
    		$data[$pmsCounter]['balance1'] = "";
    		$data[$pmsCounter]['extra1']="";
    		$pmsCounter++;
    	}
    	foreach($inventorydata['JET'] as $jet){
    		$data[$jetCounter]['date2'] = $jet['date']!=""?Carbon::parse($jet['date'])->toDateString():"";
    		$data[$jetCounter]['bought2'] = $jet['bought']?$jet['bought']:"0";
    		$data[$jetCounter]['orderNo2'] = $jet['orderNo']?$jet['orderNo']:"0";
    		$data[$jetCounter]['delivered2'] = $jet['delivered']?$jet['delivered']:"0";
    		$data[$jetCounter]['balance2'] = $jet['balance']?$jet['balance']:"0";
    		$jetCounter++;
    	}

    	return $data;
    }
}
