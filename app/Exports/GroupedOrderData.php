<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\System\Order\Order;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class GroupedOrderData implements FromCollection, WithColumnFormatting, WithHeadings, ShouldAutoSize, WithMapping
{
    private $products;

    public function __construct($products, $start, $end, $supplier)
    {
        $this->products = $products;
        $this->start = Carbon::parse($start)->startOfDay();
        $this->end = Carbon::parse($end)->endOfDay();
        $this->supplier = $supplier;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if($this->supplier == "0"){
            $importer=0;
            $omc=0;
            $supplier=0; 
        } else {
            $supplierData=explode("_",$this->supplier);
            $importer=(int)$supplierData[2];
            $omc=(int)$supplierData[0];
            $supplier=(int)$supplierData[1];
        } 
        $query = Order::whereBetween('created_at' ,[$this->start,$this->end] );
        if($importer){
            $query = $query->where('importer_id',$importer);
        }
        if($omc){
            $query = $query->where('omc_id',$omc);
        }
        if($supplier){
            $query = $query->where('supplier_omc_id',$supplier);
        }
        $order= $query->where('is_complete',1)->with('Importer','Omc', 'SupplierOmc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','Chambers','Chambers.Product','Chambers.vehicleChamber','Truck','OrderProducts.OrderProductMeter')->get();
        $productData=[];
        $productData['Total Result']['onBehalfOf']='Total Result';
        $products = $this->products;
        $orderdata=$order->map( function( $q  ) use($products, &$productData)  {
            $omc = $q->Omc ? $q->Omc->company_name.'/' : '';
            $importer = $q->Importer ? $q->Importer->company_name : '';
            $supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : '';
            $importeromc = $omc.''.$supplierOmc.''.$importer;
            if(!array_key_exists($importeromc,$productData)){
                $productData[$importeromc]=[];
                $productData[$importeromc]['onBehalfOf']=$importeromc;
            }
            foreach($products as $product){
                if(!array_key_exists($product->id,$productData[$importeromc])){
                    $productData[$importeromc][$product->id] = 0;
                } 
                if(!array_key_exists($product->id,$productData['Total Result'])){
                    $productData['Total Result'][$product->id] = 0;
                }
                foreach($q->OrderProducts as $productLoaded){
                    if($productLoaded->product_id == $product->id){

                        if(!is_null($q->Truck)){
                            $productData[$importeromc][$product->id] = $productData[$importeromc][$product->id]+$q->Truck->net_weight;
                            $productData['Total Result'][$product->id] =$productData['Total Result'][$product->id]+$q->Truck->net_weight;
                        } else {

                            $productData[$importeromc][$product->id] = $productData[$importeromc][$product->id]+$productLoaded->OrderProductMeter->quantity;
                            $productData['Total Result'][$product->id] = $productData['Total Result'][$product->id]+$productLoaded->OrderProductMeter->quantity;

                        }
                    }
                }
            }
            return $productData;
        });
        $value = $productData['Total Result'];
        unset($productData['Total Result']);
        $productData['Total Result'] = $value;
        return(collect($productData));
    }

    public function headings(): array
    {
        $headings=[
            'On Behalf Of',
        ];
        foreach($this->products as $product){
            $unit=$product->measuring_unit=='ltr'?"(L)":"(KG)";
            $product_name = $product->name;
            if ($product_name == 'PMS') {
                $product_name = 'Gasoline';
            }
            array_push($headings,"SUM - ".$product_name." ".$unit);
        }
        return $headings;
    }

    public function map($order) :array
    {	
        return[$order];
    } 

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'C' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
