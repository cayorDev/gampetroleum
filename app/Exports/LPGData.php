<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Models\System\Order\Order;
use Carbon\Carbon;

class LPGData implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithTitle
{
	private $date;

	public function __construct($start, $end)
	{
		$this->start = Carbon::parse($start)->startOfDay();
        $this->end = Carbon::parse($end)->endOfDay();
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	return Order::whereBetween('created_at' ,[$this->start,$this->end] )->where('is_complete',1)->whereHas('Truck')->with('Vehicle','Driver','Truck','OrderProducts','OrderProducts.Product')->get();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'LPG ';
    }

    public function headings(): array
    {
    	$headings=[
            "DATE",
    		"DRIVERS NAME" ,
    		"PLATE NUMBER",
    		"EMPTY WEIGHT",
    		"FULL WEIGHT",
    		"QUANTITY DELIVERED(KG)", 
    		"DESTINATION",
    	];
    	return $headings;
    }

    public function map($orderdata) :array
    {	
    	$lpgData=[];
    	if($orderdata->OrderProducts->first()->Product->sku == 'LPG')
    	{
            $lpg['date'] = $orderdata->created_at;
    		$lpg['driver'] = $orderdata->Driver->name;
    		$lpg['vehicle'] = $orderdata->Vehicle->license_plate_number;
    		$lpg['empty_weight'] = $orderdata->Truck->weight_before_loading;
    		$lpg['full_weight'] = $orderdata->Truck->weight_after_loading;
    		$lpg['quantity'] = $orderdata->Truck->net_weight;
    		$lpg['destination'] = $orderdata->Truck->location;
    		array_push($lpgData,$lpg);
    	} 
    	return $lpgData;
    }
}