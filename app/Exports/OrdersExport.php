<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersExport implements WithMultipleSheets
{
	 use Exportable;

	public function __construct($product, $dateRange)
    {
        $this->product = $product;
        $this->start = $dateRange['start'];
        $this->end = $dateRange['end'];
        $this->supplier =$dateRange['supplier'];
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
            $sheets[1] = new GroupedOrderData($this->product,$this->start,$this->end,$this->supplier);
            $sheets[2] = new allOrderData($this->product,$this->supplier,$this->start,$this->end);
     return $sheets;
    }
  
}
