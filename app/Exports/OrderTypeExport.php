<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderTypeExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithTitle
{
	public function __construct($data, $filename)
	{

		$this->data = $data;
        $this->filename = $filename;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$orderData=$this->data;
        // dd($orderData);
    	$data   = $orderData
			->map( function( $q ) {
                // dd($q);
				if(is_null($q->Importer)){
					$importer_data = $q->Importer()->withTrashed()->first();
					$importer_company = $importer_data ? $importer_data->company_name : "";
				} 
				if(is_null($q->Omc)){
					$omc_data = $q->Omc()->withTrashed()->first();
					$omc_company = $omc_data ? $omc_data->company_name.'/' : "";
				}
				if(is_null($q->SupplierOmc)){
					$supplier_omc_data = $q->SupplierOmc()->withTrashed()->first();
					$supplier_company = $supplier_omc_data ? $supplier_omc_data->company_name.'/' : "";
				}

				$omc = $q->Omc ? $q->Omc->company_name.'/' : $omc_company;
				$importer = $q->Importer ? $q->Importer->company_name : $importer_company;
				$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : $supplier_company;
                $products="";
                foreach($q->OrderProducts as $product){
                    $products=$products."Product Name : ".$product->Product->name.", Quantity : ".$product->quantity." | ";

                }
				return [
					'order_reference'   => $q->purchase_order,
					'supplier'         => $omc.''.$supplierOmc.''.$importer,
					'vehicle'          => $q->Vehicle ? $q->Vehicle->license_plate_number : '',
					'driver'           => $q->Driver ? $q->Driver->name : '',
                    'product' =>rtrim($products, '| '),
				];        
			});
			
        return $data;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->filename;
    }

    public function headings(): array
    {
    	$headings=[
            "ORDER REFERENCE",
    		"SUPPLIER" ,
    		"VEHICLE",
    		"DRIVER",
            "PRODUCTS",
    	];
    	return $headings;
    }

    public function map($data) :array
    {	
    	
    	return $data;
    }
}
