<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DailyHfoLpgData implements WithMultipleSheets
{
	use Exportable;

	public function __construct($dateRange )
	{
        $this->start = $dateRange['start'];
        $this->end = $dateRange['end'];
	}

    /**
     * @return array
     */
    public function sheets(): array
    {
    	$sheets = [];
    	$sheets[1] = new HFOData($this->start,$this->end);
    	$sheets[2] = new LPGData($this->start,$this->end);
    	return $sheets;
    }

}
