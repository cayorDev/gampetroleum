<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\System\Order\Order;
use Carbon\Carbon;

class allOrderData implements  FromCollection, WithColumnFormatting, WithHeadings, ShouldAutoSize, WithMapping
{
    private $products;

    public function __construct($products, $supplier,$start, $end)
    {
        $this->products = $products;
        $this->supplier = $supplier;
         $this->start = Carbon::parse($start)->startOfDay();
        $this->end = Carbon::parse($end)->endOfDay();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if($this->supplier == "0"){
            $importer=0;
            $omc=0;
            $supplier=0; 
        } else {
            $supplierData=explode("_",$this->supplier);
            $importer=(int)$supplierData[2];
            $omc=(int)$supplierData[0];
            $supplier=(int)$supplierData[1];
        }
          $query = Order::orderBy('order_id', 'DESC')->where('is_complete',1)->whereBetween('created_at' ,[$this->start,$this->end] );
        // $query = Order::where('is_complete',1);
        if($importer){
            $query = $query->where('importer_id',$importer);
        }
        if($omc){
          $query = $query->where('omc_id',$omc);
      }
      if($supplier){
          $query = $query->where('supplier_omc_id',$supplier);
      }
      
        return $query->with('Importer','Omc', 'SupplierOmc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','Chambers','Chambers.Product','Chambers.vehicleChamber','Truck','OrderProducts.OrderProductMeter')->get();
    }

    public function headings(): array
    {
        $headings=[
            'ID',
            'Year',
            'Ticket Date',
            'Purchase Order Number',
            'Load To',
            'On Behalf Of',
            'Vehicle Number',
        ];
        foreach($this->products as $product){
            $unit=$product->measuring_unit=='ltr'?"(L)":"(KG)";
            $product_name = $product->name;
            if ($product_name == 'PMS') {
                $product_name = 'Gasoline';
            }
            array_push($headings,$product_name." Delivered ".$unit);
        }
        return $headings;
    }

    public function map($order) :array
    {   
        $productData=[];
        foreach($this->products as $key => $product) {
            $productData[$product->id] = 0;

            if(!is_null($order->Truck)) {
                foreach($order->OrderProducts as $productLoaded) {
                    if($productLoaded->product_id == $product->id) {
                        $productData[$product->id] = $productData[$product->id] + $order->Truck->net_weight;
                    }
                } 
            } else {
                foreach($order->OrderProducts as $productLoaded) {
                   
                    if($productLoaded->product_id == $product->id) {
                        if (array_key_exists($product->id,$productData)){
                            $productData[$product->id] = $productData[$product->id]+$productLoaded->OrderProductMeter->quantity;
                        } else {
                            $productData[$product->id] = $productLoaded->OrderProductMeter->quantity;
                        }
                    }
                }
            }    
        }
        $omc = $order->Omc ? $order->Omc->company_name.'/' : '';
        $importer = $order->Importer ? $order->Importer->company_name : '';
        $supplierOmc =  $order->SupplierOmc ? $order->SupplierOmc->company_name.'/' : '';
        $importeromc = $omc.''.$supplierOmc.''.$importer;

        $finalData=[
            $order->order_id,
            // date_format($order->created_at,"m/d/Y H:i"),
            date_format($order->created_at,"Y"),
            Date::dateTimeToExcel($order->created_at),
            $order->purchase_order,
            $order->Driver->name,
            $importeromc,
            $order->Vehicle->license_plate_number,
        ];
        foreach($productData as $totalLoaded){
            array_push($finalData,$totalLoaded);

        } 
        // dump($finalData);
        return $finalData;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_XLSX22,
            // 'C' => NumberFormat::FORMAT_DATE_DATETIME,
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
