<?php

namespace App\Exports;

use App\Importer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class ImporterExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithTitle
{
	public function __construct($data, $filename)
	{

		$this->data = $data;
        $this->filename = $filename;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        return collect($this->data);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->filename;
    }

    public function headings(): array
    {
    	$headings=[
            "ORDER REFERENCE",
    		"DATE" ,
    		"TIME",
            "PRODUCT",
    		"QUANTITY",
    		"INVENTORY BALANCE BEFORE",
    	];
    	return $headings;
    }

    public function map($data) :array
    {

    	return $data;
    }
}
