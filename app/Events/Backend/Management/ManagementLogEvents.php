<?php

namespace App\Events\Backend\Management;

use Illuminate\Queue\SerializesModels;

/**
 * Class ManagementLogEvents.
 */
class ManagementLogEvents
{
    use SerializesModels;

    /**
     * @var
     */
    public $management;
    public $action;
    public $user;
    /**
     * @param $management
     */
    public function __construct($management, $action, $user)
    {
        $this->management = $management;
        $this->action = $action;
        $this->user = $user;
    }
}
