<?php

namespace App\Events\Backend\Vehicle;

use Illuminate\Queue\SerializesModels;


class VehicleLogEvents
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $vehicle;
    public $action;
    public $user;

    public function __construct( $vehicle, $action, $user) 
    {
        $this->vehicle = $vehicle;
        $this->action = $action;
        $this->user = $user;
    }
}


