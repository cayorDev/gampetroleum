<?php

namespace App\Events\Backend\Order;

use Illuminate\Queue\SerializesModels;


class OrderLogEvents
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $order;
    public $action;
    public $user;

    public function __construct( $order, $action, $user) 
    {
        $this->order = $order;
        $this->action = $action;
        $this->user = $user;
    }
}


