<?php

namespace App\Events\Backend\Driver;

use Illuminate\Queue\SerializesModels;

/**
 * Class DriverLogEvents.
 */
class DriverLogEvents
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;
    public $action;
    public $user;
    /**
     * @param $driver
     */
    public function __construct($driver, $action, $user)
    {
        $this->driver = $driver;
        $this->action = $action;
        $this->user = $user;
    }
}
