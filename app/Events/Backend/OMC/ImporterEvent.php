<?php

namespace App\Events\Backend\OMC;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ImporterEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var App\Models\System\Importer\Importer
     */
    public $_importer;

    /**
     * @var string
     */
    public $_type;

    /**
     * @var App\Models\Auth\User
     */
    public $_user;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($importer, $type, $user)
    {
        $this->_importer    = $importer;

        $this->_type        = $type;

        $this->_user        = $user;   
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}
