<?php

namespace App\Events\Backend\OMC;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OmcEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var App\Models\System\omc\omc
     */
    public $_omc;

    /**
     * @var string
     */
    public $_type;

    /**
     * @var App\Models\Auth\User
     */
    public $_user;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($omc, $type, $user)
    {
        $this->_omc   = $omc;

        $this->_type        = $type;

        $this->_user        = $user;   
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}
