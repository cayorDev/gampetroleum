<?php

namespace App\Events\Backend\Loader;

use Illuminate\Queue\SerializesModels;

/**
 * Class LoaderLogEvents.
 */
class LoaderLogEvents
{
    use SerializesModels;

    /**
     * @var
     */
    public $loader;
    public $action;
    public $user;
    /**
     * @param $loader
     */
    public function __construct($loader, $action, $user)
    {
        $this->loader = $loader;
        $this->action = $action;
        $this->user = $user;
    }
}
