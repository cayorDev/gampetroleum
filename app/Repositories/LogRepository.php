<?php

namespace App\Repositories;

use App\Models\Log;
use Config;
use App\Models\Role\User;
use App\Models\System\Importer\Importer;
use App\Models\System\OMC\Omc;
use App\Models\Driver;
use App\Models\Vechicle;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;


/**
 * Class VehicleRepository.
 */
class LogRepository extends BaseRepository
{

	/**
     * @return string
     */
    public function model()
    {
        return Log::class;
    }

    /**
     * @param array $data
     *
     * @return Log
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Log
    {
        return DB::transaction(function () use ($data) {
            $log = parent::create($data);
            if ($log) {
                return $log;
            }
            throw new GeneralException(__('exceptions.backend.access.log.create_error'));
        });
    }
     /**
     * return pattren to controller
     *
     * @return Mix data
     * @throws \Exception
     * @throws \Throwable
     */
    public function displayLog()
    {
            $log = Log::
            leftjoin('users', 'logs.action_done_by', '=', 'users.id')
           ->select('users.first_name','users.last_name','logs.action_type','logs.created_at','logs.log_id','logs.changed_record_id')
            ->get();
            $field='';
            foreach($log as $data)
            {
             $table= Config::get('log.model')[$data->log_id];
             $role[]=$table;
             switch ($table) {
                 case 'drivers':
                     $field='name';
                     break;
                     case 'omc':
                     $field='company_name';
                     break;
                     case 'importers':
                     $field='company_name';
                     break;
                     case 'users':
                     $field='first_name';
                     break;
                     default:
                      $field='';
                     break;
            }
                    $name[]=DB::table($table)->select($field)->where('id',$data->changed_record_id)->first();
        }
       
        return ['log'=>$log,'name'=>$name,'role'=>$role];
    }
}