<?php

namespace App\Repositories\Backend\OMC;

use App\Models\System\Addresses;
use App\Repositories\BaseRepository;
/**
* Class OmcRepositoryEloquent
*/

class AddressesRepository extends BaseRepository
{
	/**
	* @var Omc
	*/
	protected $model;

	public function __construct(Addresses $model)
	{

		$this->model = $model;
	}

	public function model()
	{
		return addresses::class;
	}
	/**
	 * Get record forom Addresses 
	 * @param int  $id
	 * @return mixed
	 */	
	public function getDetail($id)
    {   
		$omc= $this->model->with('omc')->find($id);
	    if( $omc ){
	    	return $omc;
	    }
	    else{
	    	
	    }
	}
	/**
	 * delete  Address 
	 * @param int  $id
	 * @return bool
	 */
	public function deleteAddress($id):bool
    {
		$omc= parent::deleteById($id);
	     return $omc;
	}
	/**
	 * Update  Address 
	 * @param mixed
	 * @return address
	 */
	public function updateAddress($request,$id):addresses
	{	
		$data=[
				'street'	=> $request->street,
				'city'		=> $request->city,
				'country'	=>$request->country,
		];
		
		$omc= parent::updateById($id,$data);
		return $omc;
	}
	/**
	 * Store  Address 
	 * @param mixed
	 * @return address
	 */
	public function store($request):addresses
	{	
		$data=[
				'street'	=> $request->street,
				'city'		=> $request->city,
				'country'	=>$request->country,
		];
		$omc= parent::create($data);
		return $omc;
	}
	/**
	 * Find omc id 
	 * @param int $id
	 * @return int $omc_id
	 */

	public function findOmcId($id)
	{
		$address =$this->model->where('id', $id)->with('omc')->first();
		if ( !is_null( $address->omc ) && isset( $address->omc ) ) {
			return $address->omc->id;
		}
		return 0;
	}
}