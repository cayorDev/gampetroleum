<?php

namespace App\Repositories\Backend\OMC;

use App\Models\System\OMC\Omc;
use App\Events\Backend\OMC\OmcCreated;
use App\Repositories\BaseRepository;
use App\Models\System\Contacts;
use App\Models\System\Locations;
use App\Models\System\Addresses;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Log;
use Auth;
use App\Models\Product;
use App\Repositories\Backend\ProductsRepository;
/**
* Class OmcRepositoryEloquent
*/

class OmcRepository extends BaseRepository   
{
	/**
	* @var Omc
	*/
	protected $model;

	/**
	 * @var  ProductsRepository
	 */
	protected $_productRepository;

	public function __construct(ProductsRepository $ProductsRepository)
	{
		$this->_productRepository = $ProductsRepository;
		parent::__construct();
	}

	
	public function model()
	{
		return omc::class;
	}

	/**
	 * Creating omc record
	 * @param  array  $data Details for omc
	 * @return bool
	 */
	public function create( array $data = [] )
	{
		$omc = new $this->model;

		if ( $omc->save() ) {
			event(new OmcCreated($omc));
		}


	}
	/**
	 * Delete omc record
	 * @param  int $id
	 * @return bool
	 */
	public function deleteOmc( $id )
	{
		if($omc=parent::deleteById($id)){
			return $omc;
		}
		throw new GeneralException(__('exceptions.backend.access.omc.delete_error'));

	}
	
	/**
	 * Get record form omc 
	 * @param int $id
	 * @return mixed
	 */
	
	public function getDetail( $id ) : Omc
	{    
		$omc= $this->model->with(['addresses','contacts', 'locations', 'productsInStock', 'productstransactions'])->where('id', $id)->first();		
		if($omc){
			return $omc;
		}
		throw new GeneralException(__('exceptions.backend.access.omc.not_confirmed'));
	}
	/**
	 * Update record form omc 
	 * @param mixed
	 * @return mixed
	 */
	public function update( $request , $id )
	{
		$data=[
		'company_name'	=> $request->company_name,
		'owner_name'	=> $request->owner_name,
		'email'			=> $request->email,
		'tel'			=> $request->tel
		];
		$omc= parent::updateById($id,$data);
		return $omc;
	}
	/**
	 * connect new address form omc 
	 * @param mixed
	 * @return mixed
	 */
	public function link($request,$id)
	{
		$data=[
		'address_id'	=> $request->id,
		];
		$omc= parent::updateById($id,$data);
		return $omc;
	}
	
	


	/**
	 * Create omc record along with address, contacts and locations
	 * @param  array  $omc       array containing omc details
	 * @param  array  $address   array containing address details
	 * @param  array  $contact   array containing contacts details
	 * @param  array  $locations array containing locations details
	 * @param  array  $products  array containing products details
	 * @param  array  $importers  array containing importers for product stock
	 * @return void
	 */
	public function createWithAddressContactsAndLocations( array $omc = [], array $address = [], array $contacts = [], array $locations = [], array $products = [], array $importers = []) : Omc
	{
		try{
			return DB::transaction(function() use( $omc, $address, $contacts, $locations, $products, $importers ) {
				$address_id = 0;
				if ( count( $address ) ) {
					$omc_address 	= Addresses::create( $address );
					$address_id		= $omc_address->id;
				}

				if ( $omc ) {

					$omc['address_id'] = $address_id;
					$omc_record = $this->model->create($omc);
					if ($omc_record) {


						if ( count( $contacts ) ) {

							
							$contact_records 	= $this->__createOmcContacts( $contacts, $omc_record->id );
						}

						if ( count( $locations ) ) {

							$location_records 	= $this->__createOmcLocations( $locations, $omc_record->id );
						}

						if ( count( $products ) ) {
							$product_records 	= $this->__createStockRecord( $omc_record, $products, $importers );
						}
						
					} else {
						throw new GeneralException(__('exceptions.backend.access.omc.create_error'));
					}
					return $omc_record;
				}
			});
		} catch( Excepiton $e ) {
			Log::error(sprintf('[%s],[%d] ERROR:[%s]', __METHOD__, __LINE__, json_encode($e->getMessage(), true)));
		}
	}

	/**
	 * Create Omc Contacts
	 * @param  array   $data 
	 * @param  integer $id   
	 * @return void
	 */
	private function __createOmcContacts( array $data = [], $id = 0 )
	{
		if ( $id && is_array( $data ) && count( $data ) ) {
			foreach( $data as $contact ) {
				$contact['omc_id'] = $id;
				Contacts::create($contact);
			}
			
		} else {
			throw new Exception(sprintf('[%s], Data:[%s]', __('exceptions.backend.access.driver.not_enough_data'), json_encode(['data'=> $data, 'id' => $id], true) ) );
		}
	}

	/**
	 * Create Omc Locations
	 * @param  array   $data 
	 * @param  integer $id   
	 * @return void
	 */

	private function __createOmcLocations( array $data = [], $id = 0 )
	{      
		if ( $id && is_array( $data ) && count( $data ) ) {
			foreach ( $data as $location ) {
				$location['omc_id'] = $id;
				Locations::create($location);
			}
		} else {
			throw new Exception(sprintf('[%s], Data:[%s]', __('exceptions.backend.access.driver.not_enough_data'), json_encode(['data'=> $data, 'id' => $id], true) ) );
		}
	}

	/**
     * Create Omc product stock record
     * @param  Omc    $omc
     * @param  array  $data
     * @param  array  $importers
     * @return void
     */
    private function __createStockRecord( $omc, array $data = [], array $importers = [] ): void
    {   
        if ( count( $data ) ) {
            foreach ( $data as $key => $product_value ) {
                if ( $key == 'count' ) {
                    continue;
                } else {
                    $product_id = Product::where('name', $key)->pluck('id')->toArray();
                    //Saving stock value to product of Importers
                    $manage = $omc->productsInStock()->create([
                        'product_id'            => $product_id[0],
                        'outstanding_balance'   => intval($product_value),   
                        'created_by'            => Auth::id(),
                        'importer_id'			=> (int)$importers[$key]
                    ]);

                    //Saving Transactions of Importers
                    // $omc->productstransactions()->create([
                    //     'product_id'            =>$manage->product_id,
                    //     'transaction_type'      =>'add',
                    //     'quantity'              =>$manage->outstanding_balance,
                    //     'created_by'            => Auth::id()
                    // ]);
                }
            }
        } 
    }

	/**
	 * [getAllRecordsForDatatable description]
	 * @return Illuminate\database\Eloquent\Builder
	 */
	public function getAllRecordsForDatatable()
	{
		return $this->model->orderBy('id');
	}

	/**
     * getList
     * @return \Illuminate\Http\Response
     */
	public function getList()
	{
		return $this->model->pluck('company_name','id');
	}

	/**
     * Return name of Products
     */ 
    public function getproductsName($data, $id)
    {
        $products = [];
         foreach($data->productstransactions as $key => $producted){
             $id=$data->productstransactions[$key]->product_id;
            foreach( Product::get(['id', 'name'])->where('id',$id)->toArray() as $key => $product ) {
                $products[$product['name']] = $product['name']; 
            }
    }
    
        return $products;
    }

    /**
     * Return id of Products
     */
     public function getproductsId($data, $id)
    {
        foreach($data->productstransactions as $product)
        {
            $id=$product->product_id;
            $products[] = Product::where('id',$id)->pluck('id')->toArray();
        }
        return $products;
    }

    public function getTrashedWithId($id)
    {
    	return $this->model->where('id', $id)->withTrashed()->first();
    }

    public function getTrashedSupplierWithId($id)
    {
    	return $this->model->where('id', $id)->withTrashed()->first();
    }

}