<?php

namespace App\Repositories\Backend\OMC;

use App\Models\System\Locations;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;


/**
* Class OmcRepositoryEloquent
*/
class OmcLocationRepository extends BaseRepository
{
	/**
	* @var model
	*/
	protected $model;

    /**
     * OmcLocationRepository constructor.
     */
	public function __construct(Locations $model)
	{
		$this->model = $model;
	}

	/**
     * @return string
     */
	public function model()
    {
        return Locations::class;
    }

    /*public function getById($id, array $columns = ['*']):Omc
    {
        return $this->_model->find($id);
        
    }*/
    /**
     * Get all the locations of specific omc from the database.
     *
     * @param int $id
     * @return object
     */
	public function getloc($id)
	{		
            return $this->model->all()->where('omc_id', $id);

	}

    /**
     * Edit the specific location of  omc in the database.
     *
     * @param int $id
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
	public function editLoc($id, $loc)
	{
         return parent::updateById($id,$loc); 
	}
	
    /**
     * Delete the specific location of  omc in the database.
     *
     * @param  int  $id
     * @return bool
     */
	public function deleteById($id):bool
	{
		return  parent::deleteById($id);
	}

    /**
     * Add new location for specific omc in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
	public function createLocation($val, $id) 
	{
		    $val['omc_id']  =  $id;
		    return  parent::create($val);
	}

}