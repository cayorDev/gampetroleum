<?php

namespace App\Repositories\Backend\Omc;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Auth;
use App\Models\System\OMC\OmcProductTransactions;
use App\Models\System\OMC\OmcProductsStock;
use App\Models\System\Order\Release;
use Carbon\Carbon;

/**
 * Class OmcProductTransactions.
 */
class OmcProductStockRepository extends BaseRepository
{

    public function __construct()
    {
        //callling constructor of parent repository
        parent::__construct();
    }
	/**
     * @return string
     */
    public function model()
    {
        return OmcProductsStock::class;
    }

    /**
     * getProductList
     * @param  int    $omcID 
     * @return [type]             [description]
     */
    public function getProductList(int $omcID)
    {
    	return $this->model->where('omc_id', $omcID)->whereNotIn('product_id',[3,6])->with('omcProducts', 'importer', 'omc')->get();  	
    }

    /**
     * getProductListAll
     * @param  int    $omcID 
     * @return [type]             [description]
     */
    public function getProductListAll()
    {
        return $this->model->with('omcProducts', 'importer', 'omc')->whereNotIn('product_id',[3,6])->withTrashed()->get();    
    }


    /**
     * getProductListAll
     * @param  int    $omcID 
     * @return [type]             [description]
     */
    public function getProductListAllOmcBased(int $omc_id)
    {
        return $this->model->with('omcProducts', 'importer', 'omc')->where('omc_id', $omc_id)->wherenotnull('supplier_omc_id')->whereNotIn('product_id',[3,6])->get();    
    }
    
    /**
     * getProductListImporterBased
     * @param  int    $omcID 
     * @return [type]             [description]
     */
    public function getProductListImporterBased(int $omcID, int $importer_id)
    {
        return $this->model->where('omc_id', $omcID)->where('importer_id', $importer_id)->with('omcProducts','importer')->get();
        
    }

    /**
     * getProductListImporterBasedSupplierOmc
     * @param  int    $omcID 
     * @param  int    $importer_id 
     * @param  int    $supplier_omc_id 
     * @return [type]             [description]
     */
    public function getProductListImporterBasedSupplierOmc(int $omcID, int $importer_id, int $supplier_omc_id)
    {
        return $this->model->where('omc_id', $omcID)->where('importer_id', $importer_id)->where('supplier_omc_id', $supplier_omc_id)->with('omcProducts','importer')->get();
    }

    /**
     * getProductListImporterBased
     * @param  int    $omcID 
     * @return [type]             [description]
     */
    public function getLpgProductListImporterBased(int $omcID, int $importer_id)
    {
        return $this->model->where('omc_id', $omcID)->where('importer_id', $importer_id)->with('omcProducts','importer')->where('product_id', 6)->first();
        
    }

    /**
     * Update importer product stocks
     * @param  array $data
     * @param  int $omc_id
     * @return mixed
     */
    public function updateOmcStock( array $data = [], int $omc_id = 0 )
    {   
        //Defining parameters
        $id             = (int)$data['stock_id'];
        $importer_id    = (int)$data['importer_id'];
        $operation      = (int)$data['operation'];
        $balance        = (int)$data['append_quantity'];
        $productId      = (int)$data['product_id'];

        if ( $this->model->where([ 'id' => $id, 'importer_id' => $importer_id ])->exists() ) {
            //Select related instance
            $stock  = $this->model->where([ 'id' => $id, 'importer_id' => $importer_id ])->first();

            $old_stock_balance = $stock->outstanding_balance;

        } else {
            //Create new instance
            $stock                  = new $this->model;
            $stock->omc_id          = $omc_id;
            $stock->importer_id     = $importer_id;
            $stock->product_id      = $productId;
            $old_stock_balance      = 0;
            $stock->created_by      =  Auth::id();
        }

        if ( $operation ) {
            $stock->outstanding_balance = $old_stock_balance - (int) $balance;
        } else {
            $stock->outstanding_balance = $old_stock_balance + (int) $balance;
        }
        
        if ( $stock->save() ) {
            $this->__updateStockInventory( $old_stock_balance, $stock );
        }            
    }

    /**
     * UPdate omc stock with release record
     * @param  Release $release <p>Model instance for Release model</p>
     * @param  int $productId
     * @param  int $balance
     * @throws \Exception
     * @return OmcProductsStock
     */
    public function updateOmcStockWithRelease( Release $release, int $productId, int $balance )
    {   
        if ( $this->model->where(['omc_id' => $release->omc_id, 'product_id' => $productId , 'importer_id' => $release->importer_id])->exists() ) {
            //Assign model value 
            $stock = $this->model->where([ 'omc_id' => $release->omc_id, 'product_id' => $productId, 'importer_id' => $release->importer_id ])->first();
            //setting balance 
            $old_stock_balance          =   $stock->outstanding_balance;

            $stock->outstanding_balance =   $stock->outstanding_balance + $balance;
        } else {
            //Create new instance
            $stock = new $this->model;
            $stock->omc_id                  = $release->omc_id;
            $stock->importer_id             = $release->importer_id;
            $stock->product_id              = $productId;
            $stock->outstanding_balance     = $balance;
            $stock->created_by              = Auth::id();
            $old_stock_balance = 0;
        }
        //Saving record
        if ( $stock->save() ) {
            //Update Stock Inventory
            $this->__updateStockInventory( $old_stock_balance, $stock, $release->id );
        }
        return $stock;
    }

     /**
     * UPdate omc stock with release record
     * @param  Release $release <p>Model instance for Release model</p>
     * @param  int $productId
     * @param  int $balance
     * @throws \Exception
     * @return OmcProductsStock
     */
     public function updateOmcStockWithReleaseOmc( Release $release, int $productId, int $balance )
     {  
        if ( $this->model->where(['omc_id' => $release->omc_id, 'product_id' => $productId , 'importer_id' => $release->importer_id, 'supplier_omc_id' => $release->supplier_omc_id])->exists() ) {
            //Assign model value 
            $stock = $this->model->where([ 'omc_id' => $release->omc_id, 'product_id' => $productId, 'importer_id' => $release->importer_id, 'supplier_omc_id' => $release->supplier_omc_id ])->first();
            //setting balance 
            $old_stock_balance          =   $stock->outstanding_balance;

            $stock->outstanding_balance =   $stock->outstanding_balance + $balance;
        } else {
            //Create new instance
            $stock = new $this->model;
            $stock->omc_id                  = $release->omc_id;
            $stock->importer_id             = $release->importer_id;
            $stock->supplier_omc_id         = $release->supplier_omc_id;
            $stock->product_id              = $productId;
            $stock->outstanding_balance     = $balance;
            $stock->created_by              = Auth::id();
            $old_stock_balance = 0;
        }
        //Saving record
        if ( $stock->save() ) {
            //Update Stock Inventory
            $this->__updateStockInventory( $old_stock_balance, $stock, $release->id );
        }
        return $stock;
    }

    /**
     * UPdate omc stock with release record
     * @param  Release $release <p>Model instance for Release model</p>
     * @param  int $productId
     * @param  int $balance
     * @throws \Exception
     * @return OmcProductsStock
     */
    public function updateOmcStockWithReleaseOmcAsImporter( Release $release, int $productId, int $balance )
    {  
        if ( $this->model->where(['omc_id' => $release->supplier_omc_id, 'product_id' => $productId , 'importer_id' => $release->importer_id])->exists() ) {
            //Assign model value 
            $stock = $this->model->where([ 'omc_id' => $release->supplier_omc_id, 'product_id' => $productId, 'importer_id' => $release->importer_id ])->first();
            //setting balance 
            $old_stock_balance          =   $stock->outstanding_balance;

            $stock->outstanding_balance =   $stock->outstanding_balance - $balance;

            //Saving record
            if ( $stock->save() ) {
                //Update Stock Inventory
                $this->__updateStockInventory( $old_stock_balance, $stock, $release->id );
            }
            
            return $stock;
        } 
    }


    /**
     * Create a new inventory record for updated balance
     * @param  int $old_balance old balance
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @param  int ReleaseId <p>( optional ) Id of Release</p>
     * @return
     */
    private function __updateStockInventory( $old_balance , $stock, int $releaseId = 0 )
    {
        $inventory              = new OmcProductTransactions;
        $inventory->omc_id      = $stock->omc_id;
        $inventory->product_id  = $stock->product_id;
        $inventory->balance_before_transaction = (int)$stock->outstanding_balance;

        if ( $stock->outstanding_balance > $old_balance ) {
            $quantity   = (int) ( $stock->outstanding_balance - $old_balance );
            $type         = 'add';
        } else {
            $quantity   = (int) ( $old_balance - $stock->outstanding_balance );
            $type         = 'deduct';
        }

        if ( $releaseId ) {
            $inventory->release_id      = $releaseId;
        }

        $inventory->quantity            = $quantity;
        $inventory->transaction_type    = $type;
        $inventory->created_by          = Auth::id();
        return $inventory->save();
    }

    /**
     * Create a new inventory record for updated balance Based on order
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function updateOrderInventory( $order, $chambers){
        $omc_id = $order->omc_id;
        $orderProducts = [];
        foreach ($chambers as $key => $chamber) {
            $previous = isset($orderProducts[$chamber->product_id]['quantity']) ? $orderProducts[$chamber->product_id]['quantity'] : 0;
            $orderProducts[$chamber->product_id]['product_id'] =  $chamber->product_id;
            $orderProducts[$chamber->product_id]['quantity'] = $previous + $chamber->OrderProductMeter->quantity;
            $orderProducts[$chamber->product_id]['order_id'] =  $chamber->order_id;

        }
        
        foreach ($orderProducts as  $orderProduct) {
            $OmcProductTransactions = OmcProductTransactions::where('product_id',$orderProduct['product_id'])->where('omc_id',$omc_id)->where('order_id',$orderProduct['order_id'])->first();
            if(!$OmcProductTransactions) {
                $OmcProductTransactions = new OmcProductTransactions;
            }
            $OmcProductTransactions->omc_id = $omc_id;
            $OmcProductTransactions->product_id  = $orderProduct['product_id'];
            $OmcProductTransactions->quantity  = $OmcProductTransactions->quantity + $orderProduct['quantity'];
            $OmcProductTransactions->order_id  = $orderProduct['order_id'];
            $OmcProductTransactions->transaction_type  = 'deduct';
            $OmcProductTransactions->created_by  = Auth::id();
            if($OmcProductTransactions->save()){

                if ($order->supplier_omc_id) {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->where('supplier_omc_id', $order->supplier_omc_id)->first();
                } else {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->first(); 
                }

                if ($OmcProductsStock) {
                    $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                    $OmcProductTransactions->save();
                    $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance - $OmcProductTransactions->quantity;
                    $OmcProductsStock->save();
                }
            }
        }
    }

    /**
     * Create a new inventory record for updated balance Based on order for LPG 
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function updateLpgOrderInventory( $order, $quantity) {
        $OmcProductTransactions = OmcProductTransactions::where('product_id', 6)->where('omc_id',$order->omc_id)->where('order_id',$order->id)->first();
        if(!$OmcProductTransactions) {
            $OmcProductTransactions = new OmcProductTransactions;
        }
        $OmcProductTransactions->omc_id = $order->omc_id;
        $OmcProductTransactions->product_id  = 6;
        $OmcProductTransactions->quantity  = $OmcProductTransactions->quantity + $quantity;
        $OmcProductTransactions->order_id  = $order->id;
        $OmcProductTransactions->transaction_type  = 'deduct';
        $OmcProductTransactions->created_by  = Auth::id();
        if($OmcProductTransactions->save()){

            $OmcProductsStock = $this->model->where('omc_id', $order->omc_id)->where('product_id', 6)->where('importer_id', $order->importer_id)->first();

            if ($OmcProductsStock) {
                $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                $OmcProductTransactions->save();
                $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance - $OmcProductTransactions->quantity;
                $OmcProductsStock->save();
            }
            
        }
    }

    /**
     * [getTransactionData description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getTransactionData($data)
    {
        $start = Carbon::parse($data['start'])->startOfDay();
        $end = Carbon::parse($data['end'])->endOfDay();
        $query = OmcProductTransactions::where('omc_id',$data['omc'])->whereBetween('created_at' ,[$start,$end] );
        $omcProductTransactions =  $query->with('Product')->get();
        return $omcProductTransactions;
    }

    /**
     * [getBalance description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getBalance($data)
    {
        $start = Carbon::parse($data['start'])->startOfDay();
        $query = OmcProductTransactions::where('omc_id',$data['omc'])->where('created_at' ,'<',  $start);
        $omcTransactionBalance =  $query->with('Product')->orderBy('created_at','Desc')->get();
        $data=[];
        foreach($omcTransactionBalance as $transaction )
        {
            if($transaction->Product->sku == "ADO"){
                if (!array_key_exists("ADO",$data)){
                    $data["AGO"]= $transaction;
                }
            } else if($transaction->Product->sku == "PMS"){
                if (!array_key_exists("PMS",$data)){
                   $data["PMS"]= $transaction;
               }
           } else if($transaction->Product->sku == "KEROSENE" || $transaction->Product->sku == "JET"){
            if (!array_key_exists("JET",$data)){
                $data["JET"]= $transaction;
            }
        }
    }
    return $data;
}

/**
     * Create a new inventory record for updated balance Based on order for LPG 
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function editLpgOrderInventory( $order, $quantity) {
        $OmcProductTransactions = OmcProductTransactions::where('product_id', 6)->where('omc_id',$order->omc_id)->where('order_id',$order->id)->first();
        if(!$OmcProductTransactions) {
            $OmcProductTransactions = new OmcProductTransactions;
        }

        $OmcProductTransactions->omc_id = $order->omc_id;
        $OmcProductTransactions->product_id  = 6;
        $OmcProductTransactions->quantity  = $OmcProductTransactions->quantity - $quantity;
        $OmcProductTransactions->order_id  = $order->id;
        $OmcProductTransactions->transaction_type  = 'add';
        $OmcProductTransactions->created_by  = Auth::id();
        if($OmcProductTransactions->save()){

            $OmcProductsStock = $this->model->where('omc_id', $order->omc_id)->where('product_id', 6)->where('importer_id', $order->importer_id)->first();

            if ($OmcProductsStock) {
                $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                $OmcProductTransactions->save();
                $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance + $OmcProductTransactions->quantity;
                $OmcProductsStock->save();
            }
            
        }
    }

    /**
     * Create a new inventory record for updated balance Based on order
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function editOrderInventory( $order, $chambers){
        $omc_id = $order->omc_id;
        $orderProducts = [];
        foreach ($chambers as $key => $chamber) {
            $previous = isset($orderProducts[$chamber->product_id]['quantity']) ? $orderProducts[$chamber->product_id]['quantity'] : 0;

            $orderProducts[$chamber->product_id]['product_id'] =  $chamber->product_id;
            $orderProducts[$chamber->product_id]['quantity'] =  $chamber->OrderProductMeter->quantity;
            $orderProducts[$chamber->product_id]['order_id'] =  $chamber->order_id;

        }
        
        foreach ($orderProducts as  $orderProduct) {
            $OmcProductTransactions = OmcProductTransactions::where('product_id',$orderProduct['product_id'])->where('omc_id',$omc_id)->where('order_id',$orderProduct['order_id'])->first();
            if(!$OmcProductTransactions) {
                $OmcProductTransactions = new OmcProductTransactions;
            }   
            $OmcProductTransactions->omc_id = $omc_id;
            $OmcProductTransactions->product_id  = $orderProduct['product_id'];
            $OmcProductTransactions->quantity  = $OmcProductTransactions->quantity - $orderProduct['quantity'];
            $OmcProductTransactions->order_id  = $orderProduct['order_id'];
            $OmcProductTransactions->transaction_type  = 'add';
            $OmcProductTransactions->created_by  = Auth::id();
           
            if($OmcProductTransactions->save()){

                if ($order->supplier_omc_id) {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->where('supplier_omc_id', $order->supplier_omc_id)->first();
                } else {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->first(); 
                }

                if ($OmcProductsStock) {
                    $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                    $OmcProductTransactions->save();
                    $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance + $OmcProductTransactions->quantity;
                     // dd($OmcProductsStock);
                    $OmcProductsStock->save();
                }
            }
        }
    }


    /**
     * Create inventory record for updated balance Based on order delete for LPG 
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function editLpgOrderInventoryOnDelete( $order, $quantity) {
        $OmcProductTransactions = new OmcProductTransactions;
        $OmcProductTransactions->omc_id = $order->omc_id;
        $OmcProductTransactions->product_id  = 6;
        $OmcProductTransactions->quantity  = $quantity;
        $OmcProductTransactions->order_id  = $order->id;
        $OmcProductTransactions->transaction_type  = 'add';
        $OmcProductTransactions->created_by  = Auth::id();
        if($OmcProductTransactions->save()){
            $OmcProductsStock = $this->model->where('omc_id', $order->omc_id)->where('product_id', 6)->where('importer_id', $order->importer_id)->first();

            if ($OmcProductsStock) {
                $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                $OmcProductTransactions->save();
                $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance + $OmcProductTransactions->quantity;
                $OmcProductsStock->save();
            }
            
        }
    }

    /**
     * Create a new inventory record for updated balance Based on order delete
     * @param  int $omc_id array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function editOrderInventoryOnDelete( $order, $chambers){
        $omc_id = $order->omc_id;
        $orderProducts = [];
        foreach ($chambers as $key => $chamber) {
            $orderProducts[$chamber->product_id]['product_id'] =  $chamber->product_id;
            $orderProducts[$chamber->product_id]['quantity'] =  $chamber->OrderProductMeter->quantity;
            $orderProducts[$chamber->product_id]['order_id'] =  $chamber->order_id;

        }
        foreach ($orderProducts as  $orderProduct) {
            $OmcProductTransactions = new OmcProductTransactions;  
            $OmcProductTransactions->omc_id = $omc_id;
            $OmcProductTransactions->product_id  = $orderProduct['product_id'];
            $OmcProductTransactions->quantity  =  $orderProduct['quantity'];
            $OmcProductTransactions->order_id  = $orderProduct['order_id'];
            $OmcProductTransactions->transaction_type  = 'add';
            $OmcProductTransactions->created_by  = Auth::id();
           
            if($OmcProductTransactions->save()){

                if ($order->supplier_omc_id) {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->where('supplier_omc_id', $order->supplier_omc_id)->first();
                } else {
                    $OmcProductsStock = $this->model->where('omc_id', $omc_id)->where('product_id', $OmcProductTransactions->product_id)->where('importer_id', $order->importer_id)->first(); 
                }

                if ($OmcProductsStock) {
                    $OmcProductTransactions->balance_before_transaction = (int)$OmcProductsStock->outstanding_balance;
                    $OmcProductTransactions->save();
                    $OmcProductsStock->outstanding_balance = $OmcProductsStock->outstanding_balance + $OmcProductTransactions->quantity;
                    $OmcProductsStock->save();
                }
            }
        }
    }
}
