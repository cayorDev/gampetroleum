<?php

namespace App\Repositories\Backend;

use App\Models\VehicleChamber;
use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Backend\Vehicle\VehicleCreated;

/**
 * Class VehicleChamberRepository.
 */
class VehicleChamberRepository extends BaseRepository
{

	/**
     * @return string
     */
    public function model()
    {
        return VehicleChamber::class;
    }

    /**
     * @param array $data
     * @param $vehicle
     *
     * @return VehicleChamber
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : VehicleChamber
    {   
        return DB::transaction(function () use ($data) {
            foreach($data as $chamberData){
                $vehicle_chamber = parent::create($chamberData);
            }
            if ($vehicle_chamber) {
                return $vehicle_chamber;
            }
            throw new GeneralException(__('exceptions.backend.access.vehicle.create_error'));
        });
    }
    
    /**
     * @param array $data
     * @param $vehicle
     *
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(VehicleChamber $vehicle, array $data) 
    {
        return DB::transaction(function () use ($vehicle,$data) { 
        $vehicle_chamber=$vehicle_chamber->update($data);
        if ($vehicle_chamber) {
            return $vehicle_chamber;
            }
            throw new GeneralException(__('exceptions.backend.access.vehicle.update_error'));
        });
    }

    /**
     * processChamberData 
     * @param  array  $chamberData 
     * @param  int    $vehicleID   
     * @return array
     */
    public function processChamberData(array $chamberData, int $vehicleID) : array
    {
        $count = 1;
        foreach( $chamberData as $data){
            $chamber_data[$count]['vehicle_id'] = $vehicleID;
            $chamber_data[$count]['chamber_number'] = $count;
            $chamber_data[$count]['capacity'] = $data;
            $count++;
        }
        return  $chamber_data;
    }

    /**
     * processChamberDataForEdit 
     * @param  array  $chamberData 
     * @param  int    $vehicleID  
     * @return array
     */
    public function processChamberDataForEdit(array $chamberData, $vehicleID) : array
    {
        $this->model->where('vehicle_id', $vehicleID)->delete();
        $count = 1;
        foreach( $chamberData as $data){
          $chamber_data[$count]['vehicle_id'] = $vehicleID;
            $chamber_data[$count]['chamber_number'] = $count;
            $chamber_data[$count]['capacity'] = $data;
            $count++;
        }
        return  $chamber_data;
    }
   
}