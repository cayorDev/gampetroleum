<?php

namespace App\Repositories\Backend;

use App\Models\Driver;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Backend\Driver\DriverLogEvents;
use Auth;
/**
 * Class DriverRepository.
 */
class DriverRepository extends BaseRepository
{

	/**
     * @return string
     */
    public function model()
    {
        return Driver::class;
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Driver
    {
        return DB::transaction(function () use ($data) {
            $data =  isset($data['driver']) ? $data['driver'] : $data;
            $driver = parent::create($data);
            if ($driver) {
                event(new DriverLogEvents($driver, "DRIVER CREATED", Auth::user()->full_name));
                return $driver;
            }
            throw new GeneralException(__('exceptions.backend.access.driver.create_error'));
        });
    }


	/**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAllRecordsForDatatable()
    {
        return $this->model->orderBy('id','desc');
    }

    /**
     * @param Driver  $driver
     * @param array $data
     *
     * @return Driver
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Driver $driver, array $data) : Driver
    {
        if (! isset($data['permissions']) || ! count($data['permissions'])) {
            $data['permissions'] = [];
        }
        return DB::transaction(function () use ($driver, $data) {
            if ($driver->update([
                'name' => $data['name'],
                'drivers_licence_number' => $data['drivers_licence_number'],
                'tel_number_1' => $data['tel_number_1'],
                'tel_number_2' => $data['tel_number_2'],
            ])) {
                    event(new DriverLogEvents($driver, "DRIVER UPDATED", Auth::user()->full_name));
                    return $driver;
            }

            throw new GeneralException(__('exceptions.backend.access.drivers.update_error'));
        });
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        dd($this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged));
    }

    /**
     * @param int    $id
     * @return Driver
     */

    public function getDeletedDriver($id) : Driver
    {
       $driver=$this->model->onlyTrashed()->where('id',$id)->get()->first();
       return($driver);
    }

    /**
     * @param int    $id
     * @return Driver
     */
    public function getdriver($id) : Driver
    {
       $driver=$this->model->find($id);
       if($driver){
            return($driver);
        }
        throw new GeneralException(__('exceptions.backend.access.driver.find_error'));

    }

     /**
     * @param Driver $driver
     *
     * @return Driver
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Driver $driver) : Driver
    {
        if (is_null($driver->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.driver.delete_first'));
        }

        return DB::transaction(function () use ($driver) {
            if ($driver->forceDelete()) {
                 event(new DriverLogEvents($driver, "DRIVER PERMANENTLY DELETED", Auth::user()->full_name));
            return $driver;
            }

            throw new GeneralException(__('exceptions.backend.access.driver.delete_error'));
        });
    }

    /**
     * @param Driver $driver
     *
     * @return Driver
     * @throws GeneralException
     */
    public function restore(Driver $driver) : Driver
    {
        if (is_null($driver->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.driver.cant_restore'));
        }

        if ($driver->restore()) {
             event(new DriverLogEvents($driver, "DRIVER RESTORED", Auth::user()->full_name));
            return $driver;
        }

        throw new GeneralException(__('exceptions.backend.access.driver.restore_error'));
    }

    /**
     * @param bool
     *
     * @return bool
     * @throws GeneralException
     */
    public function deleteById($id) : bool
    {
        $driver=$this->model->find($id);
        if($driver)
        {
            if($this->getById($id)->delete()) {
                event(new DriverLogEvents($driver, "DRIVER DELETED", Auth::user()->full_name));
            return true;
            }
            throw new GeneralException(__('exceptions.backend.access.driver.delete_error'));
        }
         throw new GeneralException(__('exceptions.backend.access.driver.delete_error'));
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->select('id','name')->get();
    }

    public function getAllDeletedRecordsForDatatable()
    {
        return $this->model->orderBy('id','desc')->onlyTrashed();
    }



}
