<?php

namespace App\Repositories\Backend\Importers;

use App\Models\System\Importer\ImporterProductsStock;
use App\Models\System\Importer\ImporterProductTransactions;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Auth;
use App\Models\System\Order\Release;
use Carbon\Carbon;

/**
 * Class ImporterProductStockRepository.
 */
class ImporterProductStockRepository extends BaseRepository
{

    public function __construct()
    {
        //callling constructor of parent repository
        parent::__construct();
    }
	/**
     * @return string
     */
    public function model()
    {
        return ImporterProductsStock::class;
    }

    /**
     * getProductList
     * @param  int    $importerID
     */
    public function getProductList(int $importerID)
    {
    	return $this->model->where('importer_id', $importerID)->with('importerProducts')->get();

    }

    /**
     * Update importer product stocks
     * @param  int $id record id
     * @param  mixed $balance
     * @return mixed
     */
    public function updateImporterStock( int $importer_id = 0, int $product_id, $balance, $operation = 0 )
    {
        if ( $this->model->where('importer_id', $importer_id)->where('product_id', $product_id)->exists() ) {
            $stock = $this->model->where('importer_id', $importer_id)->where('product_id', $product_id)->first();
            $old_stock_balance = $stock->outstanding_balance;
            if ( $operation ) {
                $stock->outstanding_balance = $old_stock_balance - (int) $balance;
            } else {
                $stock->outstanding_balance = $old_stock_balance + (int) $balance;
            }
            if ( $stock->save() ) {
                $this->__updateStockInventory( $old_stock_balance, $stock );
            }
        } else {
            // create new transaction
            $stock                      = new $this->model;
            $stock->importer_id         = $importer_id;
            $stock->product_id          = $product_id;
            $stock->outstanding_balance = $balance;
            $stock->created_by          = Auth::id();
            $old_stock_balance          = 0;
            if ( $stock->save() ) {
                $this->__updateStockInventory( $old_stock_balance, $stock );
            } 
        }

    }

    /**
     * Create a new inventory record for updated balance
     * @param  int $old_balance old balance
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @param  int $releaseId
     * @return
     */
    private function __updateStockInventory( $old_balance, $stock,  int $releaseId = 0, $orderID = null)
    {
        $inventory              = new ImporterProductTransactions;
        $inventory->importer_id = $stock->importer_id;
        $inventory->product_id  = $stock->product_id;
        $inventory->balance_before_transaction = (int)$old_balance;

        if ( $stock->outstanding_balance > $old_balance ) {
            $quantity   = (int) ( $stock->outstanding_balance - $old_balance );
            $type         = 'add';
        } else {
            $quantity   = (int) ( $old_balance - $stock->outstanding_balance );
            $type         = 'deduct';
        }

        if ( $releaseId ) {
            $inventory->release_id  = $releaseId;
        }

        $inventory->quantity            = $quantity;
        $inventory->order_id            = $orderID;
        $inventory->transaction_type    = $type;
        $inventory->created_by          = Auth::id();
        return $inventory->save();
    }

    /**
     * UPdate Importer stock with release record
     * @param  Release $release <p>Model instance for Release model</p>
     * @param  int $productId
     * @param  int $balance
     * @throws \Exception
     * @return OmcProductsStock
     */
    public function updateImporterStockWithRelease(  Release $release, int $productId, int $balance  )
    {
        //Default response
        $stock  = $this->model;
        $check  = $this->checkBalanceAvailableForImporter( $release->importer_id, $productId, $balance );
        if ( !$check['status'] ) {
            //Throw exception for error
            throw new GeneralException( $check['message'] );
        }

        if ( $this->model->where(['importer_id' => $release->importer_id, 'product_id' => $productId])->exists() ) {
            //Assign model value
            $stock = $this->model->where(['importer_id' => $release->importer_id, 'product_id' => $productId])->first();
            //Setting balance
            $old_stock_balance          = $stock->outstanding_balance;
            //setting balance
            $stock->outstanding_balance = $stock->outstanding_balance - $balance;

            //Saving record
            if ( $stock->save() ) {
                //Update Stock Inventory
                $this->__updateStockInventory( $old_stock_balance, $stock, $release->id );
            }
        }

        return $stock;
    }

    /**
     * Check if importer has available balance to handle or provide
     * @param  int|integer $importerId
     * @param  int|integer $productId
     * @param  int       $order_quantity [description]
     * @return array
     */
    private function checkBalanceAvailableForImporter( int $importerId = 0, int $productId = 0, int $order_quantity = 0 )
    {
        if ( $stock = $this->model->where(['importer_id' => $importerId, 'product_id' => $productId])->first() ) {
                $balance    = $stock->outstanding_balance;
                if ( (int) $stock->outstanding_balance < (int) $order_quantity ) {
                    $response['status'] = false;
                    $response['message'] = 'Importer Inventory for '.$stock->importerProducts->name. '(Balance: '.$stock->outstanding_balance.') is less than Ordered quantity ';

                } else {
                    $response['status'] = true;
                }
        } else {
            $response['status']     = false;
            $response['message']    = $response['message'] = 'Importer Inventory for '.$stock->importerProducts->name. ' is not available ';
        }
        return $response;
    }

    /**
     * Create a new inventory record for updated balance Based on order
     * @param  int $importer_od array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function updateOrderInventory( int $importer_id, $chambers)
    {
        foreach ($chambers as  $chamber) {
            $ImporterProductTransactions = ImporterProductTransactions::where('product_id',$chamber->product_id)->where('importer_id',$importer_id)->where('order_id',$chamber->order_id)->first();
            if(!$ImporterProductTransactions) {
                $ImporterProductTransactions = new ImporterProductTransactions;
            }
            $ImporterProductTransactions->importer_id = $importer_id;
            $ImporterProductTransactions->product_id  = $chamber->product_id;
            $ImporterProductTransactions->quantity  = $chamber->total_loaded;
            $ImporterProductTransactions->order_id  = $chamber->order_id;
            $ImporterProductTransactions->transaction_type  = 'deduct';
            $ImporterProductTransactions->created_by  = Auth::id();
            if($ImporterProductTransactions->save()){
                $ImporterProductsStock = $this->model->where('importer_id',$importer_id)->where('product_id',$ImporterProductTransactions->product_id)->first();
                if ($ImporterProductsStock) {
                    $ImporterProductTransactions->balance_before_transaction = (int)$ImporterProductsStock->outstanding_balance;
                    $ImporterProductTransactions->save();
                    $ImporterProductsStock->outstanding_balance = $ImporterProductsStock->outstanding_balance - $ImporterProductTransactions->quantity;
                    $ImporterProductsStock->save();
                }
            }
        }

    }

    public function getImporterProductBased(int $product_id){
        return $this->model->with('importers')->where('product_id',$product_id)->where('outstanding_balance','>',0)->get()->pluck('importers.company_name','importers.id');
    }

    public function getProductInventory(int $importer_id, int $product_id){
        return $this->model->where('importer_id',$importer_id)->where('product_id',$product_id)->first();
    }

    /**
     * updateStockByImporterForOrder description
     * @param  int|integer $importer_id     [description]
     * @param  int|integer $product_id      [description]
     * @param  int|integer $stock_to_reduce [description]
     * @return void
     */
    public function updateStockByImporterForOrder( int $importer_id = 0, int $product_id = 0, int $stock_to_reduce = 0 , int $order_id)
    {
        $stock  = $this->model->where( ['importer_id' => $importer_id, 'product_id' => $product_id] )->first();
        if ( !is_null( $stock ) ) {
            $old_stock_balance           = $stock->outstanding_balance;   
            $stock->outstanding_balance  = (int)$stock->outstanding_balance - (int)$stock_to_reduce;
        } else {
            $stock                      = new $this->model;
            $stock->importer_id         = $importer_id;
            $stock->product_id          = $product_id;
            $stock->outstanding_balance = 0;
            $stock->created_by          = Auth::id();
            $old_stock_balance         = 0;
        }

        if ( $stock->save() ) {
            $this->__updateStockInventory( $old_stock_balance, $stock , 0 , $order_id);
        }
    }  

      /**
     * updateStockByImporterForOrder description
     * @param  int|integer $importer_id     [description]
     * @param  int|integer $product_id      [description]
     * @param  int|integer $stock_to_add [description]
     * @return void
     */
    public function editStockByImporterForOrder( int $importer_id = 0, int $product_id = 0, int $stock_to_add = 0 , int $order_id)
    {
        $stock  = $this->model->where( ['importer_id' => $importer_id, 'product_id' => $product_id] )->first();
        if ( !is_null( $stock ) ) {
            $old_stock_balance           = $stock->outstanding_balance;   
            $stock->outstanding_balance  = (int)$stock->outstanding_balance + (int)$stock_to_add;
        } else {
            $stock                      = new $this->model;
            $stock->importer_id         = $importer_id;
            $stock->product_id          = $product_id;
            $stock->outstanding_balance = 0;
            $stock->created_by          = Auth::id();
            $old_stock_balance         = 0;
        }

        if ( $stock->save() ) {
            $this->__updateStockInventory( $old_stock_balance, $stock , 0 , $order_id);
        }
    } 

    /**
     * Create a new inventory record for updated balance Based on order
     * @param  int $importer_od array Chambers
     * @param  App\Models\System\Importer\ImporterProductsStock
     * @return
     */
    public function editOrderInventory( int $importer_id, $chambers)
    {
        foreach ($chambers as  $chamber) {
            $ImporterProductTransactions = ImporterProductTransactions::where('product_id',$chamber->product_id)->where('importer_id',$importer_id)->where('order_id',$chamber->order_id)->first();
            if(!$ImporterProductTransactions) {
                $ImporterProductTransactions = new ImporterProductTransactions;
            }
            $ImporterProductTransactions->importer_id = $importer_id;
            $ImporterProductTransactions->product_id  = $chamber->product_id;
            $ImporterProductTransactions->quantity  = $chamber->total_loaded;
            $ImporterProductTransactions->order_id  = $chamber->order_id;
            $ImporterProductTransactions->transaction_type  = 'add';
            $ImporterProductTransactions->created_by  = Auth::id();
            if($ImporterProductTransactions->save()){
                $ImporterProductsStock = $this->model->where('importer_id',$importer_id)->where('product_id',$ImporterProductTransactions->product_id)->first();
                if ($ImporterProductsStock) {
                    $ImporterProductTransactions->balance_before_transaction = (int)$ImporterProductsStock->outstanding_balance;
                    $ImporterProductTransactions->save();
                    $ImporterProductsStock->outstanding_balance = $ImporterProductsStock->outstanding_balance + $ImporterProductTransactions->quantity;
                    $ImporterProductsStock->save();
                }
            }
        }

    }
}
