<?php

namespace App\Repositories\Backend\Importers;

use App\Models\System\Importer\Importer;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Backend\Auth\PermissionRepository;
use Auth;
use App\Models\System\Addresses; 
use App\Models\Product;
/**
 * Class VehicleRepository.
 */
class ImporterRepository extends BaseRepository
{ 

    public function __construct()
    {
        //calling constructor of parent repository
        parent::__construct();
    }
	/**
     * @return string
     */
    public function model()
    {
        return Importer::class;
    }

    /**
     * @param array $data
     *
     * @return Importer
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data=[]) : Importer
    {
        return DB::transaction(function () use ($data) {
            if ( !$data['address_id'] && count( $data['address'] ) ) {
                
                $address = Addresses::create( $data['address'] );

                $data['importer']['address_id'] = $address->id;
            } else {
                $data['importer']['address_id'] = $data['address_id'] ? $data['address_id'] : 0;
            }
            //Create record for importer
            $importer = parent::create($data['importer']);
            if ($importer) {
                foreach( $data['contacts'] as $contact ) {
                    $importer->contacts()->create($contact);
                }
                $this->__createStockRecord($importer, $data['products']);
                return $importer;
            }
            throw new GeneralException(__('exceptions.backend.access.importers.create_error'));
        });
    }

    /**
     * Create Importer product stock record
     * @param  Importer $importer
     * @param  array  $data 
     * @return void
     */
    private function __createStockRecord( $importer, array $data = [] ): void
    {   
        if ( count( $data ) ) {
            foreach ( $data as $key => $product_value ) {
                if ( $key == 'count' ) {
                    continue;
                } else {
                    $product_id = Product::where('name', $key)->pluck('id')->toArray();
                    //Saving stock value to product of Importers
                    $manage = $importer->productsInStock()->create([
                        'product_id'            => $product_id[0],
                        'outstanding_balance'   => intval($product_value),   
                        'created_by'            => Auth::id()
                    ]);
                    //Saving Transactions of Importers
                    $importer->productstransactions()->create([
                        'product_id'            =>$manage->product_id,
                        'transaction_type'      =>'add',
                        'quantity'              =>$manage->outstanding_balance,
                        'created_by'            => Auth::id()
                    ]);
                   
                }
            }
        } 
    }
    public function getAllRecordsForDatatable()
    {
        return $this->model->orderBy('id');
    }
    /**
     * Delete Importer
     * @return bool $importer
     */
    public function deleteImporter($id)
    {
        $importer=parent::deleteById($id);
        return $importer;
    }
    /**
     * Importer
     */
    public function getImporters( $ids)
    {

        if ( is_array( $ids ) ) {
            return $this->model->whereIn('id', $ids)->with([ 'productsInStock', 'productstransactions', 'contacts', 'locations', 'addresses' ])->get();
        } else {
            return $this->model->where('id', $ids)->with([ 'productsInStock', 'productstransactions', 'locations', 'addresses', 'contacts' ])->first();
        }

    }
    /**
     * Return name of Products
     */ 
    public function getproductsName($data, $id)
    {
        $id=1;
        $products = [];
        if($data->productstransactions->isEmpty())
            {
                throw new GeneralException(__('exceptions.backend.access.importers.show_error'));
            }
            else
            {
                foreach($data->productstransactions as $key => $producted){     
                    $id=$data->productstransactions[$key]->product_id;          
                    foreach( Product::get(['id', 'name','sku'])->where('id',$id)->toArray() as $key => $product ) {
                        $products[$product['sku']] = $product['sku']; 
                    }      
                }
                return $products;
            }
     

        }
    
    /**
     * Return id of Products
     */
     public function getproductsId($data, $id)
    {
        foreach($data->productstransactions as $product)
        {
            $id=$product->product_id;
            $products[] = Product::where('id',$id)->pluck('id')->toArray();
        }
       
        return $products;
    }
    /**
     * Update Importer
     */
    public function updatesImporter($request,$id)
    {   
        $data=[
                'company_name'    => $request->company_name,
                'owner_name'      => $request->owner_name,
                'email'           =>$request->email,
        ];
        $importer= parent::updateById($id,$data);
        return $importer;
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->pluck('company_name','id');
    }

    public function getDetail( $id ) : Importer
    {    
        $importerAdd= $this->model->with(['addresses','contacts', 'locations', 'productsInStock', 'productstransactions'])->find($id);      
        if($importerAdd){
            return $importerAdd;
        }
        throw new GeneralException(__('exceptions.backend.access.omc.not_confirmed'));
    }

    /**
     * Create or update importer locations
     * @param  array  $data 
     * @param  string/int $importer_id
     * @return mixed
     */
    public function createOrUpdateLocation( array $data = [], $importer_id )
    {
        if ( !empty($data) ) {
            $importer   = $this->model->where('id', $importer_id)->first();
            if ( $importer->locations()->where('id', $data['id'])->count() ) {
                $location = $importer->locations()->where('id', $data['id'])->first();
                $location->update($data);
            } else {
                $importer->locations()->create($data);
            }
        }
    }

    /**
     * Create or update importer contacts
     * @param  array  $data 
     * @param  string/int $importer_id
     * @return mixed
     */
    public function createOrUpdateContact( array $data = [], $importer_id )
    {
        if ( !empty($data) ) {
            $data['omc_id'] = null;
            $importer       = $this->model->where('id', $importer_id)->first();
            if ( $importer->contacts()->where('id', $data['id'])->count() ) {
                $contact   = $importer->contacts()->where('id', $data['id'])->first();
                $contact->update($data);
            } else {
                $importer->contacts()->create($data);
            }
        }
    }

    /**
     * connect new address form omc 
     * @param mixed
     * @return mixed
     */
    public function link($request,$id)
    {
        $data=[
        'address_id'    => $request->id,
        ];
        $importer= parent::updateById($id,$data);
        return $importer;
    }

    /**
     * get importers outstanding inventory
     * @param mixed
     * @return mixed
     */
    public function getOutstandingInventory(int $importer_id)
    {
       return parent::with('productsInStock', 'productsInStock.importerProducts')->getById($importer_id);
    }

    public function getTrashedWithId($id)
    {
        return $this->model->where('id', $id)->withTrashed()->first();
    }

}