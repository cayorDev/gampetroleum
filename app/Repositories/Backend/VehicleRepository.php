<?php

namespace App\Repositories\Backend;

use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Events\Backend\Vehicle\VehicleLogEvents;
use Auth;
/**
 * Class VehicleRepository.
 */
class VehicleRepository extends BaseRepository
{

	/**
     * @return string
     */
    public function model()
    {
        return Vehicle::class;
    }

    /**
     * @param array $data
     *
     * @return Vehicle
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Vehicle
    {
        return DB::transaction(function () use ($data) {
            $vehicle = parent::create($data);
            if ($vehicle) {
                return $vehicle;
            }
            throw new GeneralException(__('exceptions.backend.access.vehicle.create_error'));
        });
    }


	/**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAllRecordsForDatatable()
    {
        return $this->model->orderBy('id','Desc');
    }

    /**
     * Get the specified model record from the database.
     *
     * @param       $id
     *
     * @return Model
     */
    public function getVehicleById($id) :Vehicle
    {
        $vehicle= $this->model->find($id);
        return $vehicle;

    }

    /**
     * getChamber
     * @param  int $id
     * @return Response
     */
    public function getChamber($id)
    {
       $chamber=$this->model->with('vehicle_chamber')->find($id);
        return $chamber;
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */

     public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param Vehicle  $vehicle
     * @param array $data
     *
     * @return Vehicle
     * @throws GeneralException
     */
    public function update(Vehicle $vehicle, array $data) :Vehicle
    {
        return DB::transaction(function () use ($vehicle, $data) {
        if ($vehicle->update($data)) {
            event(new VehicleLogEvents($vehicle, "VEHICLE UPDATED", Auth::user()->full_name));
            return $vehicle;
            }
            throw new GeneralException(__('exceptions.backend.access.vehicle.update_error'));
        });
    }

     /**
     *  Get deleted vehicles
     *
     * @param $id
     *
     * @return $deleted;

     */
    public function getDeletedVehicles($id){
        $deleted=$this->model->onlyTrashed()->where('id',$id)->get()->first();
        return $deleted;
    }

    /**
     * Delete the specified model record from the database.
     *
     * @param $id
     *
     * @return bool|null
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();
        $vehicle=$this->getById($id);
        if($this->getById($id)->delete()){
            event(new VehicleLogEvents($vehicle, "VEHICLE DELETED", Auth::user()->full_name));
            return true;
        }
        throw new GeneralException(__('exceptions.backend.access.vehicle.delete_error'));
    }


   /**
     * @param Vehicle  $vehicle
     *
     * @return Vehicle
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Vehicle $vehicle) : Vehicle
    {
        if (is_null($vehicle->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.vehicle.delete_first'));
        }
        return DB::transaction(function () use ($vehicle) {
            if ($vehicle->forceDelete()) {
                event(new VehicleLogEvents($vehicle, "VEHICLE PERMANENTLY DELETED", Auth::user()->full_name));
                return $vehicle;
            }
            throw new GeneralException(__('exceptions.backend.access.vehicle.delete_error'));
        });
    }


    /**
     * @param Vehicle $vehicle
     *
     * @return Vehicle
     * @throws GeneralException
     */
    public function restore(Vehicle $vehicle) :Vehicle
    {
        if (is_null($vehicle->id)) {
            throw new GeneralException(__('exceptions.backend.access.vehicle.cant_restore'));
        }
        if ($vehicle->restore()) {
            event(new VehicleLogEvents($vehicle, "VEHICLE RESTORED", Auth::user()->full_name));
            return $vehicle;
        }
        throw new GeneralException(__('exceptions.backend.access.vehicle.restore_error'));
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->select('id','license_plate_number')->where('status','active')->get();
    }

    public function getAllDeletedRecordsForDatatable()
    {
        return $this->model->orderBy('id','Desc')->onlyTrashed();
    }



}
