<?php

namespace App\Repositories\Backend;

use App\Models\Management;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Backend\Management\ManagementLogEvents;
use Auth;
/**
 * Class ManagementRepository.
 */
class ManagementRepository extends BaseRepository
{

/**
     * @return string
     */
    public function model()
    {
        return Management::class;
    }


	/**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAllRecordsForDatatable()
    {
        return $this->model->orderBy('id','desc');
    }

    /**
     * @param management  $management
     * @param array $data
     *
     * @return management
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(management $management, array $data) : management
    {
        if (! isset($data['permissions']) || ! count($data['permissions'])) {
            $data['permissions'] = [];
        }
        return DB::transaction(function () use ($management, $data) {
            if ($management->update([
                'name' => $data['name'],
            ])) {
                    event(new ManagementLogEvents($management, "Managemet record UPDATED", Auth::user()->full_name));
                    return $management;
            }

            throw new GeneralException(__('exceptions.backend.access.managements.update_error'));
        });
    }

    /**
     * @param int    $id
     * @return management
     */
    public function getmanagement($id) : management
    {
       $management=$this->model->find($id);
       if($management){
            return($management);
        }
        throw new GeneralException(__('exceptions.backend.access.management.find_error'));

    }

    /**
     * @param int    $id
     * @return management
     */
    public function getByPosition($position) : management
    {
       $management=$this->model->where('position',$position)->first();
       if($management){
            return($management);
        }
        throw new GeneralException(__('exceptions.backend.access.management.find_error'));

    }


    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->select('name')->get();
    }

    public function findByName($name)
    {
    	return $this->model->where('name',$name)->get()->first();
    }




}
