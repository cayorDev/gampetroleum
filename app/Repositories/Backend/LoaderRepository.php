<?php

namespace App\Repositories\Backend;

use App\Models\Loader;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Backend\Loader\LoaderLogEvents;
use Auth;
/**
 * Class LoaderRepository.
 */
class LoaderRepository extends BaseRepository
{

/**
     * @return string
     */
    public function model()
    {
        return Loader::class;
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Loader
    {
        return DB::transaction(function () use ($data) {
            $data =  isset($data['loader']) ? $data['loader'] : $data;
            $loader = parent::create($data);
            if ($loader) {
                event(new LoaderLogEvents($loader, "LOADER CREATED", Auth::user()->full_name));
                return $loader;
            }
            throw new GeneralException(__('exceptions.backend.access.loader.create_error'));
        });
    }


	/**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAllRecordsForDatatable()
    {
        return $this->model->orderBy('id','desc');
    }

    /**
     * @param loader  $loader
     * @param array $data
     *
     * @return loader
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(loader $loader, array $data) : loader
    {
        if (! isset($data['permissions']) || ! count($data['permissions'])) {
            $data['permissions'] = [];
        }
        return DB::transaction(function () use ($loader, $data) {
            if ($loader->update([
                'name' => $data['name'],
            ])) {
                    event(new LoaderLogEvents($loader, "LOADER UPDATED", Auth::user()->full_name));
                    return $loader;
            }

            throw new GeneralException(__('exceptions.backend.access.loaders.update_error'));
        });
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    
    /**
     * @param int    $id
     * @return loader
     */

    public function getDeletedLoader($id) : loader
    {
       $loader=$this->model->onlyTrashed()->where('id',$id)->get()->first();
       return($loader); 
    }

    /**
     * @param int    $id
     * @return loader
     */
    public function getloader($id) : loader
    {
       $loader=$this->model->find($id);
       if($loader){
            return($loader);
        }
        throw new GeneralException(__('exceptions.backend.access.loader.find_error'));

    }

     /**
     * @param loader $loader
     *
     * @return loader
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(loader $loader) : loader
    {
        if (is_null($loader->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.loader.delete_first'));
        }

        return DB::transaction(function () use ($loader) {
            if ($loader->forceDelete()) {
                 event(new LoaderLogEvents($loader, "LOADER PERMANENTLY DELETED", Auth::user()->full_name));
            return $loader;
            }

            throw new GeneralException(__('exceptions.backend.access.loader.delete_error'));
        });
    }

    /**
     * @param loader $loader
     *
     * @return loader
     * @throws GeneralException
     */
    public function restore(loader $loader) : loader
    {
        if (is_null($loader->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.loader.cant_restore'));
        }

        if ($loader->restore()) {
             event(new LoaderLogEvents($loader, "LOADER RESTORED", Auth::user()->full_name));
            return $loader;
        }

        throw new GeneralException(__('exceptions.backend.access.loader.restore_error'));
    }

    /**
     * @param bool
     *
     * @return bool
     * @throws GeneralException
     */
    public function deleteById($id) : bool
    {
        $loader=$this->model->find($id);
        if($loader)
        {
            if($this->getById($id)->delete()) {
                event(new LoaderLogEvents($loader, "LOADER DELETED", Auth::user()->full_name));
            return true;
            }
            throw new GeneralException(__('exceptions.backend.access.loader.delete_error'));
        }
         throw new GeneralException(__('exceptions.backend.access.loader.delete_error'));
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->select('name')->get();
    }

    public function findByName($name)
    {
    	return $this->model->where('name',$name)->get()->first();
    }

    public function getAllDeletedRecordsForDatatable()
    {
        return $this->model->orderBy('id','desc')->onlyTrashed();
    }

    
   
}