<?php

namespace App\Repositories\Backend;

use App\Models\System\Contacts;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class DriverRepository.
 */
class ContactsRepository extends BaseRepository 
{

	/**
     * @return string
     */
    public function model()
    {
        return Contacts::class;
    }
    
    /**
     * @param array $data
     *
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    public function createContact( $val , $omc_id )
    {
        $val['omc_id']  =  $omc_id;

        return  parent::create($val);
            

    }

	/**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

      public function editContact($id, $contact)  //Edit contact 
    {

         return parent::updateById($id , $contact); 
    }

    public function deleteById($id):bool
    {
        return  parent::deleteById($id);
    }
}