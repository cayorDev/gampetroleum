<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\ChamberAssignment;
use App\Repositories\Backend\Orders\OrderRepository;
use App\Repositories\Backend\LoaderRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
/**
* Class OrderRepository
*/

class ChamberAssignmentRepository extends BaseRepository
{
	/**
     * @return string
     */
	public function model()
	{
		return ChamberAssignment::class;
	}

	public function create(array $data) : ChamberAssignment
	{
		return DB::transaction(function () use ($data) {
			foreach($data['products'] as $productData)
			{
				if ( (int)$productData['quantity'] ) {
					if(!is_null($productData['productID']) && $productData['quantity'] <= 0)
					{
						throw new GeneralException(__('exceptions.backend.access.order.create_error'));

					} else if(!is_null($productData['productID'])) {
						if( isset($productData['loader'])){
							$loader = new LoaderRepository();
							$loaderData =$loader->findByName($productData['loader']);
							if(is_null($loaderData)){
								$loader->create(['name'=>$productData['loader']]);
							}
						}
						$assignChamber['order_id'] = $data['orderID'];
						$assignChamber['vehicle_chamber_id'] = $productData['chamberID'];
						$assignChamber['product_id'] = $productData['productID'];
						$assignChamber['quantity'] = $productData['quantity'];
						$assignChamber['loader'] = isset($productData['loader']) ? $productData['loader'] : '';
						$assignChamber['total_loaded'] = isset($productData['total_loaded']) ? $productData['total_loaded'] : 0;
						if(isset($productData['id'] ) && !is_null($productData['id'])){
							$chamberProduct = $this->model->find($productData['id']);
							$chamberProduct->update($assignChamber);
						} else {
							$chamberProduct = parent::create($assignChamber);
							$OrderRepository = new OrderRepository();
							$OrderRepository->markChamberAssigned($data['orderID']);
						}
					} else {
						throw new GeneralException(__('exceptions.backend.access.order.create_error'));
						
					}
				}
				
			}
			if ($chamberProduct) {
				return $chamberProduct;
			}
			throw new GeneralException(__('exceptions.backend.access.order.create_error'));
		});

	}

	/**
	 * deleteChambersAssigned
	 * @param  int    $orderID
	 * @return response
	 */
	public function deleteChambersAssigned(int $orderID)
	{
		$assignedChambers=parent::where('order_id', $orderID)->delete();
		return $assignedChambers;
	}

	public function getAllLoaders()
	{
		return $this->model->select('loader')->whereNotNull('loader')->groupBy('loader')->get();
	}

	public function update(array $data) : ChamberAssignment
	{
		return DB::transaction(function () use ($data) {
			$this->deleteChambersAssigned($data['order_id']);
			foreach($data['products'] as $productData)
			{
				if(isset($productData['id'] ) && ($productData['quantity'] == 0 || is_null($productData['productID']))){
					parent::where('id', $productData['id'])->delete();
					continue;
				}
				if ( (int)$productData['quantity'] ) {
					
					if(!is_null($productData['productID']) && $productData['quantity'] <= 0)
					{
						throw new GeneralException(__('exceptions.backend.access.order.create_error'));

					} else if(!is_null($productData['productID'])) {
						if( isset($productData['loader'])){
							$loader = new LoaderRepository();
							$loaderData =$loader->findByName($productData['loader']);
							if(is_null($loaderData)){
								$loader->create(['name'=>$productData['loader']]);
							}
						}
						$assignChamber['order_id'] = $data['order_id'];
						$assignChamber['vehicle_chamber_id'] = $productData['chamberID'];
						$assignChamber['product_id'] = $productData['productID'];
						$assignChamber['quantity'] = $productData['quantity'];
						$assignChamber['loader'] = isset($productData['loader']) ? $productData['loader'] : '';
						$assignChamber['total_loaded'] = isset($productData['total_loaded']) ? $productData['total_loaded'] : 0;
							$chamberProduct = parent::create($assignChamber);
							$OrderRepository = new OrderRepository();
							$OrderRepository->markChamberAssigned($data['order_id']);
						
					} else {
						$chamberProduct = $this->model;
					}
				}
				
			}
			if ($chamberProduct) {
				return $chamberProduct;
			}
			throw new GeneralException(__('exceptions.backend.access.order.create_error'));
		});

	}
}