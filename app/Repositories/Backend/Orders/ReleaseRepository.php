<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\Release;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;

/**
* Class ReleaseRepository
*/

class ReleaseRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Release::class;
    }

    /**
     * @param array $data
     *
     * @return Release
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Release
    {
        return DB::transaction(function () use ($data) {
            $release = parent::create($data);
            if ($release) {
                return $release;
            }
            throw new GeneralException(__('exceptions.backend.access.release.create_error'));
        });
    }

    /**
     * getAllRecordsForDatatable 
     */
    public function getAllRecordsForDatatable()
    {
        
        return parent::with('Importer','Omc','SupplierOmc')->orderBy('id');
    }

    /**
     * getOrderData
     * @param  int    $orderID
     */
    public function getReleaseData(int $releaseID)
    {
        $release = parent::with('releaseProducts','releaseProducts.Product')->getById($releaseID); 
         $importer = $release->Importer()->withTrashed()->first();
        $omc = $release->Omc()->withTrashed()->first();
        $supplier_omc = $release->supplierOmc()->withTrashed()->first();
        $release['Importer']=$importer;
        $release['Omc']=$omc;
        $release['SupplierOmc']=$supplier_omc;
        return $release;
    }

}