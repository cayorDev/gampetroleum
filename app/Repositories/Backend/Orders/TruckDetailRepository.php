<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\TruckDetail;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Auth;
/**
* Class TruckDetailRepository
*/

class TruckDetailRepository extends BaseRepository
{
	/**
     * @return string
     */
	public function model()
	{
		return TruckDetail::class;
	}

	/**
     * @param array $data
     *
     * @return TruckDetail
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : TruckDetail
    {

         return DB::transaction(function () use ($data) {
            $truckData = $this->model->where('order_id',$data['orderID'])->get()->first();
            $data['truck']['order_id'] = $data['orderID'];
            // $data['truck']['supervisor'] = Auth::user()->id;
            if($truckData){
                $truckData->update($data['truck']);
            }else{
                $truckData = parent::create($data['truck']);
            }

            if ($truckData) {
                return $truckData;
            }
            throw new GeneralException(__('exceptions.backend.access.order.create_error'));
        });
     }

    public function deleteTruckDetail(int $orderID)
    {
        $truckDetail=parent::where('order_id', $orderID)->delete();
        return $truckDetail;
    }

    
}