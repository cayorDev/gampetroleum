<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\OrderProductMeter;
use App\Repositories\Backend\Orders\OrderRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;

/**
* Class OrderProductMeterRepository
*/

class OrderProductMeterRepository extends BaseRepository
{
	/**
     * @return string
     */
	public function model()
	{
		return OrderProductMeter::class;
	}

	/**
     * @param array $data
     *
     * @return OrderProductMeter
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : OrderProductMeter
    {
        return DB::transaction(function () use ($data) {
        	foreach($data['product'] as $detail) {
                $detail['order_id'] = $data['orderID'];
                if(isset($detail['id']) && $detail['id']){
                    $orderProductMeter = $this->model->find($detail['id']);
                    $orderProductMeter->update($detail);
                } else {


                   $orderProductMeter = parent::create($detail);
                   $OrderRepository = new OrderRepository();
                   $OrderRepository->markOrderVerified($data['orderID']);
               }
           }
           if ($orderProductMeter) {
            return $orderProductMeter;
        }
        throw new GeneralException(__('exceptions.backend.access.order.create_error'));
    });
    }

    public function deleteOrderProductMeter(int $orderID)
    {
        $assignedProductMeter=parent::where('order_id', $orderID)->delete();
        return $assignedProductMeter;
    }

    public function update(array $data, $orderProducts, $orderID) : OrderProductMeter
    {
        return DB::transaction(function () use ($data, &$orderID, &$orderProducts) {
         $this->deleteOrderProductMeter($orderID);
         foreach($data['product'] as $detail) {
            $detail['order_id'] = $data['id'];
            foreach($orderProducts as $orderProd){
                if($detail['product_id']==$orderProd->product_id){
                    $detail['order_product_id'] = $orderProd->id;
                }
            }
            $orderProductMeter = parent::create($detail);
        }
        if ($orderProductMeter) {
            return $orderProductMeter;
        }
        throw new GeneralException(__('exceptions.backend.access.order.create_error'));
    });
    }
}