<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\Order;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
/**
* Class OrderRepository
*/

class OrderRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
      return Order::class;
    }

    /**
     * @param array $data
     *
     * @return Order
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Order
    {
      return DB::transaction(function () use ($data) {

        // $order = $this->model->orderBy('order_id','DESC')->first();
        // $data['order_id'] = $order->order_id + 1;
        // $rules = array(
        //   'order_id' => 'unique:orders,order_id,NULL,id,deleted_at,NULL'
        //   );
        // $validate = Validator::make($data,$rules);
        // if($validate->passes())
        // {
        $data['created_by'] = \Auth::id();
        $order = parent::create($data);
        if ($order) {
          return $order;
        }
        // } 

        throw new GeneralException(__('exceptions.backend.access.order.create_error'));
      });
    }

    /**
     * getAllRecordsForDatatable
     */
    public function getAllRecordsForDatatable()
    {
      return parent::with('Importer','Omc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','OrderProducts.Product','Chambers','Chambers.Product','OrderProducts.OrderProductMeter')->orderBy('id');
    }

    /**
     * getAllRecordsForDatatable
     */
    public function getOrdersDataForDatatable($type = 'uplifting')
    {
      $orders = parent::with('Importer','Omc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','OrderProducts.Product','Chambers','Chambers.Product','OrderProducts.OrderProductMeter');
      switch ($type) {
        case 'loadverify':
        return $orders->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',0)->where('is_order_verified',0)->where('is_complete',0)->orderBy('id');
        break;
        case 'orderforapproval':
        return $orders->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',1)->where('is_order_verified',1)->where('is_complete',0)->orderBy('id');
        break;
        case 'released':
        return $orders->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',1)->where('is_order_verified',1)->where('is_complete',1)->orderBy('id');
        break;
        default:
        return $orders->where('is_chamber_assigned', 0)->orwhere('is_vehicle_loaded',0)->orderBy('id');
        break;
      }
      return [];
    }


    /**
     * get Orders Count
     */
    public function getOrderCount()
    {
      $upliftingOrders = $this->model->where('is_chamber_assigned', 0)->orwhere('is_vehicle_loaded',0)->count();
      $ordersForVerification  = $this->model->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',0)->where('is_order_verified',0)->where('is_complete',0)->count();
      $pendingapproval = $this->model->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',1)->where('is_order_verified',1)->where('is_complete',0)->count();
      $completeOrders = $this->model->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',1)->where('is_order_verified',1)->where('is_complete',1)->count();
      return ['uplifting' => $upliftingOrders, 'loadverify' => $ordersForVerification,'orderforapproval' => $pendingapproval ,'released' => $completeOrders];
    }
    /**
     * Mark Chamber Assignment
     * @param   $orderID
     */
    public function markChamberAssigned($orderID)
    {
      $order = $this->model->find($orderID);
      if (!$order->is_chamber_assigned){
        $order->is_chamber_assigned = true;
        $order->chamber_assigned_at = \Carbon\Carbon::now();
        $order->save();
      }
      return $order;
    }

    /**
     * Mark Vehicle Loaded
     * @param   $orderID
     */
    public function markVehicleLoaded($orderID)
    {
      $order = $this->model->find($orderID);
      if (!$order->is_vehicle_loaded){
        $order->is_vehicle_loaded = true;
        $order->vehicle_loaded_at = \Carbon\Carbon::now();
        $order->save();
      }
      return $order;
    }

    /**
     * Mark Order verified
     * @param   $orderID
     */
    public function markOrderVerified($orderID)
    {
      $order = $this->model->find($orderID);
      if (!$order->is_order_verified){
        $order->is_order_verified = true;
        $order->order_verified_at = \Carbon\Carbon::now();
        $order->save();
      }
      return $order;
    }

    /**
     * Mark Order verified
     * @param   $orderID
     */
    public function approveOrder($orderID)
    { 
      $order = $this->model->find($orderID);

      if (!$order->is_complete){
        $previous_order = $this->model->orderBy('order_id','DESC')->first();
        $data['order_id'] = $previous_order->order_id + 1;
        $rules = array(
          'order_id' => 'unique:orders,order_id,NULL,id,deleted_at,NULL'
        );
        $validate = Validator::make($data,$rules);
        if($validate->passes())
        {
          $order->order_id =  $data['order_id'];
          $order->is_complete = true;
          $order->completed_at = \Carbon\Carbon::now();
          $order->approved_by = \Auth::id();
          $order->save();
        }

      }
      return $order;
    }


    /**
     * deleteOrder
     * @param   $orderID
     */
    public function deleteOrder($orderID)
    {
      $order = $this->model->find($orderID);
      if ($order->is_complete){
        $this->rearrangeNextOrders($orderID);
      } 
      $order=parent::deleteById($orderID);
      return $order;

    }

    /**
     * getOrderData
     * @param  int    $orderID
     */
    public function getOrderData(int $orderID)
    {

      $order = parent::with('Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','OrderProducts.Product','Chambers','Chambers.Product','Chambers.vehicleChamber', 'OrderProducts.OrderProductMeter','Truck','Truck.Supervisor','getOmcOutstandingInventory','AuthorisedBy','CreatedBy')->getById($orderID);


      $importer = $order->Importer()->withTrashed()->first();
      $omc = $order->Omc()->withTrashed()->first();
      $supplier_omc = $order->supplierOmc()->withTrashed()->first();
      $order['importer']=$importer;
      $order['omc']=$omc;
      $order['supplierOmc']=$supplier_omc;
      return $order;
    }

    /**
     * getOrderData
     * @param  int    $orderID
     */
    public function getOrderDataSpecial(int $orderID)
    {
      $order = parent::with('Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','OrderProducts.Product','Truck','getOmcOutstandingInventory','AuthorisedBy','CreatedBy')->getById($orderID);
      $importer = $order->Importer()->withTrashed()->first();
      $omc = $order->Omc()->withTrashed()->first();
      $supplier_omc = $order->supplierOmc()->withTrashed()->first();
      $order['importer']=$importer;
      $order['omc']=$omc;
      $order['supplierOmc']=$supplier_omc;
      return $order;
    }

    /**
     * update
     * @param  Order  $order
     * @param  array  $data
     */
    public function update(Order $order, array $data) : Order
    {
      return DB::transaction(function () use ($order, $data) {

        $data['seen_at'] = isset($data['seen_by'])&&!is_null($data['seen_by']) ? \Carbon\Carbon::now() : null;
        if ($order->update($data)) {
                // if (!is_null($order->loader1)) {
          $this->markVehicleLoaded($order->id);
                // } 
          if(!is_null($order->authorized_by)) {
            $this->markAuthorizedAt($order->id);
          } 
          return $order;
        }
        throw new GeneralException(__('exceptions.backend.access.order.edit_error'));
      });
    }

    /**
     * Mark Authorized At
     * @param   $orderID 
     */
    public function markAuthorizedAt($orderID)
    {
      $order = $this->model->find($orderID);
      if (!is_null($order->authorized_by)){
        $order->authorized_at = \Carbon\Carbon::now();
        $order->save(); 
      }
      return $order;
    }

    /**
     * get Omc Balance
     * @param  int  $orderID
     */
    public function getOmcBalance(int $orderID) {
      return parent::with('getOmcOutstandingInventory','getOmcOutstandingInventory.omcProducts','OrderProducts','OrderProducts.Product')->getById($orderID);
    }

    /**
     * get Inventory Balance
     * @param  array  $order
     */
    public function getOutStandingBalance($order) {
      $products = [];
      $omcInventory = $order->getSupplierOmcOutstandingInventory?:getOmcOutstandingInventory;

      foreach ($omcInventory as $key => $inventory) {
        if ($order->importer_id === $inventory->importer_id) {
          $products[$inventory->product_id] = $inventory->outstanding_balance;
        }
      }
      return $products;
    }

    /**
     * get Inventory Balance
     * @param  array  $order
     */
    public function getImporterOutstandingInventory($order) {
      $products = [];
      $omcInventory = $order->getSupplierOmcOutstandingInventory?:getOmcOutstandingInventory;
      foreach ($omcInventory as $key => $inventory) {
        if ($order->importer_id === $inventory->importer_id) {
          $products[$inventory->product_id] = $inventory->outstanding_balance;
        }
      }
      return $products;
    }


    /**
     * Rearrange Next Orders
     *
     * @comment
     * int $prev_order_id
     *
     * @return void
    */

    protected function rearrangeNextOrders(int $prev_order_id)
    {
      if ($prev_order_id != null) {
        $order = $this->model->where('id', $prev_order_id)->first();
        if (!is_null($order)) {
          $prev_order_order_id = $order->order_id;
          $next_orders = $this->getNextOrders($prev_order_order_id);
          if (! is_null($next_orders)) {
            $order_count = $prev_order_order_id;
            foreach ($next_orders as $next_order) {
              $next_order->order_id = $order_count;
              $next_order->save();
              $order_count++;
            }
          }
        }
      }
    }

    /**
     * Get Next Orders
     *
     * @comment param int $order_order_id
     *
     * @return Collection
     */
    protected function getNextOrders(int $order_order_id)
    {
      $next_orders = null;
      if ($order_order_id != null) {
        $next_orders = $this->model
        ->where('order_id', '>', $order_order_id)->where('is_complete',1)->get();

      }
      return $next_orders;
    }

    /**
     * getAllData
     */
    public function getOrderReportData($start, $end, $supplier=0)
    {
      if($supplier == "0"){
        $importer=0;
        $omc=0;
        $supplier=0; 
      } else {
        $supplierData=explode("_",$supplier);
        $importer=(int)$supplierData[2];
        $omc=(int)$supplierData[0];
        $supplier=(int)$supplierData[1];
      } 
      $start = Carbon::parse($start)->startOfDay();
      $end = Carbon::parse($end)->endOfDay();

      $query = parent::whereBetween('created_at' ,[$start,$end] );

      if($importer){
        $query = $query->where('importer_id',$importer);
      }
      if($omc){
        $query = $query->where('omc_id',$omc);
      }
      if($supplier){
        $query = $query->where('supplier_omc_id',$supplier);
      }
      $order = $query->where('is_complete',1)->with('Importer','Omc', 'SupplierOmc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','Chambers','Chambers.Product','Chambers.vehicleChamber','Truck','OrderProducts.OrderProductMeter')->get();
      return $order;
    }

    public function getSuppliers(){
      $suppliers = parent::where('is_complete',1)->with('Importer','Omc', 'SupplierOmc')->get();
      return $suppliers;
    }

    public function getDailyReportData($start, $end)
    {   
      $start = Carbon::parse($start)->startOfDay();
      $end = Carbon::parse($end)->endOfDay();
      $query = parent::whereBetween('created_at' ,[$start,$end] );
      $order = $query->where('is_complete',1)->whereHas('Truck')->with('Vehicle','Driver','Truck','OrderProducts','OrderProducts.Product')->get();
      return $order;
    }

    public function getAllLoadersForDatatable()
    {
      return $this->model->select('loader1', 'loader2','id')->orWhereNotNull('loader1')->orWhereNotNull('loader2');
    }

    public function getCustomsOrdersDataForDatatable()
    {
      $orders = parent::with('Importer','Omc','Vehicle','Vehicle.vehicle_chamber','Driver','OrderProducts','OrderProducts.Product','Chambers','Chambers.Product','OrderProducts.OrderProductMeter')->where('enable_customs',1)->where('is_chamber_assigned', 1)->where('is_vehicle_loaded',1)->where('is_order_verified',1)->where('is_complete',1)->orderBy('id');

      return $orders;

    }

    public function updateCustomApproved(int $orderID){
      $order = $this->model->find($orderID);
      if($order){
        if ($order->enable_customs === 0){
          $order->enable_customs = 1;
        } else {
          $order->enable_customs = 0;
        }
        $order->save();

      } 
      return $order;
    }

    /**
     * editOrder
     * @param  Order  $order
     * @param  array  $data
     */
    public function editOrder(Order $order, array $data) : Order
    {
      return DB::transaction(function () use ($order, $data) {
        if ($order->update($data)) {
          return $order;
        }
        throw new GeneralException(__('exceptions.backend.access.order.edit_error'));
      });
    }

  }
