<?php

namespace App\Repositories\Backend\Orders;

use App\Models\System\Order\OrderProduct;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
/**
* Class OrderProductRepository
*/

class OrderProductRepository extends BaseRepository
{
/**
     * @return string
     */
public function model()
{
    return OrderProduct::class;
}

    /**
     * @param array $data
     *
     * @return Order
     * @throws \Exception
     * @throws \Throwable
     */
    public function addOrderProduct(array $data, int $orderID) : OrderProduct
    {   
        return DB::transaction(function () use ($data, &$orderID) {
            unset($data['products']['count']);

            foreach($data['products'] as $productID => $quantity) {
                if ( $quantity > 0 ) {
                    $orderProductData['product_id'] = $productID;
                    $orderProductData['order_id'] = $orderID;
                    $orderProductData['quantity'] = $quantity;
                    $order_product = parent::create($orderProductData);
                }
            }
            if ($order_product) {
                return $order_product;
            }
            throw new GeneralException(__('exceptions.backend.access.order.create_error'));
        });
    }



    public function deleteOrderProducts(int $orderID)
    {
        $orderProducts=parent::where('order_id', $orderID)->delete();
        return $orderProducts;
    }

    public function getOrderProducts(int $orderID)
    {
        return parent::where('order_id', $orderID)->with('Product')->get()->pluck('Product.id');
    }

    public function updateOrderProduct(array $data, int $orderID) : OrderProduct
    {
        return DB::transaction(function () use ($data, &$orderID) {
            $this->model->where('order_id', $orderID)->delete();
            foreach($data['products'] as $product) {
                $orderProductData['product_id'] = $product['productID'];
                $orderProductData['order_id'] = $orderID;
                $orderProductData['quantity'] = $product['quantity'];
                $order_product = parent::create($orderProductData);
            }
            if ($order_product) {
                return $order_product;
            }
            throw new GeneralException(__('exceptions.backend.access.order.edit_error'));
        });
    }

    public function checkifSpecial(int $order_id)
    {
        return $this->model->whereIn('product_id',[3,6])->where('order_id',$order_id)->count();
    }

    /**
     * [editOrderProduct description]
     * @param  array  $data    [description]
     * @param  int    $orderID [description]
     * @return [type]          [description]
     */
    public function editOrderProduct(array $data, int $orderID) 
    {   $orderProductDetail=array();

        return DB::transaction(function () use ($data, &$orderID, &$orderProductDetail) {
            $this->deleteOrderProducts($orderID);
              unset($data['products']['count']);
            foreach($data['products'] as $productID => $quantity) {
                if ( $quantity > 0 ) {
                    $orderProductData['product_id'] = $productID;
                    $orderProductData['order_id'] = $orderID;
                    $orderProductData['quantity'] = $quantity;
                   
                    $order_product = parent::create($orderProductData);
                    array_push($orderProductDetail,$order_product);
                }
            }
            if (count($orderProductDetail)) {
                return $orderProductDetail;
            }
            throw new GeneralException(__('exceptions.backend.access.order.create_error'));
        });
    }

    public function getOrderProductData(int $orderID)
    {
        return parent::where('order_id', $orderID)->get();
    }
}