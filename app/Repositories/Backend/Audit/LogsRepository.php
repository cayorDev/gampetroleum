<?php

namespace App\Repositories\Backend\Audit;

use App\Models\Log;
use Config;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class VehicleRepository.
 */
class LogsRepository extends BaseRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return Log::class;
    }

    /**
     * @param array $data
     *
     * @return Config
     */
    public function logConfigs()
    {   
        return Config::get('logs');
    }

    /**
     * @param array $data
     *
     * @return Log
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Log
    {
        return DB::transaction(function () use ($data) {
            $log = parent::create($data);
            if ($log) {
                return $log;
            }
            throw new GeneralException(__('exceptions.backend.access.log.create_error'));
        });
    }
     /**
     * return pattren to controller
     *
     * @return Mix data
     * @throws \Exception
     * @throws \Throwable
     */
     public function getData()
     {
        $auditLogs = $this->model
        ->leftjoin('users', 'logs.action_done_by', '=', 'users.id')
        ->select('users.first_name','users.last_name','logs.action_type','logs.created_at','logs.log_id','logs.changed_record_id', 'logs.id')
        ->orderby('created_at','DESC')
        ->paginate(15);
        $data =[];
        foreach($auditLogs as $log)
        {
            if ( $log->log_id ) {
                $loglabel = $this->logConfigs()[$log->log_id];
                $query=DB::table($loglabel['model']);
                switch ($loglabel['model']) {
                    case 'users':
                    $fields = ['first_name','last_name'];
                    $query = $query->where('id',$log->changed_record_id);
                    break;
                    case 'vehicles':
                    $fields = 'license_plate_number';
                     $query = $query->where('id',$log->changed_record_id);
                    break;    
                    case 'drivers':
                    $fields = 'name';
                     $query = $query->where('id',$log->changed_record_id);
                    break;
                    case 'omc':
                    $fields = 'company_name';
                     $query = $query->where('id',$log->changed_record_id);
                    break;
                    case 'importers':
                    $fields = 'company_name';
                     $query = $query->where('id',$log->changed_record_id);
                    break;
                    case 'importer_product_stocks':
                    $fields = ['importer_id', 'product_id','importers.company_name'];
                    $query  = $query->where('importer_product_stocks.id',$log->changed_record_id)->leftJoin('importers', 'importer_product_stocks.importer_id', '=', 'importers.id');
                    break;
                    case 'orders':
                    $fields = ['omc_id','id','order_id'];
                    $query = $query->where('id',$log->changed_record_id);
                    break;
                    case 'omc_product_stocks':
                    $fields = ['omc_id', 'product_id','omc.company_name'];
                    $query  = $query->where('omc_product_stocks.id',$log->changed_record_id)->leftJoin('omc', 'omc_product_stocks.omc_id', '=', 'omc.id');
                    break;
                    case 'importer_product_transactions':
                    $fields = ['importer_id', 'product_id','importers.company_name'];
                      $query  = $query->where('importer_product_transactions.id',$log->changed_record_id)->leftJoin('importers', 'importer_product_transactions.importer_id', '=', 'importers.id');
                    break;
                    case 'omc_product_transactions':
                    $fields = ['omc_id', 'product_id','omc.company_name'];
                    $query  = $query->where('omc_product_transactions.id',$log->changed_record_id)->leftJoin('omc', 'omc_product_transactions.omc_id', '=', 'omc.id');
                    break;
                    case 'chamber_assignment':
                    $fields = 'orders.order_id';
                    $query  = $query->where('chamber_assignment.id',$log->changed_record_id)->leftJoin('orders', 'chamber_assignment.order_id', '=', 'orders.id');
                    break;
                    case 'order_product_meter':
                    $fields = 'orders.order_id';
                    $query  = $query->where('order_product_meter.id',$log->changed_record_id)->leftJoin('orders', 'order_product_meter.order_id', '=', 'orders.id');
                    break;
                    case 'release':
                    $fields = 'id';
                    $query = $query->where('id',$log->changed_record_id);
                    break;
                }
                $values  = $query->select($fields)->first();
                $data[] = array(
                    'updated_by'                => $log->first_name.' '.$log->last_name,
                    'action'                    => $log->action_type,
                    'updates'                   => $values,
                    'placeholder'               => $loglabel['placeholder'],
                    'time_ago'                  => \Carbon\Carbon::parse($log->created_at)->diffForHumans(\Carbon\Carbon::now()),
                    'model'                     =>$loglabel['model'],
                    'super_admin_action'        => ( Auth::check() && Auth::user()->isSuperAdmin() ) ? '<a href="/admin/audit/log/'.$log->id.'/remove" title="Remove log" style="color:indianred;"><i class="fas fa-times-circle" area-hidden="true"></i></a>' : ''
                );
            }
        }
        return (['data'=>$data, 'paginationdata'=>$auditLogs]);
        // return $this->convertAndPaginate($data);
    }

    /**
     * Convert to collection and paginate array|Object
     * @param  array|Object $items
     * @param  int $perPage 
     * @param  array $options
     * @return  Collection
     */
    private function convertAndPaginate( $items, int $perPage = 15, $page = null, $options = [] )
    {
        //page options
        $page   = $page ?: (\Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
        //$items as collection
        $items  = $items instanceof \Illuminate\Support\Collection ? $items : collect($items);

        $paginatedItems = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        $paginatedItems->setPath(request()->url()); 

        return $paginatedItems;
    }

    /**
     * Remove record for log
     * @param  int|integer $id log id
     * @return return bool
     */
    public function removeLog( int $id = 0 )
    {
        return $this->model->where( 'id', $id )->delete();
    }

    /**
     * deleteOrderLogs
     * @param   $orderData 
     * @return response
     */
    public function deleteOrderLogs($orderData )
    {
        foreach ($this->logConfigs() as $label) {
            if ($label['type'] === 'ORDER') {
                $order=parent::where('log_id', $label['id'])->where('changed_record_id' ,$orderData->id)->delete();
            }
            if ($label['type'] === 'CHAMBER_ASSIGNMENT') {
                if($orderData->Chambers){
                    foreach($orderData->Chambers as $chamber){
                        $chamberAssigned = parent::where('log_id',$label['id'])->where('changed_record_id' ,$chamber->id)->delete();
                    }
                } 
            }
            if ($label['type'] === 'ORDER_PRODUCT_METER') {
                if($orderData->OrderProducts){
                    foreach($orderData->OrderProducts as $meterData){
                        if($meterData->OrderProductMeter){
                           $meter = parent::where('log_id', $label['id'])->where('changed_record_id' ,$meterData->OrderProductMeter->id)->delete(); 
                       }

                   }
                }
            }
        }
        return true;
    }
}