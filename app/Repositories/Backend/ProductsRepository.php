<?php

namespace App\Repositories\Backend;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Backend\Driver\DriverLogEvents;
use Auth;
/**
 * Class DriverRepository.
 */
class ProductsRepository extends BaseRepository
{

	/**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        return $this->model->pluck('name','id');
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getSelectedList($productIDs)
    {
        return $this->model->whereIn('id',$productIDs)->pluck('name','id');
    }

    /**
     * get Hfo Lpg Products
     * @return \Illuminate\Http\Response
     */
    public function getHfoLpgProducts()
    {
        return $this->model->whereIn('sku',['HFO','LPG'])->pluck('name','id');
    }

    /**
     * Get record by sku
     * @param  string $productSku 
     * @return mixed
     */
    public function getBySku( string $productSku = '' )
    {
        return $this->model->where('sku', $productSku)->first();
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return $this->model->orderBy('product_order', 'ASC')->get();
    }

    /**
     * getList
     * @return \Illuminate\Http\Response
     */
    public function getOne($id)
    {
        return $this->model->where('id', $id)->first();
    }

   
}