<?php

namespace App\Helpers\General;
use Auth;
use Config;
use App\Repositories\Backend\Audit\LogsRepository;

class LogHelper
{
	/**
     * $logRepository 
     */
    protected $logsRepository;

    public function __construct(LogsRepository $logsRepository)
    {
        $this->logsRepository = $logsRepository;
    }

    public function logConfigs(){
        return Config::get('logs');
    }

    public function log_data($modelData, $actionType, $logType)
    {   

        foreach ($this->logConfigs() as $label) {
            if ($label['type'] === $logType) {
                $logData = [
                                'log_id'            => $label['id'],
                                'action_type'       => $actionType,
                                'changed_record_id' => $modelData->id, 
                                'action_done_by'    => Auth::check() ? Auth::user()->id : 1,
                            ];
                $this->logsRepository->create($logData);
            }
        }
    }
}