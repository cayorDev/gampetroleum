<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\System\Order\ChamberAssignment;
use App\Models\System\Order\OrderProductMeter;
use App\Models\System\Order\OrderProduct;
use App\Models\System\Order\TruckDetail;
use App\Models\System\Order\Order;
use App\Models\System\Importer\ImporterProductTransactions;
use App\Models\System\OMC\OmcProductTransactions;
use Carbon\Carbon;

class OrderImport implements ToCollection, WithStartRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $date=new Carbon('2019-10-02');
            if(!is_null($row[11]) || !is_null($row[12])){
                Order::where('id',(int)$row[0])->update(['created_at'=>$date]);TruckDetail::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
                if(!is_null($row[11])){
                    ImporterProductTransactions::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
                } else {
                    OmcProductTransactions::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
                }
            } else {
                Order::where('id',(int)$row[0])->update(['created_at'=>$date,'chamber_assigned_at'=>$date]);  
                OrderProductMeter::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
                ChamberAssignment::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
                OmcProductTransactions::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
            }
            OrderProduct::where('order_id',(int)$row[0])->update(['created_at'=>$date]);
        }
        return true;
    }

    public function startRow(): int
    {
        return 2;
    }
}
