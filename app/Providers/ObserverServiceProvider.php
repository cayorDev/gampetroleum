<?php

namespace App\Providers;

use App\Models\Auth\User;
use App\Observers\User\UserObserver;
use App\Models\Driver;
use App\Observers\Driver\DriverObserver;
use App\Models\Vehicle;
use App\Observers\Vehicle\VehicleObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\System\OMC\Omc;
use App\Models\System\Addresses; 
use App\Models\System\Contacts;
use App\Models\System\Locations;
use App\Models\System\Order\Release;
use App\Models\System\Importer\ImporterProductsStock;
use App\Models\System\Importer\ImporterProductTransactions;
use App\Models\System\Importer\Importer;
use App\Models\System\OMC\OmcProductsStock;
use App\Models\System\OMC\OmcProductTransactions;
use App\Observers\OMC\OmcObserver;
use App\Observers\Backend\AddressObserver;
use App\Observers\Backednd\ContactsObserver;
use App\Observers\Backednd\LocationsObserver;
use App\Observers\Importer\ImporterObserver;
use App\Observers\Importer\ImporterProductStockObserver;
use App\Observers\OMC\OmcProductStockObserver;
use App\Observers\OMC\OmcProductTransactionObserver;
use App\Observers\Importer\ImporterProductTransactionObserver;
use App\Models\System\Order\Order;
use App\Observers\Backend\Order\OrderObserver;
use App\Models\System\Order\ChamberAssignment;
use App\Observers\Backend\Order\ChamberAssignmentObserver;
use App\Models\System\Order\OrderProductMeter;
use App\Observers\Backend\Order\OrderProductMeterObserver;
use App\Observers\Backend\Order\ReleaseOrderObserver;

/**
 * Class ObserverServiceProvider.
 */
class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        /**
         * Defining observers for model's
         */
        Omc::observe( OmcObserver::class );  //Basic observer for Omc model 
        
        Addresses::observe( AddressObserver::class );      //Basic observer for OmcAddress model 

        Contacts::observe( ContactsObserver::class );     //Basic observer for OmcContancts model

        Locations::observe( LocationsObserver::class );     //Basic observer for OmcLocations model
        
        ImporterProductsStock::observe( ImporterProductStockObserver::class );     //Basic observer for ImporterProductsStock model

        ImporterProductTransactions::observe( ImporterProductTransactionObserver::class );     //Basic observer for ImporterProductTransactions model

        OmcProductTransactions::observe( OmcProductTransactionObserver::class );     //Basic observer for OmcProductTransactions model

        Importer::observe( ImporterObserver::class ); //Basic observer for Importer model

        OmcProductsStock::observe( OmcProductStockObserver::class );     //Basic observer for OmcProductsStock model

        Driver::observe(DriverObserver::class);
        
        Vehicle::observe(VehicleObserver::class);

        Order::observe( OrderObserver::class );     //Observer binding for order model 

        ChamberAssignment::observe( ChamberAssignmentObserver::class );  //Observer for Chamber Assignment model

        OrderProductMeter::observe( OrderProductMeterObserver::class );     //Observer for Order Product Meter model
        
        Release::observe(ReleaseOrderObserver::class);      //Observer for Release Order
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
