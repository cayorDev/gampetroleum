<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait RoleMethod.
 */
trait RoleMethod
{

    /**
     * Super Admin role defining
     * @return mixed
     */
    public function isSuperAdmin()
    {
        return $this->name === config('access.users.superadmin_role');
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->name === config('access.users.admin_role');
    }

    /**
     * @return mixed
     */
    public function isSupervisor()
    {
        return $this->name === config('access.users.supervisor_role');
    }

    /**
     * @return mixed
     */
    public function isOperator()
    {
        return $this->name === config('access.users.operator_role');
    }

    /**
     * @return mixed
     */
    public function isCustomOfficer()
    {
        return $this->name === config('access.users.custom_officer_role');
    }
}
