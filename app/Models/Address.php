<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
	use SoftDeletes;

    public $timestamps = true;

    protected $table  	= 'addresses';

    protected $fillable = ['street', 'city', 'country'];
}
