<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Attribute\DriverAttribute;

class Driver extends Model
{
	use SoftDeletes,
        DriverAttribute;

    public $timestamps = true;

    protected $fillable = ['name', 'drivers_licence_number', 'tel_number_1', 'tel_number_2'];

    public function vehicle()
    {
     	return $this->belongsToMany('App\Models\Vehicle');
    }
}
