<?php

namespace App\Models\System\OMC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\System\Contacts;
use App\Models\System\Locations;
use App\Models\System\Addresses;

class Omc extends Model
{
    use SoftDeletes;

    protected static function boot() {
        parent::boot();

        static::deleted(function ($omc) {
          $omc->contacts()->delete();
          $omc->locations()->delete();
          $omc->addresses()->delete();
          $omc->productsInStock()->delete();
          $omc->productstransactions()->delete();
      });
    }
    protected $dates = ['deleted_at'];
    protected $SoftDelete = true;

    protected $table = 'omc';
    public $timestamps = true;
    protected $fillable = [
    	'company_name',
    	'owner_name',
    	'email',
        'tel',
        'address_id',
        'is_importer'
    ];

    public static function rules() {
    	return [
    		'company_name' 	=> 'required|min:1|max:200',
    		'owner_name'	=> 'required|min:1|max:200',
    		'email'			=> 'required|email|unique:omc'
    	];
    }

    //Defaine type of column to return on data fetch\
    protected $casts = [
        'company_name' => 'string',
        'owner_name'   => 'string',
        'email'        => 'string',
        'is_importer'  => 'bool',
        'tel'          => 'string' 
    ];

    //Contacts for omc
    public function contacts() 
    {
    	return $this->hasMany(Contacts::class, 'omc_id', 'id');
    }

    //Locations for omc
    public function locations() 
    {
        return $this->hasMany(Locations::class, 'omc_id', 'id');
    }

     //addressess for omc
    public function addresses() 
    {
       return $this->belongsTo(Addresses::class,'address_id','id');

    }

    //Products for omc
    public function productsInStock()
    {
        return $this->hasMany(OmcProductsStock::class, 'omc_id', 'id');
    }

    public function productstransactions()
    {
        return $this->hasMany(OmcProductTransactions::class, 'omc_id', 'id');
    }

}
