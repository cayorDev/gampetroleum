<?php

namespace App\Models\System\OMC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OmcProductTransactions extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table    = 'omc_product_transactions';

    public $timestamps = true;

    protected $fillable = ['omc_id', 'product_id', 'quantity', 'created_by'];
    
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

     public function scopeActive($query)
    {
        return $query->groupby('product_id');
    }
}