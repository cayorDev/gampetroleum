<?php

namespace App\Models\System\OMC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OmcProductsStock extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table    = 'omc_product_stocks';

    public $timestamps = true;

    protected $fillable = ['omc_id', 'product_id', 'importer_id', 'supplier_omc_id', 'outstanding_balance', 'created_by'];

    //Products for omc product stock
    public function omcProducts()
    {
    	return $this->belongsTo('App\Models\Product','product_id','id');
    }

    //Products for omc product stock
    public function omc()
    {
        return $this->belongsTo('App\Models\System\OMC\Omc','omc_id','id');
    }

    public function importer()
    {
        return $this->belongsTo('App\Models\System\Importer\Importer','importer_id','id');
    }

    public function SupplierOmc() 
    {
        return $this->hasOne('App\Models\System\OMC\Omc', 'id', 'supplier_omc_id');
    }
}
