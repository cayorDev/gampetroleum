<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contacts extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $SoftDelete = true;

    protected $table = 'contacts';
    public $timestamps = true;
    protected $fillable = [
    	'omc_id',
        'importer_id',
    	'name',
    	'email',
    	'tel1',
    	'tel2'
    ];

    //Rules(validation) 
    public static function rules() {
    	return [
    		'name' 	=> 'required|min:1|max:200',
    		'email'	=> 'required|email|unique:omc',
    		'tel1'	=> 'min:1',
    		'tel2'  => 'min:1'
    	];
    }

    public function Omc() 
    {
        return $this->belongsTo(\App\Models\System\OMC\Omc::class);
    }

    public function Importer() 
    {
        return $this->belongsTo(\App\Models\System\Importer\Importer::class);
    }
}
