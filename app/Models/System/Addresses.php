<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Models\System\OMC\Omc;
use App\Models\System\Importer\Importer;

class Addresses extends Model
{
     use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $SoftDelete = true;

    protected $table = 'addresses';
    public $timestamps = true;
    protected $fillable = [
    	'street',
    	'city',
    	'country'
    ];

    //Rules(validation) 
    public static function rules() {
    	return [
    		'street' 	=> 'required|min:1|max:200',
    		'city'    	=> 'required|min:1|max:200',
    		'country'   => 'required|email|unique:omc',
    		
    	];
    }

    public function omc() 
    {
        return $this->hasone(Omc::class,'address_id', 'id');
    	
    }

    public function importer() 
    {
        return $this->hasone(Importer::class,'address_id', 'id');
        
    }
}
