<?php

namespace App\Models\System\Importer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\System\Contacts;
use App\Models\System\Locations;
use App\Models\System\Addresses;
use App\Models\System\OMC\OmcProductsStock;

class Importer extends Model
{
    use SoftDeletes;
    protected static function boot() {
        parent::boot();

        static::deleted(function ($importer) {
          $importer->contacts()->delete();
          $importer->locations()->delete();
          $importer->addresses()->delete();
          $importer->productsInStock()->delete();
          $importer->productstransactions()->delete();
          $importer->omcProductsStock()->delete();
      });
    }
    protected $dates = ['deleted_at'];
    protected $SoftDelete = true;

    protected $table = 'importers';
    public $timestamps = true;
    protected $fillable = [
    	'company_name',
    	'owner_name',
    	'email',
        'tel',
    	'address_id'
    ];

    public static function rules() {
    	return [
    		'company_name' 	=> 'required|min:1|max:200',
    		'owner_name'	=> 'required|min:1|max:200',
    		'email'			=> 'required|email|unique:importers'
    	];
    }

    //Defaine type of column to return on data fetch\
    protected $casts = [
        'company_name' => 'string',
        'owner_name'   => 'string',
        'email'        => 'string',
        'tel'          => 'string' 
    ];

    //Products for omc/importer
    public function productsInStock()
    {
        return $this->hasMany(ImporterProductsStock::class, 'importer_id', 'id');
    }
    //Transactions for omc/importer
    public function productstransactions()
    {
        return $this->hasMany(ImporterProductTransactions::class, 'importer_id', 'id');
    }

    //addressess for Importer
    public function addresses() 
    {
         return $this->belongsTo(Addresses::class,'address_id','id');
      
    }

    //Contacts for Importer
    public function contacts() 
    {
         return $this->hasMany(Contacts::class,'importer_id','id');
      
    }

    //Locations for Importer
    public function locations() 
    {
         return $this->hasMany(Locations::class,'importer_id','id');
      
    }

    //Transactions for omc product stock
    public function omcProductsStock()
    {
        return $this->hasMany(OmcProductsStock::class, 'importer_id', 'id');
    }

}
