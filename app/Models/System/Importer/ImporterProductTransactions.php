<?php

namespace App\Models\System\Importer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ImporterProductTransactions extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table    = 'importer_product_transactions';
    public $timestamps = true;

    protected $fillable = ['importer_id', 'product_id', 'quantity', 'created_by', 'order_id'];

    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }
}