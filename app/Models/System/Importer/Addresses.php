<?php

namespace App\Models\System\Importer;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
     use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $SoftDelete = true;

    protected $table = 'addresses';
    public $timestamps = true;
    protected $fillable = [
    	'street',
    	'city',
    	'country'
    ];

    //Rules(validation) 
    public static function rules() {
    	return [
    		'street' 	=> 'required|min:1|max:200',
    		'city'    	=> 'required|min:1|max:200',
    		'country'   => 'required|email|unique:omc',
    		
    	];
    }

    public function importer() 
    {
        return $this->hasone(Importer::class,'address_id', 'id');
        
    }
}
