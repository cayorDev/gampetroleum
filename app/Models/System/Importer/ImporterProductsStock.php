<?php

namespace App\Models\System\Importer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImporterProductsStock extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table    = 'importer_product_stocks';

    public $timestamps = true;

    protected $fillable = ['importer_id', 'product_id', 'outstanding_balance', 'created_by'];

    //Products for importer product stock
    public function importerProducts()
    {
    	return $this->belongsTo('App\Models\Product','product_id','id');
    }

    public function importers()
    {
        return $this->hasOne('App\Models\System\Importer\Importer','id','importer_id');
    }
}
