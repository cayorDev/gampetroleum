<?php

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;
    protected static function boot() {
        parent::boot();

        static::deleted(function ($order) {
          $order->OrderProducts()->delete();
          $order->Chambers()->delete();
          $order->OrderProductMeter()->delete();
          $order->Truck()->delete();
      });
    }
	protected $dates = ['created_at','updated_at','deleted_at'];
    protected $table = 'orders';
    public $timestamps = true;
    protected $fillable = [
    'importer_id',
    'omc_id',
    'supplier_omc_id',
    'vehicle_id',
    'driver_id',
    'order_id',
    'purchase_order',
    'loader1',
    'loader2',
    'seals',
    'comments',
    'is_chamber_assigned',
    'is_vehicle_loaded',
    'is_order_verified',
    'is_complete',
    'seen_by',
    'seen_at',
    'authorized_by',
    'authorized_at',
    'created_by',
    ];

    public function Omc()
    {
        return $this->hasOne('App\Models\System\OMC\Omc', 'id', 'omc_id');
    }

    public function SupplierOmc()
    {
        return $this->hasOne('App\Models\System\OMC\Omc', 'id', 'supplier_omc_id');
    }

    public function Importer()
    {
        return $this->hasOne('App\Models\System\Importer\Importer', 'id', 'importer_id');
    }

    public function Vehicle()
    {
        return $this->hasOne('App\Models\Vehicle', 'id', 'vehicle_id')->withTrashed();
    }

    public function Driver()
    {
        return $this->hasOne('App\Models\Driver', 'id', 'driver_id')->withTrashed();
    }

    public function OrderProducts()
    {
        return $this->hasMany('App\Models\System\Order\OrderProduct','order_id','id')->withTrashed();
    }

    public function Chambers()
    {
        return $this->hasMany('App\Models\System\Order\ChamberAssignment','order_id','id');
    }

    public function OrderProductMeter()
    {
        return $this->hasMany('App\Models\System\Order\OrderProductMeter','order_id','id');
    }

    public function Truck()
    {
        return $this->belongsTo('App\Models\System\Order\TruckDetail', 'id', 'order_id');
    }

    public function getOmcOutstandingInventory()
    {
        return $this->hasMany('App\Models\System\OMC\OmcProductsStock', 'omc_id', 'omc_id')->withTrashed();
    }

    public function getSupplierOmcOutstandingInventory()
    {
        return $this->hasMany('App\Models\System\OMC\OmcProductsStock', 'omc_id', 'supplier_omc_id')->withTrashed();
    }

    public function getSealsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function AuthorisedBy()
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'authorized_by');
    }

    public function SeenBy()
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'seen_by');
    }

    public function CreatedBy()
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'created_by');
    }

    public function getImporterOutstandingInventory()
    {
        return $this->hasMany('App\Models\System\Importer\ImporterProductsStock', 'importer_id', 'importer_id')->withTrashed();
    }
}

