<?php

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChamberAssignment extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'chamber_assignment';
    public $timestamps = true;
    protected $fillable = [
    	'order_id',
    	'vehicle_chamber_id',
        'product_id',
    	'quantity',
        'loader',
        'total_loaded',
    ];

    public function Product() 
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function vehicleChamber() 
    {
        return $this->hasOne('App\Models\VehicleChamber', 'id', 'vehicle_chamber_id');
    }
    //
}
