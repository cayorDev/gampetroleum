<?php

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProductMeter extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'order_product_meter';
    public $timestamps = true;
    protected $fillable = [
    	'order_product_id',
    	'meter_id',
    	'opening_meter',
    	'closing_meter',
    	'quantity',
    	'order_id'
    ];
    //
}
