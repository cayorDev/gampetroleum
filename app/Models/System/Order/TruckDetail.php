<?php

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TruckDetail extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'order_truck_details';
    public $timestamps = true;
    protected $fillable = [
    	'order_id',
    	'supervisor',
    	'truck_loading_ticket_number',
    	'weight_before_loading',
    	'weight_after_loading',
    	'net_weight',
        'location'
    ];

    public function Supervisor() 
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'supervisor');
    }
    //
}
