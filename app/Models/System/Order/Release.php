<?php 

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Release extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'release';
    public $timestamps = true;
    protected $fillable = [
    	'omc_id',
    	'importer_id',
        'supplier_omc_id',
    	'purchase_order'
    ];

    public function Omc() 
    {
        return $this->hasOne('App\Models\System\OMC\Omc', 'id', 'omc_id');
    }

    public function Importer() 
    {
        return $this->hasOne('App\Models\System\Importer\Importer', 'id', 'importer_id');
    }

    public function releaseProducts()
    {
        return $this->hasMany('App\Models\System\OMC\OmcProductTransactions', 'release_id', 'id');
    }

    public function SupplierOmc() 
    {
        return $this->hasOne('App\Models\System\OMC\Omc', 'id', 'supplier_omc_id');
    }
}
