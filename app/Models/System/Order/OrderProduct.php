<?php

namespace App\Models\System\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'order_products';
    public $timestamps = true;
    protected $fillable = [
    'order_id',
    'product_id',
    'quantity',
    ];

    public function Product() 
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function OrderProductMeter() 
    {
        return $this->hasOne('App\Models\System\Order\OrderProductMeter', 'order_product_id', 'id');
    }

    public function transaction(){
        return $this->belongsToMany('App\Models\System\OMC\OmcProductTransactions', 'omc_product_tranactions', 'order_id', 'product_id');
    }
}
