<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
	use SoftDeletes;

	public $timestamps = true;

    protected $fillable = ['log_id', 'action_type', 'changed_record_id', 'action_done_by'];
}
