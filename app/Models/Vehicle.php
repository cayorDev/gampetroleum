<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Attribute\VehicleAttribute;

class Vehicle extends Model
{
 	use SoftDeletes,
        VehicleAttribute;

    public $timestamps = true;

    protected $fillable = ['status','transporter_name','transporter_tel_number','license_plate_number', 'number_of_chambers', 'upper_seals','lower_seals'];

    public function vehicle_chamber()
    {
        return $this->hasMany('App\Models\VehicleChamber','vehicle_id','id');
    }

    public function driver()
    {
     	return $this->belongsToMany('App\Models\Driver');
    }
}
