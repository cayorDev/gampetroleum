<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleChamber extends Model
{
    public $timestamps = true;

    protected $fillable = ['vehicle_id', 'chamber_number', 'capacity'];

     public function vehicle()
    {
     	return $this->belongsTo('App\Models\Vehicle');
    }
}
