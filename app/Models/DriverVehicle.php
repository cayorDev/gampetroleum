<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverVehicle extends Model
{
    public $timestamps = true;

    protected $fillable = ['vehicle_id', 'driver_id'];

    protected $table = 'vehicle_driver';

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver');
    }

}
