<?php

namespace App\Http\Requests\Backend\Driver;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DriverStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => ['required'],
            'drivers_licence_number'     => ['required', Rule::unique('drivers')],
            'tel_number_1'  => ['required','regex:/^([0-9\s\-\+\(\)]*)$/'],
            'tel_number_2'    => ['required','regex:/^([0-9\s\-\+\(\)]*)$/'],
        ];
    }
}
