<?php

namespace App\Http\Requests\Backend\Loader;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class LoaderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
