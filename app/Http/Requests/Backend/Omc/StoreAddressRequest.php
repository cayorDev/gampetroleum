<?php

namespace App\Http\Requests\Backend\Omc;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'city' => ['required', 'max:191' , 'min:3'],
            'street'  => ['required', 'max:191', 'min:3'],
            'country'  => ['required', 'max:191', 'min:3']
        ];
    }
}
