<?php

namespace App\Http\Requests\Backend\Omc;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class OmcLocationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        foreach( $this->request->get('locations') as $key => $val) {
            $rules['locations.'.$key.'.name'] = 'required|max:100|min:3';
            $rules['locations.'.$key.'.contact_name'] = 'required|max:100|min:3';
            $rules['locations.'.$key.'.tel1'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/';
        } 
        return $rules;
    }
     /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
       public function messages()
    {
      return [
          'locations.*.required'=>' Location Name Required Field',
          'locations.*.max'=>' Location Name may not be greater than 100 characters',
          // 'locations.*.tel1.min'=> ' Location telephone number must be at least :min.',
          // 'locations.*.tel2.min'=> ' Location alternate telephone number must be at least :min.',
          'locations.*.regex' => 'Location telephone number format is invalid.', 
          
      ]; 
    }
}
