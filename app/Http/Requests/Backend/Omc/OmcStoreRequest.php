<?php

namespace App\Http\Requests\Backend\Omc;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class OmcStoreRequest extends FormRequest
{
	/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
  public function authorize()
  {
    return true;
  }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $rules = [];
      $rules = [
      'company_name'=>'required|max:100',
      'owner_name'=>'required|max:100',
      'email'  => 'required',
      'tel'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/' 
      ];
      foreach($this->request->get('address') as $key => $val)
      {
        $rules['address.'.$key] = 'required|max:100|min:3';
      }
      
        foreach( $this->request->get('contact') as $key => $val)
        {
          $rules['contact.'.$key.'.name'] = 'required|max:100';
          $rules['contact.'.$key.'.email'] = 'required|max:100';
          $rules['contact.'.$key.'.tel1'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/';
        } 
        
      if($this->type==2||$this->type==0) {
        foreach( $this->request->get('locations') as $key => $val)
        {
          $rules['locations.'.$key.'.name'] = 'required|max:100';
          $rules['locations.'.$key.'.contact_name'] = 'required|max:100';
          $rules['locations.'.$key.'.tel1'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/';
        } 
      }
      if($this->type==1||$this->type==2){      
        $rules['products.count'] = 'required';
      }      
      return $rules;
    } 
    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
    public function messages()
    {
      return [
      'address.*.required'=>'Address  :attribute Required Field ',
      'address.street.max'=> 'Address: Street  may not be greater than :max.',
      'address.city.max'=> ' Address: City may not be greater than :max.',
      'contact.*.required'=>'Required Field',
      'contact.*.max'=> ' Contact Name may not be greater than :max.',
      'contact.*.email'=> ' Contact Email must be a valid email address.',
      // 'contact.*.tel1.min'=> 'Contact telephone number must be at least :min.',
      // 'contact.*.tel2.min'=> 'Contact alternate telephone number must be at least :min.',
      'contact.*.regex' => 'Contact telephone number format is invalid.',
      'locations.*.required'=>' Location Name Required Field',
      // 'locations.*.tel1.min'=> ' Location telephone number must be at least :min.',
      // 'locations.*.tel2.min'=> ' Location alternate telephone number must be at least :min.',
      'locations.*.regex' => 'Location telephone number format is invalid.', 
      'tel.min'=> ' Company telephone number must be at least :min.',
      'products.count.required'=>'Please select a product',
      ]; 
    }
  }