<?php

namespace App\Http\Requests\Backend\Omc;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateOmcRequest.
 */
class UpdateOmcRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'email' => ['required', 'email', 'max:191', 'min:3'],
            'company_name'  => ['required', 'max:191', 'min:3'],
            'owner_name'  => ['required', 'max:191', 'min:3']
        ];
    }
}
