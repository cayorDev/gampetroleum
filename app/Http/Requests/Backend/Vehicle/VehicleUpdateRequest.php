<?php 
 
namespace App\Http\Requests\Backend\Vehicle;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class VehicleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license_plate_number'     => ['required', 'unique:vehicles,license_plate_number,'.$this->id],
            'number_of_chambers'  => ['required'],
            'upper_seals' => ['required'],
            'lower_seals' => ['required'],
        ];
    }
}
