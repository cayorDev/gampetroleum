<?php

namespace App\Http\Requests\Backend\Importer;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ImporterUpdateRequest extends FormRequest
{
	/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $rules = [
   			    'company_name'        =>  'required|max:100|min:3',
	    	    'owner_name'          =>  'required|max:100|min:3',
            'email'               =>  'required| email|max:191',
            "address.street"      =>  'required|max:100|min:3',
            "address.city"        =>  'required|max:100|min:3',
            "address.country"     =>  'required|max:100|min:3',

  		];
  		return $rules;
  	}
      /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
       public function messages()
      {
        return [
          'address.street.required'   => 'Street field required',
          'address.city.required'     => ' City field required',
          'address.country.required'  => 'Country field required',
          'address.street.min'        => 'Street name  must have at least :min characters.',
          'address.city.min'          => 'City name must have at least :min characters.',
          'address.country.min'       => 'Country name  must be at least :min characters.',
          'address.street.max'        => 'Street name  not more then :max characters.',
          'address.city.max'          => 'City name not more then  :max  characters.',
          'address.country.max'       => 'Country name not more then:max characters.',
        ];
      }
}
