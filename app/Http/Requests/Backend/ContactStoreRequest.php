<?php

namespace App\Http\Requests\Backend;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ContactStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() 
    {
        $rules=[];
        foreach( $this->request->get('contact') as $key => $val){
            $rules['contact.'.$key.'.name'] = 'required|min:3';
            $rules['contact.'.$key.'.email'] = 'required|max:100';
            $rules['contact.'.$key.'.tel1'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/';
            $rules['contact.'.$key.'.tel2'] = 'regex:/^([0-9\s\-\+\(\)]*)$/';
        }
        return $rules;
        
    }
    public function messages() 
    {
        return[
            'contact.*.name.min'=>'Name must be :min characters',
            'contact.*.name.required'=>'Name required',
            'contact.*.email.required'=>'Email required',
            'contact.*.tel1.required'=>'Telephone 1 required',
            'contact.*.tel1.regex'=>'Enter correct telephone number 1',
            'contact.*.tel2.regex'=>'Enter correct telephone number 2',
            
        ];
    }
}


