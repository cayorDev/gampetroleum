<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->request->get('products');
        unset($product['count']);
        $rules = [
            'importer_id'=>'required|exists:importers,id',
            'supplier_omc_id'=>'sometimes|exists:omc,id',
            // 'omc_id'=>'required|sometimes|exists:omc,id',
            'purchase_order' => 'required', 
            'driver.name' => 'required|sometimes',
            'vehicle_id' => 'required|exists:vehicles,id'
        ];
         if(is_array($product)) {
        foreach($product as $key =>$quantity)
        {
          $rules['products.'.$key] = 'required|integer|min:1';
        }
      }
        return  $rules;
    }

     /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
     public function messages()
     {
      return [
      'products.*.required' =>  'Please add product Quantity', 
      'products.*.min'  =>  'Please add product quantity greater than 0 ', 
      'importer_id.required'  =>  'Please Select a Supplier',
      'omc_id.required' =>  'Please select an OMC',
      'driver.name.required'  => 'Driver is required',
      'purchase_order.required'  => 'Please specify Purchase Order',
      'vehicle_id.required'  => 'Please select vehicle',
      ];
  }
}
