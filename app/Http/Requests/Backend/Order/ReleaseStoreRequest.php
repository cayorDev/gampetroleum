<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ReleaseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $product = $this->request->get('products');
      unset($product['count']);
      $rules = [
      'importer_id'=>'required|exists:importers,id',
      'omc_id'=>'required|exists:omc,id',
      'purchase_order' => 'required|unique:release,purchase_order',
      ];

      if(is_array($product)) {
        foreach($product as $key =>$quantity)
        {
          $rules['products.'.$key] = 'required|integer|min:1';
        }
      }
      return  $rules;
    }
    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
    public function messages()
    {
      return [
      'products.*.required' =>  'Please Add Product Quantity', 
      'products.*.min'  =>  'Please Add Product Quantity Greater Then 0 ', 
      'importer_id.required'  =>  'Please Select An Importer',
      'omc_id.required' =>  'Please Select An OMC',
      'purchase_order.required'  => 'Please specify Purchase Order',
      'purchase_order.unique'  => 'Please Specify Correct Purchase Order / PO already taken',

      ]; 
    }
  }
