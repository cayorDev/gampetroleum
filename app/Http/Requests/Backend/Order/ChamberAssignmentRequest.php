<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;

class ChamberAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->request->get('products');
        foreach($product as $key =>$data)    
        {
            $rules['products.'.$key.'.'.'productID']= 'nullable|exists:products,id';
            // $rules['products.'.$key.'.'.'quantity']= 'required_with:products.'.$key.'.productID|integer';
        }
        $rules['loader1'] = 'sometimes|required';
        return  $rules;
    }

    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
       public function messages()
    {
      return [
          'products.*.productID.required'=>'Please Select A Product', 
          'products.*.productID.quantity'=>'Please Add Product Quantity', 
          'loader1.required' => 'Please specify loader name'
      ]; 
    }
}
