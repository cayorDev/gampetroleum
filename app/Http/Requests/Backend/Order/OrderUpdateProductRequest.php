<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $products = $this->request->get('products');
     
        foreach($products as $key =>$product)
        {  
            $rules['products.'.$key.'.quantity']= 'required|integer|min:1';
        }

        return  $rules;

    }

    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
       public function messages()
    {

      return [
          'products.*.required'=>'Please Add Product Quantity', 
           'products.*.min'=>'Please Add Product Quantity Greater Then 0 ', 
        
      ]; 
    }
}
