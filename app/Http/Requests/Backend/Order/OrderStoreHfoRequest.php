<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderStoreHfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      $rules = [
      'importer_id'     =>  'required|exists:importers,id',
      // 'purchase_order'  =>  'required', 
      'driver.name'     =>  'required|sometimes',
      'vehicle_id'      =>  'required|exists:vehicles,id',
      'location'        =>  'required',
      'beforeloading'   =>  'required|integer|min:1',
      'afterloading'    =>  'required|integer|min:1',
      'netweight'       =>  'required|integer|min:1',
      'authorised_by'   =>  'required'
      ];

      
      return  $rules;
    }
    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
    public function messages()
    {
      return [
      'importer_id.required'    =>  'Please Select a Supplier',
      'vehicle_id.required'     =>  'Please Select a Vehicle',
      'driver.name.required'    =>  'Driver is required',
      'purchase_order.required' =>  'Please specify Purchase Order',
      'authorised_by.required'  =>  'Please specify Authorised user',
      ]; 
    }
  }
