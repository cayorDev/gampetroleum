<?php

namespace App\Http\Requests\Backend\Order;

use Illuminate\Foundation\Http\FormRequest;

class VehicleUpliftmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $meterData = $this->request->get('product');
      $chamberSeals = $this->request->get('vehicle_chamber');
      $productData = $this->request->get('products');
      $rules['authorized_by'] = 'required|exists:users,id';
      foreach($meterData as $key =>$data)    
      {
        $rules['product.'.$key.'.'.'meter_id']= 'required';
        $rules['product.'.$key.'.'.'opening_meter']= 'required|integer';
        $rules['product.'.$key.'.'.'closing_meter']= 'required|integer';
        $rules['product.'.$key.'.'.'quantity']= 'required|integer';
      }
      // foreach($productData as $productkey =>$productdata)    
      // {
      //   $rules['products.'.$productkey.'.'.'total_loaded']= 'required|integer';
      // }

     
        $rules['vehicle_chamber.upper_seal']= 'required';
     
     
       $rules['vehicle_chamber.lower_seal']= 'required';
     

     // $rules['loader1'] = 'sometimes|required';
     
     return  $rules;
     
   }

    /**
     * Get the validation messages according to rule.
     *
     * @return array
     */
    public function messages()
    {
      return [
        'product.*.meter_id.required'=>'Please Select A Meter ID', 
        'product.*.opening_meter.required'=>'Please Add Opening Meter Reading', 
        'product.*.closing_meter.required'=>'Please Add Closing Meter Reading', 
        'product.*.quantity.required'=>'Please Add Quantity', 
        'vehicle_chamber.upper_seal.required'=>'Please Add Upper Seals', 
        'vehicle_chamber.lower_seal.required'=>'Please Add Lower Seals',
        // 'loader1.required' => 'Please specify loader name',
        // 'products.*.total_loaded.required'=>'Please Add Total Quantity Loaded For Each Product', 
        
      ]; 
    }
  }
