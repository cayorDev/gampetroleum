<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Repositories\Backend\Orders\OrderRepository;

/**
 * Class GlobalComposer.
 */
class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $orderRepository = new OrderRepository();
        $orderCounts    =  $orderRepository->getOrderCount();

        $view->with('logged_in_user', auth()->user());
        $view->with('orderCount', $orderCounts);

    }
}
