<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\Backend\Importers\ImporterRepository;
use App\Repositories\Backend\Orders\OrderRepository;
use DB;
use \Carbon\Carbon;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{   
    /**
     * OMC repository
     * @var OmcRepository
     */
    protected $_omcRepository;

    /**
     * Importer repository instance
     * @var ImporterRepository
     */
    protected $_importerRepository;

    /**
     * Order repository instance
     * @var OrderRepository
     */
    protected $_orderRepository;

	/**
	 * Constructor for Dashboard Controller
     * @param  OmcRepository $OmcRepository
	 */
	public function __construct(
        OmcRepository $OmcRepository,
        ImporterRepository $ImporterRepository,
        OrderRepository $OrderRepository
        )
	{
		$this->_omcRepository 				= $OmcRepository;
		$this->_importerRepository 			= $ImporterRepository;
        $this->_orderRepository             = $OrderRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {    
        $range          = null;
        $start          = request()->input('start');
        $end            = request()->input('end');

        $ordercounts = $this->_orderRepository->getOrderCount();
        //Checking start and end of date parameters for filteration
        if ( !is_null( $start ) && !is_null( $end ) ) {
            $start  = Carbon::parse($start)->startOfDay();
            $end    = Carbon::parse($end)->endOfDay();

            $importers    = $this->_importerRepository
                                        ->getAllRecordsForDatatable()->whereBetween('created_at',[ $start->toDateTimeString(), $end->toDateTimeString() ]);
            $omcs           = $this->_omcRepository
                                        ->getAllRecordsForDatatable()->whereBetween('created_at',[ $start->toDateTimeString(), $end->toDateTimeString() ]);
            $range          = $start->format('M d, Y').'-'.$end->format('M d, Y');
        } else {
            $importers    = $this->_importerRepository
                                        ->getAllRecordsForDatatable();
            $omcs    = $this->_omcRepository
                                ->getAllRecordsForDatatable();
        }
        
        $importers  = $importers->with('productsInStock', 'productsInStock.importerProducts','productstransactions','productstransactions.product')->get()->map(function($q) {
            $products = [];
            foreach ( $q->productsInStock as $product ) {
                $products[ $product->importerProducts->name ] = $product->outstanding_balance;
            } 

            return [
            'id'            => $q->id,
            'name'          => $q->company_name,
            'balance'       => $products,
            'transactions'  => $q->productstransactions
            ];
        })->toArray();

        $omcs       = $omcs->with('productsInStock', 'productsInStock.omcProducts','productstransactions','productstransactions.product')->get()->map(function($q) {
            $products = [];
            foreach ( $q->productsInStock as $key => $product ) {
                $importer = $product->importer ? $product->importer->company_name : '';
                $supplieromc = $product->SupplierOmc ? ' / '.$product->SupplierOmc->company_name : '';
                $products[$key][ 'importer' ] = $q->company_name.''.$supplieromc.' / ' .$importer;
                $products[$key][ 'product' ] =  $product->omcProducts->name;
                $products[$key][ 'balance' ] = $product->outstanding_balance;
            } 

            return [
            'id'            => $q->id,
            'name'          => $q->company_name,
            'balance'       => $products,
            'transactions'  => $q->productstransactions
            ];
        })->toArray();
        return view('backend.dashboard')
        ->withOmcCount($this->_omcRepository->getAllRecordsForDatatable()->count())
        ->withImportersCount($this->_importerRepository->getAllRecordsForDatatable()->count())
        ->withOrderCounts($ordercounts)->withImporters($importers)->withOmcs($omcs)->withRange($range);
    }

    /**
     * Fetch various stats
     * @return JSON
     */
    public function getStats()
    {
    	$data 	= [];
        $data['orders']                         = $this->_orderRepository->getOrderCount();
        $data['importer_inventory_balance']     = $this->_importerRepository
        ->getAllRecordsForDatatable()->with('productsInStock', 'productsInStock.importerProducts','productstransactions')->get()->map(function($q) {
            $products = [];
            foreach ( $q->productsInStock as $product ) {
                $products[ $product->importerProducts->name ] = $product->outstanding_balance;
            } 

            return [
            'id'            => $q->id,
            'name'          => $q->company_name,
            'balance'       => $products
            ];
        })->toArray();     
        $status			            = count($data) ? 200 : 400;
        return response()->json([
            'status' 	=> $status,
            'data'		=> $data		
            ], $status);
    }

}
