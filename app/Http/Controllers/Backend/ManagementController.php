<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\ManagementRepository;
use App\Http\Requests\Backend\Management\ManagementUpdateRequest;
use App\Models\Management;
use Yajra\Datatables\Datatables;

class ManagementController extends Controller
{
    /**
     * @var managementRepository
     */
    protected $managementRepository;

    /**
     * UserController constructor.
     *
     * @param ManagementRepository $managementRepository
     */
    public function __construct(ManagementRepository $managementRepository)
    {
        $this->managementRepository = $managementRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.management.index');
    }


    public function getManagementData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->managementRepository->getAllRecordsForDatatable();

        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('name','like', '%'.$value.'%')
                ->orWhere('position', 'like', '%'.$value.'%');
            });
        }

        $data   = $data->get()
        ->map( function( $q ) {
            return [
                'id'                        => $q->id,
                'position'                  => $q->position,
                'name'                      => $q->name,
            ];
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.management.show',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i></button></a>&nbsp;<a href="'.route('admin.management.edit',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-warning"><i class="fas fa-edit" ></i></button></a>&nbsp;';
        })
        ->make(true);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $management=$this->managementRepository->getmanagement($id);
        return view('backend.management.show')->withManagement($management);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $management=$this->managementRepository->getmanagement($id);
     return view('backend.management.edit')->withManagement($management);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ManagementUpdateRequest $request, $id)
    {
     $management=$this->managementRepository->getmanagement($id);
     $this->managementRepository->update($management, $request->only(
        'name'
    ));

     return redirect()->route('admin.management.index')->withFlashSuccess(__('alerts.backend.management.updated'));
 }

}
