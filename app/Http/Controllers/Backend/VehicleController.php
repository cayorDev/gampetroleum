<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\VehicleRepository;
use App\Repositories\Backend\VehicleChamberRepository;
use App\Http\Requests\Backend\Vehicle\VehicleStoreRequest;
use App\Http\Requests\Backend\Vehicle\VehicleUpdateRequest;
use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;
use App\Events\Backend\Vehicle\VehicleLogEvents;
use App\Repositories\Backend\DriverRepository;
use Auth;
use Yajra\Datatables\Datatables;

class VehicleController extends Controller
{

    /**
     * @var VehicleRepository
     */
    protected $vehicleRepository;

    /**
     * @var vehicleChamberRepository
     */
    protected $vehicleChamberRepository;

    /**
     * VehicleController constructor.
     *
     * @param VehicleRepository $vehicleRepository
     */
    public function __construct(VehicleRepository $vehicleRepository, VehicleChamberRepository $vehicleChamberRepository, DriverRepository $driverRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->vehicleChamberRepository = $vehicleChamberRepository;
        $this->driverRepository = $driverRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.vehicle.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.vehicle.add');
    }

    public function getVehicleData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->vehicleRepository->getAllRecordsForDatatable();

        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('license_plate_number','like', '%'.$value.'%')
                ->orWhere('number_of_chambers', 'like', '%'.$value.'%')
                ->orWhere('upper_seals', 'like', '%'.$value.'%')
                ->orWhere('lower_seals', 'like', '%'.$value.'%')
                ->orWhere('status', 'like', '%'.$value.'%')
                ->orWhere('transporter_name', 'like', '%'.$value.'%');
            });
        }

        $data   = $data->get()
        ->map( function( $q ) {
            return [
            'id'                      => $q->id,
            'license_plate_number'    => $q->license_plate_number,
            'transporter_name'        => $q->transporter_name,
            'number_of_chambers'      => $q->number_of_chambers,
            'upper_seals'             => $q->upper_seals,
            'lower_seals'             => $q->lower_seals,
            'status'                  => $q->status,
            'registration_date'       => date( 'D, M j, Y \a\t g:ia', strtotime( $q->created_at ) )
            ];
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.vehicle.show',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i></button></a>&nbsp;<a href="'.route('admin.vehicle.edit',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-warning"><i class="fas fa-edit" ></i></button></a>&nbsp;<a href="'. route("admin.vehicle.destroy",$val['id']) .'" onclick="return confirm('."'Do you want to delete Vehicle ?'".');"  title="Delete Vehicle"><button class="btn-sm btn btn-danger"><i class="fas fa-trash"></i></button></a>&nbsp';
             })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $vehicle = $this->vehicleRepository->create($request->except('_token','drivers'));
            $chamberData = $request->except('_token','status','transporter_name','transporter_tel_number','license_plate_number','number_of_chambers','upper_seals','lower_seals');
            $vehicle_chamber_data = $this->vehicleChamberRepository->processChamberData($chamberData, $vehicle->id);
            $vehicle_chamber = $this->vehicleChamberRepository->create($vehicle_chamber_data);
            DB::commit();
            event(new VehicleLogEvents($vehicle, "VEHICLE CREATED", Auth::user()->full_name));
            $response['success'] = true;
            $response['message'] = __('alerts.backend.vehicle.created');
            $request->session()->flash('flash_success', __('alerts.backend.vehicle.created'));
            return response()->json($response);
        }catch (\Exception $e) {
            DB::rollback();
            $response['success'] = false;
            $response['message'] = __('alerts.backend.vehicle.createderror');
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.vehicle.show')
        ->withVehicles($this->vehicleRepository->getChamber($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle=$this->vehicleRepository->getChamber($id);
        return view('backend.vehicle.edit',compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(VehicleUpdateRequest $request, $id )
    {
        try {
            DB::beginTransaction();
            $id=$request->id;
            $vehicle=  $this->vehicleRepository->getChamber($id);
            $this->vehicleRepository->update($vehicle, $request->only(
                'transporter_name',
                'transporter_tel_number',
                'license_plate_number',
                'number_of_chambers',
                'upper_seals',
                'lower_seals',
                'status'
                ));
            $chamberData = $request->except('_token','_method','transporter_name','transporter_tel_number','status','license_plate_number','number_of_chambers','upper_seals','lower_seals','id','drivers');
            $vehicle_chamber_data= $this->vehicleChamberRepository->processChamberDataForEdit($chamberData, $id);
            $vehicle_chamber= $this->vehicleChamberRepository->create($vehicle_chamber_data);
            DB::commit();

            event(new VehicleLogEvents($vehicle, "VEHICLE UPDATED", Auth::user()->full_name));
            $response['success'] = true;
            $response['message'] = __('alerts.backend.vehicle.updated');
            $request->session()->flash('flash_success', __('alerts.backend.vehicle.updated'));
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response['success'] = false;
            $response['message'] = __('alerts.backend.vehicle.updatederror');
            return response()->json($response);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle=  $this->vehicleRepository->getVehicleById($id);
        if($vehicle) {
            $this->vehicleRepository->deleteById($id);
            return redirect()->route('admin.vehicle.index')->withFlashSuccess(__('alerts.backend.vehicle.deleted'));
        }
    }

    /**
     * Delete the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function delete($id)
    // {
    //     $deleted=$this->vehicleRepository->getDeletedVehicles($id);
    //     $this->vehicleRepository->forceDelete($deleted);
    //     return redirect()->route('admin.vehicle.deleted')->withFlashSuccess(__('alerts.backend.vehicle.deleted'));
    // }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $deleted=$this->vehicleRepository->getDeletedVehicles($id);
        $this->vehicleRepository->restore($deleted);
        return redirect()->route('admin.vehicle.index')->withFlashSuccess(__('alerts.backend.vehicle.restored'));
    }

    /**
     * Show the deleted resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleted_vehicles()
    {
        return view('backend.vehicle.deleted');
    }

    public function getDriverList($vehicleID)
    {
        $vehicleDrivers = $this->vehicleRepository->getChamber($vehicleID);
        return json_encode($vehicleDrivers->driver);
    }

    public function getDeletedVehicleData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->vehicleRepository->getAllDeletedRecordsForDatatable();

        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('license_plate_number','like', '%'.$value.'%')
                ->orWhere('number_of_chambers', 'like', '%'.$value.'%')
                ->orWhere('upper_seals', 'like', '%'.$value.'%')
                ->orWhere('lower_seals', 'like', '%'.$value.'%');
            });
        }

        $data   = $data->get()
        ->map( function( $q ) {
            return [
            'id'                      => $q->id,
            'license_plate_number'    => $q->license_plate_number,
            'number_of_chambers'      => $q->number_of_chambers,
            'upper_seals'             => $q->upper_seals,
            'lower_seals'             => $q->lower_seals
            ];
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.vehicle.restore',$val['id']).'" title="Restore" ><button class="btn-sm btn btn-primary"><i class="fas fa-check" ></i></button></a>&nbsp;';
             })
       ->make(true);
    }

}
