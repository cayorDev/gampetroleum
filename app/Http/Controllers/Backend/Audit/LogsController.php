<?php

namespace App\Http\Controllers\Backend\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Audit\LogsRepository;
use App\Repositories\Backend\Auth\UserRepository;


class LogsController extends Controller
{
    public function __construct(LogsRepository $LogsRepository,UserRepository $UserRepository)
    {
        $this->_logsRepository  = $LogsRepository;
        $this->_userRepository = $UserRepository;
        $this->actionIcons = array(
            "update"    => "fa-save",
            "delete"    => "fa-trash",
            "create"    => "fa-plus",
            "restored"  => "fa-window-restore",
            );
    }

     /**
     * Display logview.
     *
     * @return data,icon and time diffrence
     */
     public function index()
     {
        $logs = $this->_logsRepository->getData();   	
        $icons = $this->actionIcons;
        return view('backend.audits.logs')->withlogs( $logs )->withicons( $icons );
    }

    /**
     * Remove log record
     * @param  int|integer $id 
     * @return \Redirect
     */
    public function remove( int $id = 0 )
    {
        if ( $id && $this->_logsRepository->removeLog( $id ) ) {
            return redirect()->back()
                            ->withFlashSuccess(__('alerts.backend.log.removed'));
        }
        return redirect()->back()
                            ->withFlashFail(__('alerts.backend.log.remove_fail'));
    }
}
