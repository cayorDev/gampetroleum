<?php

namespace App\Http\Controllers\Backend\OMC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\Backend\OMC\OmcRepositoryEloquent;
use App\Repositories\Backend\Importers\ImporterRepository;
use App\Repositories\Backend\ProductsRepository;
use App\Models\Product;
use App\Http\Requests\Backend\ContactStoreRequest;
use App\Models\System\Importer\ImporterProductsStock;
use App\Models\System\Importer\ImporterProductTransactions;
use App\Http\Requests\Backend\Importer\ImporterUpdateRequest;
use App\Repositories\Backend\Importers\ImporterProductStockRepository;
use App\Repositories\Backend\OMC\OmcLocationRepository;
use App\Repositories\Backend\ContactsRepository;
use App\Http\Requests\Backend\Omc\OmcLocationStoreRequest;
use App\Repositories\Backend\OMC\AddressesRepository;
use Excel;
use App\Exports\ImporterExport;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ImporterController extends Controller
{
    /**
     * @var  to specify static resources path for Importer section
     */
    STATIC $PATH = 'backend.Importer.';

    /**
     * @var  ImporterRepository
     */
    protected $_importerRepository;

    /**
     * @var  ImporterProductStockRepository
     */
    protected $_importerProductStockRepository;
    /**
    * @var OmcLocationRepo
    */
    protected $_OmcLocationRepo;
    /**
     * @var ContactsRepository
     */
    protected $ContactsRepository;

    /**
     * @var addressesRepository
     */
    protected $_addressesRepository;

    /**
     * Contructor for Importer controller
     */
    public function __construct(ImporterRepository $ImporterRepository, ImporterProductStockRepository $ImporterProductStockRepository,OmcLocationRepository $OmcLocationRepository, ContactsRepository $contactsRepository, AddressesRepository $addressesRepository, ProductsRepository $productsRepository)
    {
        $this->_importerRepository = $ImporterRepository;
        $this->_importerProductStockRepository = $ImporterProductStockRepository;
        $this->contactsRepository = $contactsRepository;
        $this->productsRepository = $productsRepository;
        $this->_OmcLocationRepo = $OmcLocationRepository;
        $this->_addressesRepository= $addressesRepository;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        return view(self::$PATH.'index');
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        $url=url()->previous();
        $products = [];
        $meta = ['title'=>'Importer Management | Add new Importer', 'value' => 1];

        foreach( Product::get(['id', 'name'])->toArray() as $product ) {
            $products[$product['name']] = $product['name'];
        }
        return view(self::$PATH.'create')->withProducts($products)->withUrl($url)->withMeta($meta);
    }

    /**
    * Collect data for datatable of Importer
    * @param Request $request
    */
    public function data( Request $request )
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data =  $this->_importerRepository->getAllRecordsForDatatable();

        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('company_name','like', '%'.$value.'%')
                ->orWhere('owner_name', 'like', '%'.$value.'%')
                ->orWhere('email', 'like', '%'.$value.'%');
            });
        }

        $data   = $data->get()
        ->map( function( $q ) {
            return [
                'id'                => $q->id,
                'name'              => $q->company_name,
                'owner'             => $q->owner_name,
                'updated_at'        => date( 'D, M j, Y \a\t g:ia', strtotime( $q->updated_at ) ),
            ];
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<div class="actions" style="position:relative;"><a href="'.route('admin.importer.manage',$val['id']).'" title="Manage" class="col-md-4 col-sm-4 col-lg-4" ><i class="fas fa-eye" ></i></a>&nbsp;<a  href="'. route("admin.importer.delete",$val['id']) .'"title="Delete" style="color:indianred;" onclick="return confirm(\'Are you sure you want to delete this? All data associated with the Importer will be deleted.\');"><i class="fas fa-trash delete" style="color:#FF5733"></i></a>
            </div>'; })
        ->make(true);
    }

     /**
     * Delete Importer
     * @return bool importer
     */
     public function delete($id)
     {
       try{
        DB::beginTransaction();
        $importer=$this->_importerRepository->deleteImporter($id);
        DB::commit();
        return redirect()->route('admin.importer.index')->withFlashSuccess(__('alerts.backend.importer.deleted'));
    } catch (\Exception $e) {
        DB::rollback();
        return  redirect()->route('admin.importer.index')->withFlashError(__('alerts.backend.importer.deleted_error'));
    }
}

    /**
    *Show the form for editing the specified Importer.
    */
    public function manage(int $id )
    {
        $importer           =   $this->_importerRepository->getImporters( $id);
        $productsRepository = new ProductsRepository();
        $rawProducts        = $productsRepository->getList();
        $products       =   $importer->productsInStock->map(function($q) {
            return [
                'id'        => $q->id,
                'name'      => isset( $q->importerProducts->name ) ? $q->importerProducts->name : 'NA',
                'balance'   => $q->outstanding_balance
            ];
        });
        if ( $importer->productstransactions()->count() ) {
            $transactions = $importer->productstransactions()
            ->orderBy('id', 'desc')
            ->join('products', 'importer_product_transactions.product_id', '=', 'products.id')
            ->join('users', 'importer_product_transactions.created_by','=', 'users.id' )
             ->leftJoin('release', 'importer_product_transactions.release_id','=', 'release.id')
            ->leftJoin('orders', 'importer_product_transactions.order_id','=', 'orders.id' )
            ->select('importer_product_transactions.id','importer_product_transactions.quantity', 'importer_product_transactions.transaction_type','users.first_name as first_name','users.last_name as last_name', 'products.name as product_name','release_id','importer_product_transactions.order_id', 'orders.purchase_order as orders_purchase_order', 'release.purchase_order as release_purchase_order')
            ->get()->toArray();
        } else {
            $transactions = [];
        }
        return view(self::$PATH.'manage')
        ->withimporter($importer)
        ->withaddress($importer->addresses)
        ->withimporterproducts($products)
        ->withproducts($rawProducts)
        ->withtransactions($transactions);
    }

    /**
    *Update the specified Importer and product quantity in storage.
    */
    public function manageImporter(ImporterUpdateRequest $request ,int $id )
    {
        $importer =  $this->_importerRepository->getImporters( $id);

        if ( isset( $request->address ) && count($request->address) ) {
            if($request->address_change != 1) {
                $addressId=$importer->Addresses->id;
                $address=$this->_addressesRepository->updateAddress((object)$request->address,$addressId);
            } else {
                $address=$this->_addressesRepository->store((object)$request->address);
                $importer=$this->_importerRepository->link($address,$id);
            }

        }

        $manage=$request->manage;

        if($manage==1){
            $this->_importerRepository->updatesImporter($request,$id);
        }

        return redirect()->back()->withFlashSuccess(__('alerts.backend.importer.updated'));
    }

    /**
     *Delete Address of omc record
     * @return redirect to View with omc id
     */
    public function addressdelete($id)
    {
        $importer =  $this->_importerRepository->getImporters( $id);
        if ( $importer->addresses()->delete() ) {
            return  redirect()->route('admin.importer.manage',$id)->withFlashSuccess(__('alerts.backend.importer.address.deleted'));
        }

        return  redirect()->route('admin.importer.manage',$id)->withFlashSuccess(__('alerts.backend.importer.address.delete_fail'));

    }


    public function getImporterProductList(int $importerID)
    {
        $products = $this->_importerProductStockRepository->getProductList($importerID);
        $status = false;
        $data = [];
        if (count($products)) {
            foreach ($products as $key => $product) {
                $data['product'][$product->product_id] = $product->importerProducts->name.'('.$product->importerProducts->sku.')';
                $data['inventory'][$product->product_id] = $product->outstanding_balance;
            }
            $status = true;
        }
        $response = ['status' => $status , 'data' => $data];
        return json_encode($response);

    }

    /**
     * manageImporterInventory
     * @param  Request $request [description]
     * @param  int  $id      Importer id
     * @return mixed           [description]
     */
    public function manageImporterInventory( Request $request, $id )
    {
        if( isset( $request->product_id ) && isset( $request->append_quantity ) && isset( $request->operation ) ) {
            $this->_importerProductStockRepository->updateImporterStock($id, $request->product_id, $request->append_quantity , $request->operation);
            return redirect()->back()->withFlashSuccess('Operation completed successfully.');
        }
        return redirect()->back()->withFlashFail('Something went wrong. Unable to proceed.');
    }

    /**
     * Display the locations of specified omc.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function location($id)
    {
        $locations = [];
        //importer details
        $importer=$this->_importerRepository->getImporters($id);

        if ( !is_null( $importer ) && $importer->locations->count() ) {
            $locations  = $importer->locations;
        }
        return view(self::$PATH.'locations')->withids($id)->withdata($locations);
    }

     /**
     * Store a newly created location and edit existing location in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $importer_id
     * @return  \Illuminate\Http\Response
     */
     public function storeLocations(OmcLocationStoreRequest $request, $importer_id)
     {
        if ( count( $request->except('_token') ))
        {
            foreach ($request->locations as $key => $value) {
                $this->_importerRepository->createOrUpdateLocation($value, $importer_id);
            }

            return back()->withFlashSuccess(__('alerts.backend.importer.location.update'));
        }
        return Redirect::back()->withInput(Input::all())->withFlashFail(__('alerts.backend.omc.create_fail'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteLocation($id)
    {
        $val=$this->_OmcLocationRepo->deleteById($id);

        if($val == true) {
            return back()->withFlashSuccess(__('alerts.backend.location.delete'));
        } else {
            return back()->withFlashError(__('exceptions.backend.access.location.delete_error'));
        }
    }

    /**
     * Fetching contacts for omc
     * @param  int $id
     */

    public function contacts( $id )
    {
        $contacts    = [];

        if ( $importer  = $this->_importerRepository->getImporters($id) ) {
            $contacts    = $importer->contacts;
        }

        return view(self::$PATH.'.contacts')->withcontacts($contacts)->withdata($contacts)->withids($id);
    }

    /**
     * Contact for importers
     * @param  Request $request
     * @return mixed
     */
    public function createContact(ContactStoreRequest $request, $id)
    {
        if ( count( $request->except('_token') ))
        {
            foreach ($request->contact as $key => $value) {
                $this->_importerRepository->createOrUpdateContact($value, $id);
            }

            return back()->withFlashSuccess(__('alerts.backend.importer.contacts.update'));
        }

        return Redirect::back()->withInput(Input::all())->withFlashFail(__('alerts.backend.omc.create_fail'));
    }

    /**
     * delete Contact for importer
     * @param  int|integer $id contact id
     * @return \Redirect
     */
    public function deleteContact(int $id = 0 )
    {
        if($this->contactsRepository->deleteById($id)) {
            return back()->withFlashSuccess(__('alerts.backend.contacts.delete'));
        } else {
            return back()->withFlashError(__('exceptions.backend.access.contacts.delete_error'));
        }
    }

    /**
    * Show the transactions for specified Importer.
    */
    public function viewTransactions(int $id )
    {
        $data = $this->_importerRepository->getDetail($id);
        $products = $this->productsRepository->getList();
        $product=array();
			$product['0']="All";
			foreach($products as $ind=>$val){
				$product[$ind]=$val;
			}
        return view(self::$PATH.'transactions')
        ->withImporter($data)
        ->withProducts($product);

        // ->withTransactions(count($data['transactions']) ? $data['transactions']->toArray() : []);
    }

    /**
    * Download Excelspread sheet for the transactions for specified Importer.
    */
    public function excelTransactions(Request $request, int $id)
    {
        // dd($request->all);
        $data = $this->getImporterTransactionsDetail($request->all());
        // dd($data);
        // $data = $this->getImporterTransactions($id);

        // Initialize the array which will be passed into the Excel generator.
        $transactionsArray = [];

        // Define the Excel spreadsheet headers


        // Convert each transaction of the returned collection into an array and append it to the transactions array.
        $transactions = count($data['transactions']) ? $data['transactions']->toArray() : [];
        if (count($transactions)) {
            foreach ($transactions as $transaction) {

                $arranged_transaction = [];

                if (!is_null($transaction['release_number'])) {
                    $arranged_transaction[] = $transaction['release_purchase_order'];
                }else if (!is_null($transaction['order_id'])) {
                    $arranged_transaction[] = $transaction['orders_purchase_order'];
                } else {
                    $arranged_transaction[] = 'WebPortal';
                }
                $arranged_transaction[] = \Carbon\Carbon::parse($transaction['created_at'])->format('Y-m-d');
                $arranged_transaction[] = \Carbon\Carbon::parse($transaction['created_at'])->format('h:i:s');
                $arranged_transaction[] = $transaction['product_name'];
                if ($transaction['transaction_type'] == "add") {
                    $arranged_transaction[] = '+ '.$transaction['quantity'];
                } elseif ($transaction['transaction_type'] == "deduct") {
                    $arranged_transaction[] = '- '.$transaction['quantity'];
                } else {
                    $arranged_transaction[] = 'NA';
                }

                $arranged_transaction[] = $transaction['balance_before_transaction']?$transaction['balance_before_transaction']:"0";

                $transactionsArray[] = $arranged_transaction;
            }
            $file_name = $data['importer']->company_name.'_trans';

            return Excel::download(new ImporterExport($transactionsArray,$file_name), $file_name.'.xlsx');
        } else {
            return redirect()->back()->withFlashFail('Something went wrong. Unable to proceed.');
        }

    }

    public function getImporterTransactionData(Request $request){
        $data = $this->getImporterTransactionsDetail($request->all());

        return view(self::$PATH.'transaction_data')->withTransactions($data['transactions']);
    }

    public function getImporterTransactionsDetail($data) {
        $transactions = null;
        $start = Carbon::parse($data['start'])->startOfDay();
        $end = Carbon::parse($data['end'])->endOfDay();
        $product_id = (int) $data['product'];
        $importer =  $this->_importerRepository->getImporters( $data['importer']);
        if ( $importer->productstransactions()->count() ) {
            if ($product_id == 0) {
                $transactions = $importer->productstransactions()
            ->whereBetween('importer_product_transactions.created_at', [$start,$end])
            ->orderBy('id', 'desc')
            ->join('products', 'importer_product_transactions.product_id', '=', 'products.id')
            ->join('users', 'importer_product_transactions.created_by', '=', 'users.id')
            ->leftJoin('release', 'importer_product_transactions.release_id', '=', 'release.id')
            ->leftJoin('orders', 'importer_product_transactions.order_id', '=', 'orders.id')
            ->select('importer_product_transactions.release_id', 'importer_product_transactions.order_id', 'importer_product_transactions.id', 'importer_product_transactions.quantity', 'importer_product_transactions.balance_before_transaction', 'importer_product_transactions.transaction_type', 'importer_product_transactions.created_at', 'users.first_name as first_name', 'users.last_name as last_name', 'products.name as product_name', 'release.id as release_number', 'release.purchase_order as release_purchase_order', 'orders.order_id as order_number', 'orders.purchase_order as orders_purchase_order')
            ->get();
            }
            else {
                $transactions = $importer->productstransactions()
                ->where('product_id',$product_id)
            ->whereBetween('importer_product_transactions.created_at', [$start,$end])
            ->orderBy('id', 'desc')
            ->join('products', 'importer_product_transactions.product_id', '=', 'products.id')
            ->join('users', 'importer_product_transactions.created_by', '=', 'users.id')
            ->leftJoin('release', 'importer_product_transactions.release_id', '=', 'release.id')
            ->leftJoin('orders', 'importer_product_transactions.order_id', '=', 'orders.id')
            ->select('importer_product_transactions.release_id', 'importer_product_transactions.order_id', 'importer_product_transactions.id', 'importer_product_transactions.quantity', 'importer_product_transactions.balance_before_transaction', 'importer_product_transactions.transaction_type', 'importer_product_transactions.created_at', 'users.first_name as first_name', 'users.last_name as last_name', 'products.name as product_name', 'release.id as release_number', 'release.purchase_order as release_purchase_order', 'orders.order_id as order_number', 'orders.purchase_order as orders_purchase_order')
            ->get();
            }
        }

        return ['importer' => $importer, 'transactions' => $transactions];
    }


}
