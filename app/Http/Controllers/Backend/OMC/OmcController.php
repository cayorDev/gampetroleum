<?php

namespace App\Http\Controllers\Backend\OMC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\System\OMC\OmcLocations;
use App\Repositories\Backend\OMC\OmcLocationRepository;
use App\Repositories\Backend\OMC\OmcProductStockRepository;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\Backend\ProductsRepository;
use App\Repositories\Backend\ContactsRepository;
use App\Http\Requests\Backend\ContactStoreRequest;
use App\Repositories\Backend\OMC\AddressesRepository;
use App\Http\Requests\Backend\Omc\StoreAddressRequest;
use App\Http\Requests\Backend\Omc\UpdateOmcRequest;
use App\Repositories\Backend\Importers\ImporterRepository;
use App\Models\Product;
use App\Http\Requests\Backend\Omc\OmcStoreRequest;
use App\Http\Requests\Backend\Omc\OmcLocationStoreRequest;
use Excel;
use App\Exports\OmcExport;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OmcController extends Controller
{

    /**
     * @var  to specify static resources path for omc section
     */
    STATIC $PATH = 'backend.omc.';

    /**
    * @var OmcLocationRepo
    * @var  OmcRepository <p>For omc repository instance</p>
    * @var  ImporterRepository
    */
    protected $_OmcLocationRepo;

    /**
     * @var  OmcRepository <p>For omc repository instance</p>
     * @var  ImporterRepository

     */
    protected $_omcRepository;

    /**
     * @var  ImporterRepository
     */

    protected $_importerRepository;

    /**
     * @var ContactsRepository
     */
    protected $ContactsRepository;
    /**
     * OmcContactsController constructor.
     *
     * @param OmcContactsRepository $OmcContactsRepository
     * * @param ImporterRepository $ImporterRepository
     */

    protected $_addressesRepository;

    /**
     * @var OmcProductStockRepository $OmcProductStockRepository
     */

    protected $_omcProductStockRepository;

    /**
    * OmcController constructor.
    * @param OmcLocationRepository $OmcLocationRepository
    * @param ImporterRepository $ImporterRepository
    */
    public function __construct(OmcRepository $OmcRepository,OmcLocationRepository $OmcLocationRepository, ImporterRepository $ImporterRepository,ContactsRepository $contactsRepository,AddressesRepository $addressesRepository, OmcProductStockRepository $OmcProductStockRepository, ProductsRepository $productsRepository)
    {
        $this->_importerRepository = $ImporterRepository;
        $this->_addressesRepository= $addressesRepository;
        $this->contactsRepository = $contactsRepository;
        $this->productsRepository = $productsRepository;
        $this->_OmcLocationRepo = $OmcLocationRepository;
        $this->_omcRepository   = $OmcRepository;
        $this->_omcProductStockRepository = $OmcProductStockRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
    	return view(self::$PATH.'index');
    }

    /**
    * Collect data for datatable of omc
    * @param Request $request
    */
    public function data( Request $request )
    {
    	//Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->_omcRepository->getAllRecordsForDatatable();

        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('company_name','like', '%'.$value.'%')
                ->orWhere('owner_name', 'like', '%'.$value.'%')
                ->orWhere('email', 'like', '%'.$value.'%');
            });
        }

        $data   = $data->get()
        ->map( function( $q ) {
            return [
                'id'                => $q->id,
                'name'              => $q->company_name,
                'owner'             => $q->owner_name,
                'updated_at'        => date( 'D, M j, Y \a\t g:ia', strtotime( $q->updated_at ) ),


            ];

        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<div class="actions" style="position:relative;"><a href="'.route('admin.omc.show',$val['id']).'" title="Manage" class="col-md-4 col-sm-4 col-lg-4" ><i class="fas fa-eye" ></i></a>&nbsp;<a title="Delete" style="color:indianred;" href="'. route("admin.omc.delete",$val['id']) .'" onclick="return confirm(\'Are you sure you want to delete this? All data associated with the OMC will be deleted.\');"><i class="fas fa-trash delete" style="color:#FF5733"></i></a>
            </div>'; })
        ->make(true);

    }


    public function contact($omc_id)
    {
        $contact=$this->contactsRepository->getActivePaginated($omc_id, 'id', 'asc');
    }
    /**
     * Fetching contacts for omc
     * @param  int $id
     */

    public function contacts(int $id = 0)
    {


        if ( $omc = $this->_omcRepository->getDetail($id) ) {
            $contact = $omc->contacts;
        }

        return view('backend.omc.contacts')->withcontact($contact)->withids($id);
    }

    /**
     * Display the locations of specified omc.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function location(int $id = 0)
    {
        $locations=$this->_OmcLocationRepo->getloc($id);
        return view(self::$PATH.'locations')->withids($id)->withdata($locations);
    }

    /**
     * Store a newly created location and edit existing location in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $omc_id
     * @return  \Illuminate\Http\Response
     */
    public function storeLocations(OmcLocationStoreRequest $request, $omc_id)
    {
        if ( count( $request->except('_token') ))
        {
            $store=0;
            foreach ($request->locations as $key => $value) {

                if($value['id'] == null) {
                    $val = $request->locations["$key"];
                    $this->_OmcLocationRepo->createLocation($val, $omc_id);
                    $store=1;
                } else {
                    $loc=$request->locations[$key];
                    $id = $value['id'];
                    $this->_OmcLocationRepo->editLoc($id, $loc);
                }
            }
            if($store==1){
                return back()->withFlashSuccess(__('alerts.backend.omc.location.add'));
            } else {
                return back()->withFlashSuccess(__('alerts.backend.omc.location.update'));
            }
        }
        return Redirect::back()->withInput(Input::all())->withFlashFail(__('alerts.backend.omc.create_fail'));
    }




    public function locations()
    {
        $data=$this->OmcLocationRepository->all();
        return view('backend.omc.show')->withdata($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteLocation($id)
    {
        $val=$this->_OmcLocationRepo->deleteById($id);

        if($val == true) {
            return back()->withFlashSuccess(__('alerts.backend.location.delete'));
        } else {
            return back()->withFlashError(__('exceptions.backend.access.location.delete_error'));
        }
    }

    public function createContact(ContactStoreRequest $request,$omc_id)
    {

        if ( count( $request->except('_token') ))
        {
            $store=0;
            foreach ($request->contact as $key => $value) {
                if($value['id'] == null) {
                    $val = $request->contact["$key"];
                    $this->contactsRepository->createContact($val , $omc_id);
                    $store=1;
                } else {
                    $contact=$request->contact[$key];
                    $id = $value['id'];
                    $this->contactsRepository->editContact($id , $contact);
                }
            }
            if($store==1){
                return back()->withFlashSuccess(__('alerts.backend.omc.contacts.add'));
            } else {
                return back()->withFlashSuccess(__('alerts.backend.omc.contacts.update'));
            }
        }
        return Redirect::back()->withInput(Input::all())->withFlashFail(__('alerts.backend.omc.create_fail'));

    }


    public function deleteContact($id)
    {
        $val=$this->contactsRepository->deleteById($id);

        if($val == true) {
            return back()->withFlashSuccess(__('alerts.backend.contacts.delete'));
        } else {
            return back()->withFlashError(__('exceptions.backend.access.contacts.delete_error'));
        }
    }


    public function address()
    {
        $address=$this->_addressesRepository->getActivePaginated(25, 'id', 'asc');
        return view('backend.omc.address',compact('address'));
    }

    /**
     * Display section for new omc creation
     * @return \View
     */
    public function create()
    {
        $url=url()->previous();
        $meta = ['title'=>'Omc Management | Add new OMC', 'value'=> 0];
        $products = [];
        foreach( Product::get(['id', 'name'])->toArray() as $product ) {
            $products[$product['name']] = $product['name'];
        }
        $importers  = [];

        $importers = $this->_importerRepository->getAllRecordsForDatatable()->get(['id', 'company_name'])->map(function($q){
            return $q->toArray();
        });

        return view(self::$PATH.'create')->withProducts($products)->withUrl($url)->withImporters($importers
        )->withMeta($meta);
    }


    public function store(Request $request)
    {

        $data=$this->OmcLocationRepository->all();
        $omc= $this->OmcLocationRepository->astore($request);

        return redirect()->route('admin.omc.locations')->withdata($data)
        ->with('success','data added succesfully');

    }


    /**
     * Store OMC details provided
     * @param  Request $request All parameters from form
     * @return Illuminate\Routing\Redirector   redirect
     */
    public function Omcstore( OmcStoreRequest $request )
    {

        if ( count( $request->except('_token') ) && $data = $request->except('_token') ) {

            $type = isset( $request->type ) ? (int)$request->type : 0;
            //Omc details
            $record['omc']            = $request->only('company_name', 'owner_name', 'email', 'tel');
            //Omc address details
            $record['address']        = isset( $data['address'] ) ? $data['address'] : [];
            //Omc contacts details
            $record['contacts']       = isset( $data['contact'] ) && count( $data['contact'] )  ? $data['contact'] : [];
            //omc locations
            $record['locations']      = isset( $data['locations'] ) && count( $data['locations'] )  ? $data['locations'] : [];
            //omc products
            $record['products']       = isset( $data['products'] ) ? $data['products']: [];
            //Importer for product stock
            $record['importers']      = isset( $data['importers'] ) ? $data['importers']: [];

            //Differenciating according to type
            if ( $type == 2 ) {
                //Creating omc record
                $omc = $this->__createOmc( $record );
                //Creating Importer record
                $this->__createImporter( $record, $omc->address_id );
                return Redirect()->intended($request->url)
                ->withFlashSuccess(__('alerts.backend.omc.create_both'));
            } elseif( $type == 1 ) {
                //Create Importer record
                $this->__createImporter( $record, 0 );
                return Redirect()->intended($request->url)
                ->withFlashSuccess(__('alerts.backend.importer.create'));
            } else {
                //Creating Omc record
                $this->__createOmc( $record );
                return Redirect()->intended($request->url)
                ->withFlashSuccess(__('alerts.backend.omc.create'));
            }

        }
        //Default response
        return redirect()->back()
        ->withInput()
        ->withFlashFail(__('alerts.backend.omc.create_fail'));

    }
    /**
     * Display omc detail of a record
     * @return redirect to View
     */
    public function show($id)
    {
        $omc        =   $this->_omcRepository->getDetail($id);
        $products   =   $omc->productsInStock->map(function($q) {
            return [
            'id'            => $q->id,
            'name'          => isset( $q->omcProducts->name ) ? $q->omcProducts->name : 'NA',
            'balance'       => $q->outstanding_balance,
            'importer'      => !is_null( $q->importer_id ) ? $q->importer_id : 0,
            'product_id'    => $q->product_id,
            'importer_name' => !is_null( $q->importer ) ? $q->importer->company_name : '',
            'omc_importer_name' => !is_null( $q->SupplierOmc ) ? $q->SupplierOmc->company_name.'/' : '',
            ];
        });
        if ( $omc->productstransactions()->count() ) {
            $transactions = $omc->productstransactions()
            ->orderBy('id', 'desc')
            ->join('products', 'omc_product_transactions.product_id', '=', 'products.id')
            ->join('users', 'omc_product_transactions.created_by','=', 'users.id' )
            ->leftJoin('release', 'omc_product_transactions.release_id','=', 'release.id')
            ->leftJoin('orders', 'omc_product_transactions.order_id','=', 'orders.id' )
            ->select('omc_product_transactions.id','omc_product_transactions.quantity','omc_product_transactions.order_id','omc_product_transactions.release_id', 'omc_product_transactions.transaction_type','users.first_name as first_name','users.last_name as last_name', 'products.name as product_name', 'orders.purchase_order as orders_purchase_order', 'release.purchase_order as release_purchase_order')
            ->get()->toArray();
        } else {
            $transactions = [];
        }

        $importers = $this->_importerRepository->getAllRecordsForDatatable()->get(['id', 'company_name'])->toArray();
        return view(self::$PATH.'address')
        ->withomc($omc)
        ->withAddress($omc->addresses)
        ->withproducts($products)
        ->withtransactions($transactions)
        ->withImporters($importers);
    }
    /**
     *Delete omc
     * @return redirect to View with omc id
     */
    public function deleteOmc($id)
    {
        try{
            DB::beginTransaction();
            $omc=$this->_omcRepository->deleteOmc($id);
            DB::commit();
            return  redirect()->route('admin.omc.index')->withFlashSuccess(__('alerts.backend.omc.deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            return  redirect()->route('admin.omc.index')->withFlashError(__('alerts.backend.omc.deleted_error'));
        }

    }
    /**
     * Update address
     * @return redirect to View
     */

    public function addressupdate(StoreAddressRequest $request,$id)
    {
        $omc_id=$request->addid;
        $omc=$this->_addressesRepository->updateAddress($request,$id);
        return redirect()->route('admin.omc.show',$omc_id)->withFlashSuccess(__('alerts.backend.omc.address.updated'));
    }
    /**
     *Delete Address of omc record
     * @return redirect to View with omc id
     */
    public function addressdelete($id)
    {
        $omc = $this->_omcRepository->getDetail($id);
        if ( $omc->addresses()->delete() ) {
            return  redirect()->route('admin.omc.show',$id)->withFlashSuccess(__('alerts.backend.omc.address.deleted'));
        }
        return  redirect()->route('admin.omc.show',$id)->withFlashSuccess(__('alerts.backend.omc.address.delete_fail'));
    }

    /**
     * manageOmcInventory
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function manageOmcInventory( Request $request, int $id = 0 )
    {
        if( isset( $request->stock_id ) && isset( $request->append_quantity ) && isset( $request->operation ) ) {
            $this->_omcProductStockRepository->updateOmcStock( $request->except('_token'), $id);
            return redirect()->back()->withFlashSuccess('Operation completed successfully.');
        }
        return redirect()->back()->withFlashFail('Something went wrong. Unable to proceed.');
    }
     /**
     * Update omc record
     * @param  UpdateOmcRequest $request data to update
     * @param  int $id omc id to work with
     * @return redirect to route with omc id
     */
     public function update(UpdateOmcRequest $request, int $id = 0 )
     {
        $omc=$this->_omcRepository->update($request,$id);

        return redirect()->route('admin.omc.show',$id)->withFlashSuccess(__('alerts.backend.omc.updated'));
    }
     /**
     *Store Address of omc record and update address_id
     * @return redirect to View with omc id
     */
     public function addressStore(StoreAddressRequest $request,$id)
     {
        $add=$this->_addressesRepository->store($request);
        $omc=$this->_omcRepository->link($add,$id);
        return redirect()->route('admin.omc.show',$id)->withFlashSuccess(__('alerts.backend.omc.address.store'));
    }

    /**
    *Update the specified product quantity in storage.
    */
    public function manageOmcProduct(Request $request ,int $id )
    {
        $data = $this->_omcRepository->getDetail($id);
        $products=$this->_omcRepository->getproductsId($data,$id);
        $quantity=$request->quantity;
        $transaction=$request->transaction;
        $transaction_type = $transaction==0?'add':'deduct';
        $product_id = $request->product_id;

        $data->productsInStock()->where('product_id',$product_id)->update([
            'product_id'            => $product_id,
            'outstanding_balance'   => $quantity,
        ]);
        $data->productstransactions()->where('product_id',$product_id)->update([
            'product_id'            => $product_id,
            'quantity'              => $quantity,
            'transaction_type'      => $transaction_type,
        ]);

        return redirect()->route('admin.omc.show',$data->id)
        ->withomc($data)
        ->withproduct($products)
        ->withFlashSuccess(__('alerts.backend.importer.updated'));
    }

    /**
     * Create omc record
     * @param  array  $data
     * @return void
     */
    private function __createOmc( array $data = [] )
    {
        return $this->_omcRepository->createWithAddressContactsAndLocations($data['omc'], $data['address'], $data['contacts'], $data['locations'], $data['products'], $data['importers'] );
    }

    /**
     * Create Importer record
     * @param  array  $data [description]
     * @return void
     */
    private function __createImporter( array $data = [], int $address_id = 0 )
    {
        return $this->_importerRepository->create( [ 'importer' => $data['omc'], 'contacts' => $data['contacts'] , 'products' => $data['products'], 'address_id' => $address_id, 'address' => $data['address'] ] );
    }

    /**
    * Show the transactions for specified Importer.
    */
    public function viewTransactions(int $id )
    {
        $data = $this->_omcRepository->getDetail($id);
        $products = $this->productsRepository->getList();
        $product=array();
			$product['0']="All";
			foreach($products as $ind=>$val){
				$product[$ind]=$val;
			}
        return view(self::$PATH.'transactions')
        ->withOmc($data)
        ->withProducts($product);

        // ->withTransactions(count($data['transactions']) ? $data['transactions']->toArray() : []);
    }

    /**
    * Download Excelspread sheet for the transactions for specified Importer.
    */
    public function excelTransactions(Request $request, int $id)
    {
        // $data = $this->getOmcTransactions($id);
         $data = $this->getOmcTransactionsDetail($request->all());

        // Initialize the array which will be passed into the Excel generator.
        $transactionsArray = [];

        // Define the Excel spreadsheet headers
        // $transactionsArray[] = ['Order Reference', 'Date', 'Time', 'Vehicle', 'Driver', 'Order Quantity', 'Inventory Balance Before'];

        // Convert each transaction of the returned collection into an array and append it to the transactions array.
        $transactions = count($data['transactions']) ? $data['transactions']->toArray() : [];
        if (count($transactions)) {
            foreach ($transactions as $transaction) {
                $arranged_transaction = [];

                if (! is_null($transaction['order_number'])) {
                    $arranged_transaction[] = $transaction['orders_purchase_order'];
                } elseif (!is_null($transaction['release_number'])) {
                    $arranged_transaction[] = $transaction['release_purchase_order'];
                } else {
                    $arranged_transaction[] = '';
                }

                $arranged_transaction[] = \Carbon\Carbon::parse($transaction['created_at'])->format('Y-m-d');
                $arranged_transaction[] = \Carbon\Carbon::parse($transaction['created_at'])->format('h:i:s');
                $arranged_transaction[] = $transaction['product_name'];
                $arranged_transaction[] = $transaction['vehicle_number'];
                $arranged_transaction[] = $transaction['driver_name'];
                if ($transaction['transaction_type'] == "add") {
                    $arranged_transaction[] = '+ '.$transaction['quantity'];
                } elseif ($transaction['transaction_type'] == "deduct") {
                    $arranged_transaction[] = '- '.$transaction['quantity'];
                } else {
                    $arranged_transaction[] = 'NA';
                }

                $arranged_transaction[] = $transaction['balance_before_transaction']?$transaction['balance_before_transaction']:"0";

                $transactionsArray[] = $arranged_transaction;
            }

            $file_name = $data['omc']->company_name.'_trans';

            return Excel::download(new OmcExport($transactionsArray,$file_name), $file_name.'.xlsx');
        } else {
            return redirect()->back()->withFlashFail('Something went wrong. Unable to proceed.');
        }


    }

    public function getOmcTransactionData(Request $request)
    {
        $data = $this->getOmcTransactionsDetail($request->all());
        return view(self::$PATH.'transaction_data')->withTransactions($data['transactions']);
    }

    public function getOmcTransactionsDetail($data) {
        $transactions = null;
        $start = Carbon::parse($data['start'])->startOfDay();
        $end = Carbon::parse($data['end'])->endOfDay();
        $omc = $this->_omcRepository->getDetail($data['omc']);
        $product_id = (int) $data['product'];
        if ( $omc->productstransactions()->count() ) {
            if ($product_id == 0) {
                $transactions = $omc->productstransactions()
            ->whereBetween('omc_product_transactions.created_at', [$start,$end])
            ->orderBy('id', 'desc')
            ->leftJoin('products', 'omc_product_transactions.product_id', '=', 'products.id')
            ->leftJoin('users', 'omc_product_transactions.created_by', '=', 'users.id')
            ->leftJoin('release', 'omc_product_transactions.release_id', '=', 'release.id')
            ->leftJoin('orders', 'omc_product_transactions.order_id', '=', 'orders.id')
            ->leftJoin('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
            ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
            ->select('omc_product_transactions.id', 'omc_product_transactions.quantity', 'omc_product_transactions.balance_before_transaction', 'omc_product_transactions.transaction_type', 'omc_product_transactions.created_at', 'users.first_name as first_name', 'users.last_name as last_name', 'products.name as product_name', 'orders.order_id as order_number', 'vehicles.license_plate_number as vehicle_number', 'drivers.name as driver_name', 'release.id as release_number', 'release.purchase_order as release_purchase_order', 'orders.purchase_order as orders_purchase_order')
            ->get();
            }
            else{
                $transactions = $omc->productstransactions()
            ->where('product_id',$product_id)
            ->whereBetween('omc_product_transactions.created_at', [$start,$end])
            ->orderBy('id', 'desc')
            ->leftJoin('products', 'omc_product_transactions.product_id', '=', 'products.id')
            ->leftJoin('users', 'omc_product_transactions.created_by', '=', 'users.id')
            ->leftJoin('release', 'omc_product_transactions.release_id', '=', 'release.id')
            ->leftJoin('orders', 'omc_product_transactions.order_id', '=', 'orders.id')
            ->leftJoin('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
            ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
            ->select('omc_product_transactions.id', 'omc_product_transactions.quantity', 'omc_product_transactions.balance_before_transaction', 'omc_product_transactions.transaction_type', 'omc_product_transactions.created_at', 'users.first_name as first_name', 'users.last_name as last_name', 'products.name as product_name', 'orders.order_id as order_number', 'vehicles.license_plate_number as vehicle_number', 'drivers.name as driver_name', 'release.id as release_number', 'release.purchase_order as release_purchase_order', 'orders.purchase_order as orders_purchase_order')
            ->get();
            }
        }
        return ['omc' => $omc, 'transactions' => $transactions];
        // return $transactions;
    }

}
