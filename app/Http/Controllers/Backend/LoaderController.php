<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\LoaderRepository;
use App\Http\Requests\Backend\Loader\LoaderStoreRequest;
use App\Http\Requests\Backend\Loader\LoaderUpdateRequest;
use App\Models\Loader;
use Yajra\Datatables\Datatables;


class LoaderController extends Controller
{
    /**
     * @var VehicleRepository
     */
    protected $loaderRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $vehicleRepository
     */
    public function __construct(LoaderRepository $loaderRepository)
    {
        $this->loaderRepository = $loaderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.loader.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.loader.add');
    }

    public function getLoaderData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->loaderRepository->getAllRecordsForDatatable();
        
        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('name','like', '%'.$value.'%');
            });
        }
        
        $data   = $data->get()
        ->map( function( $q ) {
            return [
                'id'                        => $q->id,
                'name'                      => $q->name,   
            ];        
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.loader.show',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i></button></a>&nbsp;<a href="'.route('admin.loader.edit',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-warning"><i class="fas fa-edit" ></i></button></a>&nbsp;<a href="'. route("admin.loader.destroy",$val['id']) .'" onclick="return confirm('."'Do you want to delete Loader ?'".');"  title="Delete Loader"><button class="btn-sm btn btn-danger"><i class="fas fa-trash"></i></button></a>&nbsp';
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoaderStoreRequest $request)
    {  
        $this->loaderRepository->create($request->except('_token'));
        return redirect()->route('admin.loader.index')->withFlashSuccess(__('alerts.backend.loader.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loader=$this->loaderRepository->getloader($id);
        return view('backend.loader.show')->withDriver($loader);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $loader=$this->loaderRepository->getloader($id);
     return view('backend.loader.edit')->withLoader($loader);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoaderUpdateRequest $request, $id)
    {
     $loader=$this->loaderRepository->getloader($id);
     $this->loaderRepository->update($loader, $request->only(
        'name'
    ));

     return redirect()->route('admin.loader.index')->withFlashSuccess(__('alerts.backend.loader.updated'));
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $loader=$this->loaderRepository->getloader($id);
        $this->loaderRepository->deleteById($id);  
        return redirect()->route('admin.loader.index')->withFlashSuccess(__('alerts.backend.loader.deleted'));
    }

    /**
     * Delete record from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function delete($id)
    // {
    //     $deletedLoader=$this->loaderRepository->getDeletedLoader($id);
    //     $this->loaderRepository->forceDelete($deletedLoader);
    //     return redirect()->route('admin.loader.deleted')->withFlashSuccess(__('alerts.backend.loader.deleted_permanently'));
    // }

     /**
     * Restore loader record in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function restore($id)
     {
        $deletedLoader=$this->loaderRepository->getDeletedLoader($id);
        $this->loaderRepository->restore($deletedLoader);
        return redirect()->route('admin.loader.index')->withFlashSuccess(__('alerts.backend.loader.restored'));
    }

     /**
     * Restore the specified resource in  storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllDeleted()
     {
        return view('backend.loader.deleted');
    }

    public function getDeletedLoader(Request $request)
    {
         //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->loaderRepository->getAllDeletedRecordsForDatatable();
        
        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('name','like', '%'.$value.'%');
            });
        }
        
        $data   = $data->get()
        ->map( function( $q ) {
            return [
                'id'                        => $q->id,
                'name'                      => $q->name,   
            ];        
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.loader.restore',$val['id']).'" title="Restore" ><button class="btn-sm btn btn-primary"><i class="fas fa-check" ></i></button></a>&nbsp;';
        })
        ->make(true);
    }
}
