<?php

namespace App\Http\Controllers\Backend\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Importers\ImporterRepository;
use App\Repositories\Backend\Importers\ImporterProductStockRepository;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\Backend\OMC\OmcProductStockRepository;
use App\Repositories\Backend\DriverRepository;
use App\Repositories\Backend\VehicleRepository;
use App\Repositories\Backend\ProductsRepository;
use App\Repositories\Backend\Orders\OrderRepository;
use App\Repositories\Backend\Orders\OrderProductRepository;
use App\Repositories\Backend\Orders\OrderProductMeterRepository;
use App\Repositories\Backend\Orders\ChamberAssignmentRepository;
use App\Repositories\Backend\Orders\TruckDetailRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Audit\LogsRepository;
use App\Repositories\Backend\LoaderRepository;
use App\Repositories\Backend\ManagementRepository;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Backend\Order\OrderStoreRequest;
use App\Http\Requests\Backend\Order\OrderStoreHfoRequest;
use App\Http\Requests\Backend\Order\OrderUpdateRequest;
use App\Http\Requests\Backend\Order\OrderUpdateProductRequest;
use App\Events\Backend\Order\OrderLogEvents;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Backend\Order\OrderCreated;
use Yajra\Datatables\Datatables;
use Config;
use Excel;
use App\Exports\OrdersExport;
use App\Exports\DailyHfoLpgData;
use App\Exports\InventoryExcel;
use App\Exports\OrderTypeExport;

class OrderController extends Controller
{
		/**
		 * @var  ImporterRepository <p>For importer repository instance</p>
		 */
		protected $_importerRepository;

		/**
		 * @var  OmcRepository <p>For omc repository instance</p>
		 */
		protected $_omcRepository;

		/**
		 * @var  DriverRepository <p>For driver repository instance</p>
		 */
		protected $_driverRepository;

		/**
		 * @var  VehicleRepository <p>For vehicle repository instance</p>
		 */
		protected $_vehicleRepository;

		/**
		 * @var  ProductsRepository <p>For product repository instance</p>
		 */
		protected $_productRepository;

		/**
		 * @var  OrderProductMeterRepository <p>For order product meter repository instance</p>
		 */
		protected $_orderProductMeterRepository;

		/**
		 * @var  LogsRepository <p>For Logs repository instance</p>
		 */
		protected $_logsRepository;

		/**
		* OrderController constructor.
		*/

		public function __construct( ImporterRepository $_importerRepository, DriverRepository $_driverRepository, VehicleRepository $_vehicleRepository, ProductsRepository $_productsRepository, OmcRepository $_omcRepository, OrderRepository $_orderRepository, OrderProductRepository $_orderProductRepository, ImporterProductStockRepository $_importerProductStockRepository, OmcProductStockRepository  $_omcProductStockRepository, OrderProductMeterRepository $_orderProductMeterRepository,
			ChamberAssignmentRepository $_chamberAssignmentRepository, UserRepository $_userRepository,TruckDetailRepository $_truckDetailRepository,LogsRepository $_logsRepository,LoaderRepository $loaderRepository,managementRepository $managementRepository)
		{
			$this->_importerRepository = $_importerRepository;
			$this->_driverRepository = $_driverRepository;
			$this->_vehicleRepository = $_vehicleRepository;
			$this->_productsRepository = $_productsRepository;
			$this->_omcRepository = $_omcRepository;
			$this->_orderRepository = $_orderRepository;
			$this->_orderProductRepository = $_orderProductRepository;
			$this->_importerProductStockRepository = $_importerProductStockRepository;
			$this->_omcProductStockRepository = $_omcProductStockRepository;
			$this->_orderProductMeterRepository = $_orderProductMeterRepository;
			$this->_chamberAssignmentRepository = $_chamberAssignmentRepository;
			$this->_userRepository = $_userRepository;
			$this->_truckDetailRepository = $_truckDetailRepository;
			$this->_logsRepository = $_logsRepository;
			$this->loaderRepository = $loaderRepository;
            $this->managementRepository = $managementRepository;
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index( Request $request )
		{
			$type = $request->input('type');
			$orderCounts 	=  $this->_orderRepository->getOrderCount();
			// dd($orderCounts);
			return view('backend.order.index')->withOrders($orderCounts)->withType($type);
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			$importers = $this->_importerRepository->getList();
			$vehicles = $this->_vehicleRepository->getList();
			$driversList = $this->_driverRepository->getList();
			$omc = $this->_omcRepository->getList();
			$adminsupervisors = $this->_userRepository->getAdminAndSupervisorList();
			$hfoproducts = $this->_productsRepository->getHfoLpgProducts();

			return view('backend.order.add')->withImporters($importers)->withVehicles($vehicles)->withOmc($omc)->withDrivers($driversList)->withAdminsupervisors($adminsupervisors)->withHfoproducts($hfoproducts);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(OrderStoreRequest $request)
		{
			try {
				DB::beginTransaction();
				if (!isset($request->products)){
					$response['success'] = false;
					$response['message'] = 'no products specified for an order';
					return response()->json($response);
				}
				$checkCapacity = $this->checkVehicleOrderCapacity($request->all());
				if($checkCapacity['status']){
					if($request->driver_id == 0 || is_null($request->driver_id)){
						$driver = $this->_driverRepository->create($request->only('driver'));
						$request->merge([
							'driver_id' => $driver->id,
						]);
					}

					$order = $this->_orderRepository->create($request->except('_token','products','driver'));

					$order_product = $this->_orderProductRepository->addOrderProduct($request->only('products'), $order->id);

					$chamberData =  ['products' => $request->chambers , 'orderID' => $order->id];

					$assignchamber =  $this->_chamberAssignmentRepository->create($chamberData);

					DB::commit();
					event(new OrderLogEvents($order, "ORDER CREATED", Auth::user()->full_name));
					$response['success'] = true;
					$response['message'] = __('alerts.backend.order.created');
					$response['orderId'] = $order->id;
					$request->session()->flash('flash_success', __('alerts.backend.order.created'));
				}
				else {
					$response['success'] = false;
					$response['message'] = $checkCapacity['message'];
					if(isset($checkCapacity['importers'])) {
						$response['importers'] = $checkCapacity['importers'];
					}
				}
				return response()->json($response);
			} catch (\Exception $e) {
				DB::rollback();
				$response['success'] = false;
				$response['message'] = __('alerts.backend.order.createderror');
				return response()->json($response);
			}
		}

		public function hfostore(OrderStoreHfoRequest $request)
		{
			try{
				//check if importer have ordered quantity in inventory

				$checkInventory =  $this->checkInventoryIsAvailable($request->all());
				if(!$checkInventory['success']) {
					return response()->json($checkInventory);
				}
				DB::beginTransaction();

				//Providing default response parameters
				$response['status']		= false;
				$response['message']	= 'No inputs provided to fulfill order process';

				if ( count( $request->except('_token') ) ) {
					if($request->driver_id == 0 || is_null($request->driver_id)){
						$driver = $this->_driverRepository->create($request->only('driver'));
						$request->merge([
							'driver_id' => $driver->id,
						]);
					}

					$data['importer_id']			= (int)$request->input('importer_id');
					$data['omc_id']			= (int)$request->input('omc_id');
					$data['purchase_order']			= $request->input('purchase_order');
					$data['is_chamber_assigned']	= 1;
					$data['authorized_by']			= (int)$request->input('authorised_by');
					$data['approved_by']			= \Auth::id();
					$data['is_vehicle_loaded']		= 1;
					$data['is_order_verified']		= 1;
					$data['is_complete']			= 0;
					$data['vehicle_id']				= (int)$request->input('vehicle_id');
					$data['driver_id']				= (int)$request->input('driver_id');
					$data['authorized_at']			= \Carbon\Carbon::now();
					//Setting records related to orders
					$order 		= $this->_orderRepository->create($data);
					if ( !is_null( $order ) ) {
						$data['products'][$request->product_id]	= $request->input('netweight');
						$product_record			=  $this->_orderProductRepository->addOrderProduct($data, $order->id);
						$truck['orderID']		= $order->id;
						$truck['truck']['truck_loading_ticket_number']	= $order->id;
						$truck['truck']['weight_before_loading']		= $request->input('beforeloading');
						$truck['truck']['weight_after_loading']			= $request->input('afterloading');
						$truck['truck']['net_weight']					= $request->input('netweight');
						$truck['truck']['location']						= $request->input('location');
						$truck_reading									= $this->_truckDetailRepository->create($truck);
						// if ($request->product_id == 3) {
						// 	$this->updateImporterStock( $data['importer_id'], $request->product_id, $truck['truck']['net_weight'], $order->id );
						// } else {
						// 	$this->_omcProductStockRepository->updateLpgOrderInventory( $order, $truck['truck']['net_weight'] );
						// }

						DB::commit();

						//Response for order success
						$response['success'] = true;
						$response['message'] = __('alerts.backend.order.created');
						$response['order_id'] = $order->id;
					}
				}

			} catch ( \Exception $e ) {
				DB::rollback();
				$response['success'] = false;
				$response['message'] = __('alerts.backend.order.createderror');
			}

			return response()->json($response);
		}

		public function checkInventoryIsAvailable(array $payload)
		{
			//check for HFO orders
			$response['success'] = true;
			if ($payload['product_id']  == 3 ) {

				$inventory = $this->_importerProductStockRepository->getProductInventory($payload['importer_id'], $payload['product_id']);
				if ($inventory) {
					if ($inventory->outstanding_balance < $payload['netweight']) {
						$response['success'] = false;
						$response['message'] = 'Importer does not enough Order quantity, Please reduce weight';
					}
				} else {
					$response['success'] = false;
					$response['message'] = 'Importer does not have inventory for selected product';
				}
			} else {
				//for lpg case , get omc data
				if (!$payload['omc_id']) {
					$response['success'] = false;
					$response['message'] = 'Please select OMC';
					return $response;
				}

				$omcLpgInventory =  $this->_omcProductStockRepository->getLpgProductListImporterBased($payload['omc_id'], $payload['importer_id']);
				if(!$omcLpgInventory){
					$response['success'] = false;
					$response['message'] = 'Selected Omc does not have LPG Inventory for specified Importer,  Please create release for OMC first';
					return $response;
				}
			}

			return $response;
		}

		/**
		 * Update Importer stock by product
		 * @param  int|integer $importerId [description]
		 * @param  int|integer $weight     [description]
		 * @return void
		 */
		public function updateImporterStock( int $importerID = 0, int $productID = 0 ,int $weight = 0 , $orderid)
		{
			return $this->_importerProductStockRepository->updateStockByImporterForOrder( $importerID, $productID, $weight, $orderid );
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{

			$checkifSpecial = $this->_orderProductRepository->checkifSpecial($id);
			if ($checkifSpecial) {
				$orderDetail = $this->_orderRepository->getOrderDataSpecial($id);
			} else {
				$orderDetail = $this->_orderRepository->getOrderData($id);
			}

			return view('backend.order.show')->withOrder($orderDetail);
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
			$importersData=[];
			$inventory=[];
			$vehicles = $this->_vehicleRepository->getList();
			$omc = $this->_omcRepository->getList();
			$checkifSpecial = $this->_orderProductRepository->checkifSpecial($id);
			if ($checkifSpecial) {
				$orderDetail = $this->_orderRepository->getOrderDataSpecial($id);
				$importersData = $this->_importerRepository->getList();
			} else {
				$orderDetail = $this->_orderRepository->getOrderData($id);
			}
			$products=[];
			$supplier =$orderDetail->importer_id;
			if(!is_null($orderDetail->importer_id) && !is_null($orderDetail->omc_id) && is_null($orderDetail->supplier_omc_id) ){
				$products=json_decode($this->getOmcProductList($orderDetail->omc_id), true);
				foreach($products['data'] as $product){
					foreach($product as $impId=>$p){
						$importersData[$impId]=$p['importer_name'];
						if($impId==$orderDetail->importer_id){
							foreach($p['product'] as $productKey=>$productname){
								foreach($p['inventory'] as $inventoryProductKey=>$inventoryData){
									if($inventoryProductKey==$productKey){
										$inventory[$productname]=$inventoryData;
									}
								}
							}
							$products=$p['product'];
						}
					}
				}
			} else if(!is_null($orderDetail->supplier_omc_id) && !is_null($orderDetail->omc_id))  {
				$supplier = $orderDetail->supplier_omc_id."".$orderDetail->importer_id;
				$omcProducts = $this->_omcProductStockRepository->getProductListAllOmcBased($orderDetail->omc_id);
				$productList =$this->processOmcProductList($omcProducts,$orderDetail->omc_id);
				foreach($productList['data'] as $supplierID=>$product){
					$importersData[$supplierID]= $product['name'];
					if($product['importer_id']==$orderDetail->importer_id){
						foreach($product['products'] as $prodId=>$p){
							$inventory[$p['product_name']]=$p['balance'];
							$products[$prodId]=$p['product_name'];
						}
					}
				}
			}
			$selectedProducts=[];
			foreach($orderDetail->OrderProducts as $orderproducts){
				array_push($selectedProducts,$orderproducts->Product->id);
			}
			$driversList = $this->_driverRepository->getList();
			$meterId = Config::get('meter-id');
			$seals = $orderDetail->seals;
			$hfoproducts = $this->_productsRepository->getHfoLpgProducts();
			$adminsupervisors = $this->_userRepository->getAdminAndSupervisorList();

			return view('backend.order.edit')->withOrder($orderDetail)->withImporters($importersData)->withVehicles($vehicles)->withOmc($omc)->withDrivers($driversList)->withSelectedProducts($selectedProducts)->withAllProducts($products)->withInventory($inventory)->withSupplier($supplier)->withMeter($meterId)->withSeals($seals)->withIsSepcial($checkifSpecial)->withHfoproducts($hfoproducts)->withAdminsupervisors($adminsupervisors);

		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(OrderUpdateRequest $request, $id)
		{
			try {
				DB::beginTransaction();
				$orderData=  $this->_orderRepository->getById($id);
				$checkifSpecial = $this->_orderProductRepository->checkifSpecial($id);
				if($checkifSpecial){
					if($orderData->is_complete){
					$orderDetail = $this->_orderRepository->getOrderDataSpecial($id);
					if ($orderDetail->OrderProducts->first()->product_id == 3) {
						$this->_importerProductStockRepository->editStockByImporterForOrder( $orderDetail->importer_id, $orderDetail->OrderProducts->first()->product_id, $orderDetail->Truck->net_weight,$orderDetail->id);
					} else {
						$this->_omcProductStockRepository->editLpgOrderInventory( $orderDetail ,$orderDetail->Truck->net_weight);
					}
				}
					$checkInventory =  $this->checkInventoryIsAvailable($request->all());

					if(!$checkInventory['success']) {
						return response()->json($checkInventory);
					}
					if($request->driver_id == 0 || is_null($request->driver_id)){
						$driver = $this->_driverRepository->create($request->only('driver'));
						$request->merge([
							'driver_id' => $driver->id,
						]);
					}
					$data['importer_id']			= (int)$request->input('importer_id');
					$data['omc_id']			=  (int)$request->input('omc_id');
					$data['purchase_order']			= $request->input('purchase_order');
					$data['is_chamber_assigned']	= 1;
					$data['authorized_by']			= (int)$request->input('authorised_by');
					$data['approved_by']			= \Auth::id();
					$data['is_vehicle_loaded']		= 1;
					$data['is_order_verified']		= 1;
					$data['is_complete']			= 0;
					$data['vehicle_id']				= (int)$request->input('vehicle_id');
					$data['driver_id']				= (int)$request->input('driver_id');
					$data['authorized_at']			= \Carbon\Carbon::now();
					$data['order_id']			= null;
					//Setting records related to orders
					$order 		= $this->_orderRepository->update($orderData, $data);
					if ( !is_null( $order ) ) {
						$data['products'][$request->product_id]	= $request->input('netweight');
						$product_record			=  $this->_orderProductRepository->editOrderProduct($data, $order->id);
						$truck['orderID']		= $order->id;
						$truck['truck']['truck_loading_ticket_number']	= $order->id;
						$truck['truck']['weight_before_loading']		= $request->input('beforeloading');
						$truck['truck']['weight_after_loading']			= $request->input('afterloading');
						$truck['truck']['net_weight']					= $request->input('netweight');
						$truck['truck']['location']						= $request->input('location');
						$truck_reading									= $this->_truckDetailRepository->create($truck);
						if ($request->product_id == 3) {
							$this->updateImporterStock( $data['importer_id'], $request->product_id, $truck['truck']['net_weight'], $order->id );
						} else {
							$this->_omcProductStockRepository->updateLpgOrderInventory( $order, $truck['truck']['net_weight'] );
						}
					}
				} else {
					if (!isset($request->products)){
						$response['success'] = false;
						$response['message'] = 'no products specified for an order';
						return response()->json($response);
					}
					if( $orderData->is_complete){
						// $this->_importerProductStockRepository->editOrderInventory($orderData->importer_id, $orderData->Chambers);
						$this->_omcProductStockRepository->editOrderInventory($orderData, $orderData->OrderProducts);
					}
					$checkCapacity = $this->checkVehicleOrderCapacity($request->all());
					if($checkCapacity['status']){
						if($request->driver_id == 0 || is_null($request->driver_id)){
							$driver = $this->_driverRepository->create($request->only('driver'));
							$request->merge([
								'driver_id' => $driver->id,
							]);
						}
						$order = $this->_orderRepository->editOrder($orderData,$request->except('_token','products','driver'));

						$order_product = $this->_orderProductRepository->editOrderProduct($request->only('products'), $order->id);
						$chamberData =  ['products' => $request->chambers , 'order_id' => $order->id];
						$assignchamber =  $this->_chamberAssignmentRepository->update($chamberData);
						if($orderData->is_vehicle_loaded ===1){
							$orderProductMeter = $this->_orderProductMeterRepository->update($request->only('product','id'),$order_product, $order->id);
							$data['seals']=json_encode($request->only('vehicle_chamber'));
							$data['comments']=$request->comments;
							$data['seen_by']=$orderData->seen_by;
							if( $orderData->is_complete){
								$data['is_complete']=0;
								$data['order_id']		= null;
							}
							$order = $this->_orderRepository->update($orderData, $data);
						}
					} else {
						$response['success'] = false;
						$response['message'] = $checkCapacity['message'];
						if(isset($checkCapacity['importers'])) {
							$response['importers'] = $checkCapacity['importers'];
						}
						return response()->json($response);
					}
				}
				DB::commit();
				event(new OrderLogEvents($order, "ORDER UPDATED", Auth::user()->full_name));
				$response['success'] = true;
				$response['message'] = __('alerts.backend.order.updated');
				return response()->json($response);
			} catch (\Exception $e) {

				DB::rollback();
				$response['success'] = false;
				$response['message'] = __('alerts.backend.order.updatederror');
				return response()->json($response);
			}


		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
				//
		}

		/**
		 * [supervisorIndex description]
		 * @return [type] [description]
		 */
		public function supervisorIndex()
		{
			return view('backend.order.supervisor.index');
		}

		/**
		* Collect data for datatable of Order for Supervisor
		* @param Request $request
		*/
		public function supervisorData(Request $request)
		{
			$draw   = $request->input( 'draw' );
			$order  = $request->input( 'order' );
			$start  = $request->input( 'start', 0 );
			$length = $request->input( 'length' );
			$search = $request->input( 'search' );
			$secho  = $request->input( 'sEcho' );

			$order_column    = $order[ 0 ][ 'column' ];
			$order_direction = $order[ 0 ][ 'dir' ];

			if ( empty( $order_column ) ) {
				$order_column = '0';
			}
			if ( empty( $order_direction ) ) {
				$order_direction = 'asc';
			}

			$data =  $this->_orderRepository->getOrdersDataForDatatable($request->type);

			if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
				$value = $search['value'];

				$data->where('id', $value)->orwhere(function($query) use ($value){
					$query->where('purchase_order','like', '%'.$value.'%');
					// $query->with(['Importer' => function ($query1) {
					// 	$query1->where('Importer.company_name','like', '%'.$value.'%');
					// }]);
					// $query->with(['Omc' => function ($query1) {
					// 	$query1->where('Omc.company_name','like', '%'.$value.'%');
					// }]);
					// $query->with(['Vehicle' => function ($query1) {
					// 	$query1->where('Vehicle.license_plate_number','like', '%'.$value.'%');
					// }]);
					// $query->with(['Driver' => function ($query1) {
					// 	$query1->where('Driver.name','like', '%'.$value.'%');
					// }]);
				});
			}
			if ( $start ) {
				$data = $data->skip( $start );
			}
			if ( $length ) {
				$data = $data->take( $length );
			}
			$orderData= $data->get();
			$data   = $orderData
			->map( function( $q ) {
				if(is_null($q->Importer)){
					$importer_data = $q->Importer()->withTrashed()->first();
					$importer_company = $importer_data ? $importer_data->company_name : "";
				}
				if(is_null($q->Omc)){
					$omc_data = $q->Omc()->withTrashed()->first();
					$omc_company = $omc_data ? $omc_data->company_name.'/' : "";
				}
				if(is_null($q->SupplierOmc)){
					$supplier_omc_data = $q->SupplierOmc()->withTrashed()->first();
					$supplier_company = $supplier_omc_data ? $supplier_omc_data->company_name.'/' : "";
				}

				$omc = $q->Omc ? $q->Omc->company_name.'/' : $omc_company;
				$importer = $q->Importer ? $q->Importer->company_name : $importer_company;
				$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : $supplier_company;
				$isHfo   =  $q->OrderProducts()->join('products', 'order_products.product_id', '=', 'products.id')									->whereIn('products.sku', ['HFO','LPG'])->count();
				$chamberAssign =  ($q->is_chamber_assigned) ? '<a href="'.route('order.assignchamber.index',$q->id).'" title="Edit Compartment Assignment"><button class="btn-sm btn btn-primary"> <i class="fas fa-edit"></i> Edit  Compartment Assignment</button></a>&nbsp' : '<a href="'.route('order.assignchamber.index',$q->id).'" title="Compartment Assignment"><button class="btn-sm btn btn-warning"> <i class="fas fa-tools"></i> Assign Vehicle Compartments</button></a>&nbsp';

				$truckLoad = '<a href="'.route('order.assignchamber.truckLoadingConfirmation',$q->id).'" title="Manage Vehicle Loading Process"><button class="btn-sm btn btn-warning"><i class="fas fa-tools"></i> Manage Vehicle Load</button></a>&nbsp';

				$viewUpliftingForm = '<a href="'.route('order.viewupliftingform',$q->id).'" title="View Uplifting Form"><button class="btn-sm btn btn-success"><i class="fas fa-eye"></i> View Uplifting Form</button></a>&nbsp';

				$approveOrder = '<a href="'.route('order.approveorder',$q->id).'" title="View Uplifting Form" onclick="return confirm('."'Do you want to approve Order ?'".');"><button class="btn-sm btn btn-success"><i class="fas fa-check" ></i> Approve Order </button></a>&nbsp';

				$orderEdit = '<a href="'.route('order.edit',$q->id).'" title="Manage"  ><button class="btn-sm btn btn-default"><i class="fas fa-edit" ></i>Edit Order</button></a>&nbsp';

				$orderApprovedEdit = '<a href="'.route('order.edit',$q->id).'" title="Manage"  onclick="return confirm('."'Editing this Order will reset it to initial state.Do you want to edit this Order ? '".');"><button class="btn-sm btn btn-default"><i class="fas fa-edit" ></i>Edit Order</button></a>&nbsp';

				$viewOriginalOrder = '<a href="'.route('order.view',$q->id).'" title="Manage"  ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i> View Original Order </button></a>&nbsp';

				$orderDelete ='<a href="'. route("order.delete",$q->id) .'" onclick="return confirm('."'Do you want to delete Order ?'".');"  title="Delete Order"><button class="btn-sm btn btn-danger"><i class="fas fa-trash"></i> Delete Order </button></a>&nbsp';

				$upliftingVerification = '<a href="'. route("order.assignchamber.upliftingVerification",$q->id) .'" title="Uplifting Verification" style="color:indianred;" ><button class="btn-sm btn btn-danger"> <i class="fas fa-plus"></i>Uplift & Verify Load</button></a>&nbsp';

				$finalOrder = '<a href="'.route('order.finalOrder',$q->id).'" title="View Final Order"><button class="btn-sm btn btn-info"><i class="fas fa-eye"></i> View Final Order</button></a>&nbsp';

				if($q->enable_customs === 0){
					$enableCustoms = '<a href="'.route('order.markCustomApproved',$q->id).'" title="Mark For Customs"><button class="btn-sm btn btn-warning"><i class="fas fa-check"></i> Publish</button></a>&nbsp';
				} else {
					$enableCustoms = '<a href="'.route('order.markCustomApproved',$q->id).'" title="Mark For Customs"><button class="btn-sm btn btn-warning"><i class="fas fa-times"></i> Unpublish</button></a>&nbsp';
				}
				$action =  '';
				if(!$q->is_chamber_assigned) {
					$action = $action.$chamberAssign;
					if (Auth::user()->isAdmin() || Auth::user()->isSuperAdmin()|| Auth::user()->isSupervisor()){
						$action = $action.$orderEdit;
					}
				}

				if($q->is_chamber_assigned && !$q->is_vehicle_loaded) {
					$action = $action.$chamberAssign.$upliftingVerification.$orderEdit;
				}

				if($q->is_chamber_assigned && $q->is_vehicle_loaded) {
					$action = $action.$viewUpliftingForm;
				}

				if($q->is_chamber_assigned && $q->is_vehicle_loaded && !$q->is_order_verified) {
					$action=$action.$upliftingVerification.$orderEdit;
				}

				if($q->is_chamber_assigned && $q->is_vehicle_loaded && $q->is_order_verified)
				{
					$action= $action.$finalOrder;


					if ( (Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor()) && (!$q->is_complete) ) {
						$action= $action.$approveOrder.$orderEdit;
					}
				}

				$action =  $action.$viewOriginalOrder;

				if (Auth::user()->isSuperAdmin()){
					$action= $action.$orderDelete;
				}
				else if ((Auth::user()->isAdmin() || Auth::user()->isSupervisor())  && (!$q->is_complete)){
					$action= $action.$orderDelete;
				}

				if($q->is_chamber_assigned && $q->is_vehicle_loaded && $q->is_order_verified && $q->is_complete)
				{
					$action= $action.$orderApprovedEdit.$enableCustoms;
				}


				return [
					'id'   => $q->id,
					'order_reference'   => $q->purchase_order,
					'supplier'         => $omc.''.$supplierOmc.''.$importer,
					'vehicle'          => $q->Vehicle ? $q->Vehicle->license_plate_number : '',
					'driver'           => $q->Driver ? $q->Driver->name : '',
					'updated_at'       => date( 'D, M j, Y \a\t g:ia', strtotime( $q->updated_at ) ),
					'action' =>$action

				];
			});
switch($order_column){
	case 0:
	$field ='id';
	break;
	case 1:
	$field ='order_reference';
	break;
	case 2:
	$field ='supplier';
	break;
	case 3:
	$field ='vehicle';
	break;
	case 4:
	$field ='driver';
	break;
	case 5:
	$field ='updated_at';
	break;
	default:
	$field ='id';
}

if($order_direction == 'desc'){
	$data=$data->sortByDesc($field)->values();
} else{
	$data=$data->sortBy($field)->values();
}
			// count all results so we can paginate

$countData =  $this->_orderRepository->getOrdersDataForDatatable($request->type);
if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
	$value = $search['value'];
	$countData->where('id', $value)->orwhere(function($query) use ($value){
		$query->where('purchase_order','like', '%'.$value.'%');

				// $query->with(['Importer' => function ($query1) {
				// 	$query1->where('Importer.company_name','like', '%'.$value.'%');
				// }]);
				// $query->with(['Omc' => function ($query1) {
				// 	$query1->where('Omc.company_name','like', '%'.$value.'%');
				// }]);
				// $query->with(['Vehicle' => function ($query1) {
				// 	$query1->where('Vehicle.license_plate_number','like', '%'.$value.'%');
				// }]);
				// $query->with(['Driver' => function ($query1) {
				// 	$query1->where('Driver.name','like', '%'.$value.'%');

				// }]);

	});
}
$countData = $countData->count();
        // return the needed object to datatables
return response()->json( [ 'draw' => $draw, "recordsTotal" => $data->count(), "recordsFiltered" => ( is_null( $countData ) ) ? ( 0 ) : $countData, "data" => $data ] );
}

		/**
		 * delete
		 * @param  int    $orderID
		 */
		public function delete(int $orderID)
		{
			try {
				DB::beginTransaction();
				$checkifSpecial = $this->_orderProductRepository->checkifSpecial($orderID);
				if($checkifSpecial){
					$orderData = $this->_orderRepository->getOrderDataSpecial($orderID);
				} else {
					$orderData = $this->_orderRepository->getOrderData($orderID);
				}
				if($orderData->is_complete){
					if ($orderData->OrderProducts->first()->product_id == 3) {
						$this->_importerProductStockRepository->editStockByImporterForOrder( $orderData->importer_id, 3, $orderData->Truck->net_weight, $orderData->id );
					} else if($orderData->OrderProducts->first()->product_id == 6){
						$this->_omcProductStockRepository->editLpgOrderInventoryOnDelete( $orderData, $orderData->Truck->net_weight );
					} else{
						$this->_omcProductStockRepository->editOrderInventoryOnDelete($orderData, $orderData->OrderProducts);
					}
				}
				$orderProducts = $this->_orderProductRepository->deleteOrderProducts($orderID);
				$assignedChambers = $this->_chamberAssignmentRepository->deleteChambersAssigned($orderID);
				$orderMeter = $this->_orderProductMeterRepository->deleteOrderProductMeter($orderID);
				$order=$this->_orderRepository->deleteOrder($orderID);
				$truck = $this->_truckDetailRepository->deleteTruckDetail($orderID);
				$logs = $this->_logsRepository->deleteOrderLogs($orderData);
				DB::commit();
				return redirect()->route('order.index')->withFlashSuccess(__('alerts.backend.order.deleted'));
			}catch (\Exception $e) {

				DB::rollback();
				return redirect()->route('order.index')->withFlashSuccess(__('alerts.backend.order.deletederror'));
			}
		}

		/**
		 * update_products
		 * @param  OrderUpdateProductRequest $request
		 * @return json
		 */
		public function update_products(OrderUpdateProductRequest $request)
		{
			try {
				DB::beginTransaction();
				$checkCapacity = $this->checkVehicleOrderCapacity($request->all());
				if($checkCapacity['status']){
					$this->_orderProductRepository->updateOrderProduct($request->only('products'), $request->order_id);
					DB::commit();
					$response['success'] = true;
				} else {
					$response['success'] = false;
					$response['message'] = $checkCapacity['message'];
				}
				return response()->json($response);
			}	catch (\Exception $e)	{
				DB::rollback();
				$response['success'] = false;
				$response['message'] = __('alerts.backend.order.products.updatedError');
				return response()->json($response);
			}
		}

		/**
		 * checkVehicleOrderCapacity
		 * @param  array $data
		 * @return boolean
		 */
		public function checkVehicleOrderCapacity($data)
		{

			$response['status'] = true;
			$vehicleChambers = $this->_vehicleRepository->getChamber($data['vehicle_id']);
			$vehicleCapacity = 0;
			foreach($vehicleChambers->vehicle_chamber as $chambers)
			{
				$vehicleCapacity += $chambers->capacity;
			}
			$orderedProducts = [];
			$chamberProducts = [];
			$totalQuantityOrdered = 0;
			$totalChamberQuantityAssigned = 0;
			//calculate order products
			if(isset($data['products']['count'])){
				unset($data['products']['count']);
				foreach($data['products'] as $key => $capacity){
					$totalQuantityOrdered+= $capacity;
					$orderedProducts[$key] = $capacity;
				}
			} else {
				foreach($data['products'] as $key => $product)
				{
					$previous = isset($orderedProducts[$product['productID']])? $orderedProducts[$product['productID']] : 0;
					$totalQuantityOrdered+= $product['quantity'];
					$orderedProducts[$key] = $previous + $product['quantity'];
				}
			}

			//calculate chambers
			foreach($data['chambers'] as $key => $product)
			{
				$previous = isset($chamberProducts[$product['productID']])? $chamberProducts[$product['productID']] : 0;
				$totalChamberQuantityAssigned+= $product['quantity'];
				$chamberProducts[$product['productID']] = $previous + $product['quantity'];

				//check chamber capacity
				foreach($vehicleChambers->vehicle_chamber as $chambers){
					if($chambers->id == $product['chamberID']){
						if( $chambers->capacity < $product['quantity']){
							$response['status'] = false;
							$response['message'] = 'Quantity assigned for compartment '.$chambers->chamber_number.' is more then compartment capacity.';
							return $response;
						}
					}

				}
			}

			//make sure product quantities are there
			if ($totalQuantityOrdered == 0) {
				$response['status'] = false;
				$response['message'] = 'Please provide quantity for Order , it can not be 0';
				return $response;
			}

			if (isset($data['supplier_omc_id'])) {
				if ($data['supplier_omc_id'] == $data['omc_id']) {
					$response['status'] = false;
					$response['message'] = 'Please select another Supplier , Supplier and Omc can\'t be same';
					return $response;
				}
			}

			//to check if chambers are assigned
			if ($totalChamberQuantityAssigned == 0) {
				$response['status'] = false;
				$response['message'] = 'Please assign Quantities to Chambers';
				return $response;
			}

			//check if quantities in chambers are less than or equal to ordered quantity
			if ($totalChamberQuantityAssigned > $totalQuantityOrdered) {
				$response['status'] = false;
				$response['message'] = 'You can not assign more than ordered quantity in Chambers';
				return $response;
			}

			// check vehicle chambers capacity for ordered quantity
			if($vehicleCapacity >= $totalQuantityOrdered) {
				//check for omc inventory balance

				$omc_id = $data['omc_id'];
				if (isset($data['supplier_omc_id'])) {
					$omcInventory =  $this->_omcProductStockRepository->getProductListImporterBasedSupplierOmc($omc_id, $data['importer_id'], $data['supplier_omc_id']);
				} else {
					$omcInventory =  $this->_omcProductStockRepository->getProductListImporterBased($omc_id, $data['importer_id']);
				}

				if(count($omcInventory)) {
					foreach ($omcInventory as $inventory) {
						if (isset($orderedProducts[$inventory->product_id]) && (int) $inventory->outstanding_balance < $orderedProducts[$inventory->product_id] ) {

							if (isset($data['supplier_omc_id'])) {
								$response['status'] = false;
								$response['message'] = 'Supplier does not have enough borrowing Quantity, please enter lesser quantity';
								return $response;
							}

							if (isset($data['importer_id'])) {
								//check if importer have inventory
								$borrowedQuantity = $orderedProducts[$inventory->product_id] - $inventory->outstanding_balance;
								$importerInventory =  $this->_importerProductStockRepository->getProductInventory($data['importer_id'], $inventory->product_id);
								if ($importerInventory && $importerInventory->outstanding_balance > $borrowedQuantity) {
									$response['status'] =  true;
									$response['borrow'] = true;
									return $response;
								} else {
									$response['status'] = false;
									$response['message'] = 'Supplier does not have enough borrowing Quantity, please enter lesser quantity';
									return $response;
								}
							}
						}
					}
				} else {
					$response['status'] = false;
					$response['message'] = 'Product not found in Omc Inventory';
					return $response;
				}

			} else {
				$response['status'] = false;
				$response['message'] = __('alerts.backend.order.products.capacityError');
				return $response;
			}
			return $response;

		}

		/**
		 * getVehicleChambers
		 * @param  int $vehicle_id
		 * @return view
		 */
		public function getOmcProductList(int $omc_id)
		{
			$omcProducts = $this->_omcProductStockRepository->getProductList($omc_id);
			$status = false;
			$data = [];
			if (count($omcProducts)) {
				foreach ($omcProducts as $key => $product) {
					$data['importers'][$product->importer_id]['product'][$product->product_id] = $product->omcProducts->name.'('.$product->omcProducts->sku.')';
					$data['importers'][$product->importer_id]['inventory'][$product->product_id] = $product->outstanding_balance;

					$data['importers'][$product->importer_id]['importer_name'] = $product->importer!=null?$product->importer->company_name:"";
				}
				$status = true;
			}

			$response = [ 'status' => $status , 'data' => $data ];
			return json_encode($response);
		}

		/**
		 * getVehicleChambers
		 * @param  int $vehicle_id
		 * @return view
		 */
		public function getSpecifiedOmcProductList(Request $request){
			$omcProducts = $this->_omcProductStockRepository->getProductListAllOmcBased($request->omc_id);
			$responseData =$this->processOmcProductList($omcProducts,$request->omc_id);
			return json_encode($responseData);
		}

		public function processOmcProductList($omcProducts, $omcID){
			$status = false;
			$data = [];
			$sortedData = [];
			if (count($omcProducts)) {
				foreach ($omcProducts as $key => $product) {
					if (isset($omcID)  && ($omcID = $product->omc_id))
					{
						$omc=$product->SupplierOmc != null?$product->SupplierOmc->company_name:"";
						$importer = $product->importer != null?$product->importer->company_name:"";
						$data[$product->supplier_omc_id][$product->importer_id]['omc_id'] = $product->supplier_omc_id;
						$data[$product->supplier_omc_id][$product->importer_id]['importer_id'] = $product->importer_id;
						$data[$product->supplier_omc_id][$product->importer_id]['name'] = $omc.'/'.$importer;
						$data[$product->supplier_omc_id][$product->importer_id]['products'][$product->product_id]['balance'] = $product->outstanding_balance;
						$data[$product->supplier_omc_id][$product->importer_id]['products'][$product->product_id]['product_name'] = $product->omcProducts->name.'('.$product->omcProducts->sku.')';
						$data[$product->supplier_omc_id][$product->importer_id]['name'] = trim($data[$product->supplier_omc_id][$product->importer_id]['name'],"/");

					}
				}

				foreach ($data as $supplier_omc_id => $importers) {
					foreach ($importers as $importer_id => $importer) {
						$sortedData[$supplier_omc_id.''.$importer_id] = $importer;
					}
				}
				$status = true;
			}
			return [ 'status' => $status, 'data' => $sortedData, 'omc_id' => count($sortedData) ? array_keys($sortedData)[0] : ''];
		}

		public function getSpecifiedOmcProductListOmcBased(Request $request){
			$omcProducts = $this->_omcProductStockRepository->getProductListAll();
			$status = false;
			$data = [];
			$sortedData = [];
			if (count($omcProducts)) {
				foreach ($omcProducts as $key => $product) {
					$omc=$product->omc != null?$product->omc->company_name:"";
					$importer = $product->importer != null?$product->importer->company_name:"";
					if (isset($request->omc_id)  && ($request->omc_id != $product->omc_id))
					{
						$data[$product->omc_id][$product->importer_id]['omc_id'] = $product->omc_id;
						$data[$product->omc_id][$product->importer_id]['importer_id'] = $product->importer_id;
						$data[$product->omc_id][$product->importer_id]['name'] = $omc.'/'.$importer;
						$data[$product->omc_id][$product->importer_id]['products'][$product->product_id]['balance'] = $product->outstanding_balance;
						$data[$product->omc_id][$product->importer_id]['products'][$product->product_id]['product_name'] = $product->omcProducts->name.'('.$product->omcProducts->sku.')';
					}
				}

				foreach ($data as $omc_id => $importers) {
					foreach ($importers as $importer_id => $importer) {
						$sortedData[$omc_id.''.$importer_id] = $importer;
					}
				}
				$status = true;
			}
			$response = [ 'status' => $status, 'data' => $sortedData, 'omc_id' => count($sortedData) ? array_keys($sortedData)[0] : ''];
			return json_encode($response);
		}

		/**
		 * getVehicleChambers
		 * @param  int $vehicle_id
		 * @return view
		 */
		public function getVehicleChambers(int $vehicle_id, Request $request){
			$products = [];

			if (isset($request->products)) {
				$product = explode(',', $request->products);
				$products =  $this->_productsRepository->getSelectedList($product);
			}
			$vehicle = $this->_vehicleRepository->getChamber($vehicle_id);
			$orderData=null;
			$loaders =null;
			if (isset($request->order)) {
				$checkifSpecial = $this->_orderProductRepository->checkifSpecial($request->order);
				if ($checkifSpecial) {
					return response()->json(array('success' => true, 'html'=>""));
				} else {
					$orderData=$this->_orderRepository->getOrderData((int)$request->order);
					$loaders = $this->loaderRepository->getList();
					$returnHTML = view('backend.order.includes.editchambers')->withVehicle($vehicle)->withProducts($products)->withOrder($orderData)->withLoader($loaders)->render();
				}
			} else {
				$returnHTML = view('backend.order.includes.chambers')->withVehicle($vehicle)->withProducts($products)->render();
			}


			return response()->json(array('success' => true, 'html'=>$returnHTML));

		}

		/**
		 * finalOrder
		 * @param  int $orderID
		 * @return view
		 */
		public function finalOrder($orderID)
		{
			$checkifSpecial = $this->_orderProductRepository->checkifSpecial($orderID);
			if ($checkifSpecial) {
				$orderDetail = $this->_orderRepository->getOrderDataSpecial($orderID);
			} else {
				$orderDetail = $this->_orderRepository->getOrderData($orderID);
			}
            $generalManager = $this->managementRepository->getByPosition('General Manager');
            $operationsManager = $this->managementRepository->getByPosition('Operations Manager');

			return view('backend.order.finalOrder')->withGeneralmanager($generalManager->name)->withOperationsmanager($operationsManager->name)->withOrder($orderDetail)->withisHFO($checkifSpecial);
		}

		/**
		 * finalOrder
		 * @param  int $orderID
		 * @return view
		 */
		public function approveOrder($orderID){
			$checkifSpecial = $this->_orderProductRepository->checkifSpecial($orderID);
			if($checkifSpecial){
				$order = $this->_orderRepository->getOrderDataSpecial($orderID);
				if (!$order->is_complete) {
					//update inventory
					if ($order->OrderProducts->first()->product_id == 3) {
						$this->updateImporterStock( $order->importer_id, $order->OrderProducts->first()->product_id, $order->Truck->net_weight, $orderID );
					} else {
						$this->_omcProductStockRepository->updateLpgOrderInventory( $order, $order->Truck->net_weight);
					}
					$this->_orderRepository->approveOrder($orderID);
				}
			} else{
				$order = $this->_orderRepository->getOrderData($orderID);
				if (!$order->is_complete) {
				//update importer inventory
					if (count($order->Chambers)) {
					// $this->_importerProductStockRepository->updateOrderInventory($order->importer_id, $order->Chambers);
						$this->_omcProductStockRepository->updateOrderInventory($order, $order->OrderProducts);
						$this->_orderRepository->approveOrder($orderID);
					}
				}
			}

			return redirect()->route('order.finalOrder',[$order->id,'isRequestFromApproved'=>1]);
		}

		public function orderExcel(Request $request)
		{

			$products = $this->_productsRepository->getAll();
			$product = $products->map(function ($item, $key) {
				return $item;
			});
			return Excel::download(new OrdersExport($product->all(), $request->all()), 'orders.xlsx');
		}


		public function orderReport($year= null)
		{

			$suppliers = $this->_orderRepository->getSuppliers();
			$supplier=array();
			$supplier['0']="All";
			foreach($suppliers as $q){
				$omc = $q->Omc ? $q->Omc->company_name.'/' : '';
				$omcID = $q->Omc ? $q->omc_id.'_' : '0_';
				$importer = $q->Importer ? $q->Importer->company_name : '';
				$importerID = $q->Importer ? $q->importer_id : '0';
				$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : '';
				$supplierID = $q->SupplierOmc ? $q->supplier_omc_id.'_' : '0_';
				$importeromcID = $omcID.''.$supplierID.''.$importerID;
				$importeromc = $omc.''.$supplierOmc.''.$importer;
				$supplier[$importeromcID]=$importeromc;
			}

			return view('backend.order.orderReport')->withSupplier($supplier);
		}

		/**
		 * getYearData
		 * @param  Request $request
		 * @return Response
		 */
		public function getYearData(Request $request)
		{
			$products = $this->_productsRepository->getAll();
			$order = $this->_orderRepository->getOrderReportData($request->start, $request->end, $request->supplier);
			$reorderedProducts=[];
			$orderData = $this->processOrderData($order, $products);
			return view('backend.order.orderData')->withOrder($orderData)->withProduct($products);
		}

		/**
		 * processOrderData
		 * @param  $order
		 * @param  $products
		 * @return Response
		 */
		public function processOrderData($order, $products)
		{
			$productData=[];
			$productData['Total']=[];
			$orderdata=$order->map( function( $q  ) use($products, &$productData)  {
				$omc = $q->Omc ? $q->Omc->company_name.'/' : '';
				$importer = $q->Importer ? $q->Importer->company_name : '';
				$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : '';
				$importeromc = $omc.''.$supplierOmc.''.$importer;
				if(!array_key_exists($importeromc,$productData)){
					$productData[$importeromc]=[];
				}
				foreach($products as $product){
					if(!array_key_exists($product->id,$productData[$importeromc])){
						$productData[$importeromc][$product->id] = 0;

					}
					if(!array_key_exists($product->id,$productData['Total'])){
						$productData['Total'][$product->id] = 0;
					}
					foreach($q->OrderProducts as $productLoaded){
						if($productLoaded->product_id == $product->id){
							if(!is_null($q->Truck)){

								$productData[$importeromc][$product->id] = $productData[$importeromc][$product->id]+$q->Truck->net_weight;
								$productData['Total'][$product->id] =$productData['Total'][$product->id]+$q->Truck->net_weight;
							} else {
								$productData[$importeromc][$product->id] = $productData[$importeromc][$product->id]+$productLoaded->OrderProductMeter->quantity;
								$productData['Total'][$product->id] = $productData['Total'][$product->id]+$productLoaded->OrderProductMeter->quantity;
							}
						}
					}
				}
				return $productData;
			});
			$value = $productData['Total'];
			unset($productData['Total']);
			$productData['Total'] = $value;

			return $productData;
		}

		/**
		 * getOrderTransaction
		 * @param  Request $request
		 * @return json Respomse
		 */
		public function getOrderTransaction(Request $request)
		{
			$products = $this->_productsRepository->getList();
			$order = $this->_orderRepository->getOrderReportData($request->start, $request->end);
			$orderData = $this->processOrderTransaction($order, $products);
			return response()->json($orderData);
		}

		/**
		 * processOrderTransaction
		 * @param  order
		 * @param  $products
		 * @return array Response
		 */
		public function processOrderTransaction($order, $products)
		{
			$productData=array();
			$labelData=array();
			foreach($products as $key=>$product)
			{
				$productData[$product]=[];
				$orderdata=$order->map( function( $q  ) use($product, &$productData, &$key, &$labelData)  {
					$omc = $q->Omc ? $q->Omc->company_name.'/' : '';
					$importer = $q->Importer ? $q->Importer->company_name : '';
					$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : '';
					$omcImporter = $omc.''.$supplierOmc.''.$importer;
					if (!in_array($omcImporter, $labelData)){
						array_push($labelData,$omcImporter);
					}
					if (!array_key_exists($omcImporter, $productData[$product])){
						$productData[$product][$omcImporter]=0;
					}
					foreach($q->OrderProducts as $productLoaded){
						if($productLoaded->product_id == $key){
							if(!is_null($q->Truck)){
								$productData[$product][$omcImporter] = $productData[$product][$omcImporter]+$q->Truck->net_weight;
							} else {

								$productData[$product][$omcImporter] = $productData[$product][$omcImporter]+$productLoaded->OrderProductMeter->quantity;

							}
						}
					}
					return $productData;
				});
			}
			$responsedata['labels']= $labelData;
			$responsedata['chartData']= $productData;
			return $responsedata;
		}

		public function dailyReport($year = null)
		{
			return view('backend.order.dailyReport');
		}

		public function getDailyData(Request $request)
		{
			$order = $this->_orderRepository->getDailyReportData($request->start, $request->end);
			$orderData = $this->processDailyOrderData($order);
			return view('backend.order.dailyData')->withOrder($orderData);
		}

		public function processDailyOrderData($order)
		{
			$hfo =[];
			$lpg=[];
			foreach( $order as $orderdata)
			{
				if($orderdata->OrderProducts->first()->Product->sku == 'HFO')
				{
					$hfo[$orderdata->id]['date'] = $orderdata->created_at;
					$hfo[$orderdata->id]['driver'] = $orderdata->Driver->name;
					$hfo[$orderdata->id]['vehicle'] = $orderdata->Vehicle->license_plate_number;
					$hfo[$orderdata->id]['empty_weight'] = $orderdata->Truck->weight_before_loading;
					$hfo[$orderdata->id]['full_weight'] = $orderdata->Truck->weight_after_loading;
					$hfo[$orderdata->id]['quantity'] = $orderdata->Truck->net_weight;
					$hfo[$orderdata->id]['destination'] = $orderdata->Truck->location;
				} else if($orderdata->OrderProducts->first()->Product->sku == 'LPG') {
					$lpg[$orderdata->id]['date'] = $orderdata->created_at;
					$lpg[$orderdata->id]['driver'] = $orderdata->Driver->name;
					$lpg[$orderdata->id]['vehicle'] = $orderdata->Vehicle->license_plate_number;
					$lpg[$orderdata->id]['empty_weight'] = $orderdata->Truck->weight_before_loading;
					$lpg[$orderdata->id]['full_weight'] = $orderdata->Truck->weight_after_loading;
					$lpg[$orderdata->id]['quantity'] = $orderdata->Truck->net_weight;
					$lpg[$orderdata->id]['destination'] = $orderdata->Truck->location;
				}
			}
			return ['HF0'=>$hfo,'LPG'=>$lpg];
		}

		public function dailyExcel(Request $request)
		{
			return Excel::download(new DailyHfoLpgData( $request->all()), 'hfo_lpg.xlsx');
		}

		public function inventoryReport()
		{
			$omc = $this->_omcRepository->getList();
			return view('backend.order.inventoryReport')->withOmc($omc);
		}

		public function getInventoryData(Request $request)
		{
			$productBalance = $this->_omcProductStockRepository->getBalance($request->all());
			$transaction = $this->_omcProductStockRepository->getTransactionData($request->all());
			$transactionData = $this->processTransactionData($transaction,$productBalance);
			return view('backend.order.inventoryData')->withTransaction($transactionData);
		}

		public function processTransactionData($transaction,$balanceCarryFw)
		{
			$pms =[];
			$ago =[];
			$jet =[];
			if (array_key_exists("ADO",$balanceCarryFw)){
				$ago[$balanceCarryFw['ADO']->id]['date'] = "";
				$ago[$balanceCarryFw['ADO']->id]['bought'] = "BALANCE(CF)" ;
				$ago[$balanceCarryFw['ADO']->id]['orderNo'] = $balanceCarryFw['ADO']->order_id;
				if($balanceCarryFw['ADO']->transaction_type == "add"){

					$ago[$balanceCarryFw['ADO']->id]['delivered'] = 0;
				} else {
					$ago[$balanceCarryFw['ADO']->id]['delivered'] = $balanceCarryFw['ADO']->quantity;
				}
				$ago[$balanceCarryFw['ADO']->id]['balance'] = $balanceCarryFw['ADO']->balance_before_transaction;
			} else {
				$ago[0]['date'] = "";
				$ago[0]['bought'] = "BALANCE(CF)" ;
				$ago[0]['orderNo'] = 0;
				$ago[0]['delivered'] = 0;
				$ago[0]['balance'] = 0;

			}
			if (array_key_exists("PMS",$balanceCarryFw)){
				$pms[$balanceCarryFw['PMS']->id]['date'] = "";
				$pms[$balanceCarryFw['PMS']->id]['bought'] = "BALANCE(CF)" ;
				$pms[$balanceCarryFw['PMS']->id]['orderNo'] = $balanceCarryFw['PMS']->order_id;
				if($balanceCarryFw['PMS']->transaction_type == "add"){

					$pms[$balanceCarryFw['PMS']->id]['delivered'] = 0;
				} else {
					$pms[$balanceCarryFw['PMS']->id]['delivered'] = $balanceCarryFw['PMS']->quantity;
				}
				$pms[$balanceCarryFw['PMS']->id]['balance'] = $balanceCarryFw['PMS']->balance_before_transaction;
			} else {
				$pms[0]['date'] = "";
				$pms[0]['bought'] = "BALANCE(CF)" ;
				$pms[0]['orderNo'] = 0;
				$pms[0]['delivered'] = 0;
				$pms[0]['balance'] = 0;
			}
			if (array_key_exists("JET",$balanceCarryFw)){
				$jet[$balanceCarryFw['JET']->id]['date'] = "";
				$jet[$balanceCarryFw['JET']->id]['bought'] = "BALANCE(CF)" ;
				$jet[$balanceCarryFw['JET']->id]['orderNo'] = $balanceCarryFw['JET']->order_id;
				if($balanceCarryFw['JET']->transaction_type == "add"){

					$jet[$balanceCarryFw['JET']->id]['delivered'] = 0;
				} else {
					$jet[$balanceCarryFw['JET']->id]['delivered'] = $balanceCarryFw['JET']->quantity;
				}
				$jet[$balanceCarryFw['JET']->id]['balance'] = $balanceCarryFw['JET']->balance_before_transaction;
			} else {
				$jet[0]['date'] = "";
				$jet[0]['bought'] = "BALANCE(CF)" ;
				$jet[0]['orderNo'] = 0;
				$jet[0]['delivered'] = 0;
				$jet[0]['balance'] = 0;
			}
			if(count($transaction)!=0){
				foreach( $transaction as $transactiondata)
				{
					if($transactiondata->Product->sku == 'ADO')
					{
						$ago[$transactiondata->id]['date'] = $transactiondata->created_at;
						if($transactiondata->transaction_type == "add"){
							$ago[$transactiondata->id]['bought'] = $transactiondata->quantity;
						} else {
							$ago[$transactiondata->id]['bought'] = 0;
						}

						$ago[$transactiondata->id]['orderNo'] = $transactiondata->order_id;
						if($transactiondata->transaction_type == "add"){

							$ago[$transactiondata->id]['delivered'] = 0;
						} else {
							$ago[$transactiondata->id]['delivered'] = $transactiondata->quantity;
						}

						$ago[$transactiondata->id]['balance'] = $transactiondata->balance_before_transaction;

					} else if($transactiondata->Product->sku ==  'PMS') {
						$pms[$transactiondata->id]['date'] = $transactiondata->created_at;
						if($transactiondata->transaction_type == "add"){
							$pms[$transactiondata->id]['bought'] = $transactiondata->quantity;
						} else {
							$pms[$transactiondata->id]['bought'] = 0;
						}

						$pms[$transactiondata->id]['orderNo'] = $transactiondata->order_id;
						if($transactiondata->transaction_type == "add"){

							$pms[$transactiondata->id]['delivered'] = 0;
						} else {
							$pms[$transactiondata->id]['delivered'] = $transactiondata->quantity;
						}

						$pms[$transactiondata->id]['balance'] = $transactiondata->balance_before_transaction;

					} else if($transactiondata->Product->sku ==  'KEROSENE' || $transactiondata->Product->sku ==  'JET'){
						$jet[$transactiondata->id]['date'] = $transactiondata->created_at;
						if($transactiondata->transaction_type == "add"){
							$jet[$transactiondata->id]['bought'] = $transactiondata->quantity;
						} else {
							$jet[$transactiondata->id]['bought'] = 0;
						}

						$jet[$transactiondata->id]['orderNo'] = $transactiondata->order_id;
						if($transactiondata->transaction_type == "add"){

							$jet[$transactiondata->id]['delivered'] = 0;
						} else {
							$jet[$transactiondata->id]['delivered'] = $transactiondata->quantity;
						}
						$jet[$transactiondata->id]['balance'] = $transactiondata->balance_before_transaction;
					}
				}
			}

			return ['AGO'=>$ago,'PMS'=>$pms,'JET'=>$jet];
		}

		public function inventoryExcel(Request $request)
		{
			$omc = $this->_omcRepository->getDetail($request->omc);
			$productBalance = $this->_omcProductStockRepository->getBalance($request->all());
			$transaction = $this->_omcProductStockRepository->getTransactionData($request->all());
			$transactionData = $this->processTransactionData($transaction,$productBalance);
			return Excel::download(new InventoryExcel($transactionData, $omc->company_name), 'omc_inventory.xlsx');
		}

		public function loaderList()
		{
			return view('backend.order.loader.loaderList');
		}

		public function getLoaderData(Request $request)
		{
			//Fetching all inputs from request
			$draw   = $request->input( 'draw' );
			$order  = $request->input( 'order' );
			$start  = $request->input( 'start', 0 );
			$length = $request->input( 'length' );
			$search = $request->input( 'search' );
			$secho  = $request->input( 'sEcho' );


			$order_column    = $order[ 0 ][ 'column' ];
			$order_direction = $order[ 0 ][ 'dir' ];

			if ( empty( $order_column ) ) {
				$order_column = '0';
			}
			if ( empty( $order_direction ) ) {
				$order_direction = 'asc';
			}

			$data = $this->_orderRepository->getAllLoadersForDatatable();

			if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
				$value = $search['value'];

				$data = $data->where(function( $query ) use($value) {
					$query->where('name','like', '%'.$value.'%');
				});
			}

			$data   = $data->get();
			$loaderResponse= array();
			foreach($data as $loader){
				if(!is_null($loader->loader1) && !is_null($loader->loader2)){
					array_push($loaderResponse,$loader->loader1);
					array_push($loaderResponse,$loader->loader2);
				} else if(is_null($loader->loader1) && !is_null($loader->loader2)){
					array_push($loaderResponse,$loader->loader2);
				} else if(!is_null($loader->loader1) && is_null($loader->loader2)){
					array_push($loaderResponse,$loader->loader1);
				}
			}
			$data=collect($loaderResponse)->map( function( $q ) {
				return [
					'name'                      => $q,
				];
			});
			return Datatables::of( $data )
			->make(true);
		}

		public function orderTypeExcel(Request $request)
		{
			$data =  $this->_orderRepository->getOrdersDataForDatatable($request->type)->get();
			$filename=$request->type."_order";
			return Excel::download(new OrderTypeExport($data, $filename), $filename.'.xlsx');
		}

		public function customOrders()
		{
			return view('backend.order.customsindex');
		}

		public function getCustomsOrderData(Request $request)
		{
			$draw   = $request->input( 'draw' );
			$order  = $request->input( 'order' );
			$start  = $request->input( 'start', 0 );
			$length = $request->input( 'length' );
			$search = $request->input( 'search' );
			$secho  = $request->input( 'sEcho' );

			$order_column    = $order[ 0 ][ 'column' ];
			$order_direction = $order[ 0 ][ 'dir' ];

			if ( empty( $order_column ) ) {
				$order_column = '0';
			}
			if ( empty( $order_direction ) ) {
				$order_direction = 'asc';
			}

			$data =  $this->_orderRepository->getCustomsOrdersDataForDatatable();

			if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
				$value = $search['value'];
				$data = $data->orWhere(function( $query ) use($value) {
					$query->with(['Importer' => function ($query1) {
						$query1->where('Importer.company_name','like', '%'.$value.'%');
					}]);
					$query->with(['SupplierOmc' => function ($query1) {
						$query1->where('Omc.company_name','like', '%'.$value.'%');
					}]);
					$query->with(['Omc' => function ($query1) {
						$query1->where('Omc.company_name','like', '%'.$value.'%');
					}]);
					$query->with(['Vehicle' => function ($query1) {
						$query1->where('Vehicle.license_plate_number','like', '%'.$value.'%');
					}]);
					$query->with(['Driver' => function ($query1) {
						$query1->where('Driver.name','like', '%'.$value.'%');

					}]);
				});
			}
			$orderData= $data->get();
			$data   = $orderData
			->map( function( $q ) {
				if(is_null($q->Importer)){
					$importer_data = $q->Importer()->withTrashed()->first();
					$importer_company = $importer_data ? $importer_data->company_name : "";
				}
				if(is_null($q->Omc)){
					$omc_data = $q->Omc()->withTrashed()->first();
					$omc_company = $omc_data ? $omc_data->company_name.'/' : "";
				}
				if(is_null($q->SupplierOmc)){
					$supplier_omc_data = $q->SupplierOmc()->withTrashed()->first();
					$supplier_company = $supplier_omc_data ? $supplier_omc_data->company_name.'/' : "";
				}

				$omc = $q->Omc ? $q->Omc->company_name.'/' : $omc_company;
				$importer = $q->Importer ? $q->Importer->company_name : $importer_company;
				$supplierOmc =  $q->SupplierOmc ? $q->SupplierOmc->company_name.'/' : $supplier_company;
				return [
					'order_reference'   => $q->purchase_order,
					'supplier'         => $omc.''.$supplierOmc.''.$importer,
					'vehicle'          => $q->Vehicle ? $q->Vehicle->license_plate_number : '',
					'driver'           => $q->Driver ? $q->Driver->name : '',
					'updated_at'       => date( 'D, M j, Y \a\t g:ia', strtotime( $q->updated_at ) ),
					'order'			   => $q
				];
			});

			return Datatables::of( $data )->addColumn('action', function ($data) {
				$order = $data['order'];
				$action =  '';
				$isHfo   =  $order->OrderProducts()->join('products', 'order_products.product_id', '=', 'products.id')									->whereIn('products.sku', ['HFO','LPG'])->count();


				$viewUpliftingForm = '<a href="'.route('order.viewupliftingform',$order->id).'" title="View Uplifting Form"><button class="btn-sm btn btn-success"><i class="fas fa-eye"></i> View Uplifting Form</button></a>&nbsp';

				$viewOriginalOrder = '<a href="'.route('order.view',$order->id).'" title="Manage" class="col-md-4 col-sm-4 col-lg-4" ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i> View Original Order </button></a>&nbsp';

				$finalOrder = '<a href="'.route('order.finalOrder',$order->id).'" title="View Final Order"><button class="btn-sm btn btn-info"><i class="fas fa-eye"></i> View Final Order</button></a>&nbsp';

				if($order->is_chamber_assigned && $order->is_vehicle_loaded) {
					$action = $action.$viewUpliftingForm;
				}

				if($order->is_chamber_assigned && $order->is_vehicle_loaded && $order->is_order_verified)
				{
					$action= $action.$finalOrder;

				}

				$action =  $action.$viewOriginalOrder;


				return $action;
			})->make(true);
		}

		public function markCustomApproved(int $orderID){
			$this->_orderRepository->updateCustomApproved($orderID);
			return redirect()->back();
		}

		public function getMeterForProduct(Request $request)
		{
			$products = $this->_productsRepository->getOne($request->product);
			$meterId = Config::get('meter-id');
			return  json_encode($meterId[$products->sku]);
		}



	}
