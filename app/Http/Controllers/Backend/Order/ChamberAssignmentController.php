<?php

namespace App\Http\Controllers\Backend\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Orders\ChamberAssignmentRepository;
use App\Http\Requests\Backend\Order\ChamberAssignmentRequest;
use App\Http\Requests\Backend\Order\VehicleUpliftmentRequest;
use App\Repositories\Backend\Orders\TruckDetailRepository;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Orders\OrderRepository;
use App\Repositories\Backend\Orders\OrderProductRepository;
use App\Repositories\Backend\VehicleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Orders\OrderProductMeterRepository;
use App\Repositories\Backend\LoaderRepository;
use App\Repositories\Backend\ManagementRepository;
use Config;

class ChamberAssignmentController extends Controller
{

    protected $_chamberAssignmentRepository;

    protected $_orderRepository;

    protected $_truckDetailRepository;

    public function __construct( ChamberAssignmentRepository $_chamberAssignmentRepository, OrderRepository $_orderRepository,TruckDetailRepository $_truckDetailRepository, UserRepository $_userRepository, OrderProductMeterRepository $_orderProductMeterRepository,LoaderRepository $loaderRepository,managementRepository $managementRepository)
    {
        $this->_chamberAssignmentRepository = $_chamberAssignmentRepository;
        $this->_orderRepository = $_orderRepository;
        $this->_truckDetailRepository = $_truckDetailRepository;
        $this->_userRepository = $_userRepository;
        $this->_orderProductMeterRepository = $_orderProductMeterRepository;
        $this->loaderRepository = $loaderRepository;
        $this->managementRepository = $managementRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChamberAssignmentRequest $request)
    {
        try {
            DB::beginTransaction();
            $orderData=  $this->_orderRepository->getById($request->order_id);
            $validateInventory = $this->checkInventory($request->order_id, $request->products, $orderData);
            if (!$validateInventory['status']) {
                return response()->json($validateInventory);
            }
            $this->_chamberAssignmentRepository->create($request->except('_token'));
            if(isset($request->loader1)){
                $this->_orderRepository->update($orderData, $request->only('loader1','loader2','order_id','seen_by'));
            }
            if(isset($request->truck)) {
                $this->_truckDetailRepository->create($request->only('order_id','truck'));
                $response['message'] = __('alerts.backend.order.Loading.created');
            } else {
                $response['message'] = __('alerts.backend.order.chamberAssignment.created');
            }

            DB::commit();
            $response['success'] = true;
            $request->session()->flash('flash_success',$response['message']);
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response['success'] = false;
            $response['message'] = __('alerts.backend.order.chamberAssignment.createderror');
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderDetail = $this->_orderRepository->getOrderDataSpecial($id);
        $products = array();
        $isHFO = false;
        foreach ($orderDetail->OrderProducts as $product) {
            $products[$product->product_id] = $product->Product->name;
            if ($product->Product->sku == 'HFO' || $product->Product->sku == 'LPG') {
                $isHFO = true;
            }
        }
        $generalManager = $this->managementRepository->getByPosition('General Manager');
        $operationsManager = $this->managementRepository->getByPosition('Operations Manager');

        $inventoryBalance =  $this->_orderRepository->getOutStandingBalance($orderDetail);

        return view('backend.order.uplifting.viewUpliftingForm')->withGeneralmanager($generalManager->name)->withOperationsmanager($operationsManager->name)->withOrder($orderDetail)->withProducts($products)->withIsHFO($isHFO)->withBalances($inventoryBalance);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * assignChamber
    * @param  int    $orderID
    */
    public function assignChamber(int $orderID)
    {

        $orderDetail = $this->_orderRepository->getOrderData($orderID);
        $products = array();
        foreach ($orderDetail->OrderProducts as $product) {
            $products[$product->product_id] = $product->Product->name;
        }
        $inventoryBalance =  $this->_orderRepository->getOutStandingBalance($orderDetail);

        return view('backend.order.uplifting.assignChamber')->withOrder($orderDetail)->withProducts($products)->withBalances($inventoryBalance);
    }

    /**
    * truckLoadingConfirmation
    * @param  int    $orderID
    */
    public function truckLoadingConfirmation(int $orderID)
    {
        $orderDetail = $this->_orderRepository->getOrderData($orderID);
        $products = array();
        $isHFO = false;
        foreach ($orderDetail->OrderProducts as $product) {
            $products[$product->product_id] = $product->Product->name;
            if ($product->Product->sku == 'HFO' || $product->Product->sku == 'LPG') {
                $isHFO = true;
            }
        }
        $inventoryBalance =  $this->_orderRepository->getOutStandingBalance($orderDetail);
        $importerBalance = $this->_orderRepository->getImporterOutstandingInventory($orderDetail);
        $user=$this->_userRepository->getAdminAndSupervisorList();
        return view('backend.order.uplifting.truckLoadingConfirmation')->withOrder($orderDetail)->withProducts($products)->withIsHFO($isHFO)->withBalances($inventoryBalance)->withIbalances($importerBalance)->withUser($user);
    }

    public function checkInventory(int $orderID, array $chamberProducts, $order){
        //validate chamber products
        $chamberProductsSorted = [];
        foreach ($chamberProducts as $key => $product) {
            if ($product['productID']){
                $previous = isset($chamberProductsSorted[$product['productID']]) ? $chamberProductsSorted[$product['productID']] : 0;
                $loaded = (isset($product['total_loaded'])) ? (int) $product['total_loaded'] : (int) $product['quantity'];
                $chamberProductsSorted[$product['productID']] = $loaded + $previous;
            }
        }

        if (count($chamberProductsSorted)) {
            //get omc outstanding inventory based on product
            $getOMCInventoryBalance = $this->_orderRepository->getOmcBalance($orderID);
            $outstandingProductsInventory = [];
            foreach ($getOMCInventoryBalance->OrderProducts as $key => $inventory) {
                if ($inventory->quantity > 0) {
                    //no need to include order inventory now , we check for outstanding balance only
                    //$outstandingProductsInventory[$inventory->product_id]['balance'] =  $inventory->quantity;
                    //$outstandingProductsInventory[$inventory->product_id]['product_name'] =  $inventory->Product->name;
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Please cross check order , Ordered Product quantity is 0';
                    return $response;
                }
            }

            foreach ($getOMCInventoryBalance->getOmcOutstandingInventory as $key => $inventory) {
                //$previous = isset($outstandingProductsInventory[$inventory->product_id]['balance']) ? $outstandingProductsInventory[$inventory->product_id]['balance'] : 0;
                $outstandingProductsInventory[$inventory->product_id]['balance'] = $inventory->outstanding_balance;
                $outstandingProductsInventory[$inventory->product_id]['product_name'] =  $inventory->omcProducts->name;
            }
            if (count($outstandingProductsInventory)) {
                foreach ($chamberProductsSorted as $key => $product) {
                    if (isset($outstandingProductsInventory[$key])) {
                        if ($chamberProductsSorted[$key] > $outstandingProductsInventory[$key]['balance'] ) {
                            if(is_null($order->importer_id)) {
                                $response['status'] = false;
                                $response['message'] = 'You can not assign more than '. $outstandingProductsInventory[$key]['balance'].' '. $outstandingProductsInventory[$key]['product_name'] .'  Volume to Chambers';
                                return $response;
                            }
                        } else if($chamberProductsSorted[$key] == 0 ) {
                            $response['status'] = false;
                            $response['message'] = 'Product loaded quantity should be greater than 0';
                            return $response;
                        }
                    } else {
                        $response['status'] = false;
                        $response['message'] = 'Inventory not available for this product at OMC, please cross check balance for product';
                        return $response;
                    }

                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Something went wrong , Inventory not assigned for OMC , Please create new order';
                return $response;
            }

            $response['status'] = true;
        } else {
            $response['status'] = false;
            $response['message'] = 'Please select products while assigning chamber';
        }
        if ($response['status']) {
            //check for vehicle capacity if inventory is all good
            return $this->checkCapacity($chamberProductsSorted, $order, $chamberProducts);
        }
        return $response;
    }

    public function checkCapacity($sortedchamberProducts, $order, $chamberProduct) {

        $response['status'] = true;
        $vehicleRepository = new VehicleRepository();
        $vehicleChambers = $vehicleRepository->getChamber($order->vehicle_id);
        $vehicleCapacity = 0;
        foreach($vehicleChambers->vehicle_chamber as $chambers)
        {
            $vehicleCapacity += $chambers->capacity;

        }
        $totalChamberAssigned = 0;
        if(count($sortedchamberProducts)){
            foreach($sortedchamberProducts as $key => $capacity){
                $totalChamberAssigned+= $capacity;
            }
        }
        $originalOrderQuantity = 0;
        if (count($order->OrderProducts)) {
            foreach ($order->OrderProducts as $key => $product) {
                $originalOrderQuantity += $product->quantity;
            }
        }
        if ($totalChamberAssigned > $originalOrderQuantity) {
            $response['status'] = false;
            $response['message'] = 'You can not assign more than Ordered '.$originalOrderQuantity.' Quantity';
            return $response;
        }

        if($vehicleCapacity < $totalChamberAssigned) {
            $response['status'] = false;
            $response['message'] = __('alerts.backend.order.products.capacityError');
            return $response;
        }

        //calculate chamber capacity
        foreach($chamberProduct as $key => $product)
        {
            foreach($vehicleChambers->vehicle_chamber as $chamber){
                if($chamber->id == $product['chamberID'])
                    if($product['quantity'] > $chamber->capacity ){
                        $response['status'] = false;
                        $response['message'] = 'Quantity assigned for compartment '.$chamber->chamber_number.' is more then compartment capacity.';
                        return $response;
                    }

                }
            }

            return $response;
        }

    /**
         * [upliftingVerification description]
         * @param  int    $orderID [description]
         */
    public function upliftingVerification(int $orderID)
    {
        $orderDetail = $this->_orderRepository->getOrderData($orderID);
        // dd($orderDetail);
        $loaders = $this->loaderRepository->getList();
        $seals = $orderDetail->seals;
         // dd($seals);
        $inventoryBalance =  $this->_orderRepository->getOutStandingBalance($orderDetail);
        $importerBalance = $this->_orderRepository->getImporterOutstandingInventory($orderDetail);
        $products = array();
        foreach ($orderDetail->OrderProducts as $product) {
            $products[$product->product_id] = $product->Product->name;
        }
        $isHFO = false;
        $user=$this->_userRepository->getAdminList();

        foreach ($orderDetail->OrderProducts as $product) {
            $products[$product->product_id] = $product->Product->name;
            if ($product->Product->sku == 'HFO' || $product->Product->sku == 'LPG') {
                $isHFO = true;
            }
        }
        $meterId = Config::get('meter-id');
        return view('backend.order.verification.upliftingVerification')->withOrder($orderDetail)->withMeter($meterId)->withSeals($seals)->withBalances($inventoryBalance)->withIbalances($importerBalance)->withIsHFO($isHFO)->withUser($user)->withLoader($loaders);
    }

    /**
     * uplift_vehicle
     * @param  VehicleUpliftmentRequest $request
     * @return json
     */
    public function uplift_vehicle(VehicleUpliftmentRequest $request)
    {

        try {
            DB::beginTransaction();
            $orderData=  $this->_orderRepository->getById($request->orderID);
            $checkOrderedQuantity = $this->checkOrderedQuantity($request->orderID, $request->products, $orderData,$request->product);
            if (!$checkOrderedQuantity['status']) {
                return response()->json($checkOrderedQuantity);
            }

            $this->_chamberAssignmentRepository->create($request->except('_token'));
            $orderProductMeter = $this->_orderProductMeterRepository->create($request->only('product','orderID'));


            $this->_orderRepository->update($orderData, $request->only('seen_by','authorized_by'));

            if(isset($request->truck)) {

                $this->_truckDetailRepository->create($request->only('orderID','truck'));
                $response['message'] = __('alerts.backend.order.Loading.created');
            } else {
                $response['message'] = __('alerts.backend.order.chamberAssignment.created');
            }

            $data['seals']=json_encode($request->only('vehicle_chamber'));
            $data['comments']=$request->comments;
            $data['seen_by']=$request->seen_by;
            $order = $this->_orderRepository->update($orderData, $data);
            DB::commit();
            $response['success'] = true;
            $response['message'] = __('alerts.backend.order.vehicleUpliftment.created');
            $request->session()->flash('flash_success', __('alerts.backend.order.vehicleUpliftment.created'));
            return response()->json($response);
        }catch (\Exception $e) {

            DB::rollback();
             dd($e);
            $response['success'] = false;
            $response['message'] = __('alerts.backend.order.vehicleUpliftment.createderror');
            return response()->json($response);
        }
    }

    public function checkOrderedQuantity(int $orderID, array $chamberProducts, $order, $meterReading){
        $response['status'] = true;
        $vehicleRepository = new VehicleRepository();
        $vehicleChambers = $vehicleRepository->getChamber($order->vehicle_id);
        $orderProducts = $vehicleRepository->getChamber($order->vehicle_id);
        foreach($chamberProducts as $products){
            foreach($vehicleChambers->vehicle_chamber as $chambers){
                if($chambers->id == $products['chamberID']){
                    if($chambers->capacity < $products['quantity'] || $chambers->capacity+30 < $products['total_loaded']){
                        $response['status'] = false;
                        $response['message'] = 'You can not assign quantity more than the Compartment Capacity '.$chambers->capacity.' for compartment '.$chambers->chamber_number.' .';
                        return $response;
                    }
                }

            }
            foreach($order->OrderProducts as $order_products){
             if($products['productID'] == $order_products->id){
                 if($order_products->quantity < $products['quantity'] || $order_products->quantity+30 < $products['total_loaded']){
                    $response['status'] = false;
                    $response['message'] = 'You can not assign quantity more than Ordered Quantity '.$order_products->quantity.'.';
                    return $response;
                }

            }
        }
        foreach($meterReading as $meters)
        {
            foreach($order->OrderProducts as $order_products){
             if($meters['order_product_id'] == $order_products->id){
                if($meters['quantity'] > $order_products->quantity+30){
                    $response['status'] = false;
                    $response['message'] = 'You can not assign quantity more than the Ordered Capacity ';
                    return $response;
                }
            }
        }
    }
}


return $response;

}
}
