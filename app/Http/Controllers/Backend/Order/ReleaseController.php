<?php

namespace App\Http\Controllers\Backend\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Order\ReleaseStoreRequest;
use App\Repositories\Backend\Importers\ImporterRepository;
use App\Repositories\Backend\Importers\ImporterProductStockRepository;
use App\Repositories\Backend\OMC\OmcRepository;
use App\Repositories\Backend\OMC\OmcProductStockRepository;
use App\Repositories\Backend\DriverRepository;
use App\Repositories\Backend\VehicleRepository;
use App\Repositories\Backend\ProductsRepository;
use App\Repositories\Backend\Orders\ReleaseRepository;
use Yajra\Datatables\Datatables;

class ReleaseController extends Controller
{
		/**
		 * @var  ImporterRepository <p>For importer repository instance</p>
		 */
		protected $_importerRepository;

		/**
		 * @var  OmcRepository <p>For omc repository instance</p>
		 */
		protected $_omcRepository;

		/**
		 * @var  DriverRepository <p>For driver repository instance</p>
		 */
		protected $_driverRepository;

		/**
		 * @var  VehicleRepository <p>For vehicle repository instance</p>
		 */
		protected $_vehicleRepository;

		/**
		 * @var  ProductsRepository <p>For product repository instance</p>
		 */
		protected $_productRepository;

		/**
		 * @var  ReleaseRepository
		 */
		protected $_releaseRepository;

		/**
		 * @var  OmcProductStockRepository
		 */
		protected $_omcProductStockRepository;

		/**
		 * @var  ImporterProductStockRepository
		 */
		protected $_importerProductStockRepository;

	    /**
	     * Constructor for ReleaseController
	     */
	    public function __construct(ImporterRepository $_importerRepository, DriverRepository $_driverRepository, VehicleRepository $_vehicleRepository, ProductsRepository $_productsRepository, OmcRepository $_omcRepository, ReleaseRepository $_releaseRepository, OmcProductStockRepository $_omcProductStockRepository, ImporterProductStockRepository $_importerProductStockRepository)
	    {
	    	$this->_importerRepository 					= $_importerRepository;
	    	$this->_driverRepository 					= $_driverRepository;
	    	$this->_vehicleRepository 					= $_vehicleRepository;
	    	$this->_productsRepository 					= $_productsRepository;
	    	$this->_omcRepository 						= $_omcRepository;
	    	$this->_releaseRepository					= $_releaseRepository;
	    	$this->_omcProductStockRepository 			= $_omcProductStockRepository;
	    	$this->_importerProductStockRepository 		= $_importerProductStockRepository;
	    }

	    public function index()
	    {
	    	return view('backend.release.index');
	    }


	    /**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
	    public function show($id)
	    {
	    	$releaseDetail = $this->_releaseRepository->getReleaseData($id);
	    	$products = [];
	    	foreach ($releaseDetail->releaseProducts as $key => $product) {
				# code...
	    		if (!isset($products[$product->product_id])) {
	    			$products[$product->product_id] = $product;
	    		}
	    	}

	    	return view('backend.release.show')->withRelease($releaseDetail)->withProducts($products);
	    }

	    /**
		 * Show the form for creating a new release form.
		 *
		 * @return \Illuminate\Http\Response
		 */
	    public function releaseadd()
	    {
	    	$importers 		= $this->_importerRepository->getList();
	    	$vehicles 		= $this->_vehicleRepository->getList();
	    	$driversList 	= $this->_driverRepository->getList();
	    	$omc 			= $this->_omcRepository->getList();
	    	return view('backend.release.add')->withImporters($importers)->withVehicles($vehicles)->withOmc($omc)->withDrivers($driversList);
	    }

		/**
		 * Add release data for importer
		 * @param Request $request 
		 * @return Illuminate\Routing\Route
		 */
		public function addRelease(ReleaseStoreRequest $request) 
		{
			try {
				$data = $request->except('_token');
				\DB::beginTransaction();
				if ( count( $data ) && $release = $this->_releaseRepository->create( $data )) {
					if ( isset( $data['products'] ) ) {
						if ( isset( $data['products']['count'] ) ) {
							unset($data['products']['count']);
						}
						foreach ( $data['products'] as $product_id => $balance ) {
							$this->_omcProductStockRepository->updateOmcStockWithRelease( $release, $product_id, $balance );
							$this->_importerProductStockRepository->updateImporterStockWithRelease( $release, $product_id, $balance );
						}
						\DB::commit();
						return redirect()->back()->withFlashSuccess(__('alerts.backend.release.created'));
					} 
					return redirect()->back()->withFlashFail('Please select Products');	
				}
			} catch ( Exception $e ) {
				return redirect()->back()->withInputs()->withFlashFail('Oop\s something went wrong in process.Unable to complete.');
			}

			return redirect()->back()->withInputs()->withFlashFail(__('exceptions.backend.access.release.create_error'));
		}


		/**
		 * Add release data for importer
		 * @param Request $request 
		 * @return Illuminate\Routing\Route
		 */
		public function addReleaseOmc(ReleaseStoreRequest $request) 
		{
			//dd($request->all());
			try {
				$data = $request->except('_token');
				\DB::beginTransaction();
				if ( count( $data ) && $release = $this->_releaseRepository->create( $data )) {
					if ( isset( $data['products'] ) ) {
						if ( isset( $data['products']['count'] ) ) {
							unset($data['products']['count']);
						}
						foreach ( $data['products'] as $product_id => $balance ) {
							$this->_omcProductStockRepository->updateOmcStockWithReleaseOmc( $release, $product_id, $balance );
							$this->_omcProductStockRepository->updateOmcStockWithReleaseOmcAsImporter( $release, $product_id, $balance );
							//$this->_importerProductStockRepository->updateImporterStockWithRelease( $release, $product_id, $balance );
						}
						\DB::commit();
						return redirect()->back()->withFlashSuccess(__('alerts.backend.release.created'));
					} 
					return redirect()->back()->withFlashFail('Please select Products');	
				}
			} catch ( Exception $e ) {
				return redirect()->back()->withInputs()->withFlashFail('Oop\s something went wrong in process.Unable to complete.');
			}

			return redirect()->back()->withInputs()->withFlashFail(__('exceptions.backend.access.release.create_error'));
		}

		/**
		* Collect data for datatable of Release
		* @param Request $request
		*/
		public function getReleaseData(Request $request)
		{
			//Fetching all inputs from request
			$draw   = $request->input( 'draw' );
			$order  = $request->input( 'order' );
			$start  = $request->input( 'start', 0 );
			$length = $request->input( 'length' );
			$search = $request->input( 'search' );
			$secho  = $request->input( 'sEcho' );


			$order_column    = $order[ 0 ][ 'column' ];
			$order_direction = $order[ 0 ][ 'dir' ];

			if ( empty( $order_column ) ) {
				$order_column = '0';
			}
			if ( empty( $order_direction ) ) {
				$order_direction = 'asc';
			}

			$data =  $this->_releaseRepository->getAllRecordsForDatatable();
			if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
				$value = $search['value'];

				$data = $data->orWhere(function( $query ) use($value) {
					$query->with(['Importer' => function ($query1) {
						$query1->where('Importer.company_name','like', '%'.$value.'%');
					}]);
					$query->with(['Omc' => function ($query1) {
						$query1->where('Omc.company_name','like', '%'.$value.'%');
					}]);
				});
			}

			$data   = $data->get()
			->map( function( $q ) {
				if(is_null($q->Importer)){
					$importer_data = $q->Importer()->withTrashed()->first();
					$importer_company = $importer_data ? $importer_data->company_name : "";
				} 
				if(is_null($q->Omc)){
					$omc_data = $q->Omc()->withTrashed()->first();
					$omc_company = $omc_data ? $omc_data->company_name.'/' : "";
				}
				if(is_null($q->SupplierOmc)){
					$supplier_omc_data = $q->SupplierOmc()->withTrashed()->first();
					$supplier_company = $supplier_omc_data ? $supplier_omc_data->company_name.'/' : "";
				}

				$supplier	=  $q->SupplierOmc ? '/'.$q->SupplierOmc->company_name : $supplier_company;
				$omc = $q->Omc ? $q->Omc->company_name : $omc_company;
				$importer = $q->Importer ? $q->Importer->company_name :$importer_company;
				
				return [
					'id'               => $q->id,
					'company'		   => $omc.''.$supplier.'/'.$importer,
					
					'purchaseOrder'	   => $q->purchase_order,		
					'created_at'        => date( 'D, M j, Y \a\t g:ia', strtotime( $q->created_at ) ),
				];        
			});

			return Datatables::of( $data )->addColumn('action', function ($val) {
				return '<a href="'.route('admin.release.show',$val['id']).'" title="Show release" class="col-md-4 col-sm-4 col-lg-4" ><i class="fas fa-eye" ></i></a>'; 
			})
			->make(true);
		}
	}
