<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\DriverRepository;
use App\Http\Requests\Backend\Driver\DriverStoreRequest;
use App\Http\Requests\Backend\Driver\DriverUpdateRequest;
use App\Models\Driver;
use Yajra\Datatables\Datatables;


class DriverController extends Controller
{
    /**
     * @var VehicleRepository
     */
    protected $driverRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $vehicleRepository
     */
    public function __construct(DriverRepository $driverRepository)
    {
        $this->driverRepository = $driverRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.driver.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.driver.add');
    }

    public function getDriverData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->driverRepository->getAllRecordsForDatatable();
        
        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('name','like', '%'.$value.'%')
                ->orWhere('drivers_licence_number', 'like', '%'.$value.'%')
                ->orWhere('tel_number_1', 'like', '%'.$value.'%')
                ->orWhere('tel_number_2', 'like', '%'.$value.'%');
            });
        }
        
        $data   = $data->get()
        ->map( function( $q ) {
            return [
            'id'                        => $q->id,
            'name'                      => $q->name,   
            'drivers_licence_number'    => $q->drivers_licence_number,
            'tel_number_1'              => $q->tel_number_1,
            'tel_number_2'              => $q->tel_number_2
            ];        
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.driver.show',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-primary"><i class="fas fa-eye" ></i></button></a>&nbsp;<a href="'.route('admin.driver.edit',$val['id']).'" title="Manage" ><button class="btn-sm btn btn-warning"><i class="fas fa-edit" ></i></button></a>&nbsp;<a href="'. route("admin.driver.destroy",$val['id']) .'" onclick="return confirm('."'Do you want to delete Driver ?'".');"  title="Delete Driver"><button class="btn-sm btn btn-danger"><i class="fas fa-trash"></i></button></a>&nbsp';
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DriverStoreRequest $request)
    {  
        $this->driverRepository->create($request->except('_token'));
        return redirect()->route('admin.driver.index')->withFlashSuccess(__('alerts.backend.driver.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $driver=$this->driverRepository->getdriver($id);
        return view('backend.driver.show')->withDriver($driver);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $driver=$this->driverRepository->getdriver($id);
     return view('backend.driver.edit')->withDriver($driver);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DriverUpdateRequest $request, $id)
    {
     $driver=$this->driverRepository->getdriver($id);
     $this->driverRepository->update($driver, $request->only(
        'name',
        'drivers_licence_number',
        'tel_number_1',
        'tel_number_2'
        ));

     return redirect()->route('admin.driver.index')->withFlashSuccess(__('alerts.backend.driver.updated'));
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $driver=$this->driverRepository->getdriver($id);
        $this->driverRepository->deleteById($id);  
        return redirect()->route('admin.driver.index')->withFlashSuccess(__('alerts.backend.driver.deleted'));
    }

    /**
     * Delete record from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function delete($id)
    // {
    //     $deletedDriver=$this->driverRepository->getDeletedDriver($id);
    //     $this->driverRepository->forceDelete($deletedDriver);
    //     return redirect()->route('admin.driver.deleted')->withFlashSuccess(__('alerts.backend.driver.deleted_permanently'));
    // }

     /**
     * Restore driver record in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function restore($id)
     {
        $deletedDriver=$this->driverRepository->getDeletedDriver($id);
        $this->driverRepository->restore($deletedDriver);
        return redirect()->route('admin.driver.index')->withFlashSuccess(__('alerts.backend.driver.restored'));
    }

     /**
     * Restore the specified resource in  storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function getDeleted()
     {
        return view('backend.driver.deleted');
    }

    public function getDeletedDriverData(Request $request)
    {
        //Fetching all inputs from request
        $draw   = $request->input( 'draw' );
        $order  = $request->input( 'order' );
        $start  = $request->input( 'start', 0 );
        $length = $request->input( 'length' );
        $search = $request->input( 'search' );
        $secho  = $request->input( 'sEcho' );


        $order_column    = $order[ 0 ][ 'column' ];
        $order_direction = $order[ 0 ][ 'dir' ];

        if ( empty( $order_column ) ) {
            $order_column = '0';
        }
        if ( empty( $order_direction ) ) {
            $order_direction = 'asc';
        }

        $data = $this->driverRepository->getAllDeletedRecordsForDatatable();
        
        if ( isset( $search['value'] ) && !empty( $search['value'] ) ) {
            $value = $search['value'];

            $data = $data->where(function( $query ) use($value) {
                $query->where('name','like', '%'.$value.'%')
                ->orWhere('drivers_licence_number', 'like', '%'.$value.'%')
                ->orWhere('tel_number_1', 'like', '%'.$value.'%')
                ->orWhere('tel_number_2', 'like', '%'.$value.'%');
            });
        }
        
        $data   = $data->get()
        ->map( function( $q ) {
            return [
            'id'                        => $q->id,
            'name'                      => $q->name,   
            'drivers_licence_number'    => $q->drivers_licence_number,
            'tel_number_1'              => $q->tel_number_1,
            'tel_number_2'              => $q->tel_number_2
            ];        
        });

        return Datatables::of( $data )->addColumn('action', function ($val) {
            return '<a href="'.route('admin.driver.restore',$val['id']).'" title="Restore" ><button class="btn-sm btn btn-primary"><i class="fas fa-check" ></i></button></a>&nbsp';
        })
        ->make(true);
    }
}
