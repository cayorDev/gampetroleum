<?php

namespace App\Observers\Importer;

use App\Models\System\Importer\Importer;
use App\Events\Backend\OMC\ImporterEvent;
use Auth;
use Log;
use App\Helpers\General\LogHelper;


class ImporterObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the importer "created" event.
     *
     * @param  \App\Models\System\Importer\Importer  $importer
     * @return void
     */
    public function created(Importer $importer)
    {
       $this->logHelper->log_data($importer, 'create', 'IMPORTERS');
    }

    /**
     * Handle the importer "updated" event.
     *
     * @param  \App\System\Importer\Importer  $importer
     * @return void
     */
    public function updated(Importer $importer)
    {
        if(!$importer->isDirty($importer->getDeletedAtColumn())) {
             $this->logHelper->log_data($importer, 'update', 'IMPORTERS');
        }
    }

    /**
     * Handle the importer "deleted" event.
     *
     * @param  \App\System\Importer\Importer  $importer
     * @return void
     */
    public function deleted(Importer $importer)
    {
         if (!$importer->isForceDeleting()) {
            $this->logHelper->log_data($importer, 'delete', 'IMPORTERS');
        }
    }

    /**
     * Handle the importer "restored" event.
     *
     * @param  \App\System\Importer\Importer  $importer
     * @return void
     */
    public function restored(Importer $importer)
    {
         $this->logHelper->log_data($importer, 'create', 'IMPORTERS');
    }

    /**
     * Handle the importer "force deleted" event.
     *
     * @param  \App\System\Importer\Importer  $importer
     * @return void
     */
    public function forceDeleted(Importer $importer)
    {
         $this->logHelper->log_data($importer, 'create', 'IMPORTERS');
    }

   
}

