<?php

namespace App\Observers\Importer;

use App\Models\System\Importer\ImporterProductsStock;
use Auth;
use Log;
use App\Helpers\General\LogHelper;


class ImporterProductStockObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the importer "created" event.
     *
     * @param  \App\Models\System\Importer\ImporterProductsStock  $importerInventary
     * @return void
     */
    public function created(ImporterProductsStock $importerProductsStock)
    {
       $this->logHelper->log_data($importerProductsStock, 'create', 'INVENTORY_UPDATE');
    }

    /**
     * Handle the importer "updated" event.
     *
     * @param  \App\System\Importer\ImporterProductsStock  $importerInventary
     * @return void
     */
    public function updated(ImporterProductsStock $importerProductsStock)
    {
        if(!$importerProductsStock->isDirty($importerProductsStock->getDeletedAtColumn())) {
             $this->logHelper->log_data($importerProductsStock, 'update', 'INVENTORY_UPDATE');
        }
    }

    /**
     * Handle the importer "deleted" event.
     *
     * @param  \App\System\Importer\ImporterProductsStock  $importerInventary
     * @return void
     */
    public function deleted(ImporterProductsStock $importerProductsStock)
    {
         if (!$importerProductsStock->isForceDeleting()) {
            $this->logHelper->log_data($importerProductsStock, 'delete', 'INVENTORY_UPDATE');
        }
    }

    /**
     * Handle the importer "restored" event.
     *
     *  @param  \App\System\Importer\ImporterProductsStock  $importerInventary
     * @return void
     */
    public function restored(ImporterProductsStock $importerProductsStock)
    {
         $this->logHelper->log_data($importerProductsStock, 'create', 'INVENTORY_UPDATE');
    }

    /**
     * Handle the importer "force deleted" event.
     *
     * @param  \App\System\Importer\ImporterProductsStock  $importerInventary
     * @return void
     */
    public function forceDeleted(ImporterProductsStock $importerProductsStock)
    {
         $this->logHelper->log_data($importerProductsStock, 'create', 'INVENTORY_UPDATE');
    }

   
}

