<?php

namespace App\Observers\Importer;

use App\Models\System\Importer\ImporterProductTransactions;
use Auth;
use Log; 
use App\Helpers\General\LogHelper;


class ImporterProducttransactionObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the importer "created" event.
     *
     * @param  \App\Models\System\Importer\ImporterProductTransactions  $importerInventary
     * @return void
     */
    public function created(ImporterProductTransactions $ImporterProductTransactions)
    {
       $this->logHelper->log_data($ImporterProductTransactions, 'create', 'INVENTORY_TRANSACTION');
    }

    /**
     * Handle the importer "updated" event.
     *
     * @param  \App\System\Importer\ImporterProductTransactions  $importerInventary
     * @return void
     */
    public function updated(ImporterProductTransactions $ImporterProductTransactions)
    {
        
        if(!$ImporterProductTransactions->isDirty($ImporterProductTransactions->getDeletedAtColumn())) {
             $this->logHelper->log_data($ImporterProductTransactions, 'update', 'INVENTORY_TRANSACTION');
        }
    }

    /**
     * Handle the importer "deleted" event.
     *
     * @param  \App\System\Importer\ImporterProductTransactions  $importerInventary
     * @return void
     */
    public function deleted(ImporterProductTransactions $ImporterProductTransactions)
    {
         if (!$ImporterProductTransactions->isForceDeleting()) {
            $this->logHelper->log_data($ImporterProductTransactions, 'delete', 'INVENTORY_TRANSACTION');
        }
    }

    /**
     * Handle the importer "restored" event.
     *
     *  @param  \App\System\Importer\ImporterProductTransactions  $importerInventary
     * @return void
     */
    public function restored(ImporterProductTransactions $ImporterProductTransactions)
    {
         $this->logHelper->log_data($ImporterProductTransactions, 'create', 'INVENTORY_TRANSACTION');
    }

    /**
     * Handle the importer "force deleted" event.
     *
     * @param  \App\System\Importer\ImporterProductTransactions  $importerInventary
     * @return void
     */
    public function forceDeleted(ImporterProductTransactions $ImporterProductTransactions)
    {
         $this->logHelper->log_data($ImporterProductTransactions, 'create', 'INVENTORY_TRANSACTION');
    }

   
}

