<?php 

namespace App\Observers\Backend\Order;

use App\Models\System\Order\Release;
use App\Helpers\General\LogHelper;

class ReleaseOrderObserver
{
    STATIC $type = 'RELEASE'; 

    /**
     * $logHelper 
     * @var  LogHelper
     */
    protected $logHelper;

    /**
     * Constructor for order model observer
     * @param LogHelper $logHelper 
     */
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the release "created" event.
     *
     * @param  \App\Models\System\Order\Release  $release
     * @return void
     */
    public function created(Release $release)
    {
         $this->logHelper->log_data($release, 'create', self::$type);
    }

    /**
     * Handle the release "updated" event.
     *
     * @param  \App\Models\System\Order\Release   $release
     * @return void
     */
    public function updated(Release $release)
    {
        if(!$importerProductsStock->isDirty($release->getDeletedAtColumn())) {
             $this->logHelper->log_data($release, 'update',  self::$type);
        }
    }

    /**
     * Handle the release "deleted" event.
     *
     * @param  \App\Models\System\Order\Release   $release
     * @return void
     */
    public function deleted(Release $release)
    {
        if (!$order->isForceDeleting()) {
            $this->logHelper->log_data($release, 'delete',  self::$type);
        }
    }

    /**
     * Handle the release "restored" event.
     *
     * @param  \App\Models\System\Order\Release   $release
     * @return void
     */
    public function restored(Release $release)
    {
        $this->logHelper->log_data($release, 'restored', self::$type);
    }

    /**
     * Handle the release "force deleted" event.
     *
     * @param  \App\Models\System\Order\Release   $release
     * @return void
     */
    public function forceDeleted(Release $release)
    {
        $this->logHelper->log_data($release, 'force delete', self::$type);
    }
}
