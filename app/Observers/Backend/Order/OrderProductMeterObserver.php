<?php

namespace App\Observers\Backend\Order;

use App\Models\System\Order\OrderProductMeter;
use Auth;
use App\Helpers\General\LogHelper;

class OrderProductMeterObserver
{

    STATIC $type = 'ORDER_PRODUCT_METER';
    
    /**
     * $logHelper 
     * @var  LogHelper
     */
    protected $logHelper;

    /**
     * Constructor for order model observer
     * @param LogHelper $logHelper 
     */
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the order product meter "created" event.
     *
     * @param  \App\Models\System\Order\OrderProductMeter  $orderProductMeter
     * @return void
     */
    public function created(OrderProductMeter $orderProductMeter)
    {
        $this->logHelper->log_data($orderProductMeter, 'create', self::$type);
    }

    /**
     * Handle the order product meter "updated" event.
     *
     * @param  \App\Models\System\Order\OrderProductMeter  $orderProductMeter
     * @return void
     */
    public function updated(OrderProductMeter $orderProductMeter)
    {
        if(!$orderProductMeter->isDirty($orderProductMeter->getDeletedAtColumn())) {
             $this->logHelper->log_data($orderProductMeter, 'update', self::$type);
        }
    }

    /**
     * Handle the order product meter "deleted" event.
     *
     * @param  \App\Models\System\Order\OrderProductMeter  $orderProductMeter
     * @return void
     */
    public function deleted(OrderProductMeter $orderProductMeter)
    {
        if (!$orderProductMeter->isForceDeleting()) {
            $this->logHelper->log_data($orderProductMeter, 'delete', self::$type);
        }
    }

    /**
     * Handle the order product meter "restored" event.
     *
     * @param  \App\Models\System\Order\OrderProductMeter  $orderProductMeter
     * @return void
     */
    public function restored(OrderProductMeter $orderProductMeter)
    {
        $this->logHelper->log_data($orderProductMeter, 'restored', self::$type);
    }

    /**
     * Handle the order product meter "force deleted" event.
     *
     * @param  \App\Models\System\Order\OrderProductMeter  $orderProductMeter
     * @return void
     */
    public function forceDeleted(OrderProductMeter $orderProductMeter)
    {
        $this->logHelper->log_data($orderProductMeter, 'force delete', self::$type);
    }
}
