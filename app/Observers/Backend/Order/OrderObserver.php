<?php

namespace App\Observers\Backend\Order;

use App\Models\System\Order\Order;
use Auth;
use App\Helpers\General\LogHelper; 

class OrderObserver
{
    STATIC $type = 'ORDER';

    /**
     * $logHelper 
     * @var  LogHelper
     */
    protected $logHelper;

    /**
     * Constructor for order model observer
     * @param LogHelper $logHelper 
     */
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }


    /**
     * Handle the order "created" event.
     *
     * @param  \System\Order\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        $this->logHelper->log_data($order, 'create', self::$type);
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \System\Order\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        if(!$order->isDirty($order->getDeletedAtColumn())) {
             $this->logHelper->log_data($order, 'update', self::$type);
        }
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \System\Order\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        if (!$order->isForceDeleting()) {
            $this->logHelper->log_data($order, 'delete', self::$type);
        }
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \System\Order\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        $this->logHelper->log_data($order, 'restored', self::$type);
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \System\Order\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        $this->logHelper->log_data($order, 'force delete', self::$type);
    }
}
