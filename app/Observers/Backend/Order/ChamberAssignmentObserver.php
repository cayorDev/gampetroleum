<?php

namespace App\Observers\Backend\Order;

use App\Models\System\Order\ChamberAssignment;
use Auth;
use App\Helpers\General\LogHelper;

class ChamberAssignmentObserver
{

    STATIC $type = 'CHAMBER_ASSIGNMENT';
    /**
     * $logHelper 
     * @var  LogHelper
     */
    protected $logHelper;

    /**
     * Constructor for order model observer
     * @param LogHelper $logHelper 
     */
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the chamber assignment "created" event.
     *
     * @param  \App\Models\System\Order\ChamberAssignment  $chamberAssignment
     * @return void
     */
    public function created(ChamberAssignment $chamberAssignment)
    {
        $this->logHelper->log_data($chamberAssignment, 'create', self::$type);
    }

    /**
     * Handle the chamber assignment "updated" event.
     *
     * @param  \App\Models\System\Order\ChamberAssignment  $chamberAssignment
     * @return void
     */
    public function updated(ChamberAssignment $chamberAssignment)
    {
        if(!$chamberAssignment->isDirty($chamberAssignment->getDeletedAtColumn())) {
             $this->logHelper->log_data($chamberAssignment, 'update', self::$type);
        }
    }

    /**
     * Handle the chamber assignment "deleted" event.
     *
     * @param  \App\Models\System\Order\ChamberAssignment  $chamberAssignment
     * @return void
     */
    public function deleted(ChamberAssignment $chamberAssignment)
    {
        if (!$chamberAssignment->isForceDeleting()) {
            $this->logHelper->log_data($chamberAssignment, 'delete', self::$type);
        }
    }

    /**
     * Handle the chamber assignment "restored" event.
     *
     * @param  \App\Models\System\Order\ChamberAssignment  $chamberAssignment
     * @return void
     */
    public function restored(ChamberAssignment $chamberAssignment)
    {
        $this->logHelper->log_data($chamberAssignment, 'restored', self::$type);
    }

    /**
     * Handle the chamber assignment "force deleted" event.
     *
     * @param  \App\Models\System\Order\ChamberAssignment  $chamberAssignment
     * @return void
     */
    public function forceDeleted(ChamberAssignment $chamberAssignment)
    {
        $this->logHelper->log_data($chamberAssignment, 'force delete', self::$type);
    }
}
