<?php

namespace App\Observers\Backend;

use App\Models\System\Contacts;

use Log;
use Auth;
use App\Helpers\General\LogHelper;


class ContactsObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }
    /**
     * Handle the omc "created" event.
     *
     * @param  \App\System\Contacts  $Contacts
     * @return void
     */
    public function created(Contacts $Contacts)
    {
        $this->logHelper->log_data($Contacts, 'create', 'OMC'); 
    }

    /**
     * Handle the omc "updated" event.
     *
     * @param  \App\System\Contacts  $Contacts             
     * @return void
     */
    public function updated(Contacts $Contacts)
    {
        if(!$Contacts->isDirty($Contacts->getDeletedAtColumn())) {
             $this->logHelper->log_data($Contacts, 'update', 'OMC');
        }
    }

    /**
     * Handle the omc "deleted" event.
     *
     * @param  \App\System\Contacts  $omcContacts
     * @return void
     */
    public function deleted(Contacts $Contacts)
    {
         if (!$Contacts->isForceDeleting()) {
            $this->logHelper->log_data($Contacts, 'delete', 'OMC');
        }
    }

    /**
     * Handle the omc "restored" event.
     *
     * @param  \App\System\Contacts  $Contacts
     * @return void
     */
    public function restored(Contacts $Contacts)
    {
        $this->logHelper->log_data($Contacts, 'restored', 'OMC');
    }

    /**
     * Handle the omc "force deleted" event.
     *
     * @param  \App\System\Contacts  $Contacts
     * @return void
     */
    public function forceDeleted(Contacts $Contacts)
    {
        $this->logHelper->log_data($Contacts, 'force delete', 'OMC');
    }

    
}
