<?php

namespace App\Observers\Backend;

use App\Models\System\Locations;

use Log;
use Auth;
use App\Helpers\General\LogHelper;


class LocationsObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }
    /**
     * Handle the omc "created" event.
     *
     * @param  \App\System\Locations  $Locations
     * @return void
     */
    public function created(Locations $Locations)
    {
        $this->logHelper->log_data($Locations, 'create', 'Location'); 
    }

    /**
     * Handle the omc "updated" event.
     *
     * @param  \App\System\Locations  $Locations             
     * @return void
     */
    public function updated(Locations $Locations)
    {
        if(!$Locations->isDirty($Locations->getDeletedAtColumn())) {
             $this->logHelper->log_data($Locations, 'update', 'Location');
        }
    }

    /**
     * Handle the omc "deleted" event.
     *
     * @param  \App\System\Locations  $Locations
     * @return void
     */
    public function deleted(Locations $Locations)
    {
         if (!$Locations->isForceDeleting()) {
            $this->logHelper->log_data($Locations, 'delete', 'Location');
        }
    }

    /**
     * Handle the omc "restored" event.
     *
     * @param  \App\System\Locations  $Locations
     * @return void
     */
    public function restored(Locations $Locations)
    {
        $this->logHelper->log_data($Locations, 'restored', 'Location');
    }

    /**
     * Handle the omc "force deleted" event.
     *
     * @param  \App\System\Locations  $Locations
     * @return void
     */
    public function forceDeleted(Locations $Locations)
    {
        $this->logHelper->log_data($Locations, 'force delete', 'Location');
    }

    
}
