<?php

namespace App\Observers\Backend;

use App\Models\System\Addresses;

use Log;
use Auth;
use App\Helpers\General\LogHelper;


class AddressObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }
    /**
     * Handle the omc "created" event.
     *
     * @param  \App\System\Addresses  $Addresses
     * @return void
     */
    public function created(Addresses $Addresses)
    {
        $this->logHelper->log_data($Addresses, 'create', 'Address'); 
    }

    /**
     * Handle the omc "updated" event.
     *
     * @param  \App\System\Addresses  $Addresses             
     * @return void
     */
    public function updated(Addresses $Addresses)
    {
        if(!$Addresses->isDirty($Addresses->getDeletedAtColumn())) {
             $this->logHelper->log_data($Addresses, 'update', 'Address');
        }
    }

    /**
     * Handle the omc "deleted" event.
     *
     * @param  \App\System\Addresses  $Addresses
     * @return void
     */
    public function deleted(Addresses $Addresses)
    {
         if (!$Addresses->isForceDeleting()) {
            $this->logHelper->log_data($Addresses, 'delete', 'Address');
        }
    }

    /**
     * Handle the omc "restored" event.
     *
     * @param  \App\System\Addresses  $Addresses
     * @return void
     */
    public function restored(Addresses $Addresses)
    {
        $this->logHelper->log_data($Addresses, 'restored', 'Address');
    }

    /**
     * Handle the omc "force deleted" event.
     *
     * @param  \App\System\Addresses  $Addresses
     * @return void
     */
    public function forceDeleted(Addresses $Addresses)
    {
        $this->logHelper->log_data($Addresses, 'force delete', 'Address');
    }

    
}
