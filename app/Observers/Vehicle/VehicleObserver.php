<?php

namespace App\Observers\Vehicle;

use App\Models\Vehicle;
use App\Helpers\General\LogHelper;

class VehicleObserver
{
     /**
     * $logHelper 
     */
     protected $logHelper;


     public function __construct( LogHelper $logHelper)
     {
        $this->logHelper = $logHelper;
    }


    /**
     * Handle the vehicle "created" event.
     *
     * @param  \App\Vehicle  $vehicle
     * @return void
     */
    public function created(Vehicle $vehicle)
    {
        $this->logHelper->log_data($vehicle, 'create', 'VEHICLES');
    }

    /**
     * Handle the vehicle "updated" event.
     *
     * @param  \App\Vehicle  $vehicle
     * @return void
     */
    public function updated(Vehicle $vehicle)
    {
        if(!$vehicle->isDirty($vehicle->getDeletedAtColumn())) {
            $this->logHelper->log_data($vehicle, 'update', 'VEHICLES');
        }
    }

    /**
     * Handle the vehicle "deleted" event.
     *
     * @param  \App\Vehicle  $vehicle
     * @return void
     */
    public function deleted(Vehicle $vehicle)
    {

        if (!$vehicle->isForceDeleting()) {
            $this->logHelper->log_data($vehicle, 'delete', 'VEHICLES');
        }
    }

    /**
     * Handle the vehicle "restored" event.
     *
     * @param  \App\Vehicle  $vehicle
     * @return void
     */
    public function restored(Vehicle $vehicle)
    {
        $this->logHelper->log_data($vehicle, 'restored', 'VEHICLES');
    }

    /**
     * Handle the vehicle "force deleted" event.
     *
     * @param  \App\Vehicle  $vehicle
     * @return void
     */
    public function forceDeleted(Vehicle $vehicle)
    {
        $this->logHelper->log_data($vehicle, 'force delete', 'VEHICLES');
    }
}
