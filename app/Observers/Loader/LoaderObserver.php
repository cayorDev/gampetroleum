<?php

namespace App\Observers\Loader;

use App\Models\Loader;
use App\Helpers\General\LogHelper;

class DriverObserver
{
    /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }


    /**
     * Handle the loader "created" event.
     *
     * @param  \App\Loader  $loader
     * @return void
     */
    public function created(Loader $loader)
    {
        $this->logHelper->log_data($loader, 'create', 'LOADERS');
    }

    /**
     * Handle the loader "updated" event.
     *
     * @param  \App\Loader  $loader
     * @return void
     */
    public function updated(Loader $loader)
    {
        if(!$loader->isDirty($loader->getDeletedAtColumn())) {
             $this->logHelper->log_data($loader, 'update', 'LOADERS');
        }
    }

    /**
     * Handle the loader "deleted" event.
     *
     * @param  \App\Loader  $loader
     * @return void
     */
    public function deleted(Loader $loader)
    {
        if (!$loader->isForceDeleting()) {
            $this->logHelper->log_data($loader, 'delete', 'LOADERS');
        }
    }

    /**
     * Handle the loader "restored" event.
     *
     * @param  \App\Loader  $loader
     * @return void
     */
    public function restored(Loader $loader)
    {
        $this->logHelper->log_data($loader, 'restored', 'LOADERS');
    }

    /**
     * Handle the loader "force deleted" event.
     *
     * @param  \App\Loader  $loader
     * @return void
     */
    public function forceDeleted(Loader $loader)
    {
        $this->logHelper->log_data($loader, 'force delete', 'LOADERS');
    }
}
