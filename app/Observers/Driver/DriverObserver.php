<?php

namespace App\Observers\Driver;

use App\Models\Driver;
use App\Helpers\General\LogHelper;

class DriverObserver
{
    /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }


    /**
     * Handle the driver "created" event.
     *
     * @param  \App\Driver  $driver
     * @return void
     */
    public function created(Driver $driver)
    {
        $this->logHelper->log_data($driver, 'create', 'DRIVERS');
    }

    /**
     * Handle the driver "updated" event.
     *
     * @param  \App\Driver  $driver
     * @return void
     */
    public function updated(Driver $driver)
    {
        if(!$driver->isDirty($driver->getDeletedAtColumn())) {
             $this->logHelper->log_data($driver, 'update', 'DRIVERS');
        }
    }

    /**
     * Handle the driver "deleted" event.
     *
     * @param  \App\Driver  $driver
     * @return void
     */
    public function deleted(Driver $driver)
    {
        if (!$driver->isForceDeleting()) {
            $this->logHelper->log_data($driver, 'delete', 'DRIVERS');
        }
    }

    /**
     * Handle the driver "restored" event.
     *
     * @param  \App\Driver  $driver
     * @return void
     */
    public function restored(Driver $driver)
    {
        $this->logHelper->log_data($driver, 'restored', 'DRIVERS');
    }

    /**
     * Handle the driver "force deleted" event.
     *
     * @param  \App\Driver  $driver
     * @return void
     */
    public function forceDeleted(Driver $driver)
    {
        $this->logHelper->log_data($driver, 'force delete', 'DRIVERS');
    }
}
