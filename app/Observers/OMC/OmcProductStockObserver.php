<?php

namespace App\Observers\OMC;

use App\Models\System\OMC\OmcProductsStock;
use Auth;
use App\Helpers\General\LogHelper;


class OmcProductStockObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the Omc "created" event.
     *
     * @param  \App\Models\System\Omc\OmcProductsStock  $OmcInventary
     * @return void
     */
    public function created(OmcProductsStock $OmcProductsStock)
    {
       $this->logHelper->log_data($OmcProductsStock, 'create', 'OMC_PRODUCTS');
    }

    /**
     * Handle the Omc "updated" event.
     *
     * @param  \App\System\Omc\OmcProductsStock  $OmcInventary
     * @return void
     */
    public function updated(OmcProductsStock $OmcProductsStock)
    {
        if(!$OmcProductsStock->isDirty($OmcProductsStock->getDeletedAtColumn())) {
             $this->logHelper->log_data($OmcProductsStock, 'update', 'OMC_PRODUCTS');
        }
    }

    /**
     * Handle the Omc "deleted" event.
     *
     * @param  \App\System\Omc\OmcProductsStock  $OmcInventary
     * @return void
     */
    public function deleted(OmcProductsStock $OmcProductsStock)
    {
         if (!$OmcProductsStock->isForceDeleting()) {
            $this->logHelper->log_data($OmcProductsStock, 'delete', 'OMC_PRODUCTS');
        }
    }

    /**
     * Handle the Omc "restored" event.
     *
     *  @param  \App\System\Omc\OmcProductsStock  $OmcInventary
     * @return void
     */
    public function restored(OmcProductsStock $OmcProductsStock)
    {
         $this->logHelper->log_data($OmcProductsStock, 'create', 'OMC_PRODUCTS');
    }

    /**
     * Handle the Omc "force deleted" event.
     *
     * @param  \App\System\Omc\OmcProductsStock  $OmcInventary
     * @return void
     */
    public function forceDeleted(OmcProductsStock $OmcProductsStock)
    {
         $this->logHelper->log_data($OmcProductsStock, 'create', 'OMC_PRODUCTS');
    }

   
}

