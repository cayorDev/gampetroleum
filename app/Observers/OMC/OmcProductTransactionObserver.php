<?php

namespace App\Observers\OMC;

use App\Models\System\OMC\OmcProductTransactions;
use Auth;
use App\Helpers\General\LogHelper;


class OmcProductTransactionObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Handle the Omc "created" event.
     *
     * @param  \App\Models\System\Omc\OmcProductsTransactions  $OmcInventary
     * @return void
     */
    public function created(OmcProductTransactions $OmcProductTransactions)
    {
       $this->logHelper->log_data($OmcProductTransactions, 'create', 'OMC_INVENTORY_TRANSACTION');
    }

    /**
     * Handle the Omc "updated" event.
     *
     * @param  \App\System\Omc\OmcProductsTransactions  $OmcInventary
     * @return void
     */
    public function updated(OmcProductTransactions $OmcProductTransactions)
    {
        if(!$OmcProductTransactions->isDirty($OmcProductTransactions->getDeletedAtColumn())) {
             $this->logHelper->log_data($OmcProductTransactions, 'update', 'OMC_INVENTORY_TRANSACTION');
        }
    }

    /**
     * Handle the Omc "deleted" event.
     *
     * @param  \App\System\Omc\OmcProductsTransactions  $OmcInventary
     * @return void
     */
    public function deleted(OmcProductTransactions $OmcProductTransactions)
    {
         if (!$OmcProductTransactions->isForceDeleting()) {
            $this->logHelper->log_data($OmcProductTransactions, 'delete', 'OMC_INVENTORY_TRANSACTION');
        }
    }

    /**
     *  
     * Handle the Omc "restored" event.
     *
     *  @param  \App\System\Omc\OmcProductsTransactions  $OmcInventary
     * @return void
     */
    public function restored(OmcProductTransactions $OmcProductTransactions)
    {
         $this->logHelper->log_data($OmcProductTransactions, 'create', 'OMC_INVENTORY_TRANSACTION');
    }

    /**
     * Handle the Omc "force deleted" event.
     *
     * @param  \App\System\Omc\OmcProductsTransactions  $OmcInventary
     * @return void
     */
    public function forceDeleted(OmcProductTransactions $OmcProductTransactions)
    {
         $this->logHelper->log_data($OmcProductTransactions, 'create', 'OMC_INVENTORY_TRANSACTION');
    }

   
}

