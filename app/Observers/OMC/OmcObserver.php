<?php

namespace App\Observers\OMC;

use App\Models\System\OMC\Omc;
use App\Events\Backend\OMC\OmcEvent;
use Auth;
use App\Helpers\General\LogHelper;


class OmcObserver
{   
     /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }
    /**
     * Handle the omc "created" event.
     *
     * @param  \App\System\OMC\Omc  $omc
     * @return void
     */
    public function created(Omc $omc)
    {
        $this->logHelper->log_data($omc, 'create', 'OMC'); 
    }

    /**
     * Handle the omc "updated" event.
     *
     * @param  \App\System\OMC\Omc  $omc
     * @return void
     */
    public function updated(Omc $omc)
    {
        if(!$omc->isDirty($omc->getDeletedAtColumn())) {
             $this->logHelper->log_data($omc, 'update', 'OMC');
        }
    }

    /**
     * Handle the omc "deleted" event.
     *
     * @param  \App\System\OMC\Omc  $omc
     * @return void
     */
    public function deleted(Omc $omc)
    {
         if (!$omc->isForceDeleting()) {
            $this->logHelper->log_data($omc, 'delete', 'OMC');
        }
    }

    /**
     * Handle the omc "restored" event.
     *
     * @param  \App\System\OMC\Omc  $omc
     * @return void
     */
    public function restored(Omc $omc)
    {
        $this->logHelper->log_data($omc, 'create', 'OMC');
    }

    /**
     * Handle the omc "force deleted" event.
     *
     * @param  \App\System\OMC\Omc  $omc
     * @return void
     */
    public function forceDeleted(Omc $omc)
    {
        $this->logHelper->log_data($omc, 'create', 'OMC');
    }


}
