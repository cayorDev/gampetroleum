<?php

namespace App\Observers\User;

use App\Models\Auth\User;
use App\Helpers\General\LogHelper;

/**
 * Class UserObserver.
 */
class UserObserver
{   
    /**
     * $logHelper 
     */
    protected $logHelper;

    
    public function __construct( LogHelper $logHelper)
    {
        $this->logHelper = $logHelper;
    }

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Auth\User  $user
     * @return void
     */
    public function created(User $user) : void
    {
        $this->logPasswordHistory($user);
        $this->logHelper->log_data($user, 'create', 'USERS');
    }

    /**
     * Listen to the User updated event.
     *
     * @param  \App\Models\Auth\User  $user
     * @return void
     */
    public function updated(User $user) : void
    {
        // Only log password history on update if the password actually changed
        if ($user->isDirty('password')) {
            $this->logPasswordHistory($user);
        }
        if($user->isDirty('active')){
            if ($user->active==0) {
                $this->logHelper->log_data($user, 'deactivated', 'USERS');
            } else
            {
                $this->logHelper->log_data($user, 'activated', 'USERS');
            }
        }
        elseif (!$user->isDirty('deleted_at')) {
            $this->logHelper->log_data($user, 'update', 'USERS');
        }
    }

    /**
     * @param User $user
     */
    private function logPasswordHistory(User $user) : void
    {
        if (config('access.users.password_history')) {
            $user->passwordHistories()->create([
                'password' => $user->password, // Password already hashed & saved so just take from model
            ]);
        }
    }

    /**
     * handle the User deleted event.
     *
     * @param  \App\Models\Auth\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        if (!$user->isForceDeleting()) {
            $this->logHelper->log_data($user, 'delete', 'USERS');
        }
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\Models\Auth\User  $user
     * @return void
     */
    public function restored(User $user)
    { 
        $this->logHelper->log_data($user, 'restored', 'USERS');
    }

    /**
     * handle the User force deleted event.
     *
     * @param  \App\Models\Auth\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        $this->logHelper->log_data($user, 'force delete', 'USERS');
    }

}
