<?php

namespace App\Listeners\Backend\Vehicle;

/**
 * Class VehicleEventListener.
 */
class VehicleEventListener
{
    /**
     * @param $event
     */
    public function VehicleLogs($event)
    {
        \Log::info($event->action.' | Vehicle Licence Plate Number : '.$event->vehicle->license_plate_number.' | By User : '.$event->user );
    }

   
    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Vehicle\VehicleLogEvents::class,
            'App\Listeners\Backend\Vehicle\VehicleEventListener@VehicleLogs'
        );
         
    }
}
