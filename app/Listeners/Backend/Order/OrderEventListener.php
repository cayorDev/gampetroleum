<?php

namespace App\Listeners\Backend\Order;

/**
 * Class OrderEventListener.
 */
class OrderEventListener
{
    /**
     * @param $event
     */
    public function OrderLogs($event)
    {
        \Log::info($event->action.' | Order ID : '.$event->order->order_id.' | By User : '.$event->user );
    }

   
    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Order\OrderLogEvents::class,
            'App\Listeners\Backend\Order\OrderEventListener@OrderLogs'
        );
         
    }
}
