<?php

namespace App\Listeners\Backend\OMC;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Backend\OMC\ImporterEvent;
use Log;

class ImporterEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ImporterEvent  $event
     * @return void
     */
    public function handle(ImporterEvent $event)
    {
        //
    }

    /**
     * In case of creation of importer model
     * @param  ImporterEvent $event 
     * @return void
     */
    public function created(ImporterEvent $event)
    {
        $importer  = $event->_importer;
        $user      = $event->_importer->first_name.' '.$event->_importer->last_name;
        //Creating a log
        Log::info( sprintf('Importer record created. Importer record: [%s], Created By:[%s]', json_encode($importer, true), json_encode($user, true) ) );
    }

    /**
     * In case of update of importer model
     * @param  ImporterEvent $event 
     * @return void
     */
    public function updated(ImporterEvent $event)
    {

    }

    /**
     * Check event types to seperate every event
     * @param  ImporterEvent $event 
     * @return void
     */
    public function checkEvent(ImporterEvent $event)
    {
        switch ($event->_type) {
            case 'created':
                $this->created($event);
                break;
            case 'updated':
                $this->updated($event);
                break;
            default:
                Log::error(sprintf('[%s],[%d] INFO: Event captured in Importer. Event:[%s]', __METHOD__, __LINE__, json_encode($event, true)));
                break;
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            ImporterEvent::class,
            'App\Listeners\Backend\OMC\ImporterEventListener@checkEvent'
        );
    }
}
