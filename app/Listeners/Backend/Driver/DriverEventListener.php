<?php

namespace App\Listeners\Backend\Driver;

/**
 * Class DriverEventListener.
 */
class DriverEventListener
{
    /**
     * @param $event
     */
    public function DriverLogs($event)
    {
        \Log::info($event->action.' | Driver Licence Number : '.$event->driver->drivers_licence_number.' | By User : '.$event->user );
    }
    
   
    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Driver\DriverLogEvents::class,
            'App\Listeners\Backend\Driver\DriverEventListener@DriverLogs'
        );
    }
}
