<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists'    => 'That role already exists. Please choose a different name.',
                'cant_delete_admin' => 'You can not delete the Administrator role.',
                'create_error'      => 'There was a problem creating this role. Please try again.',
                'delete_error'      => 'There was a problem deleting this role. Please try again.',
                'has_users'         => 'You can not delete a role with associated users.',
                'needs_permission'  => 'You must select at least one permission for this role.',
                'not_found'         => 'That role does not exist.',
                'update_error'      => 'There was a problem updating this role. Please try again.',
            ],

            'users' => [
                'already_confirmed'    => 'This user is already confirmed.',
                'cant_confirm' => 'There was a problem confirming the user account.',
                'cant_deactivate_self'  => 'You can not do that to yourself.',
                'cant_delete_admin'  => 'You can not delete the super administrator.',
                'cant_delete_self'      => 'You can not delete yourself.',
                'cant_delete_own_session' => 'You can not delete your own session.',
                'cant_restore'          => 'This user is not deleted so it can not be restored.',
                'cant_unconfirm_admin' => 'You can not un-confirm the super administrator.',
                'cant_unconfirm_self' => 'You can not un-confirm yourself.',
                'create_error'          => 'There was a problem creating this user. Please try again.',
                'delete_error'          => 'There was a problem deleting this user. Please try again.',
                'delete_first'          => 'This user must be deleted first before it can be destroyed permanently.',
                'email_error'           => 'That email address belongs to a different user.',
                'mark_error'            => 'There was a problem updating this user. Please try again.',
                'not_confirmed'            => 'This user is not confirmed.',
                'not_found'             => 'That user does not exist.',
                'restore_error'         => 'There was a problem restoring this user. Please try again.',
                'role_needed_create'    => 'You must choose at lease one role.',
                'role_needed'           => 'You must choose at least one role.',
                'session_wrong_driver'  => 'Your session driver must be set to database to use this feature.',
                'social_delete_error' => 'There was a problem removing the social account from the user.',
                'update_error'          => 'There was a problem updating this user. Please try again.',
                'update_password_error' => 'There was a problem changing this users password. Please try again.',
            ],

            'vehicle' => [
                'create_error'          => 'There was a problem creating this vehicle. Please try again.',
                'update_error'          => 'There was a problem updating thisvehicle. Please try again.',
                'cant_restore'          => 'This vehicle is not deleted so it can not be restored.',
                'restore_error'         => 'There was a problem restoring this vehicle . Please try again.',
                'delete_first'          => 'This vehicle must be deleted first before it can be destroyed permanently.',
                'delete_error'          => 'There was a problem deleting this vehicle. Please try again.',

            ],
            'vehicle_driver' => [
                'create_error'          => 'There was a problem adding this vehicle driver. Please try again.',
            ],

            'driver' => [
                'create_error'          => 'There was a problem creating this driver. Please try again.',
                'update_error'          =>'There was a problem deleting this driver.Plese try again.',
                'delete_first'          => 'This driver must be deleted first before it can be destroyed permanently.',
                'delete_error'          => 'There was a problem deleting this Driver. Please try again.',
                'cant_restore'          => 'This driver is not deleted so it can not be restored.',
                'restore_error'         => 'There was a problem restoring this driver. Please try again.',
                'find_error'            => 'Driver not found .Plese try agin',

            ],

            'loader' => [
                'create_error'          => 'There was a problem creating this loader. Please try again.',
                'update_error'          =>'There was a problem deleting this loader.Plese try again.',
                'delete_first'          => 'This loader must be deleted first before it can be destroyed permanently.',
                'delete_error'          => 'There was a problem deleting this Driver. Please try again.',
                'cant_restore'          => 'This loader is not deleted so it can not be restored.',
                'restore_error'         => 'There was a problem restoring this loader. Please try again.',
                'find_error'            => 'Loader not found .Plese try agin',

            ],

            'management' => [
                'create_error'          => 'There was a problem creating this record. Please try again.',
                'update_error'          =>'There was a problem deleting this record.Plese try again.',
                'delete_first'          => 'This record must be deleted first before it can be destroyed permanently.',
                'delete_error'          => 'There was a problem deleting this record. Please try again.',
                'cant_restore'          => 'This record is not deleted so it can not be restored.',
                'restore_error'         => 'There was a problem restoring this record. Please try again.',
                'find_error'            => 'record not found .Plese try agin',

            ],

            'contacts' => [
                'create_error'        => 'There was a problem creating this contact.',
                'edit_error'          => 'There was a problem editing this contact.',
                'delete_error'        => 'There was a problem in deleting this contact.',
            ],

            'location' => [
                'create_error'        => 'There was a problem creating this location.',
                'edit_error'          => 'There was a problem editing this location.',
                'delete_error'        => 'There was a problem in deleting this location.',
            ],
            'omc'    => [
                'create_error'          => 'There was a problem creating omc record.Please try again',
                'not_enough_data'       => 'Not enough data available to create record',
                'not_confirmed'            => 'This omc account is not confirmed.',
                'not_enough_data'       => 'Not enough data available to create record',
                'delete_error'          => 'There was a problem deleting this Omc. Please try again.',
            ],
            'importers'    => [
                'create_error'          => 'There was a problem creating importer record.Please try again',
                'show_error'            =>'There was a problem mange this importer',
                'not_enough_data'       => 'Not enough data available to create record'
            ],
            'logs' => [
                'create_error'          => 'There was a problem creating this log. Please try again.',
            ],
            'order' => [
                'create_error'          => 'There was a problem adding this order. Please try again.',
            ],
            'release' => [
                'create_error'          => 'Problem in creating release record. Please try again.'
            ]
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => 'Your account is already confirmed.',
                'confirm'           => 'Confirm your account!',
                'created_confirm'   => 'Your account was successfully created. We have sent you an e-mail to confirm your account.',
                'created_pending'   => 'Your account was successfully created and is pending approval. An e-mail will be sent when your account is approved.',
                'mismatch'          => 'Your confirmation code does not match.',
                'not_found'         => 'That confirmation code does not exist.',
                'pending'            => 'Your account is currently pending approval.',
                'resend'            => 'Your account is not confirmed. Please click the confirmation link in your e-mail, or <a href=":url">click here</a> to resend the confirmation e-mail.',
                'success'           => 'Your account has been successfully confirmed!',
                'resent'            => 'A new confirmation e-mail has been sent to the address on file.',
            ],

            'deactivated' => 'Your account has been deactivated.',
            'email_taken' => 'That e-mail address is already taken.',

            'password' => [
                'change_mismatch' => 'That is not your old password.',
                'reset_problem' => 'There was a problem resetting your password. Please resend the password reset email.',
            ],

            'registration_disabled' => 'Registration is currently closed.',
        ],
    ],
];
