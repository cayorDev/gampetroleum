<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'All',
        'yes'     => 'Yes',
        'no'      => 'No',
        'copyright' => 'Copyright',
        'custom'  => 'Custom',
        'actions' => 'Actions',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Save',
            'update' => 'Update',
        ],
        'hide'              => 'Hide',
        'inactive'          => 'Inactive',
        'none'              => 'None',
        'show'              => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
        'create_new'        => 'Create New',
        'edit'              => 'Edit',
        'toolbar_btn_groups' => 'Toolbar with button groups',
        'more'              => 'More',
        'none'              => 'None',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',
                'user_actions'        => 'User Actions',

                'table' => [
                    'confirmed'      => 'Confirmed',
                    'created'        => 'Created',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Last Updated',
                    'name'           => 'Name',
                    'first_name'     => 'First Name',
                    'last_name'      => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions'    => 'Permissions',
                    'abilities'      => 'Abilities',
                    'roles'          => 'Roles',
                    'social'         => 'Social',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                        'location'  => 'Location',


                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'email'        => 'E-mail',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'first_name'   => 'First Name',
                            'last_name'    => 'Last Name',
                            'status'       => 'Status',
                            'timezone'     => 'Timezone',
                            'contact_name' => 'Contact name',
                            'tel1'         =>  'tel1',
                            'tel2'         =>  'tel2',

                        ],
                    ],
                ],

                'view' => 'View User',
            ],
            'vehicle' => [
                'management'          => 'Vehicle Management',
                'create'              => 'Create Vehicle',
                'active'              => 'Active Vehicles',
                'edit'                => 'Edit Vehicle',
                'deleted'             => 'Deleted Vehicles',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Last Updated',
                    'license_plate_number' => 'Licence Plate Number',
                    'transporter'          => 'Transporter',
                    'transporter_name'     => 'Transporter Name',
                    'transporter_tel_number'     => 'Transporter Phone Number',
                    'number_of_chambers'   => 'Number Of Compartments',
                    'chamber_capaciy'      => 'Compartment Capacity',
                    'upper_seals'          => 'Upper Seals',
                    'lower_seals'          => 'Lower Seals',
                    'total'                => 'vehicles total',
                    'chamber'             => 'Compartment ',
                    'totalCapacity'        => 'Total Capacity',
                    'dateRegistered'        => 'Registration Date',
                    'status'                => 'Status',

                ],
                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                        'chambers' => 'Compartments',
                        'drivers'  => 'Drivers',
                    ],

                    'content' => [
                        'overview' => [
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'last_updated' => 'Last Updated',
                            'status'       => 'Status',
                            'timezone'     => 'Timezone',

                        ],
                    ],
                ],
            ],

            'driver' => [
                'management'     => 'Driver Management',
                'create'         => 'Create Driver',
                'active'         => 'Active Drivers',
                'view'           => 'View Driver',
                'edit'           => 'Edit Driver',
                'deleted'        => 'Deleted Drivers',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Last Updated',
                    'name'                 => 'Name',
                    'drivers_licence_number'   => 'Driver Licence Number',
                    'tel_number_1'          => 'Phone Number',
                    'tel_number_2'          => 'Alternate Phone Number',
                    'total'                => 'driver total|Driver total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],
                    'content' => [
                        'overview' => [
                            'avatar'                => 'Avatar',
                            'name'                  =>  'Name',
                            'drivers_licence_number'=>'Drivers Licence Number',
                            'tel_number_1'          => 'Phone Number',
                            'tel_number_2'          =>  'Alternate Phone Number',

                        ],
                    ],
                ],
            ],

            'loader' => [
                'management'     => 'Loader Management',
                'create'         => 'Create Loader',
                'active'         => 'Active Loaders',
                'view'           => 'View Loader',
                'edit'           => 'Edit Loader',
                'deleted'        => 'Deleted Loaders',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Last Updated',
                    'name'                 => 'Name',
                    'total'                => 'loader total|Loader total',
                ],
                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                    ],
                ],
            ],

            'management' => [
                'management'     => 'Management Records',
                'create'         => 'Create Record',
                'active'         => 'Active Records',
                'view'           => 'View Record',
                'edit'           => 'Edit Record',
                'deleted'        => 'Deleted Records',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Last Updated',
                    'name'                 => 'Name',
                    'position'             => 'Position',
                    'total'                => 'record total|Record total',
                ],
                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                    ],
                    'content' => [
                        'overview' => [
                            'position'     =>  'Position',
                            'name'         =>  'Name',

                        ],
                    ],
                ],
            ],

            'omc' => [
                'management'         => 'OMC Management',
                'create'             => 'Create OMC',
                'management'          => 'OMC Management',
                'create'              => 'New',
                'active'             => 'Active OMC',
                'company'            => 'Company view',
                'delete'             =>'Delete',
                'location'           => 'OMC Locations',
                'transactions'        => 'OMC Transactions',
                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Updated At',
                    'name'                 => 'Name',
                    'owner'                => 'Owner',
                ],
            ],
            'contacts' => [
                'management'          => 'Contacts Management',
                'create'              => 'Contact',
                'active'             => 'Active Contact',

                'table' => [
                    'name'                   => 'Name',
                    'email'         => 'Email',
                    'tel1'                 => 'Telephone 1',
                    'tel2'                 => 'Telephone 2',
                ],
            ],

            'importer' => [
                'management'          => 'Importer Management',
                'create'              => 'New',
                'active'              => 'Active Importer',
                'inventory_manage'    => 'Manage Inventory',
                'location'            => 'Importer Locations',
                'transactions'        => 'Importer Transactions',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Updated At',
                    'name'                 => 'Name',
                    'owner'                => 'Owner'
                ],
            ],

            'order' => [
                'management'          => 'Order Management',
                'create'              => 'New Order',
                'view'                => 'View Order Detail',
                'active'              => 'Active Orders',
                'edit'                => 'Edit Order',
                'release'              => 'Create New Release',
                'report'              => 'Order Report',
                'hfo_lpg_report'      => 'HFO/LPG Report',
                'omc_inventory_report'=> 'OMC Inventory Report',
                'loader'              => 'Loader Management',

                'table' => [
                    'id'                   => 'ID',
                    'last_updated'         => 'Last Updated',
                    'importer'             => 'Importer',
                    'omc'                  => 'OMC',
                    'vehicle'              => 'Vehicle',
                    'driver'               => 'Driver',
                    'purchaseOrder'        => 'Purchase Order',
                    'supplier'             => 'Supplier',
                    'release_reference'    => 'Release Order Reference',
                    'loader_name'          => 'Name',

                ],
                'chamberAssignment' => [
                    'chamber_assignment'    => 'Compartment Assignment',
                    'assign_chambers'       => 'Save Compartment Assignment',
                    'quantity_ordered'      => 'Quantity Ordered (LITERS)',
                    'driver_name'           => 'Name Of Driver',
                    'vehicle_number'        => 'Vehicle Number',
                    'quantity'              => 'Quantity',
                    'compartment_number'    => 'Compartment Number',
                    'compartment_capacity'  => 'Compartment Capacity',
                    'assigned_product'      => 'Assigned Product',
                    'assigned_quantity'     => 'Assigned Quantity',
                    'total_quantity_ordered'      => 'Total Quantity Ordered',
                    'truck_loading'         => 'Truck Loading Confirmation',
                    'products_orders'       => 'Products Ordered',
                    'assign_meter_reading'  => 'Record Meter Reading',
                    'loader'                => 'Loader Name',

                ],
                'vehicleUplifting' => [
                    'uplifting_form'        => 'UPLIFTING FORM',
                    'marketer'              => 'Marketer',
                    'release_number'        => 'Release Number',
                    'release_date'          => 'Release Date',
                    'release_balance'       => 'Release Balance (LITERS)',
                    'balance'    => 'Balance b/f',
                    'quantity_now'  => 'Quantity Now',
                    'signature'      => 'Signature',
                    'date'     => 'Date',
                    'authorized_by' => 'Authorized By',
                    'seen_by' => 'Seen By',
                    'truck_load_confirmation' => 'TRUCK LOADING CONFIRMATION',
                    'loader_name' => 'Loader Name',
                    'total_loaded' => 'Total Loaded',
                    'loader_signature' => 'Loader Signature',
                    'truck_loading_ticket_number' => 'Truck Loading Ticket Number',
                    'weight_before_loading' => 'Weight Of Truck Before Loading',
                    'weight_after_loading' => 'Weight Of Truck After Loading',
                    'net_weight' => 'Net Weight',
                    'supervisor_name' => 'Supervisor Name',
                    'control_room_detail' =>'CONTROL ROOM DETAILS',
                ],
                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'products'  => 'Products',
                    ],

                    'content' => [
                        'overview' => [
                            'orderID'    => 'Order Id',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'last_updated' => 'Last Updated',
                            'importer'       => 'Importer',
                            'omc'     => 'OMC',
                            'vehicle'     => 'Vehicle',
                            'driver'     => 'Driver',
                            'purchaseOrder' => 'Purchase Order',
                            'orderReference' => 'Order Reference',
                        ],
                    ],
                ],
            ],
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Register',
            'remember_me'        => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Your password has expired.',
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'update_password_button'           => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'E-mail',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'first_name'         => 'First Name',
                'last_name'          => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],

    ],
];
