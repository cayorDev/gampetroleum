<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Access',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Create User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],

            'vehicle' => [
                'all'             => 'All Vehicles',
                'add'             => 'Add Vehicle',
                'deleted'         => 'Deleted Vehicles',
                'edit'            => 'Edit Vehicle',
                'main'            => 'Vehicles',
                'view'            => 'View Vehicle Details',
            ],

            'driver' => [
                'all'             => 'All Drivers',
                'add'             => 'Add Driver',
                'deleted'         => 'Deleted Drivers',
                'edit'            => 'Edit Driver',
                'main'            => 'Drivers',
                'view'            => 'View Driver Details',
            ],
            'loader' => [
                'all'             => 'All Loaders',
                'add'             => 'Add Loader',
                'deleted'         => 'Deleted Loaders',
                'edit'            => 'Edit Loader',
                'main'            => 'Loaders',
                'view'            => 'View Loader Details',
            ],
            'management' => [
                'all'             => 'All Records',
                'add'             => 'Add Record',
                'deleted'         => 'Deleted Records',
                'edit'            => 'Edit Record',
                'main'            => 'Records',
                'view'            => 'View Record Details',
            ],

            'omc' => [
                'all'             => 'All OMC',
                'add'             => 'Add OMC/Importer',
                'deleted'         => 'Deleted OMC',
                'edit'            => 'Edit OMC',
                'main'            => 'OMC/Importer',
                'view'            => 'View OMC Details',
                'address'         =>'Address OMC',
                'locations'       => 'Locations'
            ],

            'importer' => [
                'all'             => 'All Importers',
                'add'             => 'Add Importer',
                'deleted'         => 'Deleted Importer',
                'edit'            => 'Edit Importer',
                'main'            => 'Importer',
                'view'            => 'View Importer Details',
            ],

            'order' => [
                'all'             => 'All Orders',
                'add'             => 'Create new Order',
                'main'            => 'Orders',
                'release'         => 'Create Release Form',
                'inventoryrelease'=> 'Inventory Release',
                'orderExcel'      => 'Download Excel',
                'orderReport'     => 'Order Transaction',
            ],
        ],

        'log-viewer' => [
            'main'      => 'System Logs',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
            'display'   => 'Display Logs',
        ],

        'omc' => [
            'all'             => 'All OMC',
            'add'             => 'Add OMC',
            'deleted'         => 'Deleted OMC',
            'edit'            => 'Edit OMC',
            'main'            => 'OMC',
            'view'            => 'View OMC Details',
            'address'         =>'Address OMC'
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general'   => 'General',
            'history'   => 'History',
            'system'    => 'System',
            'vehicle'   => 'Vehicles',
            'driver'    => 'Drivers',
            'management'=> 'Management',
            'omc'       => 'OMC',
            'settings'  => 'Settings',
            'orders'    => 'Orders',
            'auditlogs' => 'Audit Logs',
            'reports'    => 'Reports',
            'loader'     => 'Loaders',
        ],

        'orders'  => [
            'PFU'   => 'Pending for Uplifting',
            'OFV'   => 'Load Verification',
            'RO'    => 'Released Orders',
            'AO'    => 'Approve Orders',
            'orderReport'     => 'Order Transaction Report',
            'dailyReport'     => 'HFO/LPG Report',
            'inventoryReport' => 'Omc Inventory Report',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => 'Arabic',
            'zh'    => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da'    => 'Danish',
            'de'    => 'German',
            'el'    => 'Greek',
            'en'    => 'English',
            'es'    => 'Spanish',
            'fa'    => 'Persian',
            'fr'    => 'French',
            'he'    => 'Hebrew',
            'id'    => 'Indonesian',
            'it'    => 'Italian',
            'ja'    => 'Japanese',
            'nl'    => 'Dutch',
            'no'    => 'Norwegian',
            'pt_BR' => 'Brazilian Portuguese',
            'ru'    => 'Russian',
            'sv'    => 'Swedish',
            'th'    => 'Thai',
            'tr'    => 'Turkish',
            'uk'    => 'Ukrainian',
        ],
    ],
];
