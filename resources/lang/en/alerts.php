<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'The user was successfully created.',
            'deleted'             => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored'            => 'The user was successfully restored.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The user was successfully updated.',
            'updated_password'    => "The user's password was successfully updated.",
        ],

        'vehicle' => [

            'created'             => 'The vehicle was successfully created.',
            'deleted'             => 'The vehicle was successfully deleted.',
            'updated'             => 'The vehicle was successfully updated.',
            'restored'            => 'The vehicle was successfully restored.',
            'deleted'             => 'The vehicle was successfully deleted.',
            'createderror'        => 'Error while creating vehicle.',
            'updatederror'        => 'Error while updating vehicle.',
        ],

        'driver' => [
             'created'             => 'The driver was successfully created.',
             'deleted'             => 'The driver was successfully deleted.',
             'updated'             => 'The driver was successfully updated.',
             'deleted_permanently' => 'The driver was deleted permanently.',
             'restored'            => 'The driver was successfully restored.',
        ],

         'loader' => [
             'created'             => 'The loader was successfully created.',
             'deleted'             => 'The loader was successfully deleted.',
             'updated'             => 'The loader was successfully updated.',
             'deleted_permanently' => 'The loader was deleted permanently.',
             'restored'            => 'The loader was successfully restored.',
        ],

        'management' => [
            'created'             => 'The record was successfully created.',
            'deleted'             => 'The record was successfully deleted.',
            'updated'             => 'The record was successfully updated.',
            'deleted_permanently' => 'The record was deleted permanently.',
            'restored'            => 'The record was successfully restored.',
       ],
        'location' => [
            'update'        => 'Locations are successfully updated.',
            'delete'        => 'Location is successfully deleted.',
            'add'           => 'New Location is successfully added.',
        ],

        'contacts' => [
            'delete'    =>  'Contact deleted successfully',

            ],


        'omc' => [
            'create_fail'     => 'Unable to create OMC record.Check data.',
            'create'          => 'OMC record created successfully',
            'create_both'     => 'OMC and Importer record created successfully',
            'deleted'         => 'OMC Deleted successfully',
             'deleted_error'         => 'OMC Could Not Be Deleted successfully',
            'updated'         => 'OMC Updated successfully',
            'address'=>[
                'deleted'          => 'Address Deleted successfully',
                'updated'          => 'Address Updated successfully',
                'delete_fail'      => "Unable to delete address successfully",
                'store'            => 'Address store successfully',

            ],
            'location' => [
                'update'      => 'Locations are successfully updated.',
                'delete'      => 'Location is successfully deleted.',
                'add'         => 'New Location is successfully added.',
            ],
            'contacts' => [
                'created'     => 'The contact was successfully created.',
                'deleted'     => 'The contact was deleted successfully',
                'add'         => 'New Contact is successfully added.',
                'update'      => 'Contact is successfully updated.',
                'both'      => 'Contact is successfully added and updated.',
            ],
        ],
        'importer'    => [
            'deleted'    =>  'Importer deleted successfully',
            'updated'    =>  'Importer updated successfully',
            'create'     => 'Importer record created successfully',
            'deleted_error'         => 'Importer Could Not Be Deleted successfully',
            'address'=>[
                'deleted'          => 'Address Deleted successfully',
                'updated'          => 'Address Updated successfully',
                'delete_fail'      => "Unable to delete address successfully",
                'store'            => 'Address store successfully',
            ],
            'location' => [
                'update'      => 'Locations are successfully added and updated.',
                'delete'      => 'Location is successfully deleted.',
                'add'         => 'New Location is successfully added.',
            ],
            'contacts' => [
                'update'        => 'Contacts are successfully added and updated.',
                'created'       => 'The contact was successfully created.',
                'deleted'       => 'The contact was deleted successfully',
            ],
        ],

        'order' => [
             'created'             => 'The order was successfully created.',
             'deleted'             => 'The order was successfully deleted.',
             'updated'             => 'The order was successfully updated.',
             'deleted_permanently' => 'The order was deleted permanently.',
             'restored'            => 'The order was successfully restored.',
             'createderror'        => 'Error while creating order.',
            'updatederror'         => 'Error while updating order.',
            'deletederror'         => 'Error while deleting order.',
            'products' => [
                'capacityError'     => 'Ordered Quantity Is More Then The Vehicle Capacity',
                'updatedError'      => 'Error While Updating Products. Please Try Again',
            ],
            'chamberAssignment' => [
                'created'           => 'chambers were assigned successfully.',
                'createderror'        => 'Error while assigning chambers.',
            ],
            'Loading' => [
                'created'           => 'Vehicle Load saved Successfully',
                'createderror'        => 'Error while assigning chambers.',
            ],
            'vehicleUpliftment' => [
                'created'           => 'Vehicle upliftment done successfully.',
                'createderror'        => 'Error while vehicle upliftment.',
            ]
        ],
        'release'   => [
            'created'   =>  'The release created successfully',
        ],
        'log'       => [
            'removed'       => 'Log record removed successfully',
            'remove_fail'   => 'Failed to remove log record',
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
            ],
        ],
];
