<?php

return [
	'index' 	=> 'OMC',
	'contacts' 	=> 'Contacts',
	'locations'	=> 'Locations',
	'address'	=> 'Address',
	'importer'  => 'Importer'
];