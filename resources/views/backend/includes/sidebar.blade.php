<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            @if(Auth::user()->isCustomOfficer())
             <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('order/customOrders')) }}" href="{{ route('order.customOrders') }}">
                    <i class="nav-icon icon-basket"></i>
                        @lang('menus.backend.orders.RO')
                </a>
            </li>
            @else
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin()|| Auth::user()->isSupervisor())
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon icon-speedometer"></i> @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>
            @endif

            <!-- Section Start for OMC  -->

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/omc/*')) }}" href="{{ route('admin.omc.index') }}">
                    <i class="nav-icon icon-people"></i> @lang('omc.index')
                </a>
            </li>
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/importer/*')) }}" href="{{ route('admin.importer.index') }}">
                    <i class="nav-icon icon-people"></i> @lang('omc.importer')
                </a>
            </li>
            @endif
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor())
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/release/*')) }}" href="{{ route('admin.release.index') }}">
                    <i class="nav-icon icon-list"></i> @lang('menus.backend.access.order.inventoryrelease')
                </a>
            </li>
            @endif

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('order/*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle " >
                    <i class="nav-icon icon-basket"></i> @lang('menus.backend.sidebar.orders')
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.add') }}">
                            @lang('menus.backend.access.order.add')
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.index', [ 'type' => 'uplifting' ]) }}">
                            @lang('menus.backend.orders.PFU')
                            <span class="badge  badge-warning"> {{$orderCount['uplifting'] ?: 0}}</span>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.index', [ 'type' => 'loadverify' ]) }}">
                            @lang('menus.backend.orders.OFV')
                            <span class="badge  badge-primary"> {{$orderCount['loadverify'] ?: 0}}</span>
                        </a>
                    </li>
                    @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.index', [ 'type' => 'orderforapproval' ]) }}">
                            @lang('menus.backend.orders.AO')
                            <span class="badge  badge-warning"> {{$orderCount['orderforapproval'] ?: 0}}</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.index', [ 'type' => 'released' ]) }}">
                            @lang('menus.backend.orders.RO')
                            <span class="badge  badge-success"> {{$orderCount['released'] ?: 0}}</span>
                        </a>
                    </li>
                     @endif
                </ul>
            </li>
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor())
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('order/*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle " >
                    <i class="nav-icon icon-list"></i> @lang('menus.backend.sidebar.reports')
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.orderReport') }}">
                            @lang('menus.backend.orders.orderReport')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.dailyReport') }}">
                            @lang('menus.backend.orders.dailyReport')

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.inventoryReport') }}">
                            @lang('menus.backend.orders.inventoryReport')

                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/audit/*')) }}" href="{{ route('admin.audit.logs') }}">
                    <i class="nav-icon icon-calculator"></i>  @lang('menus.backend.sidebar.auditlogs')
                </a>
            </li>
            @endif
            <li class="divider"></li>
            <li class="nav-title">
                @lang('menus.backend.sidebar.settings')
            </li>
            <!-- Section End for OMC and its child sections -->

            <li class="divider"></li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/vehicle/index')) }}" href="{{ route('admin.vehicle.index') }}">
                    <i class="nav-icon icon-disc"></i> @lang('menus.backend.sidebar.vehicle')
                </a>
            </li>
            <li class="divider"></li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/driver/index')) }}" href="{{ route('admin.driver.index') }}">
                    <i class="nav-icon icon-people"></i> @lang('menus.backend.sidebar.driver')
                </a>
            </li>
            <li class="divider"></li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/loader/index')) }}" href="{{ route('admin.loader.index') }}">
                    <i class="nav-icon icon-people"></i> @lang('menus.backend.sidebar.loader')
                </a>
            </li>
            <li class="divider"></li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/management/index')) }}" href="{{ route('admin.management.index') }}">
                    <i class="nav-icon icon-people"></i> @lang('menus.backend.sidebar.management')
                </a>
            </li>
            <li class="divider"></li>

            @if ($logged_in_user->isAdmin() || Auth::user()->isSuperAdmin())
            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/auth*')) }}" href="#">
                    <i class="nav-icon icon-ban"></i> @lang('menus.backend.access.title')

                    @if ($pending_approval > 0)
                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                            @lang('labels.backend.access.users.management')

                            @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                            @endif
                        </a>
                    </li>
                    @if($logged_in_user->isAdmin() || Auth::user()->isSuperAdmin())
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                            @lang('labels.backend.access.roles.management')
                        </a>
                    </li>
                    @endif

                </ul>
            </li>


            @endif


            @if(Auth::user()->isSuperAdmin())
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/log-viewer*')) }}" href="#">
                    <i class="nav-icon icon-list"></i> @lang('menus.backend.log-viewer.main')
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}" href="{{ route('log-viewer::dashboard') }}">
                            @lang('menus.backend.log-viewer.dashboard')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('log-viewer::logs.list') }}">
                            @lang('menus.backend.log-viewer.logs')
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @endif

        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
