<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} <a href="#">Gam Petroleum</a></strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <div class="ml-auto">Powered by <a href="http://cayorenterprises.com">Cayor Enterprises</a></div>
</footer>
