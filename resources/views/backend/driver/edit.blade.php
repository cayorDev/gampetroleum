@extends('backend.layouts.app')

@section('title', __('labels.backend.access.driver.management') . ' | ' . __('labels.backend.access.driver.edit'))

@section('breadcrumb-links')
    @include('backend.driver.includes.breadcrumb-links')
@endsection
@section('content')
{{ html()->modelForm($driver, 'PATCH', route('admin.driver.update', $driver->id))->class('form-horizontal')->open() }}
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.driver.management')
                        <small class="text-muted">@lang('labels.backend.access.driver.edit')</small>
                    </h4>
                </div><!--col-->
            </div>
            <hr>
            <div class="row">
                @include('backend.includes.gampetrohead')
            </div>
			<div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.driver.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.driver.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.driver.drivers_licence_number'))->class('col-md-2 form-control-label')->for('drivers_licence_number') }}

                        <div class="col-md-10">
                            {{ html()->text('drivers_licence_number')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.driver.drivers_licence_number'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.driver.tel_number_1'))->class('col-md-2 form-control-label')->for('tel_number_1') }}

                        <div class="col-md-10">
                            {{ html()->text('tel_number_1')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.driver.tel_number_1'))
                                ->attribute('maxlength',20)
                                ->required() }}
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.driver.tel_number_2'))->class('col-md-2 form-control-label')->for('tel_number_2') }}

                        <div class="col-md-10">
                            {{ html()->text('tel_number_2')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.driver.tel_number_2'))
                                ->attribute('maxlength',20)
                                ->required() }}
                        </div><!--col-->
                    </div>
                      {{ html()->hidden('id',$driver->id)
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->required() }}
                </div>
            </div>
           </div>
            <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.driver.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div>
       </div>
 
{{ html()->closeModelForm() }}
@endsection