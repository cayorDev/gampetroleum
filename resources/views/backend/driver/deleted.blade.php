@extends('backend.layouts.app')

@section('title', __('labels.backend.access.driver.management') . ' | ' . __('labels.backend.access.driver.deleted'))

@section('breadcrumb-links')
    @include('backend.driver.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.driver.management')
                    <small class="text-muted">@lang('labels.backend.access.driver.deleted')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

       <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="driver-table">
                        <thead>
                            <tr>
                               <th>@lang('labels.backend.access.driver.table.name')</th>
                               <th>@lang('labels.backend.access.driver.table.drivers_licence_number')</th>
                               <th>@lang('labels.backend.access.driver.table.tel_number_1')</th>
                               <th>@lang('labels.backend.access.driver.table.tel_number_2')</th>
                               <th>@lang('labels.general.actions')</th>
                           </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
               </div>
           </div><!--col-->
       </div><!--row-->
   </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

$('#driver-table').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    ajax: '/admin/driver/getDeletedDriverData',
    columns: [
    { data: 'name', name: 'name' },
    { data: 'drivers_licence_number', name: 'drivers_licence_number' },
    { data: 'tel_number_1', name: 'tel_number_1' },
    { data: 'tel_number_2', name: 'tel_number_2' },
    {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});
</script> 
@endpush
