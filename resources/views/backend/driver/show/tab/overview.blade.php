<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
        	<tr>
                <th>@lang('labels.backend.access.driver.tabs.content.overview.name')</th>
                <td>{{ $driver->name }}</td>
            </tr>
            <tr>
               <th>@lang('labels.backend.access.driver.tabs.content.overview.drivers_licence_number')</th>
               <td>{{ $driver->drivers_licence_number }}</td>
           </tr>
            <tr>
               <th>@lang('labels.backend.access.driver.tabs.content.overview.tel_number_1')</th>
               <td>{{ $driver->tel_number_1}}</td>
           </tr>
            <tr>
               <th>@lang('labels.backend.access.driver.tabs.content.overview.tel_number_2')</th>
               <td>{{ $driver->tel_number_2 }}</td>
           </tr>
        </table>
    </div>
</div><!--table-responsive-->
