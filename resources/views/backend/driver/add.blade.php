@extends('backend.layouts.app')

@section('title', __('labels.backend.access.driver.management') . ' | ' . __('labels.backend.access.driver.create'))

@section('breadcrumb-links')
    @include('backend.driver.includes.breadcrumb-links')
@endsection

@section('content')
    {{ html()->form('POST', route('admin.driver.store'))->class('form-horizontal')->open() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            @lang('labels.backend.access.driver.management')
                            <small class="text-muted">@lang('labels.backend.access.driver.create')</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr>
                <div class="row">
                    @include('backend.includes.gampetrohead')
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.driver.name'))->class('col-md-2 form-control-label')->for('name') }}

                            <div class="col-md-10">
                                {{ html()->text('name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.driver.name'))
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.driver.drivers_licence_number'))->class('col-md-2 form-control-label')->for('drivers_licence_number') }}

                            <div class="col-md-10">
                                {{ html()->text('drivers_licence_number')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.driver.drivers_licence_number'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.driver.tel_number_1'))->class('col-md-2 form-control-label')->for('tel_number_1') }}

                            <div class="col-md-10">
                                {{ html()->text('tel_number_1')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.driver.tel_number_1'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.driver.tel_number_2'))->class('col-md-2 form-control-label')->for('tel_number_2') }}

                            <div class="col-md-10">
                                {{ html()->text('tel_number_2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.driver.tel_number_2'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group--> 
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.driver.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
