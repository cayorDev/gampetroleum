   @extends('backend.layouts.app')

   @section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
   @push('after-styles')
   <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css" />
   <style type="text/css">
     .date_btn{
      display: inline-block;
    }
  </style>
  @endpush

  @section('content')
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fas fa-user"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">OMC</span>
          <span class="info-box-number omc_count">{{$omcCount}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Importers</span>
          <span class="info-box-number importer_count">{{$importersCount}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fas fa-shopping-cart"></i></span>

        <div class="info-box-content">
          <span class="info-box-text"> Orders </span>
          <span class="info-box-number">{{$orderCounts['released']}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fas fa-list-ol"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Products</span>
          <span class="info-box-number">6</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>


  </div><!--row-->

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"
    <!-- ORDER CHART -->
    <div class="box box-primary col-md-12 col-sm-12 col-xs-12">
      <div class="box-header with-border text-center">
        <h3 class="box-title">Orders Stats Overview</h3>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12 ">
            <canvas id="pieChart"></canvas>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12">
              <ul class="chart-legend clearfix">
                <li><i class="fas fa-circle text-red"></i> Orders for  Uplifting ({{$orderCounts['uplifting']}})</li>
                <li><i class="fas fa-circle text-aqua"></i> Order for Load Verification ({{$orderCounts['loadverify']}})</li>
                <li><i class="fas fa-circle text-yellow"></i> Orders for Approval ({{$orderCounts['orderforapproval']}})</li>
                <li><i class="fas fa-circle text-green"></i> Total Orders lifted ({{$orderCounts['released']}})</li>
              </ul>
            </div>
            <div class="col-md-12 mt-3">
              <a href="{{route('order.add')}}" class="btn btn-primary btn-sm">Add new Order </a>
            </div>
            <!-- Date and time range -->

            <!-- /.form group -->
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <div class="box box-primary col-md-12 col-sm-12 col-xs-12">
      <div class="box-header with-border text-center">
        <h3 class="box-title">Orders Transaction Stats</h3>
        <div class="box-tools pull-right">
         <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
          <span></span> 
          <b class="caret"></b>
        </div>
      </div>

    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
          <canvas id="order-transaction"></canvas>
        </div>

      </div>
    </div>
    <!-- /.box-body -->
  </div>


  <!-- /.box -->
  <!--box-->
  @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Importers Stats</h3>
        <!-- <div class="date_btn pull-right">
          <button type="button" class="btn btn-default pull-right" id="daterange-btn">
            <span>
              <i class="fa fa-calendar"></i> {{ !is_null( $range ) ? $range : 'Select date for stats' }}
            </span>
            <i class="fa fa-caret-down"></i>
          </button>
          <a href="{{route('admin.dashboard')}}" class="ml-2 btn btn-danger" title="remove dates"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div> -->
        <div class="row mt-4 mb-4">
          <div class="col">
            @if ( isset($importers) && count($importers) )
            <ul class="nav nav-tabs" role="tablist">
              @foreach($importers as $key => $importer)
              <li class="nav-item">
                <a class="nav-link @if($key=='0') active @endif" data-toggle="tab" href="#importer{{$importer['id']}}" role="tab" aria-controls="importer{{$importer['id']}}" aria-expanded="true"> {{$importer['name']}}</a>
              </li>
              @endforeach
            </ul>

            <!--tab-content-->
            <div class="tab-content">
              @foreach($importers as $key => $importer)
              <div class="tab-pane @if($key=='0') active @endif" id="importer{{$importer['id']}}" role="tabpanel" aria-expanded="true">
                <div class="row">
                  <div class="col-md-4">
                    <div class="table-responsive">
                      <h6 class="text-center">Current Balance</h6>
                      <table class="table no-margin">
                        <tbody>
                          @foreach($importer['balance'] as $product => $quantity)
                          <tr>
                            <td>{{$product}}</td>
                            <td class="@if($quantity > 0) text-green @else text-red @endif ">{{$quantity}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      <hr/>
                      <h6 class="text-center">
                        <a href="{{route('admin.importer.transactions', $importer['id'])}}" class="btn btn-primary btn-sm">View Detailed Transactions</a>
                      </h6>
                    </div>

                  </div>
                  <div class="col-md-8">
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                          <tr>
                            <th>Transaction Date</th>
                            <th>Product</th>
                            <th>Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($importer['transactions']->sortByDesc('created_at')->take(5) as $transaction)
                          <tr>
                            <td>{{$transaction->created_at}}</td>
                            <td>{{$transaction->product->name}}</td>
                            <td class="@if($transaction->transaction_type == 'add') text-green @else text-red @endif ">
                              @if($transaction->transaction_type == 'add') + @else - @endif
                              {{$transaction->quantity}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div><!--tab-->
                @endforeach


              </div><!--tab-content-->
              @else
              <h4 class="text-center">No records available</h4>
              @endif
            </div><!--col-->
          </div><!--row-->

        </div>
        <!-- /.box-body -->
      </div>
      <!--box-->
      @endif
      <!--box-->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">OMC Stats</h3>
          <div class="row mt-4 mb-4">
            <div class="col">
              @if ( isset( $omcs ) && count( $omcs ) )
              <ul class="nav nav-tabs" role="tablist">
                @foreach($omcs as $key => $omc)
                <li class="nav-item">
                  <a class="nav-link @if($key=='0') active @endif" data-toggle="tab" href="#omc{{$omc['id']}}" role="tab" aria-controls="omc{{$omc['id']}}" aria-expanded="true"> {{$omc['name']}}</a>
                </li>
                @endforeach
              </ul>

              <!--tab-content-->
              <div class="tab-content">
                @foreach($omcs as $key => $omc)
                <div class="tab-pane @if($key=='0') active @endif" id="omc{{$omc['id']}}" role="tabpanel" aria-expanded="true">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="table-responsive">
                        <h6 class="text-center">Current Balance</h6>
                        <table class="table no-margin">
                          <tbody>
                            @foreach($omc['balance'] as  $product)
                            <tr>
                              <td>{{$product['importer']}}</td>
                              <td>{{$product['product']}}</td>
                              <td class="@if($product['balance'] > 0) text-green @else text-red @endif ">{{$product['balance']}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <hr/>
                        <h6 class="text-center">
                          <a href="{{route('admin.omc.transactions', $omc['id'])}}" class="btn btn-primary btn-sm">View Detailed Transactions</a>
                        </h6>
                      </div>

                    </div>
                    <div class="col-md-8">
                      <div class="table-responsive">
                        <table class="table no-margin">
                          <thead>
                            <tr>
                              <th>Transaction Date</th>
                              <th>Product</th>
                              <th>Quantity</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($omc['transactions']->sortByDesc('created_at')->take(5) as $transaction)
                            <tr>
                              <td>{{$transaction->created_at}}</td>
                              <td>{{$transaction->product->name}}</td>
                              <td class="@if($transaction->transaction_type == 'add') text-green @else text-red @endif ">@if($transaction->transaction_type == 'add') + @else - @endif
                                {{$transaction->quantity}}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div><!--tab-->
                  @endforeach
                </div><!--tab-content-->
                @else
                <h4 class="text-center">No records available</h4>
                @endif
              </div><!--col-->
            </div><!--row-->

          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    @endsection

    @push('after-scripts')
    <script type="text/javascript" src="/js/Chart.min.js"></script>
    <script type="text/javascript" src="/js/moment.min.js"></script>
    <script type="text/javascript" src="/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/js/dashboard.js"></script>

    <script src="{{ asset('js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />
    @endpush
