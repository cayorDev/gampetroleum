@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.omc.transactions'))

@section('breadcrumb-links')
@include('backend.omc.includes.breadcrumb-links')
@endsection

@push('after-styles')
<style type="text/css">
    .full{
        width: 100%;
    }
    .transactions {
        text-align: center;
        max-height: 250px;
        overflow-y: scroll;
    }
</style>
@endpush
@section('content')
<div class="card"  id = "transactionsForm">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3>@lang('strings.backend.omc.transactions') for OMC&nbsp;&nbsp;&RightArrow;&nbsp;&nbsp;<span style = "color:#3c8dbc; text-transform: capitalize;">{{$omc->company_name}}</span></h3>

                <div class="col-md-12" style="display: flex;justify-content: flex-end;">
                    {{ html()->select('product_id')
                    ->class("pull-right mr-3 float-right p-2 showHide")
                    ->options($products)
                    ->attribute('readonly', true)
                    ->attribute('id', 'product_id')
                    ->required() }} &nbsp;&nbsp;
                    <div id="reportrange" class="mr-3 float-right showHide" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 250px;">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                        <span></span>
                        <b class="caret"></b>
                    </div>

                   &nbsp;&nbsp;
                   <button class="btn btn-danger mr-3 float-right showHide" onclick="printpage();"><i class="fas fa-print" data-toggle="tooltip" title="@lang('buttons.omc.print')"></i>&nbsp; @lang('buttons.omc.print') </button>
                   &nbsp;&nbsp;
                   <a href="{{route('admin.omc.excelTransactions', $omc['id'])}}" style="display: flex;align-items: center;" class="btn btn-primary ml-1 float-right showHide exportlink ">
                    <i class="fas fa-download" data-toggle="tooltip" title="@lang('buttons.omc.excel')"></i>&nbsp;@lang('buttons.omc.excel') </a>
                </div>
                </div><!--btn-toolbar-->
            </div><!--row-->
        </div><!--card-header-->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 transaction">

                </div>
            </div>
        </div>
    </div><!--card-->
    @endsection

    @push('after-scripts')
    <script src="{{ asset('/js/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('/js/daterangepicker.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />
    <script>
      initializeDatepicker();
      function initializeDatepicker(){
          var start = moment().startOf('month');
          var end =  moment().endOf('month');

          function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
         }

     }, cb);

        cb(start, end);
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {

            start1=picker.startDate.format('YYYY-MM-DD');
            end1=picker.endDate.format('YYYY-MM-DD');

            getOmcTransactionData(start1,end1, $('#product_id').val());

        });

        start1=start.format('YYYY-MM-DD');
        end1=end.format('YYYY-MM-DD');
        getOmcTransactionData(start1,end1, $('#product_id').val());
    }

    $('#product_id').change(function(){
    let startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    let endDate=  $("#reportrange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    getOmcTransactionData(startDate,endDate, $('#product_id').val());
  });

    function getOmcTransactionData(start, end,product=0)
    {
        omc = "{{$omc->id}}";
        $('.exportlink'). attr("href", 'excelTransactions?start='+start+'&end='+end+'&omc='+omc+'&product='+product);
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method: 'POST',
          url: '/admin/omc/getOmcTransactionData',
          data: {'start' : start,'end' : end, 'omc' : omc,'product':product},
          success: function(response){
              $('.transaction').html(response);
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
              alert("Please Try Again");
          }
      });
    }
    function printpage()
    {
        var elements = document.getElementsByClassName("showHide");
        for (var i = 0; i < elements.length; i++){
            elements[i].style.display = 'none';
        }
        var printContents = document.getElementById("transactionsForm").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        for (var i = 0; i < elements.length; i++){
            elements[i].style.display = 'block';
        }
    }
</script>
@endpush
