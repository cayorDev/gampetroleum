<div class="btn-toolbar float-right" id="contacts_add" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <span class="btn btn-success btn-sm" data-toggle="tooltip" title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i> @lang('buttons.omc.add') </span>
</div><!--btn-toolbar-->



