@extends('backend.layouts.app')

@section('title', __('labels.backend.access.omc.management') . ' | ' . __('labels.backend.access.omc.location'))

@section('breadcrumb-links')
    @include('backend.omc.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card" id="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-9">
        <h4 class="card-title mb-0">
          @lang('labels.backend.access.omc.management')
          <small>@lang('labels.backend.access.omc.location')</small>
        </h4>
      </div><!--col-->
      <div class="col-sm-3">
      <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('buttons.omc.new')">
        <a href="javascript:void(0)" id="addLocations" class="btn btn-sm btn-success" data-toggle="tooltip" title="@lang('buttons.omc.new')"><i class="fas fa-plus-circle"></i>  @lang('buttons.omc.add')</a>
                        
      </div><!--btn-toolbar-->
    </div><!--col-->
    </div><!--row-->
    <hr>
    {{ html()->form('post', route('admin.omc.storeLoc',$ids))->class('form-horizontal')->id('Loc-form')->open() }}
        {{ csrf_field() }}
        <div class="block" id="main_container_locations">
          <input type="hidden" id="locations_count" name="locations_count" value="{{isset($errors) && count($errors)>0 && !is_null(old('locations_count'))? old('locations_count'): count($data)}}">
          <!--clone div -->
          <div class="row mt-4 mb-4" id="mainlocations"  style="display: {{count($data) > 0 ? 'none':'block'}}">    
              <div class="col-md-12">
                  <div class="form-group row">
                      {{ html()->hidden("locations[0][id]")
                        ->id('loc_id')
                        ->class('form-control formInput')
                        ->disabled(count($data)> 0 ? true:false)
                      }}
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.name'))->class('col-md-2 form-control-label')->for('name') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[0][name]")
                            ->class('form-control locname')
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.name'))
                            ->disabled(count($data)> 0 ? true:false)
                            ->attribute('maxlength', 191)
                            ->required(count($data) > 0 ? false:true) 
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.omc.locations.contact_name'))->class('col-md-2 form-control-label')->for('contact_name') }}
                    <div class="col-md-9">
                        {{ html()->text("locations[0][contact_name]")
                          ->class('form-control con_name')
                          ->placeholder(__('validation.attributes.backend.access.omc.locations.contact_name'))
                          ->disabled(count($data)> 0 ? true:false)
                          ->attribute('maxlength', 191)
                          ->required(count($data) > 0 ? false:true) 
                        }}
                    </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel1'))->class('col-md-2 form-control-label')->for('tel1') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[0][tel1]")
                              ->class('form-control tel1')
                              ->placeholder(__('validation.attributes.backend.access.omc.locations.tel1'))
                              ->disabled(count($data)> 0 ? true:false)
                              ->attribute('maxlength', 191)
                              ->required(count($data) > 0 ? false:true) 
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel2'))->class('col-md-2 form-control-label')->for('tel2') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[0][tel2]")
                              ->class('form-control tel2')
                              ->placeholder(__('validation.attributes.backend.access.omc.locations.tel2'))
                              ->disabled(count($data)> 0 ? true:false)
                              ->attribute('maxlength', 191)
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
              </div>
          </div>
          <!--foreach for data-->
          @if( !is_null( $data ) )
          @foreach($data as $key => $location)
              <div class="row mt-4 mb-4 div_{{$key}}">    
                  <div class="col-md-10" style="border-right: 1px solid rgba(0,0,0,.1);">
                      <div class="form-group row">
                        {{ html()->hidden("locations[$key][id]")
                          ->id('loc_id')
                          ->class('form-control formInput')
                          ->value($location->id)
                        }}
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-9">
                          {{ html()->text("locations[$key][name]")
                            ->class('form-control formInput')
                            ->value($location->name)
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.name'))
                            ->attribute('maxlength', 191)
                            ->attribute('readonly', true)
                            ->required()
                            ->autofocus() 
                          }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.contact_name'))->class('col-md-2 form-control-label')->for('contact_name') }}
                        <div class="col-md-9">
                          {{ html()->text("locations[$key][contact_name]")
                            ->class('form-control formInput')
                            ->value($location->contact_name)
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.contact_name'))
                            ->attribute('maxlength', 191)
                            ->attribute('readonly', true)
                            ->required() 
                          }}
                        </div><!--col-->
                      </div><!--form-group-->

                      <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel1'))->class('col-md-2 form-control-label')->for('tel1') }}
                        <div class="col-md-9">
                          {{ html()->text("locations[$key][tel1]")
                            ->class('form-control formInput')
                            ->value($location->tel1)
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.tel1'))
                            ->attribute('maxlength', 191)
                            ->attribute('readonly', true)
                            ->required() 
                          }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel2'))->class('col-md-2 form-control-label')->for('tel2') }}
                        <div class="col-md-9">
                          {{ html()->text("locations[$key][tel2]")
                            ->class('form-control formInput')
                            ->value($location->tel2)
                            ->attribute('readonly', true)
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.tel2'))
                            ->attribute('maxlength', 191)
                          }}
                        </div><!--col-->
                      </div><!--form-group-->
                  </div>
                  <div class="col-md-2">

                      <div class="form-group row" style="margin: 10px;">
                          {{ html()->span('<i class="fa fa-edit"></i> Edit')->id('edit')->class('btn btn-sm btn-primary col-sm-12 btnfirst')->attribute('div-name', 'div_'.$key) }}
                      </div>
                      <div class="form-group row" style="margin: 10px;">
                          <a href="{{ route('admin.omc.deleteLoc',$location->id)}}" id='del' class="btn btn-sm btn-danger col-sm-12 btnfirst" onclick="return confirm('Do you want to delete OMC-Location ?')" div-name ="div_{{$key}}"><i class="fa fa-trash"></i> Delete</a>
                      </div>
                      <div class="form-group row btnsecond" style="margin: 10px;display:none;">
                      {{ html()->span('<i class="fa fa-arrow-left float-left" style="margin-top: 3px;"></i> Back')->id('back')->class('btn btn-sm btn-danger col-md-12 ')->attribute('div-name', 'div_'.$key) }}
                    </div>
                  </div>
              </div>  

              <hr>
          @endforeach<!--end foreach-->
          @endif
                    
          @if(isset($errors) && count($errors)>0 && !is_null(old('locations_count')))
          <!--for each for errors-->
              @for($a=count($data)+1; $a<=old('locations_count'); $a++)
          <!--clone div -->
          <div class="contact">
          <div class="row mt-4 mb-4">    
              <div class="col-md-10" style="border-right: 1px solid rgba(0,0,0,.1);">
                  <div class="form-group row">
                      {{ html()->hidden("locations[$a][id]")
                        ->id('loc_id')
                        ->class('form-control formInput')
                      }}
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.name'))->class('col-md-2 form-control-label')->for('name') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[$a][name]")
                            ->class('form-control locname')
                            ->placeholder(__('validation.attributes.backend.access.omc.locations.name'))
                            ->value(old("locations.$a.name"))
                            ->attribute('maxlength', 191)
                            ->required() 
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.omc.locations.contact_name'))->class('col-md-2 form-control-label')->for('contact_name') }}
                    <div class="col-md-9">
                        {{ html()->text("locations[$a][contact_name]")
                          ->class('form-control con_name')
                          ->placeholder(__('validation.attributes.backend.access.omc.locations.contact_name'))
                          ->value(old("locations.$a.contact_name"))
                          ->attribute('maxlength', 191)
                          ->required(count($data) > 0 ? false:true) 
                        }}
                    </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel1'))->class('col-md-2 form-control-label')->for('tel1') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[$a][tel1]")
                              ->class('form-control tel1')
                              ->placeholder(__('validation.attributes.backend.access.omc.locations.tel1'))
                              ->value(old("locations.$a.tel1"))
                              ->attribute('maxlength', 191)
                              ->required(count($data) > 0 ? false:true) 
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
                  <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel2'))->class('col-md-2 form-control-label')->for('tel2') }}
                      <div class="col-md-9">
                          {{ html()->text("locations[$a][tel2]")
                              ->class('form-control tel2')
                              ->placeholder(__('validation.attributes.backend.access.omc.locations.tel2'))
                              ->value(old("locations.$a.tel2"))
                              ->attribute('maxlength', 191)
                          }}
                      </div><!--col-->
                  </div><!--form-group-->
              </div>
              <div class="col-md-2">
                <a href="javascript:void(0);" class="btn btn-danger ml-1 remove" data-toggle="tooltip" title="remove"><i class="fas fa-minus-circle"></i>Remove</a>
              </div>
          </div>
          <hr>
        </div>
        @endfor
        @endif
        </div>
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">

                    {{ form_cancel(route('admin.omc.show', $ids), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('<i class="fa fa-save"></i> '). __('buttons.general.crud.create'))->id('save')->class('btn btn-success')}}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    {{html()->form()->close() }}
  </div><!--card-body-->
</div><!--card--> 
@endsection

@push('after-scripts')
<script type="text/javascript" src="/js/omc.js"></script>
@endpush