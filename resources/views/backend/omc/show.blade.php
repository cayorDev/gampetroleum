@extends ('backend.layouts.app')
@section('title', app_name() . ' | ' . __('labels.backend.access.omc.management'))

@section('breadcrumb-links')
@include('backend.omc.includes.breadcrumb-links')
@endsection
@section('content')

<div class="card" style="background: #23a2ce; border-radius: 4px;text-align: center;position: relative; padding: 10px;" >
    <div class="icon" style="font-size: 76px;color: rgba(0,0,0,0.15);position: absolute; right:12px; bottom:12px;">
        <i class="fa fa-gas-pump"></i>
    </div>
    <div class="inner"style="text-align: left; width:100%;  padding:10px; color:white;" >
        <h1>{{$importer->productstransactions[$count]->quantity}}</h1>
        <h3>{{$products}}</h3> 
    </div>
</div>


{{ html()->form('POST', route('admin.omc.store'))->class('form-horizontal')->open() }}
@csrf 
<div class="card" id='card'>
    <div class="card-body">

        <div class="row">
            <div class="col-sm-11">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.omc.management')
                    <small>@lang('labels.backend.access.omc.location')</small>

                </h4>
            </div>

            <div class='col-sm-1' id='addCard'>
                <span class='btn btn-success'><i class='fa fa-plus'></i></span>
            </div>

            <!--col-->
        </div><!--row-->
        <!--row-->

        <hr id='hrTag'>
        @foreach($data as $value)

        <div class='overlay'>
            <div class="card childCard">
                <div class="card-body">
                    <input type='hidden' id='hidden_id' value="($value->id)">


                    <div class="row" id='cardDiv'>
                        <div class='col-md-10'>
                            <div class="row mt-4">
                                <div class='col'>
                                    <div class="form-group row">

                                        {{ html()->label(__('validation.attributes.backend.access.omc.name'))
                                        ->class('col-md-2 form-control-label')
                                        ->for('name') }}

                                        <div class="col-md-10">
                                            {{ html()->text('name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.omc.name'))
                                            ->readonly()
                                            ->value($value->name)
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                                        </div><!--col-->
                                    </div><!--form-group-->

                                </div>
                            </div>
                            <!--form-group-->
                            <!--col-->

                            <!--row-->
                            <!--card-body-->
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="form-group row">
                                        {{ html()->label(__('validation.attributes.backend.access.omc.contact_name'))
                                        ->class('col-md-2 form-control-label')
                                        ->for('name') }}

                                        <div class="col-md-10">
                                            {{ html()->text('contact_name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.omc.contact_name'))
                                            ->readonly()
                                            ->value($value->contact_name)
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                                        </div><!--col-->

                                    </div><!--form-group-->

                                </div><!--form-group-->
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="form-group row">
                                        {{ html()->label(__('validation.attributes.backend.access.omc.tel1'))
                                        ->class('col-md-2 form-control-label')
                                        ->for('tel1') }}

                                        <div class="col-md-10">
                                            {{ html()->text('tel1')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.omc.tel1'))
                                            ->value($value->tel1)
                                            ->readonly()
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                                        </div>

                                    </div><!--form-group-->

                                </div><!--form-group-->
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="form-group row">
                                        {{ html()->label(__('validation.attributes.backend.access.omc.tel2'))
                                        ->class('col-md-2 form-control-label')
                                        ->for('tel2') }}

                                        <div class="col-md-10">
                                            {{ html()->text('tel2')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.omc.tel1'))
                                            ->value($value->tel2)
                                            ->readonly()
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                                        </div><!--col-->
                                    </div><!--form-group-->

                                </div><!--form-group-->
                            </div>
                        </div>

                        <div class='col-md-2'>
                            <div class='col-md-2' id="editBtn">
                                <span class='btn btn-primary'><i class='fa fa-pen'> </i></span>
                            </div>
                            <br>
                            <div class='col-md-2' id="removeBtn">           
                             <a  href="{{ route('admin.omc.delete',$value->id)}}" class='btn btn-danger' onclick="return confirm('do you want to delete order ? ')"><i class='fa fa-trash'> </i></a>
                         </div>

                     </div>
                 </div>
             </div>
         </div>
     </div>
     @endforeach
     <div class='card-footer'>
        <div class="row" id='#footer'>
            <div class="col text-right">
                {{html()->button(__('buttons.general.save'))
                ->class('btn btn-primary btn-sm')
            }}
            {{ form_submit(__('buttons.general.crud.update')) }}

        </div>   
    </div>
</div>
</div><!-- cardbody -->
</div><!--card-->
{{ html()->closeModelForm() }}
<script src="{{ asset('/js/jquery.min.js')}}"></script>
<script>

let old_count=$('.childCard').length;
let content=$('.overlay').clone();
let new_count=old_count+1;
// content=$(content).clone()


$('#card').on('click','#addCard',function({



    $(content).insertAfter('.overlay:last').find('input').val('').removeAttr('readonly');

});


$(content).on('click','#removeBtn',function(){
    $(this).find('.childCard').remove();


});

$(content).on('click','#editBtn',function(){

    $('.childcard').find('input').attr('readonly',false);

});



$('.childCard').on('click','#removeBtn',function(){

    $(this).find('.childCard').remove();   
});

$('.childCard').on('click','#editBtn',function(){

    $(this).closest('.childCard').find('input').removeAttr('readonly');
    
});


</script>
@endsection
