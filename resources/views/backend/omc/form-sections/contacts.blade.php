<div class="col-md-12" id="main_container_for_contacts">
    <h3 class="text-center">Contacts</h3>
        <div class="btn" role="toolbar" aria-label="@lang('buttons.omc.new')">
            <a href="javascript:void(0)" id="replicateContacts" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('buttons.omc.new')"><i class="fas fa-plus-circle"></i>  @lang('buttons.omc.add')</a>
            
        </div><!--btn-toolbar-->
    <input type="hidden" id="contact_count" value="0"/>
    {{ html()->hidden()
            ->id('contact_count')
            ->value(0) }}
    <div class="col-md-12 " id ="main_contact">
        <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.name'))->class('col-md-2 col-sm-3 form-control-label')->for('name') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text("contact[0][name]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.contacts.name'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
    {{ html()->label(__('validation.attributes.backend.access.omc.contacts.email'))->class('col-md-2 col-sm-3  form-control-label')->for('email') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->email("contact[0][email]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.contacts.email'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel1'))->class('col-md-2 col-sm-3 form-control-label')->for('tel1') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text("contact[0][tel1]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel1'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel2'))->class('col-md-2 col-sm-3 form-control-label')->for('tel2') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text("contact[0][tel2]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel2'))
            }}
        </div><!--col-->
    </div><!--form-group-->

    </div>
</div><!--col for omc basic information-->
