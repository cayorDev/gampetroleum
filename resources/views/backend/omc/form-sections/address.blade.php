<!-- Col for basic address information -->
<div class="col-md-6">
    <h3 class="text-center">Address</h3>
    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.address.street'))->class('col-md-3 col-sm-3  form-control-label')->for('street') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text("address[street]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.address.street'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
    {{ html()->label(__('validation.attributes.backend.access.omc.address.city'))->class('col-md-3 col-sm-3 form-control-label')->for('city') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text("address[city]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.address.city'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.address.country'))->class('col-md-3 col-sm-3 form-control-label')->for('country') }}

        <div class="col-md-9 col-sm-3 ">
            {{ html()->text("address[country]")
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.address.country'))
                ->value('Gambia')
                ->attribute('maxlength', 191)
              
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->
</div><!--col for omc basic information-->