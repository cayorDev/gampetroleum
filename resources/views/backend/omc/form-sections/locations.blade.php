<!-- Col for basic address information -->
            <div class="col-md-12" id="main_container_for_locations">
                <h3 class="text-center">Locations</h3>
                    <div class="btn" role="toolbar" aria-label="@lang('buttons.omc.new')">
                        <a href="javascript:void(0)" id="replicateLocations" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('buttons.omc.new')"><i class="fas fa-plus-circle"></i>  @lang('buttons.omc.add')</a>
                        
                    </div><!--btn-toolbar-->
                <input type="hidden" id="locations_count" value="0"/>
                <div class="col-md-12" id="main_location">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.omc.locations.name'))->class('col-md-2 col-sm-3 form-control-label')->for('name') }}

                        <div class="col-md-9 col-sm-9">
                            {{ html()->text("locations[0][name]")
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.omc.locations.name'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.omc.locations.contact_name'))->class('col-md-2 col-sm-3 form-control-label')->for('contact_name') }}

                        <div class="col-md-9 col-sm-9">
                            {{ html()->text("locations[0][contact_name]")
                                ->class('form-control location_field')
                                ->placeholder(__('validation.attributes.backend.access.omc.locations.contact_name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel1'))->class('col-md-2 col-sm-3 form-control-label')->for('tel1') }}

                        <div class="col-md-9 col-sm-9">
                            {{ html()->text("locations[0][tel1]")
                                ->class('form-control location_field')
                                ->placeholder(__('validation.attributes.backend.access.omc.locations.tel1'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.locations.tel2'))->class('col-md-2 col-sm-3 form-control-label')->for('tel2') }}

                        <div class="col-md-9 col-sm-9">
                            {{ html()->text("locations[0][tel2]")
                                ->class('form-control location_field')
                                ->placeholder(__('validation.attributes.backend.access.omc.locations.tel2'))
                                ->attribute('maxlength', 191) }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div> 
@push('after-styles')
<style type="text/css"> 
    #main_location{
        border-right: 1px solid rgba(0,0,0,.1);   
    }
</style>
@endpush     