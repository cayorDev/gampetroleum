<!-- Col for basic address information -->
            <div class="col-md-12" id="main_container_for_products">
                <h3 class="text-center">Products Inventory</h3>
                <div class="col-md-12" id="main_product">
                    <div class="form-group col-md-12" style="display: inline-block;">
                    {{ html()->label(__('validation.attributes.backend.access.omc.products.select'))->class('col-md-12 text-center form-control-label')->for('products') }}
                        <div class="col-md-12">
                            {{ html()->multiselect("products[count]")
                                ->class('form-control products_select')
                                ->options($products)
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="products_inventory_container">
                        
                    </div>

                </div>
            </div> 
     