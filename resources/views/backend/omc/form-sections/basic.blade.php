<div class="col-md-6 basic">
    <h3 class="text-center">Details</h3>
    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.company'))->class('col-md-3 col-sm-3 form-control-label')->for('company') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text('company_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.company'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
    {{ html()->label(__('validation.attributes.backend.access.omc.owner'))->class('col-md-3 col-sm-3 form-control-label')->for('owner') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text('owner_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.owner'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->

    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.email'))->class('col-md-3 col-sm-3 form-control-label')->for('email') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->email('email')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.email'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->
    <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.omc.tel'))->class('col-md-3 col-sm-3 form-control-label')->for('tel') }}

        <div class="col-md-9 col-sm-9">
            {{ html()->text('tel')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.omc.tel'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--col-->
    </div><!--form-group-->
</div><!--col for omc basic information-->

@push('after-styles')
<style type="text/css"> 
    .basic{
        border-right: 1px solid rgba(0,0,0,.1);   
    }
</style>
@endpush