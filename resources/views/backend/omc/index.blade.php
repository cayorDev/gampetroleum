@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.omc.management'))

@section('breadcrumb-links')
    @include('backend.omc.includes.breadcrumb-links')
@endsection

@push('after-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
    <style type="text/css">
        .confirmation
        {
            display:none;
            border:solid 1px black;
            border-radius:2px;
            position: absolute;
            top:-6px;right:0px;
            /*padding:1px;*/
            background: white;
            z-index: 99;
        }
        .confirm
        {
            float: left;
            color: white;
            padding: 1px 9px;
        }

        .success {
            color: green;
        }

        .deny {
            color: indianred;
        }
    </style>
@endpush

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.omc.management') }} <small class="text-muted">{{ __('labels.backend.access.omc.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.omc.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
            
                <div class="table-responsive">
                    <table class="table table-bordered" id="omc-table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.omc.table.id')</th>
                            <th>@lang('labels.backend.access.omc.table.name')</th>
                            <th>@lang('labels.backend.access.omc.table.owner')</th>
                            <th>@lang('labels.backend.access.omc.table.last_updated')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        
            $(function() {
                $('#omc-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('admin.omc.data') !!}',
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'owner', name: 'owner' },
                        { data: 'updated_at', name: 'updated_at' },
                        { data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
             });
            $(".card").on("click",".delete",function(){
                $(this).parent().parent().find('.confirmation').toggle(200);
            });
            $(".card").on("click",".no",function(){
                $(this).parent().hide(200);
            });
    </script> 

@endpush
