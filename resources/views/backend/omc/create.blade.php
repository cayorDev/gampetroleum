@extends('backend.layouts.app')

@section('title', $meta['title'])

@section('breadcrumb-links')
    @include('backend.omc.includes.breadcrumb-links')
@endsection

@push('after-styles')
<style type="text/css">
    .type_select{
        margin:auto; 
        font-weight: bold; 
        font-size: 17px;
    }
</style>
@endpush

@section('content')
<script>
    var importers = {};
    @foreach( $importers as  $i )
        importers[ {{ $i['id'] }} ] = "{{ $i['company_name'] }}";
    @endforeach
</script>    
    {{ html()->form('POST', route('admin.omc.store'))->class('form-horizontal')->open() }}
    
        <div class="card">
            <?php
                if(Session::has('errors')){
                (\Session::all());
            }?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{$meta['title']}}
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                {{html()->hidden('url')->value($url)}}

                <hr>
                <div class="row">
                    @include('backend.includes.gampetrohead')
                </div>
                <!-- Contacts section for omc -->
                <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group col-md-4 type_select">
                            {{ html()->label(__('validation.attributes.backend.access.omc.type'))->class('col-md-12 text-center form-control-label bold')->for('type') }}
                        </div>    
                        <div class="form-group col-md-8 col-sm-3" style="margin:0 auto 15px auto;">
                            {{ html()->select("type")
                                ->class('form-control selector_type')
                                ->options(['OMC', 'Importer'])
                                ->value(old('type') ? old('type') : $meta['value'])
                                ->id('type')
                                ->required() }}
                        </div><!--col-->
                        
                    </div>    
                    @include('backend.omc.form-sections.basic')    

                    @include('backend.omc.form-sections.address')
                </div><!--row-->

                <hr>

                <!-- Basic locations section for omc-->
                <div class="row contacts_container">
                    @include('backend.omc.form-sections.contacts')
                     
                </div><!--row-->
                
               <hr class="hr locations_hr"> 

                <div class="row locations_container">
                    
                    @include('backend.omc.form-sections.locations')

                </div><!--row -->

               <hr class="hr">

                <div class="row">
                    
                    @include('backend.omc.form-sections.products')

                </div><!--row -->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.omc.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
<script type="text/javascript" src="/js/omc.js"></script>
@endpush
