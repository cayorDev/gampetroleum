@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.omc.management'))
@section('breadcrumb-links')
@include('backend.omc.includes.breadcrumb-links')
@endsection

@push('after-styles')
<style type="text/css">
.transactions {
    text-align: center; 
    max-height: 250px; 
    overflow-y: scroll;
}
</style>    
@endpush

@section('content')
<div class="card">
    <div class="card-header">

        <div class="row">
            <div class="col-sm-6">
                <h4 class="card-title mb-0">
                    {{$omc->company_name}}
                </h4>
            </div><!--col-->

            <div class="col-md-6 col-sm-6 pull-right">
                <a href="{{ route('admin.omc.index') }}" title="Back" class="btn btn-danger ml-1 float-right"><i class="fas fa-arrow-left"></i> Back</a>

                <a href="{{ route('admin.omc.contacts', $omc->id) }}" class="btn btn-success ml-1 float-right"  >
                    <i class="fas fa-address-card" data-toggle="tooltip" title="@lang('buttons.omc.contacts')"></i>&nbsp;@lang('buttons.omc.contacts') </a>

                    <a href="{{ route('admin.omc.location', $omc->id) }}" class="btn btn-success ml-1 float-right" ><i class="fas fa-location-arrow" data-toggle="tooltip" title="@lang('buttons.omc.location')"></i>&nbsp;@lang('buttons.omc.location')</a>
                </div><!--col-->
            </div><!--row-->
        </div>

        <div class='card-body'>
            <div class=row>
                @foreach($products->groupBy('product_id') as $key => $product)
                <div class="col-md-4 col-xs-2" style="margin:auto; ">
                    <div class="card" style="background: #b8ca84; border-radius: 4px;text-align: center;position: relative; padding: 10px;" >
                        <div class="icon" style="font-size: 30px;color: rgba(0,0,0,0.15);position: absolute; right:12px; bottom:12px;">
                          <i class="fa fa-gas-pump "></i>
                      </div>
                      <div class="inner"style="text-align: left; width:100%;  padding:10px; color:white;" > 
                        <h3 style="color:#6a6f5b;">{{$product->first()['name']}} ( {{array_sum(array_column($product->toArray(),'balance'))}} )  </h3> 
                        @foreach ($product as $pro)
                        <h6 class="product @if($pro['balance'] > 0 ) text-green @else text-red @endif">{!! $pro['omc_importer_name']. $pro['importer_name'] .'( <b>' .$pro['balance'].' </b>)'!!}</h6>
                        @endforeach 

                    </div>
                </div>
            </div>
            @endforeach

        </div>

        <div class='row' style="margin-left: 2%; margin-right: 2%">
            <div class='col-md-6'>
                <div class="card">
                    {{ html()->modelForm($omc, 'PATCH', route('admin.omc.update',$omc->id))->class('form-horizontal')->attribute('id','omcform')->open()}}
                    @csrf
                    <div class="card-body"> 
                        <div class="row mt-4 mb-4">
                            <div class="col">
                                <div class="form-group row">
                                    {{ html()->label(__('validation.attributes.backend.access.omc.company'))->class('col-md-4 form-control-label')->for('company_name') }}
                                    <div class="col-md-8">
                                        {{ html()->text("company_name")
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.company'))
                                        ->attribute('maxlength', 191)
                                        ->readonly()
                                        ->required() }}
                                    </div><!--col-->
                                </div><!--form-group-->
                                <div class="form-group row">
                                    {{ html()->label(__('validation.attributes.backend.access.omc.owner'))->class('col-md-4 form-control-label')->for('owner_name') }}
                                    <div class="col-md-8">
                                       {{ html()->text('owner_name')
                                       ->class('form-control')
                                       ->placeholder(__('validation.attributes.backend.access.omc.owner'))
                                       ->attribute('maxlength', 191)
                                       ->readonly()
                                       ->required() }}
                                   </div><!--col-->
                               </div><!--form-group--> 
                               <div class="form-group row">
                                 {{ html()->label(__('validation.attributes.backend.access.omc.email'))->class('col-md-4 form-control-label')->for('email') }}
                                 <div class="col-md-8">
                                    {{ html()->email('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.email'))
                                    ->attribute('maxlength', 191)
                                    ->readonly()
                                    ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->
                        </div><!--column -->
                    </div><!-- row -->
                </div><!-- cardbody -->
                <div class="card-footer">
                    <div class="row">
                        <div class="col">
                            {{ html()->button('<i class="fas fa-edit fa-lg"data-toggle="tooltip" title="'.__('buttons.omc.edit').'"></i> '.__('buttons.omc.edit').'')->class("btn btn-primary col-md-12")->attribute('id', 'edit')}}
                            {{ form_submit(__('<i class="far fa-save fa-2x" data-toggle="tooltip" title="'.__('buttons.omc.update'). '"></i> '.__('buttons.omc.update').''))->attributes(['id'=>'update','hidden'=>'true'])->class("col-md-12") }}
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-footer-->
                {{ html()->closeModelForm() }}
            </div><!-- card -->
        </div> <!-- column -->
        @if(!is_null($address))
        <div class='col-md-6 col-sm-12 col-xs-12'>
            <div class='card'>
                {{ html()->modelForm($omc, 'PATCH', route('admin.omc.addressupdate',$omc->Addresses->id))->class('form-horizontal')->attribute('id','update_address')->open()}}
                <div class='card-body'>
                 <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.omc.address.street'))->class('col-md-4 form-control-label')->for('street') }}
                            <div class="col-md-8" name="address">
                                @csrf
                                {{ html()->text('street')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.omc.address.street'))
                                ->attribute('maxlength', 191)
                                ->value($omc->Addresses->street)
                                ->readonly()
                                ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row" name="address">
                            {{ html()->label(__('validation.attributes.backend.access.omc.address.city'))->class('col-md-4 form-control-label')->for('city') }}
                            <div class="col-md-8">
                                {{ html()->text('city')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.omc.address.city'))
                                ->attribute('maxlength',91) 
                                ->value($omc->Addresses->city)
                                ->readonly()
                                ->required() }}
                            </div><!--col-->
                        </div><!--form-group--> 
                        <div class="form-group row" name="address">
                            {{ html()->label(__('validation.attributes.backend.access.omc.address.country'))->class('col-md-4 form-control-label')->for('country') }}
                            <div class="col-md-8">
                                {{ html()->text('country')
                                ->class('form-control ')
                                ->placeholder(__('validation.attributes.backend.access.omc.address.country'))
                                ->attribute('maxlength', 191)
                                ->value($omc->Addresses->country)
                                ->readonly()
                                ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        {{ html()->hidden('addid')->value($omc->id) }}
                    </div><!-- column -->
                </div><!-- row -->
            </div><!-- body -->
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        {{ html()->button('<i class="fas fa-edit fa-lg" data-toggle="tooltip" title="'.__('buttons.omc.edit').'"></i> '.__('buttons.omc.edit').'')->class("btn btn-primary   col-md-12")->attribute('id', 'addressEdit')}}
                        {{ form_submit(__('<i class="far fa-save fa-2x " data-toggle="tooltip" title="'.__('buttons.omc.update'). '"></i> '.__('buttons.omc.update').''))->attributes(['id'=>'addressupdate','hidden'=>'true'])->class("col-md-12") }}
                    </div>
                    <a href="{{route('admin.omc.addressdelete',$omc->id)}}"  class="btn btn-danger btn col-md-6" role="button" onclick="return confirm('do you want to delete OMC-address ? ')" ><i class="fas fa-trash fa-lg" data-toggle="tooltip" title="@lang('buttons.omc.delete')"></i> @lang('buttons.omc.delete')</a>
                </div><!--row-->
            </div><!--card-footer-->
            {{ html()->closeModelForm() }}
        </div><!-- card -->
    </div><!-- col-->
    @endif
    <div class='col-md-6 col-xs-12 col-xs-12' id="add_address_content" style="display: none">
        <div class='card'>
         {{ html()->modelForm($omc, 'POST', route('admin.omc.addressstore',$omc->id))->class('form-horizontal')->attribute('id','addform')->open()}}
         <div class='card-body'>
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.address.street'))->class('col-md-4 form-control-label')->for('street') }}
                        <div class="col-md-8" name="address">
                            @csrf
                            {{ html()->text('street')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.omc.address.street'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row" name="address">
                        {{ html()->label(__('validation.attributes.backend.access.omc.address.city'))->class('col-md-4 form-control-label')->for('city') }}
                        <div class="col-md-8">
                            {{ html()->text('city')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.omc.address.city'))
                            ->attribute('maxlength',91) 
                            ->required() }}
                        </div><!--col-->
                    </div><!--form-group--> 
                    <div class="form-group row" name="address">
                        {{ html()->label(__('validation.attributes.backend.access.omc.address.country'))->class('col-md-4 form-control-label')->for('country') }}
                        <div class="col-md-8">
                            {{ html()->text('country')
                            ->class('form-control country')
                            ->placeholder(__('validation.attributes.backend.access.omc.address.country'))
                            ->attribute('maxlength', 191)
                           
                            ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!-- column -->
            </div><!-- row -->
        </div><!-- body -->
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_submit(__('<i class="far fa-save fa-2x" data-toggle="tooltip" title="'.__('buttons.general.save'). '"></i> '.__('buttons.general.save').''))->attribute('id','address_store')->class("col-md-5") }}
                    {{ html()->button(__('<i class="fas fa-times " data-toggle="tooltip" title="'.__('buttons.general.cancel'). '"></i>'.__('buttons.general.cancel').''))->class("btn btn-danger col-md-5 float-right")->attribute('id', 'cancel')}}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
        {{ html()->closeModelForm() }}
    </div><!-- card -->
</div><!-- col-->
@if(is_null($address))
<div class="col-md-6 col col-sm-12 col-xs-12 " id="add_address">
    {{ html()->button('<i class="fas fa-plus-circle" data-toggle="tooltip" title="'.__('buttons.omc.address.add').'"></i> '.__('buttons.omc.address.add').'')->class("btn btn-primary")->attribute('id', 'add_address_data')}}
</div>
@endif
</div><!--row-->
</div> 
</div><!--card--> 

@if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor())
<div class= "card">
    <!-- {{ html()->modelForm($omc,'PATCH',route('admin.omc.manageOmcProduct',$omc->id))->class('form-horizontal')->open()}} -->
    @csrf
    <div class="card-header">
        <div class="row">
            <div class="col-sm-7">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.importer.inventory_manage')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-header--> 
    <div class ="manage-form-errors"></div>
    <script>
    var prods = <?php echo json_encode( $products ); ?>;
    </script>
    @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
    <div class="card-body" style="display: none;">
        @if (count($products))
        {{ html()->modelForm($omc,'POST',route('admin.omc.manageOmcInventory',$omc->id))->class('form-horizontal')->open()}}
        <div class="row">
            @csrf
            <div class="col-md-2">
                <input type="hidden" name="stock_id" id="stock_id" value="{{ $products->first()['id'] }}">
                <select class="form-control products" name="product_id">
                    @foreach( $products->groupBy('name') as $key => $product )
                    <option value="{{ $product[0]['product_id'] }}">{{$key}}</option>
                    @endforeach
                </select>

            </div><!--col-->
            <div class="col-md-2">
                {{ html()->select("transaction")
                ->name('operation')
                ->class('form-control transaction')
                ->options(['Add','Deduct'])
                ->value(0)
            }}

        </div><!--col-->
        <div class="col-md-2">
            <select class="form-control importers" id="importers" name="importer_id">
                @foreach( $importers as $importer )
                <option value="{{ $importer['id'] }}" {{ $products[0]['importer'] == $importer['id'] ? 'selected': '' }}>{{ $importer['company_name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            {{ html()->text("quantity")
            ->class('form-control quantity')
            ->name("quantity")
            ->value($products[0]['balance'])
            ->placeholder(__('validation.attributes.backend.access.omc.importers.quantity'))
            ->attribute('maxlength', 191)
            ->attribute('id','quantity')
            ->readonly()
        }} 
    </div>
    <div class="col-md-2 col-sm-2">
        {{ html()->text("added")
        ->class('form-control append_quantity')
        ->name("append_quantity")
        ->placeholder(__('validation.attributes.backend.access.omc.importers.new_quantity'))
        ->attribute('maxlength', 191)
        ->attribute('id','append_quantity')
        ->required()
        ->autofocus() }}
    </div><!--col-->
    <div class="col-md-2">
        {{ form_submit('Update Inventory')->class('float-right')}}

    </div>
</div> 
<hr>
{{ html()->closeModelForm() }} 
@else
<div class="row"><h4>No transaction record available.Kindly add products..</h4></div>     
@endif    
</div><!--card-body--> 
@endif
@if( count($transactions) ) 
<div class="card-body">
    <h6 class="text-center">
     <a href="{{route('admin.omc.transactions', $omc->id)}}" target="_blank" class="btn btn-primary btn-sm">Download Transactions</a>
 </h6>
 <h3 class="text-center">Transaction Activities</h3><hr>
 <div class="transactions">
    @foreach( $transactions as $transc )
    <p><b>{{$transc['first_name']}} {{$transc['last_name']}}</b> {{ $transc['transaction_type'] == 'add' ? 'added' : 'reduced' }} {{ $transc['product_name'] }} quantity by <b>{{ $transc['quantity'] }}</b> ltr. {!! $transc['release_id'] ? 'via <a href="/admin/release/show/'.$transc['release_id'].'" target="_blank">'.$transc['release_purchase_order'].'</a>': '' !!} {!! $transc['order_id'] ? 'via <a href="/order/view/'.$transc['order_id'].'" target="_blank">'.$transc['orders_purchase_order'].'</a>': '' !!}</p>
    @endforeach
</div>
</div>
@endif

<!-- {{ html()->closeModelForm() }} -->
</div><!--card-->  
@endif 
@endsection

@push('after-scripts')
<script type="text/javascript" src="/js/omc.js"></script>
@endpush
