@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.omc.management'))

@section('breadcrumb-links')
    @include('backend.omc.includes.breadcrumb-links')
@endsection 

@section('content')
    {{ html()->form('POST', route('admin.omc.addressstore',$id))->class('form-horizontal')->open() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            @lang('labels.backend.access.omc.management')
                            <small class="text-muted">@lang('labels.backend.access.omc.create')</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr>
                <div class="row">
                    @include('backend.includes.gampetrohead')
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.omc.street'))->class('col-md-2 form-control-label')->for('street') }}

                            <div class="col-md-10">
                                {{ html()->text('street')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.street'))
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.city'))->class('col-md-2 form-control-label')->for('city') }}

                            <div class="col-md-10">
                                {{ html()->text('city')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.city'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.country'))->class('col-md-2 form-control-label')->for('country') }}

                            <div class="col-md-10">
                                {{ html()->text('country')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.country'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group--> 
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.driver.index'), __('buttons.general.back')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
