@extends('backend.layouts.app')

@section('title', __('labels.backend.access.omc.management') . ' | ' . __('labels.backend.access.contacts.create'))

@section('breadcrumb-links')
    @include('backend.omc.includes.breadcrumb-links')
@endsection

@section('content')

{{ html()->form('post',route('admin.omc.addcontacts',$ids))
->class('form-horizontal contacts-form')
->open() }}

@csrf
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.contacts.management')
                        <small class="text-muted">@lang('labels.backend.access.contacts.create')</small>
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">
                    @include('backend.omc.includes.contacts-header-buttons')
                </div><!--col-->
            </div><!--row-->

            <hr>
            <div class="block main_content">
                {{ html()->hidden('contact_count')->value(isset($errors) && count($errors)>0 && !is_null(old('contact_count'))? old('contact_count'): count($contact)) }}
                <div class="row mt-4 mb-4" name="clone" id= "clone" style="display:{{count($contact)>0?'none':'block'}};">
                    <div class="col-md-12">
                        <div class="form-group row">
                            {{ html()->hidden("contact[0][id]")
                                ->id('id')
                                ->class('form-control formInput')
                                ->disabled(count($contact)> 0 ? true:false)
                            }}
                           
                            {{ html()->label(__('validation.attributes.backend.access.omc.contacts.name'))->class('col-md-2 col-sm-3 form-control-label')->for('name') }}
                            <div class="col-md-9 col-sm-9">    
                                {{ html()->text('contact[0][name]')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.contacts.name'))
                                    ->attribute('maxlength', 191)
                                    ->disabled(count($contact)> 0 ? true:false)
                                    ->required(count($contact)> 0 ? false:true)
                                }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.email'))->class('col-md-2 col-sm-3 form-control-label')->for('email') }}

                            <div class="col-md-9 col-sm-9">
                                {{ html()->email('contact[0][email]')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.contacts.email'))
                                    ->attribute('maxlength', 191)
                                    ->disabled(count($contact)> 0 ? true:false)
                                    ->required(count($contact)> 0 ? false:true)
                                     }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel1'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel1') }}

                            <div class="col-md-9 col-sm-9">
                                {{ html()->text('contact[0][tel1]')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel1'))
                                    ->attribute('maxlength', 191)
                                    ->disabled(count($contact)> 0 ? true:false)
                                    ->required(count($contact)> 0 ? false:true)
                                     }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel2'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel2') }}

                            <div class="col-md-9 col-sm-9">
                                {{ html()->text('contact[0][tel2]')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel2'))
                                    ->attribute('maxlength', 191)
                                    ->disabled(count($contact)> 0 ? true:false)
                                    }}
                            </div><!--col-->
                        </div><!--form-group--> 
                        <!-- <hr> -->
                        
                    </div><!--col-->
                </div>                   
                    {{ html()->hidden()
                                ->name('omc_id')  
                                ->class('contact_id')
                                ->id('contact_id')
                                ->value($ids) }}
                @if( !is_null( $contact ) )
                @foreach($contact as $count => $contacts)
                    <div class="row mt-4 mb-4 div_{{$count}}">
                        <div class="col-md-9" style="border-right: 1px solid rgba(0,0,0,.1); margin-bottom: 25px;">
                            <div class="form-group row">
                                {{ html()->hidden()
                                    ->class('id')
                                    ->name("contact[$count][id]")
                                    ->value($contacts->id) }}
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.name'))->class('col-md-2 col-sm-3 form-control-label')->for('name') }}
                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('name')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.name'))
                                        ->value($contacts->name)
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$count][name]")
                                        ->readonly()
                                        ->required()
                                        ->autofocus() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.email'))->class('col-md-2 col-sm-3 form-control-label')->for('email') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.email'))
                                        ->value($contacts->email)
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$count][email]")
                                        ->readonly()
                                        ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel1'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel1') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('tel1')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel1'))
                                        ->value($contacts->tel1)
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$count][tel1]")
                                        ->readonly()
                                        ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel2'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel2') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('tel2')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel2'))
                                        ->value($contacts->tel2)
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$count][tel2]")
                                        ->readonly()
                                    }}
                                </div><!--col-->
                            </div><!--form-group--> 
                        </div><!--col-->
                       
                        <div class="col-sm-2 show_edit">
                            <div class="form-group row "  id="contacts_edit" style="margin: 10px;" >
                                <span class="btn btn-info btn-sm col-md-12" value = "div_{{$count}}"><i class="fas fa-pen"></i> Edit</span>
                            </div><!--edit btn-toolbar-->
                            <div class="form-group row" style="margin: 10px;">
                                <a href="{{ route('admin.omc.deleteContact',$contact[$count]->id)}}" id='del' class='btn btn-danger btn-sm col-md-12' onclick="return confirm('do you want to delete contact ? ')"><i class="fa fa-trash"></i> Delete </a>
                            </div><!--Delete btn-toolbar-->
                        </div>
                        <div class="col-sm-2 hide_edit" style="display:none;">
                            <div class="form-group row " id="back" style="margin: 10px;">
                                <span class="btn btn-danger btn-sm col-md-12 " value = "div_{{$count}}"><i class="fa fa-arrow-left float-left" style="margin-top: 3px;"></i>Back</span>
                            </div><!--edit btn-toolbar-->
                        </div>   
                    </div><!--row-->
                    <hr>
                @endforeach
                @endif

                @if(isset($errors) && count($errors)>0 && !is_null(old('contact_count')))
                <!--for each for errors-->

                @for($a=count($contact)+1; $a<=old('contact_count'); $a++)
                <div class="contact">
                    <div class="row">
                        <div class="col-md-9" style="border-right: 1px solid rgba(0,0,0,.1);margin-bottom: 25px;">
                            <div class="form-group row">
                                {{ html()->hidden()
                                    ->class('id')
                                    ->name("contact[$a][id]")
                                     }}
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.name'))->class('col-md-2 col-sm-3 form-control-label')->for('name') }}
                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('name')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.name'))
                                        ->value(old("contact.$a.name"))
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$a][name]")
                                        ->required()
                                        ->autofocus() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.email'))->class('col-md-2 col-sm-3 form-control-label')->for('email') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.email'))
                                        ->value(old("contact.$a.email"))
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$a][email]")
                                        ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel1'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel1') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('tel1')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel1'))
                                        ->value(old("contact.$a.tel1"))
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$a][tel1]")
                                        ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.contacts.tel2'))->class('col-md-2 col-sm-3 form-control-label')->for('contacts.tel2') }}

                                <div class="col-md-9 col-sm-9">
                                    {{ html()->text('tel2')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.omc.contacts.tel2'))
                                        ->value(old("contact.$a.tel2"))
                                        ->attribute('maxlength', 191)
                                        ->name("contact[$a][tel2]")
                                        }}
                                </div><!--col-->
                            </div><!--form-group-->   
                        </div><!--col-->
               
                        <div class="col-sm-2 show_edit">
                            <div class="row remove form-group" style="margin: 10px;">
                                <span class="btn btn-sm btn-danger col-md-12"><i class="fas fa-minus-circle"></i> &nbsp;Remove</span>
                            </div>
                        </div>
                    </div>
                    <hr>   
                </div>

                @endfor
                @endif
            </div>
        </div>
        <div class="card-footer col clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.omc.show',$ids), __('buttons.general.cancel')) }}
                </div><!--col-->
                <div class="col text-right">
                    {{ form_submit(__('<i class="fa fa-save"></i> '). __('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div>          
    {{ html()->form()->close() }}
@endsection

@push('after-scripts')
<script type="text/javascript" src="/js/omc.js"></script>
@endpush


