<table class="table table-bordered dataTable no-footer">
	<thead>
		<th>Order Reference</th>
		<th>Date</th>
		<th>Time</th>
		<th>Vehicle</th>
		<th>Driver</th>
		<th>Product</th>
		<th>Quantity</th>
		<th>Inventory Balance Before</th>
	</thead>
	<tbody>
		@if(!is_null($transactions))
		@if(count($transactions))
		@foreach($transactions as $transaction)
		<tr>
			<td>
				@if(!is_null($transaction['order_number']))
				<a href="{{ route('order.view', $transaction['order_number']) }}">{{$transaction['orders_purchase_order']}}</a>
				<!-- #ORDER{{  $transaction['order_number']}} -->
				@elseif(!is_null($transaction['release_number']))
				<a href="{{ route('admin.release.show', $transaction['release_number']) }}">{{$transaction['release_purchase_order']}}</a>
				<!-- #RELEASE{{  $transaction['release_number']}} -->
				@else
				WebPortal
				@endif
			</td>
			<td>{{@ \Carbon\Carbon::parse($transaction['created_at'])->format('Y-m-d')}}</td>
			<td>{{@ \Carbon\Carbon::parse($transaction['created_at'])->format('h:i:s')}}</td>
			<td>{{@ $transaction['vehicle_number']}}</td>
			<td>{{@ $transaction['driver_name']}}</td>
			<td>{{$transaction['product_name']}}</td>
			<td class="@if($transaction['transaction_type'] == 'add') text-green @else text-red @endif ">
				@if($transaction['transaction_type'] == 'add') + @else - @endif
				{{$transaction['quantity']}}
			</td>
			<td>{{@ $transaction['balance_before_transaction']}}</td>
		</tr>
		@endforeach
		@else
		<tr class = "text-center">
			<td colspan = "8">No Transaction Found</td>
		</tr>
		@endif
		@else
		<tr class = "text-center">
			<td colspan = "8">No Transaction Found</td>
		</tr>
		@endif
	</tbody>
</table>