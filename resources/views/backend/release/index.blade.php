@extends('backend.layouts.app')

@section('title', app_name() . ' | Release Management' )

@section('breadcrumb-links')
    @include('backend.release.includes.breadcrumb-links')
@endsection

@push ('after-styles')
<link rel="stylesheet" type="text/css" href="/css/funky-style.css">
<style>
.badge{
    margin-left: 5px;
    margin-right: 5px;
    display: inline;
}
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Inventory Release Management 
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.release.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="order-table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.order.table.release_reference')</th>
                            <th>@lang('labels.backend.access.order.table.supplier')</th>
                            <th>@lang('labels.backend.access.order.table.purchaseOrder')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
       
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
            $(function() {
                getDataTable();
            });

            function getDataTable(type){
                $('#order-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: '/admin/release/getReleaseData',
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'company', name: 'company' },
                        { data: 'purchaseOrder', name: 'purchaseOrder' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            }
    </script> 
@endpush
