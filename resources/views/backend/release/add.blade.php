@extends('backend.layouts.app')

@section('title', __('labels.backend.access.order.management') . ' | ' . __('labels.backend.access.order.release'))

@section('breadcrumb-links')
@include('backend.release.includes.breadcrumb-links')
@endsection

@section('content')
<!-- <div id ="form-errors"></div> -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Release Management <small class="text-muted">@lang('labels.backend.access.order.release')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#orderasimporter" role="tab" aria-controls="orderasimporter" aria-expanded="true"><i class="fas fa-list"></i> Importer as Supplier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#orderasomc" role="tab" aria-controls="orderasomc" aria-expanded="true"><i class="fas fa-list"></i> OMC as Supplier</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="orderasimporter" role="tabpanel" aria-expanded="true">
                        @include('backend.release.forms.releaseasimporter')
                    </div>
                    <div class="tab-pane " id="orderasomc" role="tabpanel" aria-expanded="true">
                        @include('backend.release.forms.releaseasomc')
                    </div>
                </div>
                <!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
@push('after-scripts')
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/release.js"></script>
@endpush