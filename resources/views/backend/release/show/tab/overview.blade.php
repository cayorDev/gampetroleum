<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.order.tabs.content.overview.purchaseOrder')</th>
                <td>{{ $release->purchase_order }}</td>
            </tr>
            <tr>
               <th>Supplier</th>
               <td>{{ $release->Omc->company_name}} {{ $release->SupplierOmc ? ' / '.$release->SupplierOmc->company_name : ''}} / {{ $release->Importer->company_name }}</td>
           </tr>
        </table>
    </div>
</div><!--table-responsive-->
@include('backend.release.show.tab.product')
