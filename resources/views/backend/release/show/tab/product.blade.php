<div class="col">
    <div class="table-responsive">
    	 <table class="table table-hover">
                <tr>
                    <th>Product Code</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Units</th>
                </tr>
                
                @foreach($products as $key => $product)
                <tr>
                    <td>{{$product->Product->sku}}</td>
                    <td>{{$product->Product->name}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->Product->measuring_unit}}</td>
                </tr>
                @endforeach
            </table>
    	
    </div>
</div>