@extends('backend.layouts.app')

@section('title', __('labels.backend.access.order.management') . ' | View Release')

@section('breadcrumb-links')
@include('backend.release.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Release Management
                    <!-- <small class="text-muted">@lang('labels.backend.access.vehicle.view')</small> -->
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-cart-arrow-down"></i> @lang('labels.backend.access.order.tabs.titles.overview')</a>
                    </li>
                </ul>
               <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.release.show.tab.overview')
                    </div><!--tab-->
                </div>
                <!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($release->created_at) }} ({{ $release->created_at->diffForHumans() }}),
                    <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($release->updated_at) }} ({{ $release->updated_at->diffForHumans() }})
                    @if($release->trashed())
                        <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($release->deleted_at) }} ({{ $release->deleted_at->diffForHumans() }})
                    @endif
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
