<hr>
<div class="row">
    @include('backend.includes.gampetrohead')
</div>
{{ html()->form('POST', route('admin.release.store'))->class('form-horizontal')->attribute('id', 'orderAddForm')->open() }}

<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.importer'))->class('col-md-2 form-control-label')->for('importer') }}

            <div class="col-md-10">
                {{ html()->select('importer_id')
                ->class('form-control')
                ->options($importers)
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.importer'))
                ->attribute('id', 'importerSelect')
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.omc'))->class('col-md-2 form-control-label')->for('omc') }}

            <div class="col-md-10">
                {{ html()->select('omc_id')
                ->class('form-control')
                ->options($omc)
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.omc'))
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.purchase_order'))->class('col-md-2 form-control-label')->for('purchase_order') }}

            <div class="col-md-10">
                {{ html()->text('purchase_order')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.order.purchase_order'))
                ->attribute('min', 0)
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row " id="productDiv">
            {{ html()->label(__('validation.attributes.backend.access.order.product'))->class('col-md-2 form-control-label')->for('product') }}
            <div class="col-md-10">
                {{ html()->multiselect("products[count]")
                ->class('form-control products_select')
            }}
        </div>
    </div>
    <div class="form-group row " id="inventoryContainer">
        <label class="col-md-2 form-control-label">Inventory Available</label>
        <div class="col-md-10" id="inventoryAvailable">
        </div>
    </div>
    <div id="div_1" class="products_container">
    </div>
</div><!--col-->
</div><!--row-->
<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('admin.release.index'), __('buttons.general.cancel')) }}
        </div><!--col-->
        <div class="col text-right">
            {{ html()->button('Create')->class('btn btn-success btn-sm pull-right')->type('submit')->attribute('id', 'createOrderSubmit')}}
        </div>
    </div><!--row-->
</div><!--card-footer-->
{{ html()->form()->close() }}
