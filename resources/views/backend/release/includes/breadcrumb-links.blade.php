<li class="breadcrumb-menu">
    @if( $logged_in_user->isAdmin() )
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Release</a>
            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
               @if($logged_in_user->isAdmin())
               <a class="dropdown-item" href="{{ route('admin.release.index') }}">All Releases</a>
               <a class="dropdown-item" href="{{ route('admin.release.add') }}">Create new Inventory Release</a>
               @endif
           </div>
       </div>
   </div><!--btn-group-->
   @endif
</li>
