@extends('backend.layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        {{ __('menus.backend.log-viewer.logs') }}
    </div><!-- box-header -->
    <div class="card-body">
        @foreach($logs['data'] as $log)      
        <div class="row p-2">
            <div class="col-md-1">  
                <span class="fa-stack fa-1x">
                    <i class="fa fa-circle fa-stack-2x "style="color:#f0f3f5;"></i>
                    <i class="fa {{$icons[$log['action']]}} fa-stack-1x fa-lg"></i> 
                </span>
            </div>
            <div class="col-md-11 p-1" style="border-bottom: solid 1px; border-bottom-color:#f5f5f5;">
                <b>{{ $log['updated_by'] }}</b>
                {!! $log['action'] ."d <span class='text-uppercase'><b>" .$log['placeholder'].'</b></span>' !!}  
                @switch($log['model'])
                    @case('omc_product_transactions' )
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('omc_product_stocks')
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('orders')
                        {!!"<b>: ".$log['updates']->order_id."</b>" !!}
                    @break
                    @case('vehicles')
                        {!!"<b>: ".$log['updates']->license_plate_number."</b>" !!}
                    @break
                    @case('importer_product_stocks')
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('importer_product_transactions')
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('order_product_meter')
                        {!!"<b>: ".$log['updates']->order_id."</b>" !!}
                    @break
                    @case('chamber_assignment')
                        {!!"<b>: ".$log['updates']->order_id."</b>" !!}
                    @break
                    @case('omc')
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('users')
                        {!!"<b>: ".$log['updates']->first_name." ".$log['updates']->last_name ."</b>" !!}
                    @break
                    @case('drivers')
                        {!!"<b>: ".$log['updates']->name."</b>" !!}
                    @break
                    @case('importers')
                        {!!"<b>: ".$log['updates']->company_name."</b>" !!}
                    @break
                    @case('release')
                        {!!"<b>: ".$log['updates']->id."</b>" !!}
                    @break
                @endswitch
                <b style="color:#87ceeb">
                    @if(is_array($log['updates']))    
                    @foreach($log['updates'] as $update) 
                    {{$update}}
                    @endforeach
                    @endif
                </b>
                <div class="col-md-2 float-right">
                    <div class="float-left mr-1">
                        <i class="fa fa-clock"></i>
                    </div> 
                    {{ $log['time_ago'] }}
                    &nbsp; &nbsp;
                    {!! $log['super_admin_action'] !!}
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row" style="margin: 0 auto;">
        {!! $logs['paginationdata']->links() !!}
    </div>    
</div>
@endsection