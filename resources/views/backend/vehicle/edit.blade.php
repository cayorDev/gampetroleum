@extends('backend.layouts.app')

@section('title', __('labels.backend.access.vehicle.management') . ' | ' . __('labels.backend.access.vehicle.management'))

@section('breadcrumb-links')
    @include('backend.vehicle.includes.breadcrumb-links')
@endsection

@section('content')
<div id ="edit-form-errors"></div>
{{ html()->modelForm($vehicle, 'PATCH', route('admin.vehicle.update', $vehicle->id))->class('form-horizontal')->attribute('id', 'vehicleEditForm')->open() }}
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.vehicle.management')
                    <small class="text-muted">@lang('labels.backend.access.vehicle.edit')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <hr>
        <div class="row">
            @include('backend.includes.gampetrohead')
        </div>
        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.transporter_name'))->class('col-md-2 form-control-label')->for('transporter_name') }}

                    <div class="col-md-10">
                        {{ html()->text('transporter_name')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.transporter_name'))
                            ->attribute('maxlength', 191)
                            ->required()
                            ->autofocus() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                {{ html()->label(__('validation.attributes.backend.access.vehicle.transporter_tel_number'))->class('col-md-2 form-control-label')->for('transporter_tel_number') }}

                    <div class="col-md-10">
                        {{ html()->text('transporter_tel_number')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.transporter_tel_number'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                    </div><!--col-->
                </div>
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.license_plate_number'))->class('col-md-2 form-control-label')->for('license_plate_number') }}

                    <div class="col-md-10">
                        {{ html()->text('license_plate_number')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.license_plate_number'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.number_of_chambers'))->class('col-md-2 form-control-label')->for('number_of_chambers') }}
                        <div class="col-md-10">
                            {{ html()->text('number_of_chambers')
                            ->class('form-control')
                            ->type('number')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.number_of_chambers'))
                            ->attribute('min', 0)
                            ->attribute('id', 'chambers_no')
                            ->required() }}
                        </div><!--col-->
                </div><!--form-group-->
                <div id="Detail">
                   <?php $chamberCount = 0;?>
                    @foreach($vehicle->vehicle_chamber as $chambers)

                    <div class="form-group row" id="chamberDiv_{{$chamberCount++}}">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.chamber').$chamberCount)->class('col-md-2 form-control-label')->for('chamber') }}
                        <div class="col-md-10">
                            {{ html()->text('chamber')
                            ->class('form-control')
                            ->type('number')
                            ->name('chamber_'.$chambers->id)
                            ->value($chambers->capacity)
                            ->placeholder(__('validation.attributes.backend.access.vehicle.chamber'))
                            ->attribute('min', 0)
                            ->attribute('id', 'chamber'.$chambers->id)
                            ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                @endforeach
                </div>

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.upper_seals'))->class('col-md-2 form-control-label')->for('upper_seals') }}

                    <div class="col-md-10">
                        {{ html()->text('upper_seals')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.upper_seals'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.lower_seals'))->class('col-md-2 form-control-label')->for('lower_seals') }}

                    <div class="col-md-10">
                        {{ html()->text('lower_seals')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.vehicle.lower_seals'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.status'))->class('col-md-2 form-control-label')->for('status') }}

                    <div class="col-md-10">
                        {{ html()->select("status")
                            ->class('form-control selector_type')
                            ->attribute('value', $vehicle->status)
                            ->options(['active'=>'active', 'inactive'=>'inactive'])
                            ->attribute('id', 'status')
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                {{ html()->hidden('id',$vehicle->id)
                    ->class('form-control')
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
         </div><!--row-->
    </div><!--card-body-->
    <div class="card-footer">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.vehicle.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ html()->button('Update')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'updateVehicleSubmit')}}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->closeModelForm() }}
@endsection
@push('after-scripts')
<script type="text/javascript">

    $("#chambers_no").blur(function(){

        var totalChambers = $("#chambers_no").val();
        var ChamberCount=[];
        $("#Detail").children("div[id]").each(function(){
            ChamberCount.push(this.id);
         });
        var chamberLength= ChamberCount.length;
        if(totalChambers >= chamberLength) {
            chamberDetails="";
            for (i =chamberLength; i < totalChambers; i++) {
                chamberDetails += '<div class="form-group row" id="chamberDiv_'+ i +'"><label class="col-md-2 form-control-label">Chamber '+ (chamberLength+1) + '  </label>' +'<div class="col-md-10"><input type="text" name="newchamber_' + i + '" id="chamber ' + i+1 + '" value="" class="form-control" placeholder="Chamber Capacity"></div></div>'
            }
            $("#Detail").append(chamberDetails);
        } else {
            var removeChamberCount = chamberLength-totalChambers;
            var removeDiv = ChamberCount.slice(Math.max(ChamberCount.length - removeChamberCount, 1));
            $.each( removeDiv , function(key, value ) {
                $('#'+value).remove();
            });
        }
    });
        $('#updateVehicleSubmit').click(function(){
        $.ajax({
            url: '/admin/vehicle/update',
            type: 'post',
            dataType: 'json',
            data: $('form#vehicleEditForm').serialize(),
            success: function(data) {
                if(data.success == true){
                    window.location = "/admin/vehicle/index";
                } else {
                    errorsHtml = '<div class="alert alert-danger"><ul>';
                    errorsHtml += '<li>' + data.message + '</li>';
                    errorsHtml += '</ul></di>';
                    $( '#edit-form-errors' ).html( errorsHtml );
                }
            },
            error: function(data) {
                var errors = data.responseJSON.errors;
                errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                errorsHtml += '</ul></di>';
                $( '#edit-form-errors' ).html( errorsHtml );
            }
        });
    })
</script>
@endpush
