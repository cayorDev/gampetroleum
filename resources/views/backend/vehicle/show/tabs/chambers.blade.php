<div class="col">
	<div class="table-responsive">
		<table class="table table-hover">
			<?php $chamberCount = 1; $totalCapacity = 0;?>
			<tr>
				<th> 
					Chamber
				</th>
				<th> 
					Chamber Capacity
				</th>
			</tr>
			@foreach($vehicles->vehicle_chamber as $chambers) 
			<tr>
				<th>@lang('labels.backend.access.vehicle.table.chamber'){{$chamberCount++}}</th>
				<td>{{ $chambers->capacity}}</td>
			</tr>
			<?php $totalCapacity += $chambers->capacity;?>
			@endforeach
			<tr>
				<th>@lang('labels.backend.access.vehicle.table.totalCapacity')</th>
				<td>{{ $totalCapacity}}</td>
			</tr>
			
		</table>
	</div>
</div>