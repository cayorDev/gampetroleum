<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.vehicle.table.license_plate_number')</th>
                <td>{{ $vehicles->license_plate_number }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.vehicle.table.transporter_name')</th>
                <td>{{ $vehicles->transporter_name }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.vehicle.table.transporter_tel_number')</th>
                <td>{{ $vehicles->transporter_tel_number }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.vehicle.table.number_of_chambers')</th>
                <td>{{ $vehicles->number_of_chambers }}</td>
            </tr>
             <tr>
                <th>@lang('labels.backend.access.vehicle.table.upper_seals')</th>
                <td>{{ $vehicles->upper_seals }}</td>
            </tr>
             <tr>
                <th>@lang('labels.backend.access.vehicle.table.lower_seals')</th>
                <td>{{ $vehicles->lower_seals}}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.vehicle.table.status')</th>
                <td>{{ $vehicles->status}}</td>
            </tr>

        </table>
    </div>
</div><!--table-responsive-->
