@extends('backend.layouts.app')

@section('title', __('labels.backend.access.vehicle.management') . ' | ' . __('labels.backend.access.vehicle.create'))

@section('breadcrumb-links')
@include('backend.vehicle.includes.breadcrumb-links')
@endsection

@section('content')
<div id ="form-errors"></div>
{{ html()->form('POST', route('admin.vehicle.store'))->class('form-horizontal')->attribute('id', 'vehicleAddForm')->open() }}
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.vehicle.management')
                    <small class="text-muted">@lang('labels.backend.access.vehicle.create')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <hr>
        <div class="row">
            @include('backend.includes.gampetrohead')
        </div>
        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.vehicle.transporter_name'))->class('col-md-2 form-control-label')->for('transporter_name') }}

                            <div class="col-md-10">
                                {{ html()->text('transporter_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.vehicle.transporter_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.vehicle.transporter_tel_number'))->class('col-md-2 form-control-label')->for('transporter_tel_number') }}

                            <div class="col-md-10">
                                {{ html()->text('transporter_tel_number')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.vehicle.transporter_tel_number'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--col-->
                        </div>
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.license_plate_number'))->class('col-md-2 form-control-label')->for('license_plate_number') }}

                    <div class="col-md-10">
                        {{ html()->text('license_plate_number')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.vehicle.license_plate_number'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row" id ="chamberDiv">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.number_of_chambers'))->class('col-md-2 form-control-label')->for('number_of_chambers') }}

                    <div class="col-md-10">
                        {{ html()->text('number_of_chambers')
                        ->class('form-control')
                        ->type('number')
                        ->placeholder(__('validation.attributes.backend.access.vehicle.number_of_chambers'))
                        ->attribute('min', 0)
                        ->attribute('id', 'no_of_chambers')
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div id="chamberDetail">
                </div>
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.upper_seals'))->class('col-md-2 form-control-label')->for('upper_seals') }}

                    <div class="col-md-10">
                        {{ html()->text('upper_seals')
                        ->class('form-control')
                        ->type('number')
                        ->placeholder(__('validation.attributes.backend.access.vehicle.upper_seals'))
                        ->attribute('min', 0)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.lower_seals'))->class('col-md-2 form-control-label')->for('lower_seals') }}

                    <div class="col-md-10">
                        {{ html()->text('lower_seals')
                        ->class('form-control')
                        ->type('number')
                        ->placeholder(__('validation.attributes.backend.access.vehicle.lower_seals'))
                        ->attribute('min', 0)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.vehicle.status'))->class('col-md-2 form-control-label')->for('status') }}

                    <div class="col-md-10">
                        {{ html()->select("status")
                            ->class('form-control selector_type')
                            ->options(['active'=>'active', 'inactive'=>'inactive'])
                            ->attribute('id', 'status')
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.vehicle.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ html()->button('Create')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'crateVehicleSubmit')}}
                <!-- {{ form_submit(__('buttons.general.crud.create')) }} -->

            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->form()->close() }}
@endsection
@push('after-scripts')
<script type="text/javascript">
    $("#no_of_chambers").blur(function(){
        if($('#chamberDetail').length > 0){
           $('#chamberDetail').html("");
       }

       addChamberColumns();

   });

    function addChamberColumns()
    {
        var totalChambers = $("#no_of_chambers").val();
        chamberDetails="";
        for (i = 1; i <= totalChambers; i++) {
          chamberDetails += '<div class="form-group row"> <label class="col-md-2 form-control-label">Chamber '+ i + '  </label>' +
          '<div class="col-md-10"><input type="text" name="chamber' + i +
          '" id="chamber' + i + '" value="" class="form-control" placeholder="Chamber Capacity"></div></div>'
        }
        $("#chamberDetail").html(chamberDetails);
    }

    $('#crateVehicleSubmit').click(function(){
        $.ajax({
            url: '/admin/vehicle/store',
            type: 'post',
            dataType: 'json',
            data: $('form#vehicleAddForm').serialize(),
            success: function(data) {
                if(data.success == true){
                    window.location = "/admin/vehicle/index";
                } else {
                    errorsHtml = '<div class="alert alert-danger"><ul>';
                    errorsHtml += '<li>' + data.message + '</li>';
                    errorsHtml += '</ul></di>';
                    $( '#form-errors' ).html( errorsHtml );
                }
            },
            error: function(data) {
                var errors = data.responseJSON.errors;
                errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                errorsHtml += '</ul></di>';
                $( '#form-errors' ).html( errorsHtml );
            }
        });
    })

</script>

@endpush
