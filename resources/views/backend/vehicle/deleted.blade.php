@extends('backend.layouts.app')

@section('title', __('labels.backend.access.vehicle.management') . ' | ' . __('labels.backend.access.vehicle.deleted'))

@section('breadcrumb-links')
    @include('backend.vehicle.includes.breadcrumb-links')
@endsection 
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.vehicle.management')
                    <small class="text-muted">@lang('labels.backend.access.vehicle.deleted')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="vehicle-table">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.vehicle.table.license_plate_number')</th>
                                <th>@lang('labels.backend.access.vehicle.table.number_of_chambers')</th>
                                <th>@lang('labels.backend.access.vehicle.table.upper_seals')</th>
                                <th>@lang('labels.backend.access.vehicle.table.lower_seals')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

$('#vehicle-table').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    ajax: '/admin/vehicle/getDeletedVehicleData',
    columns: [
    { data: 'license_plate_number', name: 'license_plate_number' },
    { data: 'number_of_chambers', name: 'number_of_chambers' },
    { data: 'upper_seals', name: 'upper_seals' },
    { data: 'lower_seals', name: 'lower_seals' },
    {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});
</script> 
@endpush
