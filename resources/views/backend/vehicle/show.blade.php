@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.vehicle.management'))

@section('breadcrumb-links')
    @include('backend.vehicle.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.vehicle.management')
                    <!-- <small class="text-muted">@lang('labels.backend.access.vehicle.view')</small> -->
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-truck"></i> @lang('labels.backend.access.vehicle.tabs.titles.overview')</a>
                    </li>
                     <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#chambers" role="tab" aria-controls="chambers" aria-expanded="true"><i class="fas fa-list"></i> @lang('labels.backend.access.vehicle.tabs.titles.chambers')</a>
                    </li>
                   
                </ul>
               <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.vehicle.show.tabs.overview')
                    </div><!--tab-->
                     <div class="tab-pane " id="chambers" role="tabpanel" aria-expanded="true">
                        @include('backend.vehicle.show.tabs.chambers')
                    </div><!--tab-->
                     
                </div>
                <!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($vehicles->created_at) }} ({{ $vehicles->created_at->diffForHumans() }}),
                    <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($vehicles->updated_at) }} ({{ $vehicles->updated_at->diffForHumans() }})
                    @if($vehicles->trashed())
                        <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($vehicles->deleted_at) }} ({{ $vehicles->deleted_at->diffForHumans() }})
                    @endif
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
