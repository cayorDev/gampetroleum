<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('menus.backend.access.vehicle.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.vehicle.index') }}">@lang('menus.backend.access.vehicle.all')</a>
                <a class="dropdown-item" href="{{ route('admin.vehicle.add') }}">@lang('menus.backend.access.vehicle.add')</a>
                <a class="dropdown-item" href="{{ route('admin.vehicle.deleted') }}">@lang('menus.backend.access.vehicle.deleted')</a>
        
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
