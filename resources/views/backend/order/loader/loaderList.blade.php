@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.loader'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.loader') }} 
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
              
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="loader-table">
                        <thead>
                            <tr>
                               <th>@lang('labels.backend.access.order.table.loader_name')</th>
                           </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
               </div>
           </div><!--col-->
       </div><!--row-->
   </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

$('#loader-table').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    ajax: '/admin/loader/getLoaderData',
    columns: [
    { data: 'name', name: 'name' },
    ]
});
</script> 
@endpush
