
	<table class="table table-striped">
		<tr><th colspan="5" style="text-align: center;"> {{"DIESEL(AGO)"}}</th></tr>
		<tr>
			<th>{{"DATE" }}</th>
			<th>{{ "BOUGHT(L)"}} </th>
			<th>{{ "ORDER NO."}}</th>
			<th>{{ "DELIVERED(L)"}} </th>
			<th>{{"BALANCE" }} </th>
		</tr> 
		@foreach($transaction['AGO'] as $key=>$ago)
		<tr>
			<td>{{ $ago['date']!=""?date_format($ago['date'] ,'Y-m-d'):""}}</td>
			<td>{{ $ago['bought']}} </td>
			<td>{{ !is_null($ago['orderNo'])?$ago['orderNo']:0}}</td>
			<td>{{ $ago['delivered']}} </td>
			<td>{{ $ago['balance']}} </td>
			
		</tr> 
		@endforeach
	</table>

<hr>
	<table class="table table-striped">
		<tr><th colspan="5" style="text-align: center;"> {{"PETROL(PMS)"}}</th></tr>
		<tr>
			<th>{{"DATE" }}</th>
			<th>{{ "BOUGHT(L)"}} </th>
			<th>{{ "ORDER NO."}}</th>
			<th>{{ "DELIVERED(L)"}} </th>
			<th>{{"BALANCE" }} </th>
		</tr> 
		@foreach($transaction['PMS'] as $key=>$pms)
		<tr>
			<td>{{ $pms['date']!=""?date_format($pms['date'] ,'Y-m-d'):""}}</td>
			<td>{{ $pms['bought']}} </td>
			<td>{{ !is_null($pms['orderNo'])?$pms['orderNo']:0}}</td>
			<td>{{ $pms['delivered']}} </td>
			<td>{{ $pms['balance']}} </td>
		</tr>
		@endforeach
	</table>

	<hr>
	<table class="table table-striped">
		<tr><th colspan="5" style="text-align: center;"> {{"KEROSENE/JET"}}</th></tr>
		<tr>
			<th>{{"DATE" }}</th>
			<th>{{ "BOUGHT(L)"}} </th>
			<th>{{ "ORDER NO."}}</th>
			<th>{{ "DELIVERED(L)"}} </th>
			<th>{{"BALANCE" }} </th>
		</tr> 
		@foreach($transaction['JET'] as $key=>$jet)
		<tr>
			<td>{{ $jet['date']!=""?date_format($jet['date'] ,'Y-m-d'):""}}</td>
			<td>{{ $jet['bought']}} </td>
			<td>{{ !is_null($jet['orderNo'])?$jet['orderNo']:0}}</td>
			<td>{{ $jet['delivered']}} </td>
			<td>{{ $jet['balance']}} </td>
		</tr>
		@endforeach
	</table>
