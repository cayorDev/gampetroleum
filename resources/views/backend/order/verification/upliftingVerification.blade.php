@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card main_card">
    <div class="card-body main_card_body">
        <div class="row headerrow">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} 
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->
        
        <div class="row mt-4  mb-4" id="mainDiv" >
            <div class="col" id="upliftingVerification">
                <div class="card">
                    <div id ="form-errors"></div>
                    {{ html()->form('POST', route('order.assignchamber.uplift_vehicle'))->class('form-horizontal')->attribute('id', 'upliftVehicleForm')->open() }}
                    <div class="card-body">
                        <div class="row">

                            @include('backend.includes.gampetrohead')
                        </div>
                        <div style="text-align: center;">
                            <h4>Uplifting Verification / Add Meter Readings </h4>
                        </div>
                        {{ html()->hidden('orderID',$order->id)
                        ->class('form-control')
                        ->attribute('id', 'orderId')
                        ->required() }}
                        <?php $totalProducts = count($order->OrderProducts); ?>
                        <table class="table table-bordered table-hover" id="supplierDataTable">
                            <tr>
                                <th> 
                                    Supplier 
                                </th>
                                <td colspan={{$totalProducts}}> 
                                    {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}}
                                </td>
                                <th>
                                    @lang('labels.backend.access.order.vehicleUplifting.release_number')
                                </th>
                                <td colspan={{$totalProducts}}></td>
                                <th>
                                    @lang('labels.backend.access.order.vehicleUplifting.release_date')
                                </th>
                                <td> {{$order->created_at}}</td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-hover" id="productDataTable">
                            <tr>

                                <th colspan={{$totalProducts+1}} style="text-align: center;" class="updatespan"  >
                                    @lang('labels.backend.access.order.chamberAssignment.quantity_ordered') 
                                </th>
                                

                            </tr>
                            <tr>
                                <th>  </th>

                                @foreach($order->OrderProducts as $product)
                                <td class="updatespan" >
                                    {{$product->Product->name}}
                                </td>

                                @endforeach

                                <!-- <td class="releasebalance"></td>
                                @foreach($order->OrderProducts as $product)
                                <td class="releasebalance">
                                    {{$product->Product->name}}
                                </td>
                                @endforeach -->
                                <!-- <td colspan=2></td> -->
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.balance')
                                </th>

                                @foreach($order->OrderProducts as $product)
                                <td  class="updatespan">
                                    @if(count($ibalances)) 
                                    {{isset($ibalances[$product->product_id]) ? $ibalances[$product->product_id] : 0}} 
                                    @else 
                                    0 
                                    @endif
                                </td>
                                @endforeach
                                
                                <!-- <td class="releasebalance"></td>
                                @foreach($order->OrderProducts as $product)
                                <td class="releasebalance">
                                    @if(count($balances)) 
                                    {{isset($balances[$product->product_id]) ? $balances[$product->product_id] : 0}} 
                                    @else 
                                    0 
                                    @endif
                                </td>
                                @endforeach -->
                                <!-- <td colspan=2></td> -->
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.quantity_now')
                                </th>
                                
                                @foreach($order->OrderProducts as $product)
                                <td  class="updatespan">{{$product->quantity}}</td>
                                @endforeach

                                <!-- <td class="releasebalance"></td>
                                @foreach($order->OrderProducts as $product)
                                <td class="releasebalance">{{$product->quantity}}</td>
                                @endforeach -->
                                <!-- <td colspan=2></td> -->
                            </tr>
                        </table>
                        <table class="table table-bordered table-hover" id="driverDataTable">
                            <tr>
                                <th class="vehicleNum">
                                    @lang('labels.backend.access.order.chamberAssignment.driver_name')
                                </th>
                                <td > {{$order->Driver->name}}
                                </td>
                                <th class="vehicleNum"> 
                                    @lang('labels.backend.access.order.chamberAssignment.vehicle_number')
                                </th>
                                <td > {{$order->Vehicle->license_plate_number}}
                                </td>
                                <th width="50px"> 
                                    Signature
                                </th>
                                <td width="100px"></td>
                            </tr>
                            <tr>
                                <th>  
                                    @lang('labels.backend.access.order.vehicleUplifting.authorized_by')
                                </th>
                                <td id="authorized_by_cell">
                                    {{ html()->select('authorized_by')
                                    ->class('form-control')
                                    ->options($user)
                                    ->placeholder(__('validation.attributes.backend.access.users.select_user'))
                                    ->attribute('id', 'authorized_by')
                                    ->attribute('name', 'authorized_by')->value($order->authorized_by?$order->authorized_by:'') }}
                                </td>
                                <th>  
                                    @lang('labels.backend.access.order.vehicleUplifting.date')
                                </th>
                                <td > 
                                    {{$order->created_at}}
                                </td>
                                <th>  
                                    @lang('labels.backend.access.order.vehicleUplifting.signature')
                                </th>
                                <td></td>
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.seen_by')
                                </th>
                                <td> 
                                    {{ html()->text('seen_by')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.seen_by'))
                                    ->attribute('name', 'seen_by')->value($order->seen_by?$order->seen_by:'') }}
                                </td>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.date')
                                </th>
                                <td > 
                                    {{$order->created_at}}
                                </td>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.signature')
                                </th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>
                                    Ticket # 
                                </th>
                                <td > {{$order->order_id}}</td>
                                <th>
                                    Order #
                                </th>
                                <td colspan=3>{{$order->purchase_order}}</td>
                                <!-- <td colspan=2></td> -->
                            </tr>

                        </table>
                        
                        <table class="table table-bordered table-hover" id="compartmentTable">
                            <tr>
                                <th colspan="7" style="text-align: center;">
                                    @lang('labels.backend.access.order.chamberAssignment.assign_chambers')
                                </th>
                            </tr>
                            <tr>
                                <th class="compartNo">
                                    @lang('labels.backend.access.order.chamberAssignment.compartment_number')
                                </th>
                                <th class="compartCap">
                                    @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
                                </th>
                                <th class="assignedProd" >
                                    @lang('labels.backend.access.order.chamberAssignment.assigned_product')
                                </th>
                                <th class="assignedQuantity">
                                    @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
                                </th>
                                <th class="loader">
                                    @lang('labels.backend.access.order.chamberAssignment.loader')
                                </th>
                                <th class="total_loaded">
                                    @lang('labels.backend.access.order.vehicleUplifting.total_loaded')
                                </th>
                                <th>
                                    @lang('labels.backend.access.order.vehicleUplifting.loader_signature')
                                </th>
                            </tr>
                            @foreach($order->Vehicle->vehicle_chamber as $vehicleChamber)
                            <?php 
                            $assignedchamberID=null;
                            $assignedchamberProductID =0;
                            $assignedchamberProductName="";
                            $assignedchamberQuantity=0;
                            $assignedchamberLoader="";
                            $assignedchamberTotalLoaded=0;
                            ?>
                            @foreach($order->Chambers as $assignedChamber)
                            @if($assignedChamber->vehicle_chamber_id == $vehicleChamber->id)
                            <?php 
                            $assignedchamberID=$assignedChamber->id;
                            $assignedchamberProductID=$assignedChamber->product_id;
                            $assignedchamberProductName=$assignedChamber->Product->name;
                            $assignedchamberQuantity=$assignedChamber->quantity;
                            $assignedchamberLoader=$assignedChamber->loader;
                            $assignedchamberTotalLoaded=$assignedChamber->total_loaded;
                            ?>
                            @endif
                            @endforeach
                            <tr>
                                <td>
                                    {{$vehicleChamber->chamber_number}}
                                </td>
                                <td>
                                    {{$vehicleChamber->capacity}}
                                </td>
                                {{ html()->hidden('chamber_id',$vehicleChamber->id)
                                ->class('form-control')
                                ->attribute('name', 'products[product_'.$vehicleChamber->id.'][chamberID]')
                                ->required() }}

                                {{ html()->hidden('id',$assignedchamberID)->class('form-control')->attribute('name', 'products[product_'.$vehicleChamber->id.'][id]')->required() }}

                                <td> 
                                    {{ html()->hidden('product_id')
                                    ->class('form-control')
                                    ->value( $assignedchamberProductID)
                                    ->attribute('name', 'products[product_'.$vehicleChamber->id.'][productID]')
                                    ->attribute('readonly', true)
                                    ->required() 
                                }}
                                {{$assignedchamberProductName}} 
                            </td>
                            <td>
                                {{ html()->text('quantity')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.order.quantity'))
                                ->attribute('name', 'products[product_'.$vehicleChamber->id.'][quantity]')
                                ->attribute('value', $assignedchamberQuantity)
                                ->required() }}
                            </td>
                            <td class="loader">
                               {{ html()->text('loader')
                               ->type('text')
                               ->class('form-control typeahead tt-query')
                               ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader'))
                               ->attribute('autocomplete', 'on')
                               ->attribute('typeahead-editable', false)
                               ->attribute('spellcheck', false)
                               ->attribute('id', 'loader_'.$vehicleChamber->id)
                               ->attribute('name', 'products[product_'.$vehicleChamber->id.'][loader]')
                               ->attribute('value', $assignedchamberLoader)
                           }}
                       </td>
                       <td class="total_loaded"> {{ html()->text('total_loaded')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.order.placeholder.total_loaded'))
                        ->attribute('name', 'products[product_'.$vehicleChamber->id.'][total_loaded]')
                        ->attribute('id', $assignedChamber->Product->id)
                        ->attribute('placeholder', 'Loaded Quantity')
                        ->attribute('value', $assignedchamberTotalLoaded ?: '' )
                        ->required() }}</td>
                        <td></td>
                    </tr>
                    @endforeach
                </table>
                        <!-- <div class="form-group row loaderData">
                            {{ html()->label(__('validation.attributes.backend.access.order.placeholder.loader'))->class('col-md-2 form-control-label')->for('loader') }}

                            <div class="col-md-5">
                                {{ html()->text('loader1')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader1'))
                                ->attribute('value', $order->loader1)
                                ->required() }}
                            </div>
                            <div class="col-md-5">
                                {{ html()->text('loader2')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader2'))
                                ->attribute('value', $order->loader2) }}
                            </div>
                        </div> -->
                        <div id="chamberDetail">
                            <table class="table table-bordered table-hover">
                                <tr> <th colspan="6" style="text-align: center;">
                                    @lang('labels.backend.access.order.chamberAssignment.assign_meter_reading')
                                </th></tr>
                                <tr>
                                    <th>
                                        Product 
                                    </th>
                                    <th>
                                        Meter ID
                                    </th>
                                    <th>
                                        Opening Meter
                                    </th>
                                    <th>
                                        Closing Meter
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Unit
                                    </th>
                                </tr>
                                @foreach($order->OrderProducts as $products)

                                <tr>
                                    <td>{{$products->Product->name}}</td>
                                    {{ html()->hidden('id',$products->OrderProductMeter?$products->OrderProductMeter->id:0 )
                                    ->class('form-control')
                                    ->attribute('name', 'product['.$products->id.'][id]')
                                    ->required() }}
                                    {{ html()->hidden('order_product_id',$products->id)
                                    ->class('form-control')
                                    ->attribute('name', 'product['.$products->id.'][order_product_id]')
                                    ->required() }}
                                    <td>{{ html()->select('meter_id')
                                        ->class('form-control')
                                        ->options($meter[$products->Product->sku])
                                        ->placeholder(__('validation.attributes.backend.access.order.placeholder.meter'))
                                        ->attribute('name', 'product['.$products->id.'][meter_id]')->value($products->OrderProductMeter?$products->OrderProductMeter->meter_id:'')->required() }}</td>
                                        <td>
                                            {{ html()->text('opening_meter')->class('form-control opening-reading')->placeholder(__('validation.attributes.backend.access.order.placeholder.opening_meter'))->attribute('name', 'product['.$products->id.'][opening_meter]')->
                                            attribute('id',$products->id)->value($products->OrderProductMeter?$products->OrderProductMeter->opening_meter:'')->required() }}
                                        </td>
                                        <td>
                                            {{ html()->text('closing_meter')->class('form-control closing-reading')->placeholder(__('validation.attributes.backend.access.order.placeholder.closing_meter'))->attribute('name', 'product['.$products->id.'][closing_meter]')->attribute('id',$products->id)->value($products->OrderProductMeter?$products->OrderProductMeter->closing_meter:'')->required() }}
                                        </td>
                                        <td>
                                            {{ html()->text('quantity')->class('form-control reading-result')->placeholder(__('validation.attributes.backend.access.order.placeholder.quantity'))->attribute('name', 'product['.$products->id.'][quantity]')->value($products->OrderProductMeter?$products->OrderProductMeter->quantity:'')->required() }}
                                        </td>
                                        <td>{{$products->Product->measuring_unit}}</td>
                                    </tr>
                                    @endforeach
                                </table>

                                <h5>Upper Seals <span class="badge badge-primary">{{$order->Vehicle->upper_seals}}</span></h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ html()->textarea('upper_seal')->class('form-control')->placeholder(__('validation.attributes.backend.access.vehicle.upper_seals'))->attribute('name', 'vehicle_chamber[upper_seal]')->value($seals  ? $seals['vehicle_chamber']['upper_seal']:'')->required() }}
                                    </div>
                                    
                                </div>
                                <br>
                                <h5>Lower Seals <span class="badge badge-primary">{{$order->Vehicle->lower_seals}}</span></h5>

                                <div class="row">
                                    <div class="col-md-12">
                                        {{ html()->textarea('lower_seal')->class('form-control')->placeholder(__('validation.attributes.backend.access.vehicle.lower_seals'))->attribute('name', 'vehicle_chamber[lower_seal]')
                                        ->value( $seals  ? $seals['vehicle_chamber']['lower_seal']:'')
                                        ->required() }}
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="chamberData">
                                    <h5>Comments </h5>
                                    <div class="row">
                                    <div class=" col-md-12">

                                        {{ html()->textarea('comments')->class('form-control')->placeholder(__('validation.attributes.backend.access.order.placeholder.comments'))->value($order->comments?$order->comments:'')->required() }}
                                    </div>
                                     </div>
                                </div>
                            </div>
                            @if($isHFO)
                            <table class="table" id="controlRoomDetail">
                                <tr>
                                    <th  colspan="4" style="text-align: center;">
                                        @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.truck_loading_ticket_number')
                                    </th>
                                    <td>
                                        {{ html()->text('ticket_number')
                                        ->class('form-control')
                                        ->attribute('name', 'truck[truck_loading_ticket_number]')
                                        ->attribute('value',  $order->Truck ? $order->Truck->truck_loading_ticket_number:'0')
                                        ->required() }}
                                    </td>
                                    <th colspan="2"> 
                                        Control Room Supervisor 
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.weight_before_loading')
                                    </th>
                                    <td>
                                        {{ html()->text('weight_before_loading')
                                        ->class('form-control')
                                        ->attribute('name', 'truck[weight_before_loading]')
                                        ->attribute('value',  $order->Truck ? $order->Truck->weight_before_loading :'0')
                                        ->required() }}
                                    </td>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.supervisor_name')
                                    </th>
                                    <td>
                                        {{ $order->Truck && $order->Truck->Supervisor ? $order->Truck->Supervisor->first_name." ".$order->Truck->Supervisor->last_name : Auth::user()->fullname }}

                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.weight_after_loading')
                                    </th>
                                    <td>
                                        {{ html()->text('weight_after_loading')->class('form-control')->attribute('name', 'truck[weight_after_loading]') ->attribute('value', $order->Truck ? $order->Truck->weight_after_loading :'0')->required() }}
                                    </td>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.date')
                                    </th>
                                    <td>
                                        {{$order->created_at}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.net_weight')
                                    </th>
                                    <td> 
                                        {{ html()->text('net_weight')
                                        ->class('form-control')
                                        ->attribute('name', 'truck[net_weight]')
                                        ->attribute('value', $order->Truck ? $order->Truck->net_weight :'0')
                                        ->required() }}
                                    </td>
                                    <th>
                                        @lang('labels.backend.access.order.vehicleUplifting.signature')
                                    </th>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                            @else 
                            <table class="table" id="controlRoomDetail">
                                <tr>
                                    <th  colspan="12" style="text-align: center;">
                                        @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        @lang('labels.backend.access.order.vehicleUplifting.supervisor_name'):
                                    </th>
                                    <td>
                                        {{ $order->AuthorisedBy ? $order->AuthorisedBy->first_name.' '.$order->AuthorisedBy->last_name : ''}}
                                    </td>
                                    <th colspan="2">
                                        @lang('labels.backend.access.order.vehicleUplifting.date'):
                                    </th>
                                    <td>
                                        {{$order->created_at}}
                                    </td>
                                    <th colspan="2">
                                        @lang('labels.backend.access.order.vehicleUplifting.signature'):
                                    </th>
                                    <td>

                                    </td>

                                </tr>
                            </table>
                            @endif

                        </div>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}
                    <button class="button btn btn-primary btn-sm" onclick="printpage();"> Print </button>
                </div><!--col-->
                <div class="col text-right">
                    {{ html()->button('Submit')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'upliftVehicleSubmit')}}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->

    @endsection
    @push('after-styles')
    <style type="text/css" media="print">
        @page {
            size: auto;    
            margin: 0;  
        }

        @media print {

            header, footer { display: none !important; }
            .card {
                border: none !important;
                padding: 20px !important;
            }
            .table-bordered>tbody>tr>th{
                padding: 5px !important;
            }
            .table td ,
            .table th {
                padding: 2px !important;
            }
            .card-body { font-size: 16px !important; color:#23282c !important;}

            .managerDetail, .verificationDetail{
                width: auto;
            }
            .verificationDetail{
                margin-right: 0px;
                margin-left: auto;
            }
            .pagefooter{
                margin-left: 25px!important;
                margin-right: 25px!important;
                margin-top: 20px !important;
                background-color: white;
            }
            .card-footer{
                display: none;
            }
            .card-body { padding: none !important;}
            .form-control{
                border: none !important;
            }

            .form-control::placeholder {
                color:transparent !important;
                -webkit-text-fill-color: transparent !important;
                text-shadow: none !important;
            }
            ::-webkit-input-placeholder { /* WebKit browsers */
                color: transparent!important;
            }
            :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                color: transparent!important;
            }
            ::-moz-placeholder { /* Mozilla Firefox 19+ */
                color: transparent!important;
            }
            :-ms-input-placeholder { /* Internet Explorer 10+ */
                color: transparent!important;
            }
            .breadcrumb{
                display: none !important;
            }
            .card-title{
                display: none !important;
            }
            .alert { 
                display: none !important;
            }
            .chamberData {
                line-height: 5px !important;
            }
            #chamberDetail{
                display: none !important;
            }
            input{
                height:30px !important;
            }
            #compartmentTable{
                text-align: center !important;
                min-height: 400px !important;
            }
            #compartmentTable tr:not(:last-child){
                height: 20px !important;
            } 
            #supplierDataTable, #productDataTable, #driverDataTable tr{
                height: 20px !important;
            }
            #compartmentTable tr{
                border: none !important;
            }
            .table#compartmentTable td, .table-bordered#compartmentTable td{
                border:none !important;
            }
            .table#compartmentTable>tbody>tr>td{
                border-left: 1px solid lightgrey !important;
                border-right: 1px solid lightgrey !important;
                border-top: none !important;
                border-bottom-color: lightgray !important;
                border-bottom: 1px dashed lightgray !important;
            }
            .emptySelect{
                color:transparent ;
                -webkit-text-fill-color: transparent ;
                text-shadow: none ;
            }
            #controlRoomDetail {
                position: fixed;
                bottom: 50px; 
            }
            #upliftingVerification{
                margin-top: none !important;
            }
            .main_card{
                padding: 0px !important;
                margin: 0px !important;
            }
            .main_card_body{
                padding: 0px !important;
                margin: 0px !important;
            }
            hr{
                display: none;
            }
            .main, .container-fluid {
                padding: 0px !important;
            }
            #form-errors{
                display: none !important;
            }
            .header-fixed .app-body {
                margin-top: 0px !important;
            }
            .headerrow{
                display: none !important;
            }
            .vehicleNum{
                width:140px !important;
            }

        }
    </style>

    <style>
        .total_loaded{
            width: 130px !important;
        }
        .compartNo{
            width: 130px !important;
        }
        .compartCap{
            width: 130px !important;
        }
        .assignedProd{
            width: 130px !important;
        }
        .assignedQuantity{
            width: 130px !important;
        }
        .loader{
            width: 250px !important;
        }
        #compartmentTable th{
            text-align: center;
        }


    </style>
    <link rel="stylesheet" href="{{ asset('css/typehead.css')}}">
    @endpush
    @push('after-scripts')
    <script type="text/javascript" src="/js/typehead.js"></script>
    <script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $("#authorized_by option:first").attr("disabled", "true");
            var url = window.location.href;
            if(url.indexOf('?isOrderCreated') != -1){
                printpage(); 
            }

            //loader data
            var orderChamber =  '{!! json_encode($order->Vehicle->vehicle_chamber) !!}';
            var loader =  '{!! json_encode($loader) !!}';
            console.log(loader);
            var obj = jQuery.parseJSON(orderChamber);
            $.each(obj, function(key,value) {
              var loaders = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: JSON.parse(loader)
            });
              loaders.initialize();

              $('#loader_'+value.id).typeahead(
                null, {
                    autoSelect: true,
                    minLength: 0, 
                    limit: Infinity,
                    showHintOnFocus: true,
                    name: 'name',
                    displayKey: 'name',
                    source: loaders.ttAdapter()
                }).on('typeahead:selected', function(event, data){            
                    $('#loader_'+value.id).val(data.name); 

                })
            });
            
        });

        function printpage()
        {
            // $('.releasebalance').hide();
            // $('.updatespan').attr('colspan',3);
            var targetElement = document.getElementById("authorized_by");
            if($('#authorized_by').val() == null){
             $('#authorized_by').replaceWith('<input type="text" class="form-control" name="authorized_by" id="authorized_by" value="">');
         } else {
            var selectedvalue=$('#authorized_by option:selected').text();
            $('#authorized_by').replaceWith('<input type="text" class="form-control" name="authorized_by" id="authorized_by" value="'+selectedvalue+'">');
        }
        var element = document.getElementById("mainDiv");
        element.classList.remove("mt-4");
        var printContents = document.getElementById("upliftingVerification").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        // $('.releasebalance').show();
        // $('.updatespan').attr('colspan',1);
        document.body.innerHTML = originalContents;
    }
    $(window).on('afterprint', function () { 
        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("?"));
        } else {
            clean_uri= window.location.href;
        }
        window.location=clean_uri;
    })


</script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/order.js"></script>
@endpush

