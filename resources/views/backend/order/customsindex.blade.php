@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
    @include('backend.order.includes.breadcrumb-links')
@endsection

@push ('after-styles')
<link rel="stylesheet" type="text/css" href="/css/funky-style.css">
<style>
.badge{
    margin-left: 5px;
    margin-right: 5px;
    display: inline;
}
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-body">
            

        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} <small class="text-muted">{{ __('labels.backend.access.order.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">

            
                
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="order-table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.order.tabs.content.overview.orderReference')</th>
                            <th>Supplier</th>
                            <th>@lang('labels.backend.access.order.table.vehicle')</th>
                            <th>@lang('labels.backend.access.order.table.driver')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
       
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
                $('#order-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: '/order/getCustomsOrderData',
                    columns: [
                        { data: 'order_reference', name: 'order_reference'},
                        { data: 'supplier', name: 'supplier' },
                        { data: 'vehicle', name: 'vehicle' },
                        { data: 'driver', name: 'driver' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            
    </script> 
@endpush
