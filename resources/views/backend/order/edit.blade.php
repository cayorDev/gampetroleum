@extends('backend.layouts.app')

@section('title', __('labels.backend.access.order.management') . ' | ' . __('labels.backend.access.order.edit'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div id ="form-errors"></div>

{{ html()->modelForm($order, 'PATCH', route('order.update', $order->id))->class('form-horizontal')->attribute('id', 'orderUpdateForm')->open() }}

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.order.management')
                    <small class="text-muted">@lang('labels.backend.access.order.edit')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <hr>
        <div class="row">
            @include('backend.includes.gampetrohead')
        </div>
        <div class="row mt-4 mb-4">
            <div class="col">
                @if($isSepcial)
                    @include('backend.order.forms.edit.orderashfo')
                @else
                    @if($order->supplier_omc_id)
                        @include('backend.order.forms.edit.orderasomc')
                    @else
                        @include('backend.order.forms.edit.orderasimporter')
                    @endif
                @endif
                
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}
            </div><!--col-->
            <div class="col text-right">
                {{ html()->button('Update')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'updateOrderSubmit')}}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->form()->close() }}

@endsection
@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/typehead.css')}}">
@endpush
@push('after-scripts')
<script type="text/javascript" src="/js/typehead.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var driverData =  '{!! json_encode($drivers) !!}';
        var vehicleData =  '{!! json_encode($vehicles) !!}';

        var drivers = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: JSON.parse(driverData)
        });
        drivers.initialize();
        $('.typeaheadDriver').typeahead(
            null, {
                autoSelect: true,
                minLength: 0, 
                limit: Infinity,
                showHintOnFocus: true,
                name: 'drivers',
                displayKey: 'name',
                source: drivers.ttAdapter()
            }).on('typeahead:selected', function(event, data){
                $('#driver_id').val(data.id);
                if(data.id == 0){
                    $('#driver_Detail').show();
                } else {
                    $('#driver_Detail').hide();
                }
            }).on("typeahead:change", function(aEvent, aSuggestion) {
                var driverExists =  JSON.parse(driverData).some(element => element.name == aSuggestion);
                if (driverExists) {
                    $('#driver_Detail').hide();
                } else {
                    $('#driver_id').val(0);
                    $('#driver_Detail').show();
                }
            }); 

            var vehicles = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('license_plate_number'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: JSON.parse(vehicleData)
            });
            vehicles.initialize();

            $('#typeaheadvehicle').typeahead(
                null, {
                    autoSelect: true,
                    minLength: 0, 
                    limit: Infinity,
                    showHintOnFocus: true,
                    name: 'vehicles',
                    displayKey: 'license_plate_number',
                    source: vehicles.ttAdapter()
                }).on('typeahead:selected', function(event, data){            
                    $('#typeaheadvehicle').val(data.id); 
                    $('#vehicle_id').val(data.id);
                    var vehicleID=$('#vehicle_id').val();
                    handleChambers(vehicleID);

                }).on("typeahead:change", function(aEvent, aSuggestion) {
                    var driverExists =  JSON.parse(vehicleData).some(element => element.license_plate_number == aSuggestion);
                    if (!driverExists) {
                        $(this).val('');
                        $('#vehicle_id').val(0);
                    } 


                });
                var vehicleID=$('#vehicle_id').val();
                handleChambers(vehicleID);
            }); 

    function handleChambers(vehicleID) {
        var supplier =  '{!! $order->supplier_omc_id !!}';
        if(supplier) {
            var options = $('.products_selectOmc option');
        } else {
            var options = $('.products_select option');
        }

        var values = $.map(options ,function(option) {
            return option.value;
        });
        var queryString ="?order="+$('#orderId').val();
        if (values.length) {
            var queryString = queryString+"&products="+values;
        }
        $.ajax({
            url: '/order/getVehicleChambers/'+vehicleID+''+queryString,
            type: 'get',
            dataType: 'json',
            success: function(response) {
             if (response.success) {
                $('.chamber_container').html(response.html);
            }
        },
        error: function(data) {

            $('.chamber_container').html('');
        }
    });
    } 
    
</script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/order_edit.js"></script>
@endpush