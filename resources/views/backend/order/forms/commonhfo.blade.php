 <div class="form-group row">
    {{ html()->label('Weight Before Loading')->class('col-md-2 form-control-label')->for('beforeloading') }}

    <div class="col-md-10">
       {{ html()->text('beforeloading')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Truck weight before loading')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'beforeloading')
       ->attribute('name', 'beforeloading')
       ->required() }}
   </div><!--col-->
</div><!--form-group-->

<div class="form-group row">
    {{ html()->label('Weight After Loading')->class('col-md-2 form-control-label')->for('afterloading') }}

    <div class="col-md-10">
       {{ html()->text('afterloading')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Truck weight after loading')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'afterloading')
       ->attribute('name', 'afterloading')
       ->required() }}
   </div><!--col-->
</div><!--form-group-->


<div class="form-group row">
    {{ html()->label('Net Weight')->class('col-md-2 form-control-label')->for('netweight') }}

    <div class="col-md-10">
       {{ html()->text('netweight')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Net Weight')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'netweight')
       ->attribute('name', 'netweight')
       ->required() }}
   </div><!--col-->
</div><!--form-group-->

<div class="form-group row">
    {{ html()->label('Authorised By')->class('col-md-2 form-control-label')->for('authorised_by') }}
    <div class="col-md-10">
        {{ html()->select('authorised_by')
        ->class('form-control')
        ->options($adminsupervisors)
        ->placeholder(__('validation.attributes.backend.access.users.select_user'))
        ->attribute('name', 'authorised_by')
        ->required() }}
    </div><!--col-->
</div><!--form-group-->