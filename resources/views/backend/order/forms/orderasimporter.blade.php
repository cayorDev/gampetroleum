<div class="card-body">
    {{ html()->form('POST', route('order.store'))->class('form-horizontal')->attribute('id', 'orderAddForm')->open() }}
    <div class="row">
        @include('backend.includes.gampetrohead')
    </div>
    <div class="row mt-4 mb-4">

        <div class="col">
           <div class="form-group row ">
            {{ html()->label(__('validation.attributes.backend.access.order.omc'))->class('col-md-2 form-control-label')->for('omc') }}

            <div class="col-md-10">
                {{ html()->select('omc_id')
                ->class('form-control')
                ->options($omc)
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.omc'))
                ->attribute('id', 'omcSelect')
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row ">
            {{ html()->label(__('validation.attributes.backend.access.order.importer'))->class('col-md-2 form-control-label')->for('importer') }}
            <div class="col-md-10">
                {{ html()->select('importer_id')
                ->class('form-control')
                ->options($importers)
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.importer'))
                ->attribute('id', 'importerSelect')
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row " id="inventoryContainer">
            <div class="col-md-12 text-center" id="inventoryAvailable">
            </div>
        </div>
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.product'))->class('col-md-2 form-control-label')->for('product') }}
            <div class="col-md-10">
                {{ html()->multiselect("products[count]")
                ->class('form-control products_select')->value() }}
            </div>
        </div>
        <div class="products_container"></div>
        <div class="form-group row driverDiv " >
            {{ html()->label('Driver/Driver Name')->class('col-md-2 form-control-label')->for('driver') }}

            <div class="col-md-10">
                {{ html()->text('driver')
                ->class('form-control typeahead typeaheaddriver tt-query' )
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.driver'))
                ->attribute('autocomplete', 'on')
                ->attribute('typeahead-editable', false)
                ->attribute('spellcheck', false)
                ->attribute('name', 'driver[name]')
                ->required() }}

                {{ html()->hidden('driver_id')
                ->attribute('class', 'driver_id')
                ->required() }}
            </div><!--col-->

        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.purchase_order'))->class('col-md-2 form-control-label')->for('purchase_order') }}
            <div class="col-md-10">
                {{ html()->text('purchase_order')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.order.purchase_order'))
                ->attribute('min', 0)
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.vehicle'))->class('col-md-2 form-control-label')->for('vehicle') }}

            <div class="col-md-10">
             {{ html()->text('vehicle')
             ->class('form-control typeahead  tt-query' )
             ->placeholder(__('validation.attributes.backend.access.order.placeholder.vehicle'))
             ->attribute('autocomplete', 'on')
             ->attribute('id', 'typeaheadvehicle')
             ->attribute('typeahead-editable', false)
             ->attribute('spellcheck', false)
             ->attribute('name', 'vehicleLicencePlate')
             ->required() }}

             {{ html()->hidden('vehicle_id')
             ->attribute('id', 'vehicle_id')
             ->required() }}

         </div><!--col-->
     </div><!--form-group-->
     <div class="chamber_container"></div>
 </div><!--col-->
</div><!--row-->
</div><!--card-body-->

<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ html()->button('Create')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'createOrderSubmit')}}
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
{{ html()->form()->close() }}