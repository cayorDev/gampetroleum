

      <div class="form-group row ">
        {{ html()->label('Select Product')->class('col-md-2 form-control-label')->for('product') }}
        <div class="col-md-10">
           {{ html()->hidden('id',$order->id)
        ->class('form-control')
        ->attribute('id', 'orderId')
        ->required() }}
          {{ html()->select('product_id')
          ->class('form-control')
          ->options($hfoproducts)
          ->attribute('id', 'productSelect')
          ->required() }}
        </div><!--col-->
      </div><!--form-group-->
      <div class="form-group row " >
        {{ html()->label(__('validation.attributes.backend.access.order.omc'))->class('col-md-2 form-control-label')->for('omc') }}
        <div class="col-md-10">
          {{ html()->select('omc_id')
          ->class('form-control')
          ->options($omc)
          ->placeholder(__('validation.attributes.backend.access.order.placeholder.omc'))
          ->attribute('id', 'omcSelect')
          ->required() }}
        </div><!--col-->
      </div><!--form-group-->
      <div class="form-group row ">
        {{ html()->label(__('validation.attributes.backend.access.order.importer'))->class('col-md-2 form-control-label')->for('importer') }}
        <div class="col-md-10">
          {{ html()->select('importer_id')
          ->class('form-control')
          ->options($importers)
          ->placeholder(__('validation.attributes.backend.access.order.placeholder.importer'))
          ->attribute('id', 'importerSelectHFO')
          ->value($order->importer_id)
          ->required() }}
        </div><!--col-->
      </div><!--form-group-->
     
      <div class="form-group row driverDiv " >
        {{ html()->label('Driver/Driver Name')->class('col-md-2 form-control-label')->for('driver') }}

        <div class="col-md-10">
          {{ html()->text('driver')

          ->class('form-control typeahead typeaheadDriver tt-query' )
          ->placeholder(__('validation.attributes.backend.access.order.placeholder.driver'))
          ->attribute('autocomplete', 'on')
          ->attribute('typeahead-editable', false)
          ->attribute('spellcheck', false)
          ->attribute('name', 'driver[name]')
          ->attribute('value', $order->Driver->name)
          ->required() }}

          {{ html()->hidden('driver_id')
          ->attribute('class', 'driver_id')
           ->attribute('value', $order->driver_id)
          ->required() }}
        </div><!--col-->

      </div><!--form-group-->
      <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.order.purchase_order'))->class('col-md-2 form-control-label')->for('purchase_order') }}
        <div class="col-md-10">
          {{ html()->text('purchase_order')
          ->class('form-control')
          ->placeholder(__('validation.attributes.backend.access.order.purchase_order'))
          ->attribute('min', 0)
          ->required() }}
        </div><!--col-->
      </div><!--form-group-->
      <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.order.vehicle'))->class('col-md-2 form-control-label')->for('vehicle') }}

        <div class="col-md-10">
         {{ html()->text('vehicle')
         ->class('form-control typeahead  tt-query' )
         ->placeholder(__('validation.attributes.backend.access.order.placeholder.vehicle'))
         ->attribute('autocomplete', 'on')
         ->attribute('id', 'typeaheadvehicle')
         ->attribute('typeahead-editable', false)
         ->attribute('spellcheck', false)
         ->attribute('name', 'vehicleLicencePlate')
          ->attribute('value', $order->Vehicle->license_plate_number)
         ->required() }}

         {{ html()->hidden('vehicle_id')
         ->attribute('id', 'vehicle_id')
         ->attribute('value', $order->vehicle_id)
         ->required() }}

       </div><!--col-->
     </div><!--form-group-->

     <div class="form-group row">
      {{ html()->label(__('validation.attributes.backend.access.order.location'))->class('col-md-2 form-control-label')->for('location') }}

      <div class="col-md-10">
       {{ html()->text('location')
       ->class('form-control' )
       ->placeholder(__('validation.attributes.backend.access.order.placeholder.location'))
       ->attribute('autocomplete', 'on')
       ->attribute('spellcheck', false)
       ->attribute('name', 'location')
       ->attribute('value', $order->Truck->location)
       ->required() }}

     </div><!--col-->
   </div><!--form-group-->
  <div class="form-group row">
    {{ html()->label('Weight Before Loading')->class('col-md-2 form-control-label')->for('beforeloading') }}

    <div class="col-md-10">
       {{ html()->text('beforeloading')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Truck weight before loading')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'beforeloading')
       ->attribute('name', 'beforeloading')
       ->attribute('value', $order->Truck->weight_before_loading)
       ->required() }}
   </div><!--col-->
</div><!--form-group-->

<div class="form-group row">
    {{ html()->label('Weight After Loading')->class('col-md-2 form-control-label')->for('afterloading') }}

    <div class="col-md-10">
       {{ html()->text('afterloading')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Truck weight after loading')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'afterloading')
       ->attribute('name', 'afterloading')
         ->attribute('value', $order->Truck->weight_after_loading)
       ->required() }}
   </div><!--col-->
</div><!--form-group-->


<div class="form-group row">
    {{ html()->label('Net Weight')->class('col-md-2 form-control-label')->for('netweight') }}

    <div class="col-md-10">
       {{ html()->text('netweight')
       ->type('number')
       ->class('form-control' )
       ->placeholder('Net Weight')
       ->attribute('autocomplete', 'on')
       ->attribute('id', 'netweight')
       ->attribute('name', 'netweight')
       ->attribute('value', $order->Truck->net_weight)
       ->required() }}
   </div><!--col-->
</div><!--form-group-->

<div class="form-group row">
    {{ html()->label('Authorised By')->class('col-md-2 form-control-label')->for('authorised_by') }}
    <div class="col-md-10">
        {{ html()->select('authorised_by')
        ->class('form-control')
        ->options($adminsupervisors)
        ->placeholder(__('validation.attributes.backend.access.users.select_user'))
        ->attribute('name', 'authorised_by')
        ->value($order->authorized_by)
        ->required() }}
    </div><!--col-->
</div><!--form-group-->
 


