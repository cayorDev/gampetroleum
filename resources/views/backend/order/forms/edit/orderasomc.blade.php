
 <div class="form-group row ">
                    {{ html()->label(__('validation.attributes.backend.access.order.omc'))->class('col-md-2 form-control-label')->for('omc') }}

                    <div class="col-md-10">
                        {{ html()->select('omc_id')
                        ->class('form-control')
                        ->options($omc)
                        ->placeholder(__('validation.attributes.backend.access.order.placeholder.omc'))
                        ->attribute('id', 'omcToOmc')
                        ->value($order->omc_id)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="form-group row ">
                    {{ html()->label(__('validation.attributes.backend.access.order.importer'))->class('col-md-2 form-control-label')->for('supplierOmc') }}
                    <div class="col-md-10">
                        {{ html()->select('supplier_importer_omc_id')
                        ->class('form-control')
                        ->options($importers)
                        ->placeholder('Select Supplier')
                        ->attribute('id', 'supplierOmc')
                        ->value($supplier)
                        ->required() }}

                        {{ html()->hidden('supplier_omc_id')
                        ->attribute('id', 'supplier_omc_id')
                        ->attribute('value', $order->supplier_omc_id)
                        ->required() }}

                        {{ html()->hidden('importer_id')
                        ->attribute('id', 'omc_importer_id')
                        ->attribute('value', $order->importer_id)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="form-group row " id="inventoryContainerOmc">
                    <div class="col-md-12 text-center" id="inventoryAvailableOmc">
                        @foreach($inventory as $key=>$inv)
                        <label class="badge">{{$key.":".$inv}}</label>
                        @endforeach
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.order.product'))->class('col-md-2 form-control-label')->for('product') }}
                    <div class="col-md-10">
                        {{ html()->multiselect("products[count]",$allProducts,$selectedProducts)
                        ->class('form-control products_selectOmc')}}
                    </div>
                </div>
                <div class="products_containerOmc">
                    @foreach($order->OrderProducts as $orderproducts)
                    <div class="form-group row">
                        {{ html()->label($orderproducts->Product->name)->class('col-md-2 form-control-label')->for('product['.$orderproducts->product_id .']') }}
                        <div class="col-md-10">
                            {{ html()->text('quantity')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.order.quantity'))
                            ->attribute('name', 'products['.$orderproducts->product_id .']')
                            ->attribute('value', $orderproducts->quantity)
                            ->required() }}
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="form-group row driverDiv " >
                    {{ html()->label('Driver/Driver Name')->class('col-md-2 form-control-label')->for('driver') }}
                    <div class="col-md-10">
                        {{ html()->text('driver')
                        ->class('form-control typeahead typeaheadDriver tt-query' )
                        ->placeholder(__('validation.attributes.backend.access.order.placeholder.driver'))
                        ->attribute('value', $order->Driver->name)
                        ->attribute('autocomplete', 'off')
                        ->attribute('spellcheck', false)
                        ->attribute('name', 'driver[name]')
                        ->required() }}

                        {{ html()->hidden('driver_id')
                        ->attribute('id', 'driver_id')
                        ->attribute('value', $order->driver_id)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.order.purchase_order'))->class('col-md-2 form-control-label')->for('purchase_order') }}
                    <div class="col-md-10">
                        {{ html()->text('purchase_order')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.order.purchase_order'))
                        ->attribute('min', 0)
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.order.vehicle'))->class('col-md-2 form-control-label')->for('vehicle') }}
                    <div class="col-md-10">
                        {{ html()->text('vehicle')
                        ->class('form-control typeahead  tt-query' )
                        ->placeholder(__('validation.attributes.backend.access.order.placeholder.vehicle'))
                        ->attribute('autocomplete', 'on')
                        ->attribute('value', $order->Vehicle->license_plate_number)
                        ->attribute('id', 'typeaheadvehicle')
                        ->attribute('typeahead-editable', false)
                        ->attribute('spellcheck', false)
                        ->attribute('name', 'vehicleLicencePlate')
                        ->required() }}

                        {{ html()->hidden('vehicle_id')
                        ->attribute('id', 'vehicle_id')
                        ->attribute('value', $order->vehicle_id)
                        ->required() }}
                        {{ html()->hidden('id',$order->id)
                        ->class('form-control')
                        ->attribute('id', 'orderId')
                        ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="chamber_container"></div>
                @if($order->is_vehicle_loaded===1)
                <div id="chamberDetail">
                  <div id="meterDiv">
                    <table class="table table-bordered table-hover">
                        <tr> <th colspan="6" style="text-align: center;">
                            @lang('labels.backend.access.order.chamberAssignment.assign_meter_reading')
                        </th></tr>
                        <tr>
                            <th>
                                Product 
                            </th>
                            <th>
                                Meter ID
                            </th>
                            <th>
                                Opening Meter
                            </th>
                            <th>
                                Closing Meter
                            </th>
                            <th>
                                Quantity
                            </th>
                           
                        </tr>

                        @foreach($order->OrderProducts as $products)

                        <tr>
                            <td>{{$products->Product->name}}</td>
                            {{ html()->hidden('id',$products->OrderProductMeter?$products->OrderProductMeter->id:0 )
                            ->class('form-control')
                            ->attribute('name', 'product['.$products->id.'][id]')
                            ->required() }}
                            {{ html()->hidden('id',$products->Product->id )
                            ->class('form-control')
                            ->attribute('name', 'product['.$products->id.'][product_id]')
                            ->required() }}
                            {{ html()->hidden('order_product_id',$products->id)
                            ->class('form-control')
                            ->attribute('name', 'product['.$products->id.'][order_product_id]')
                            ->required() }}
                            <td>{{ html()->select('meter_id')
                                ->class('form-control')
                                ->options($meter[$products->Product->sku])
                                ->placeholder(__('validation.attributes.backend.access.order.placeholder.meter'))
                                ->attribute('name', 'product['.$products->id.'][meter_id]')->value($products->OrderProductMeter?$products->OrderProductMeter->meter_id:'')->required() }}</td>
                                <td>
                                    {{ html()->text('opening_meter')->class('form-control opening-reading')->placeholder(__('validation.attributes.backend.access.order.placeholder.opening_meter'))->attribute('name', 'product['.$products->id.'][opening_meter]')->
                                    attribute('id',$products->id)->value($products->OrderProductMeter?$products->OrderProductMeter->opening_meter:'')->required() }}
                                </td>
                                <td>
                                    {{ html()->text('closing_meter')->class('form-control closing-reading')->placeholder(__('validation.attributes.backend.access.order.placeholder.closing_meter'))->attribute('name', 'product['.$products->id.'][closing_meter]')->attribute('id',$products->id)->value($products->OrderProductMeter?$products->OrderProductMeter->closing_meter:'')->required() }}
                                </td>
                                <td>
                                    {{ html()->text('quantity')->class('form-control reading-result')->placeholder(__('validation.attributes.backend.access.order.placeholder.quantity'))->attribute('name', 'product['.$products->id.'][quantity]')->value($products->OrderProductMeter?$products->OrderProductMeter->quantity:'')->required() }}
                                </td>
                               
                            </tr>

                            @endforeach

                        </table>
                    </div>
                    <h5>Upper Seals <span class="badge badge-primary">{{$order->Vehicle->upper_seals}}</span></h5>
                    <div class="row">
                        <div class="col-md-12">
                            {{ html()->textarea('upper_seal')->class('form-control')->placeholder(__('validation.attributes.backend.access.vehicle.upper_seals'))->attribute('name', 'vehicle_chamber[upper_seal]')->value($seals  ? $seals['vehicle_chamber']['upper_seal']:'')->required() }}
                        </div>

                    </div>
                    <br>
                    <h5>Lower Seals <span class="badge badge-primary">{{$order->Vehicle->lower_seals}}</span></h5>

                    <div class="row">
                        <div class="col-md-12">
                            {{ html()->textarea('lower_seal')->class('form-control')->placeholder(__('validation.attributes.backend.access.vehicle.lower_seals'))->attribute('name', 'vehicle_chamber[lower_seal]')
                            ->value( $seals  ? $seals['vehicle_chamber']['lower_seal']:'')
                            ->required() }}
                        </div>

                    </div>
                    <br>
                    <div class="chamberData">
                        <h5>Comments </h5>
                        <div class="row">
                            <div class=" col-md-12">

                                {{ html()->textarea('comments')->class('form-control')->placeholder(__('validation.attributes.backend.access.order.placeholder.comments'))->value($order->comments?$order->comments:'')->required() }}
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <hr>

@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function(){
       

            function handleChambers(vehicleID) {
                var options = $('.products_selectOmc option');
                var values = $.map(options ,function(option) {
                    return option.value;
                });
                var queryString = ''
                if (values.length) {
                    var queryString = "?products="+values;
                }
                $.ajax({
                    url: '/order/getVehicleChambers/'+vehicleID+''+queryString,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $('.chamber_containerOmc').html(response.html);
                        }
                    },
                    error: function(data) {

                        $('.chamber_containerOmc').html('');
                    }
                });
            }    
        });  
    </script>
    @endpush