<div class="card-body">
    {{ html()->form('POST', route('order.store'))->class('form-horizontal')->attribute('id', 'omcOrderAddForm')->open() }}
    <div class="row">
        @include('backend.includes.gampetrohead')
    </div>
    <div class="row mt-4 mb-4">

        <div class="col">
         <div class="form-group row ">
            {{ html()->label(__('validation.attributes.backend.access.order.omc'))->class('col-md-2 form-control-label')->for('omc') }}

            <div class="col-md-10">
                {{ html()->select('omc_id')
                ->class('form-control')
                ->options($omc)
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.omc'))
                ->attribute('id', 'omcToOmc')
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row ">
            {{ html()->label(__('validation.attributes.backend.access.order.importer'))->class('col-md-2 form-control-label')->for('supplierOmc') }}
            <div class="col-md-10">
                {{ html()->select('supplier_importer_omc_id')
                ->class('form-control')
                ->options([])
                ->placeholder('Select Supplier')
                ->attribute('id', 'supplierOmc')
                ->required() }}
                
                {{ html()->hidden('supplier_omc_id')
                ->attribute('id', 'supplier_omc_id')
                ->required() }}

                {{ html()->hidden('importer_id')
                ->attribute('id', 'omc_importer_id')
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row " id="inventoryContainerOmc">
            <div class="col-md-12 text-center" id="inventoryAvailableOmc">
            </div>
        </div>
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.product'))->class('col-md-2 form-control-label')->for('product') }}
            <div class="col-md-10">
                {{ html()->multiselect("products[count]")
                ->class('form-control products_selectOmc') }}
            </div>
        </div>
        <div class="products_containerOmc"></div>
        <div class="form-group row" >
            {{ html()->label('Driver/Driver Name')->class('col-md-2 form-control-label')->for('driver') }}

            <div class="col-md-10">
                {{ html()->text('driver')
                ->class('form-control typeahead typeaheaddriver tt-query' )
                ->placeholder(__('validation.attributes.backend.access.order.placeholder.driver'))
                ->attribute('autocomplete', 'on')
                ->attribute('typeahead-editable', false)
                ->attribute('spellcheck', false)
                ->attribute('name', 'driver[name]')
                ->required() }}

                {{ html()->hidden('driver_id')
                ->attribute('class', 'driver_id')
                ->required() }}
            </div><!--col-->

        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.purchase_order'))->class('col-md-2 form-control-label')->for('purchase_order') }}
            <div class="col-md-10">
                {{ html()->text('purchase_order')
                ->class('form-control')
                ->placeholder(__('validation.attributes.backend.access.order.purchase_order'))
                ->attribute('min', 0)
                ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.order.vehicle'))->class('col-md-2 form-control-label')->for('vehicle') }}

            <div class="col-md-10">
               {{ html()->text('vehicle')
               ->class('form-control typeahead  tt-query' )
               ->placeholder(__('validation.attributes.backend.access.order.placeholder.vehicle'))
               ->attribute('autocomplete', 'on')
               ->attribute('id', 'typeaheadvehicleorderomc')
               ->attribute('typeahead-editable', false)
               ->attribute('spellcheck', false)
               ->attribute('name', 'vehicleLicencePlate')
               ->required() }}

               {{ html()->hidden('vehicle_id')
               ->attribute('id', 'omc_vehicle_id')
               ->required() }}

           </div><!--col-->
       </div><!--form-group-->
       <div class="chamber_containerOmc"></div>
   </div><!--col-->
</div><!--row-->
</div><!--card-body-->

<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ html()->button('Create')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'createOmcOrderSubmit')}}
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
{{ html()->form()->close() }}
@push('after-scripts')
<script type="text/javascript">
$(document).ready(function(){
    var vehicleData =  '{!! json_encode($vehicles) !!}';
    var vehicles = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('license_plate_number'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: JSON.parse(vehicleData)
    });
    vehicles.initialize();

    $('#typeaheadvehicleorderomc').typeahead(
        null, {
            autoSelect: true,
            minLength: 0, 
            limit: Infinity,
            showHintOnFocus: true,
            name: 'vehicles',
            displayKey: 'license_plate_number',
            source: vehicles.ttAdapter()
        }).on('typeahead:selected', function(event, data){   

            $('#typeaheadvehicleorderomc').val(data.id); 
            $('#omc_vehicle_id').val(data.id);
            handleChambers(data.id);

        }).on("typeahead:change", function(aEvent, aSuggestion) {
            var vehicleExists =  JSON.parse(vehicleData).some(element => element.license_plate_number == aSuggestion);
            if (!vehicleExists) {
                $(this).val('');
                $('#omc_vehicle_id').val(0);
                $('.chamber_containerOmc').html('');
            } 
        }); 

        function handleChambers(vehicleID) {
            var options = $('.products_selectOmc option');
            var values = $.map(options ,function(option) {
                return option.value;
            });
            var queryString = ''
            if (values.length) {
                var queryString = "?products="+values;
            }
            $.ajax({
                url: '/order/getVehicleChambers/'+vehicleID+''+queryString,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        $('.chamber_containerOmc').html(response.html);
                    }
                },
                error: function(data) {

                    $('.chamber_containerOmc').html('');
                }
            });
        }    
    });  
</script>
@endpush