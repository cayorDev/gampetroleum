<div class="tab-pane active" id="hfo" role="tabpanel" aria-expanded="true">
	<table class="table table-striped">
		<tr>
			<th>{{"DATE" }}</th>
			<th>{{"DRIVERS NAME" }}</th>
			<th>{{ "PLATE NUMBER"}} </th>
			<th> {{ "EMPTY WEIGHT"}}</th>
			<th>{{ "FULL WEIGHT"}} </th>
			<th>{{"QUANTITY DELIVERED(KG)" }} </th>
			<th>{{ "DESTINATION"}} </th>
		</tr> 
		@foreach($order['HF0'] as $key=>$hfo)
		<tr>
			<td>{{ date_format($hfo['date'] ,'Y-m-d')}}</td>
			<td>{{ $hfo['driver']}}</td>
			<td>{{ $hfo['vehicle']}} </td>
			<td> {{ $hfo['empty_weight']}}</td>
			<td>{{ $hfo['full_weight']}} </td>
			<td>{{ $hfo['quantity']}} </td>
			<td>{{ $hfo['destination']}} </td>
		</tr> 
		@endforeach
	</table>
</div>
<div class="tab-pane " id="lpg" role="tabpanel" aria-expanded="true">	
	<table class="table table-striped">
		<tr>
			<th>{{"DATE" }}</th>
			<th>{{"DRIVERS NAME" }}</th>
			<th>{{ "PLATE NUMBER"}} </th>
			<th> {{ "EMPTY WEIGHT"}}</th>
			<th>{{ "FULL WEIGHT"}} </th>
			<th>{{"QUANTITY DELIVERED(KG)" }} </th>
			<th>{{ "DESTINATION"}} </th>
		</tr> 
		@foreach($order['LPG'] as $key=>$lpg)
		<tr>
			<td>{{ date_format($lpg['date'] ,'Y-m-d')}}</td>
			<td>{{ $lpg['driver']}}</td>
			<td>{{ $lpg['vehicle']}} </td>
			<td> {{ $lpg['empty_weight']}}</td>
			<td>{{ $lpg['full_weight']}} </td>
			<td>{{ $lpg['quantity']}} </td>
			<td>{{ $lpg['destination']}} </td>
		</tr>
		@endforeach
	</table>
</div>