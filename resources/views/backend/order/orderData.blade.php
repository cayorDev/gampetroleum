<table class="table table-striped">
	<tr>
		<th>
			{{'On Behalf Of'}}
		</th>
		@foreach($product as $product)
		<th>
			<?php $unit= $product->measuring_unit=='ltr'?"(L)":"(KG)"?>
			{{$product->name." ".$unit}}

		</th>
		@endforeach
	</tr>
	
	@if(count($order)>1)
	@foreach($order as $key=>$orderdata)
	<tr>
		<td>{{$key}}</td>
		@foreach($orderdata as $data)
		<td>
			{{$data}}
		</td>
		@endforeach

	</tr>
	@endforeach

	@else
	<tr>
		<td>
			{{'No Data Found'}}
		</td>
	</tr>
	@endif
</table>
