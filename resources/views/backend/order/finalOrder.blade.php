@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card main_card">
    <div class="card-body main_card_body">
        <div class="row headerrow">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} 
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4  mb-4" id="mainDiv">
            <div class="col" id="finalForm">
                <div class="card">
                    <div class="card-body">
                     @include('backend.includes.gampetrohead')
                     <div class="row pageRow">
                        <div class="col-md-6 offset-md-6 datetimeDiv datetimeDivOffset"> 
                            <table class="table table-bordered table-hover " style="text-align: center;">
                                <tr>
                                    <th> Date </th>
                                    <th> Time </th>
                                </tr>
                                <tr>
                                    <td> {{ date_format($order->created_at ,'Y-m-d')}} </td>
                                    <td> {{ date_format($order->created_at, 'H:i:s') }} </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row pageRow">
                        <div class="col-md-6 truckloadingticket datetimeDiv" style="margin-top: auto;">  <h5>Truck Loading Ticket Number {{$order->order_id}}</h5></div>
                        <div class="col-md-6 datetimeDiv"> 
                            <table class="table table-bordered table-hover" style="text-align: center;">
                                <tr>
                                    <th colspan="2">Vehicle Number </th>
                                </tr>
                                <tr>
                                    <td colspan="2">{{$order->Vehicle ? $order->Vehicle->license_plate_number : ''}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row pageRow">
                        <div class="col-md-6 datetimeDiv"> 
                            <table class="table table-bordered table-hover" >
                                <tr>
                                    <th>Company's Name </th>
                                </tr>
                                <tr>
                                    <td> 
                                       {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}}
                                   </td>
                               </tr>
                           </table>
                       </div>
                       <div class="col-md-6 datetimeDiv "> 
                        <table class="table table-bordered table-hover" style="text-align: center;">
                            <tr>
                                <th colspan="2"> Driver's Name
                                </th> 
                            </tr>
                            <tr>
                                <td colspan="2" style="text-transform:uppercase">{{$order->Driver->name}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    @if($isHFO)
                    <tr>
                        <th>Destination</th>
                        <th class="purchaseOrder">Purchase Order #</th>
                    </tr>
                    <tr>
                        <td>{{$order->Truck->location}}</td>
                        <td class="purchaseOrder">{{$order->purchase_order}}</td>
                    </tr>
                    <tr>
                        <th>Created By</td>
                           <td>{{!is_null($order->CreatedBy)?$order->CreatedBy->first_name." ".$order->CreatedBy->last_name:""}}</td>
                       </tr>
                       @else
                       <tr>
                        <th>Purchase Order #</th>
                        <th>Created By</th>

                    </tr>
                    <tr>
                        <td>{{$order->purchase_order}}</td>
                        <td>{{!is_null($order->CreatedBy)?$order->CreatedBy->first_name." ".$order->CreatedBy->last_name:""}}</td>
                    </tr>
                    
                    @endif
                </table>
                <table class="table table-bordered table-hover" id="productMeterTable">
                    <thead>
                        <tr>
                            <th> Product  </th>
                            <th> Meter ID </th>
                            <th> Opening Meter </th>
                            <th> Closing Meter </th>
                            <th> Quantity </th>
                            <th> Unit </th>
                        </tr>
                    </thead>
                    <tbody>
                       @if($isHFO)
                       <tr>
                        <td>
                            {{$order->OrderProducts[0]->Product->name}}
                        </td>
                        <td>

                        </td>
                        <td>
                            {{$order->Truck->weight_before_loading}}
                        </td>
                        <td>
                            {{$order->Truck->weight_after_loading}}
                        </td>
                        <td>
                            {{$order->Truck->net_weight}}
                        </td>
                        <td>
                            Kgs
                        </td>
                    </tr>
                    @else
                    @foreach($order->OrderProducts as $products)
                    <tr>
                        <td>
                            {{$products->Product->name}}
                        </td>
                        <td>
                            {{ $products->OrderProductMeter->meter_id}}
                        </td>
                        <td>
                            {{$products->OrderProductMeter->opening_meter}}
                        </td>
                        <td>
                            {{$products->OrderProductMeter->closing_meter}}
                        </td>
                        <td>
                            {{$products->OrderProductMeter->quantity}}
                        </td>
                        <td>
                            {{$products->Product->measuring_unit}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>

            </table>
            <div class="row sealsDiv">
                <div class="col-md-6 offset-md-6 sealCol"> 
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <tr>
                            <td colspan="2">
                                Number Of Upper Seals ({{ $order->Vehicle->upper_seals }})
                            </td>
                            @if(is_array($order->seals['vehicle_chamber']))
                            <td>
                                @if(is_array($order->seals['vehicle_chamber']['upper_seal']) && count($order->seals['vehicle_chamber']['upper_seal']))
                                @foreach($order->seals['vehicle_chamber']['upper_seal'] as $seal) 
                                {{$seal}}
                                @endforeach
                                @else
                                {{$order->seals['vehicle_chamber']['upper_seal']}}
                                @endif    
                            </td>
                            @endif
                        </tr>
                        <tr>
                            <td  colspan="2">
                                Number Of Lower Seals ({{ $order->Vehicle->lower_seals  }})
                            </td>
                            @if( is_array($order->seals['vehicle_chamber']) &&  ( $order->seals['vehicle_chamber']['lower_seal']))
                            <td>
                                @if(is_array($order->seals['vehicle_chamber']['lower_seal']) && ($order->seals['vehicle_chamber']['lower_seal']))
                                @foreach($order->seals['vehicle_chamber']['lower_seal'] as $seal) 
                                {{$seal}}
                                @endforeach

                                @else
                                {{$order->seals['vehicle_chamber']['lower_seal']}}
                                @endif
                            </td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>

            <h5 class="commentLabel">Comments </h5>
            <div class="row col-md-12">
                {{ html()->textarea('comments')->class('form-control')->placeholder(__('validation.attributes.backend.access.order.placeholder.comments'))->value($order->comments?$order->comments:'')->attribute('id', 'comments')->attribute('rows', 3)->required() }}
            </div>
            @include('backend.order.includes.footer')
        </div>
    </div>
</div><!--col-->
</div><!--row-->
</div><!--card-body-->
<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
           <button class="button btn btn-danger btn-sm" onclick="printpage();"> Print </button>
       </div><!--col-->
       @if(!$order->is_complete)
       <div class="col text-right">
        <a href="{{ route('order.assignchamber.upliftingVerification',$order->id ) }}"><button class="button btn btn-warning btn-sm pull-right">Edit Uplifting & Load Verification</button></a>&nbsp
        @if(Auth::user()->isAdmin()||Auth::user()->isSuperAdmin()||Auth::user()->isSupervisor())
        <a href="{{ route('order.approveorder',$order->id ) }}" onclick="return confirm('Do you want to approve this Order ? ')"><button class="button btn btn-success btn-sm pull-right">Approve Order</button></a></div><!--col-->
        @endif
        @endif
    </div><!--row-->
</div><!--card-footer-->
</div><!--card-->

@endsection
@push('after-styles')
<style type="text/css" media="print">
    @page {
        size: auto;   
        margin: 0; 
    }
    @media print {
        header, footer { display: none !important; }
        .card {
            border: none !important;
            padding: 20px !important;
        }
        .table-bordered>tbody>tr>th{
            padding: 5px !important;
        }
        .table td ,
        .table th {
            padding: 2px !important;
        }
        .card-body { font-size: 16px !important ; color:#23282c !important;}
        .card-body { padding: none !important;}
        .commentLabel {
            display: none;
        }


        #comments
        {
            resize:none !important;
            background-image: -webkit-linear-gradient(left, white 10px , transparent 10px), -webkit-linear-gradient(right, white 10px, transparent 10px), -webkit-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px)!important;
            background-image: -moz-linear-gradient(left, white 10px, transparent 10px), -moz-linear-gradient(right, white 10px, transparent 10px), -moz-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px) !important;
            background-image: -ms-linear-gradient(left, white 10px, transparent 10px), -ms-linear-gradient(right, white 10px, transparent 10px), -ms-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px) !important;
            background-image: -o-linear-gradient(left, white 10px, transparent 10px), -o-linear-gradient(right, white 10px, transparent 10px), -o-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px)!important;
            background-image: linear-gradient(left, white 10px, transparent 10px), linear-gradient(right, white 10px, transparent 10px), linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px) !important;
            background-size: 100% 100%, 100% 100%, 100% 31px !important;
            border-radius: 8px !important;

            line-height: 31px !important;

            padding: 8px!important ;
            width:100% !important;
            height:100px !important;
            border: none !important;
            /*margin-bottom: 300px!important ;*/
        }
        #comments{

            -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
            color-adjust: exact !important;  /*Firefox*/ 
        }

        #comments:focus
        {
            outline: none;
        }
        #productMeterTable{
            text-align: center !important;
            min-height: 250px !important;
            height:250px !important;
        }
        #productMeterTable tr:not(:last-child){
            height: 20px !important;
            border-bottom-color: lightgray !important;
            border-bottom: 1px dashed lightgray !important;
        }
        #productMeterTable thead tr{
            height: 20px !important;
        }

        .sealsDiv{
            margin-top: 20px !important;
        }
        .sealCol{
            width:70% !important ;
            max-width: 70% !important;
            margin-left: 30% !important;
        }
        
        .table#productMeterTable td, .table-bordered#productMeterTable td{
            border:none !important;
        }
        .table#productMeterTable>tbody>tr>td{
            border-left: 1px solid lightgrey !important;
            border-right: 1px solid lightgrey !important;
            border-top: none !important;
            border-bottom: none !important;
        }
        .breadcrumb{
            display: none !important;
        }
        .alert { 
            display: none !important;
        }
        .card-title{
            display: none !important;
        }
        .pagefooter {
         margin-left: auto !important;
         margin-right: 20px !important;
         margin-top: 20px !important;
         background-color: white;
         position: fixed;
         bottom: 100px;
     }

     #finalForm{
        margin-top: none !important;
    }
    .card-footer{
        display: none;
    }
    .main_card{
        padding: 0px !important;
        margin: 0px !important;
    }
    .main_card_body{
        padding: 0px !important;
        margin: 0px !important;
    }
    .main, .container-fluid {
        padding: 0px !important;
    }
    #form-errors{
        display: none !important;
    }
    .header-fixed .app-body {
        margin-top: 0px !important;
    }
    .headerrow{
        display: none !important;
    }
    .datetimeDivOffset {
     margin-left: 50% !important;
 }
 .datetimeDiv {
    flex: 0 0 50% !important ;
    max-width: 50% !important;
}
.col-md-5 {
    flex: 0 0 41% !important ;
    max-width: 41% !important;
}
.col-md-7 {
    flex: 0 0 59% !important ;
    max-width: 59% !important;
}

.pageRow{
    height:70px !important;
}
.purchaseOrder{
    /*display: none !important;*/
}
.truckloadingticket{
    margin-top: 30px !important;
}
.logoimage{
    width:120px !important;
    height:auto !important;
}
hr{
    display: none !important;
}

}
</style>
<style>



</style>
@endpush
@push('after-scripts')
<script>
    $( document ).ready(function() {
        var url = window.location.href;
        if(url.indexOf('?isRequestFromApproved') != -1){
            printpage(); 
        }
    });
    function printpage()
    {
     var element = document.getElementById("mainDiv");
     element.classList.remove("mt-4");
     var printContents = document.getElementById("finalForm").innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
 }
 $(window).on('afterprint', function () { 
    var uri = window.location.toString();
    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
    } else {
        clean_uri= window.location.href;
    }
    window.location=clean_uri;
})
</script>

@endpush

