<li class="breadcrumb-menu">
    @if( $logged_in_user->isSupervisor() || $logged_in_user->isAdmin() )
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('menus.backend.access.order.main')</a>
            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
               @if($logged_in_user->isSupervisor())
               <a class="dropdown-item" href="{{ route('order.index') }}">@lang('menus.backend.access.order.all')</a>
               @endif
               @if($logged_in_user->isAdmin())
               <a class="dropdown-item" href="{{ route('order.index') }}">@lang('menus.backend.access.order.all')</a>
               <a class="dropdown-item" href="{{ route('order.add') }}">@lang('menus.backend.access.order.add')</a>
               @endif
           </div>
       </div>
   </div><!--btn-group-->
   @endif
</li>
