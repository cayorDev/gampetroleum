<table class="table table-bordered table-hover">
    <tr>
        <th colspan="7" style="text-align: center;">
           Assign Compartments
       </th>
   </tr>
   <tr>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.compartment_number')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.assigned_product')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
    </th>
    @if(!is_null($order) && $order->is_vehicle_loaded===1)
    <th>
       @lang('labels.backend.access.order.chamberAssignment.loader')
   </th>
   <th>
       @lang('labels.backend.access.order.vehicleUplifting.total_loaded')
   </th>
   @endif

</tr>
@foreach($vehicle->vehicle_chamber as $vehicleChamber)
<tr>
    <td>
        {{$vehicleChamber->chamber_number}}
    </td>
    <td>
        {{$vehicleChamber->capacity}}
    </td>
    {{ html()->hidden('chamber_id',$vehicleChamber->id)
    ->class('form-control')
    ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][chamberID]')
    ->required() }}
    <?php $product=0;$quantity=0;$assignedchamberLoader="";
    $assignedchamberTotalLoaded=0;?>
    @if(!is_null($order))
    @foreach($order->Chambers as $chamber)
    @if($chamber->vehicle_chamber_id === $vehicleChamber->id)
    {{ html()->hidden('id',$chamber->id)->class('form-control')->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][id]')->required() }}
    <?php $product=$chamber->product_id;$quantity=$chamber->quantity;$assignedchamberLoader=$chamber->loader;
    $assignedchamberTotalLoaded=$chamber->total_loaded;break;?>
    @endif
    @endforeach
    @endif
    <td> 
        {{ html()->select('product_id')
        ->class('form-control selectOrderedProducts')
        ->options($products)
        ->placeholder(__('validation.attributes.backend.access.order.placeholder.product'))
        ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][productID]') 
        ->attribute('class', 'chamberProducts')->required()->value( $product) }}
    </td>
    <td>
        {{ html()->text('quantity')
        ->class('form-control productQuantityAssigned')
        ->placeholder(__('validation.attributes.backend.access.order.quantity'))
        ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][quantity]')
        ->attribute('value', $quantity)
        ->required() }}
    </td>
    @if(!is_null($order) && $order->is_vehicle_loaded===1)
    <td>
        {{ html()->text('loader')
        ->type('text')
        ->class('form-control typeahead tt-query loaderdata')
        ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader'))
        ->attribute('autocomplete', 'on')
        ->attribute('typeahead-editable', false)
        ->attribute('spellcheck', false)
        ->attribute('id', 'loader_'.$vehicleChamber->id)
        ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][loader]')
        ->attribute('value', $assignedchamberLoader)
    }}
</td>
<td class="total_loaded"> {{ html()->text('total_loaded')
    ->class('form-control totalLoaded')
    ->placeholder(__('validation.attributes.backend.access.order.placeholder.total_loaded'))
    ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][total_loaded]')
    ->attribute('id', $vehicleChamber->id)
    ->attribute('placeholder', 'Loaded Quantity')
    ->attribute('value', $assignedchamberTotalLoaded ?: '' )
    ->required() }}</td>

    @endif
</tr>
@endforeach
</table>
<script>
     //loader data
   
            var orderChamber =  '{!! json_encode($order->Vehicle->vehicle_chamber) !!}';
            console.log(orderChamber);
            var loader =  '{!! json_encode($loader) !!}';
            console.log(loader);
            var obj = jQuery.parseJSON(orderChamber);
            $.each(obj, function(key,value) {
              var loaders = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: JSON.parse(loader)
            });
              loaders.initialize();

              $('#loader_'+value.id).typeahead(
                null, {
                    autoSelect: true,
                    minLength: 0, 
                    limit: Infinity,
                    showHintOnFocus: true,
                    name: 'name',
                    displayKey: 'name',
                    source: loaders.ttAdapter()
                }).on('typeahead:selected', function(event, data){            
                    $('#loader_'+value.id).val(data.name); 

                })
            }); 
               
        
    

</script>