<table class="table table-bordered table-hover">
    <tr>
        <th colspan="7" style="text-align: center;">
           Assign Compartments
       </th>
   </tr>
   <tr>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.compartment_number')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.assigned_product')
    </th>
    <th>
        @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
    </th>
</tr>
@foreach($vehicle->vehicle_chamber as $vehicleChamber)
<tr>
    <td>
        {{$vehicleChamber->chamber_number}}
    </td>
    <td>
        {{$vehicleChamber->capacity}}
    </td>
    {{ html()->hidden('chamber_id',$vehicleChamber->id)
    ->class('form-control')
    ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][chamberID]')
    ->required() }}
    <td> 
        {{ html()->select('product_id')
        ->class('form-control selectOrderedProducts')
        ->options($products)
        ->placeholder(__('validation.attributes.backend.access.order.placeholder.product'))
        ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][productID]') 
        ->attribute('class', 'chamberProducts')->required() }}
    </td>
    <td>
        {{ html()->text('quantity')
        ->class('form-control productQuantityAssigned')
        ->placeholder(__('validation.attributes.backend.access.order.quantity'))
        ->attribute('name', 'chambers[product_'.$vehicleChamber->id.'][quantity]')
        ->required() }}
    </td>
</tr>
@endforeach
</table>
