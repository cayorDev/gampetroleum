<hr>
<div class="row pagefooter">
    <div class="col-md-5 managerDetail">
        <span><u><b>Operations Manager</b></u></span><br>
        <span style="text-transform:uppercase">{{$operationsmanager}}</span>
    </div>
    <div class="col-md-7 verificationDetail" style="border:1px solid #f4f4f4;padding:5px;">
        <span><b>I verify that the data above is correct and that i have received the mentioned product(s) in good condition</b> </span>
        <br><br>
        <span style="text-transform:uppercase">{{$order->Driver->name}}</span>

    </div>
     <div class="col-md-12 text-right" style="padding-top: 20px;padding-right: 10%;">
        <span><u><b>General Manager</b></u></span><br>
        <span style="text-transform:uppercase">{{$generalmanager}}</span>
    </div>
</div>
