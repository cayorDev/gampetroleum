@extends('backend.layouts.app')

@section('title', __('labels.backend.access.order.management') . ' | ' . __('labels.backend.access.order.create'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div id ="form-errors"></div>

<div class="card" id='mainDiv'>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Create New Order
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#orderasimporter" role="tab" aria-controls="orderasimporter" aria-expanded="true"><i class="fas fa-list"></i> Importer as Supplier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#orderasomc" role="tab" aria-controls="orderasomc" aria-expanded="true"><i class="fas fa-list"></i> OMC as Supplier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#orderashfo" role="tab" aria-controls="orderashfo" aria-expanded="true"><i class="fas fa-list"></i> LPG & HFO Orders</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="orderasimporter" role="tabpanel" aria-expanded="true">
                        @include('backend.order.forms.orderasimporter')
                    </div>
                    <div class="tab-pane " id="orderasomc" role="tabpanel" aria-expanded="true">
                        @include('backend.order.forms.orderasomc')
                    </div>
                    <div class="tab-pane " id="orderashfo" role="tabpanel" aria-expanded="true">
                       @include('backend.order.forms.orderashfo')
                   </div>
               </div>
               <!--tab-content-->
           </div><!--col-->
       </div><!--row-->
   </div><!--card-body-->
</div><!--card-->

@endsection
@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/typehead.css')}}">
@endpush
@push('after-scripts')
<script type="text/javascript" src="/js/typehead.js"></script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var driverData =  '{!! json_encode($drivers) !!}';
    var vehicleData =  '{!! json_encode($vehicles) !!}';
    var drivers = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: JSON.parse(driverData)
    });
    drivers.initialize();

    $('.typeaheaddriver').typeahead(
        null, {
            autoSelect: true,
            minLength: 0, 
            limit: Infinity,
            showHintOnFocus: true,
            name: 'drivers',
            displayKey: 'name',
            source: drivers.ttAdapter()
        }).on('typeahead:selected', function(event, data){            
            $('.typeaheaddriver').val(data.id); 
            $('.driver_id').val(data.id);

        }).on("typeahead:change", function(aEvent, aSuggestion) {
            var driverExists =  JSON.parse(driverData).some(element => element.name == aSuggestion);
            if (!driverExists) {
                $('#driver_id').val(0);
            }
        });   

        var vehicles = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('license_plate_number'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: JSON.parse(vehicleData)
        });
        vehicles.initialize();

        $('#typeaheadvehicle').typeahead(
            null, {
                autoSelect: true,
                minLength: 0, 
                limit: Infinity,
                showHintOnFocus: true,
                name: 'vehicles',
                displayKey: 'license_plate_number',
                source: vehicles.ttAdapter()
            }).on('typeahead:selected', function(event, data){   

                $('#typeaheadvehicle').val(data.id); 
                $('#vehicle_id').val(data.id);
                handleChambers(data.id);

            }).on("typeahead:change", function(aEvent, aSuggestion) {
                var vehicleExists =  JSON.parse(vehicleData).some(element => element.license_plate_number == aSuggestion);
                if (!vehicleExists) {
                    $(this).val('');
                    $('#vehicle_id').val(0);
                    $('.chamber_container').html('');
                } 
            }); 

            function handleChambers(vehicleID) {
                var options = $('.products_select option');
                var values = $.map(options ,function(option) {
                    return option.value;
                });
                var queryString = ''
                if (values.length) {
                    var queryString = "?products="+values;
                }
                $.ajax({
                    url: '/order/getVehicleChambers/'+vehicleID+''+queryString,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $('.chamber_container').html(response.html);
                        }
                    },
                    error: function(data) {

                        $('.chamber_container').html('');
                    }
                });
            }    
        });  
</script>
<script type="text/javascript" src="/js/order.js"></script>
@endpush