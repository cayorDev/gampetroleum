@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
    @include('backend.order.includes.breadcrumb-links')
@endsection

@push ('after-styles')
<link rel="stylesheet" type="text/css" href="/css/funky-style.css">
<style>
.badge{
    margin-left: 5px;
    margin-right: 5px;
    display: inline;
}
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-body">


        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} <small class="text-muted">{{ __('labels.backend.access.order.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">

                @include('backend.order.includes.header-buttons')
                 <a class="exportlink float-right">
                        <button class="button btn btn-danger btn-md"> Export </button>
                    </a>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <input type="hidden" id="type" value="{{!is_null($type) ? $type : 'loadverify'}}">
            <div class="col-sm-12 funkyradio">

                <div class="funkyradio-primary funkyelm" >
                    <input type="checkbox" name="radio" class="loadverify" id="radio3" {{ $type == 'loadverify' ? 'checked' : '' }} onclick="getOrderData('loadverify')"/>
                    <label for="radio3"> Uplift & Load Verification<span class="badge badge-primary" id="shipped_orders">{{isset($orders['loadverify']) ? $orders['loadverify'] : '0'}}</span></label>
                </div>
                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSupervisor())
                <div class="funkyradio-warning funkyelm" >
                    <input type="checkbox" name="radio" class="orderforapproval" id="radio1" {{ $type == 'orderforapproval' ? 'checked' : '' }} onclick="getOrderData('orderforapproval')"/>
                    <label for="radio1" >Approve Orders<span class="badge badge-warning" id="shipped_orders">{{isset($orders['orderforapproval']) ? $orders['orderforapproval'] : '0'}}</span></label>
                </div>
                @endif
                <div class="funkyradio-success funkyelm" >
                    <input type="checkbox" name="radio" class="released" id="radio4" {{ $type == 'released' ? 'checked' : '' }} onclick="getOrderData('released')" />
                    <label for="radio4">Released Orders<span class="badge badge-success" id="unshipped_orders">{{ isset($orders['released']) ? $orders['released'] : '0'}}</span></label>
                </div>
           </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="order-table">
                        <thead>
                        <tr>
                             <th>ID</th>
                            <th>@lang('labels.backend.access.order.tabs.content.overview.orderReference')</th>
                            <th>Supplier</th>
                            <th>@lang('labels.backend.access.order.table.vehicle')</th>
                            <th>@lang('labels.backend.access.order.table.driver')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->

    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
            var start_type = 'loadverify';
            $(function() {
                if ( typeof $('#type') != 'undefined' ) {
                    start_type = $('#type').val();
                }

                getOrderData(start_type);
            });

            function getDataTable(type){
                $('#order-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: '/order/getOrderData?type='+type,
                    columns: [
                        { data: 'id', name: 'ID'},
                        { data: 'order_reference', name: 'order_reference'},
                        { data: 'supplier', name: 'supplier' },
                        { data: 'vehicle', name: 'vehicle' },
                        { data: 'driver', name: 'driver' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            }

            function getOrderData(type){
                //Disabling all checkboxes
                $('.funkyradio').find('input[type=checkbox]').prop('checked', false);
                //Check if checkbox is checked or not
                if ( type.length && typeof $('.'+type) != 'undefined' && !$('.'+type).prop('checked') ) {
                    $('.'+type).prop('checked', true);
                }
                 $('.exportlink'). attr("href", 'orderTypeExcel?type='+type);
                //Load datatable
               getDataTable(type)
            }
    </script>
@endpush
