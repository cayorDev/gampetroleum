<div class="col" id="orderoverviewcol" >
    <div class="table-responsive">
    	 <table class="table  table-hover">
                <tr>
                    <th>Item</th>
                    <th>Product Code</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Units</th>
                </tr>
                <?php $count = 1; ?>
                @foreach($order->OrderProducts as $product)
                <tr>
                    <td>{{$count++}}</td>
                    <td>{{$product->Product->sku}}</td>
                    <td>{{$product->Product->name}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->Product->measuring_unit}}</td>
                </tr>
                @endforeach
            </table>
    	
    </div>
</div>