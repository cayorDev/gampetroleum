<div class="row">
  @include('backend.includes.gampetrohead')
</div>
<div class="col" id="orderoverviewcol">
  <div class="table-responsive">
    <table class="table   table-hover">
     <tr>
      <th>@lang('labels.backend.access.order.tabs.content.overview.orderID')</th>
      <td>{{ $order->order_id }}</td>
    </tr>
    <tr>
      <th>@lang('labels.backend.access.order.tabs.content.overview.purchaseOrder')</th>
      <td>{{ $order->purchase_order }}</td>
    </tr>

    <tr>
      <th>Supplier</th>
      <td> {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}}</td>
    </tr>
   <tr>
     <th>@lang('labels.backend.access.order.tabs.content.overview.vehicle')</th>
     <td>{{ $order->Vehicle->license_plate_number }}</td>
   </tr>
   <tr>
     <th>@lang('labels.backend.access.order.tabs.content.overview.driver')</th>
     <td>{{ $order->Driver->name }}</td>
   </tr>
 </table>
</div>
</div><!--table-responsive-->
@include('backend.order.show.tab.product')
