@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <h4 class="card-title mb-0">
          {{ __('labels.backend.access.order.omc_inventory_report') }}
        </h4>
      </div><!--col-->

      <div class="col-sm-8 " style="display: flex;justify-content: flex-end;" >

        <div class="col-md-12" style="display: flex;justify-content: flex-end;">
         {{ html()->select('omc_id')
         ->class("pull-right")
         ->options($omc)
         ->attribute('readonly', true)
         ->attribute('id', 'omc_id')
         ->required() }} &nbsp;&nbsp;
         <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
          <span></span>
          <b class="caret"></b>
        </div>
        &nbsp;&nbsp;
        <a class="exportlink">
          <button class="button btn btn-danger btn-md"> Export </button>
        </a>
      </div>
    </div><!--col-->
  </div><!--row-->
  <div class="row mt-4  mb-4" id="finalForm">
    <div class="col" >
      <div class="card">
        <div class="card-body">
         @include('backend.includes.gampetrohead')
         <hr>
         <div >
          <h3 id="companyname" style="    text-align: center;"> </h3>
        </div>
        <div class="col"  id="ordercontentDiv">



        </div>
      </div>
    </div>
  </div>
</div>

</div><!--card-->

@endsection
@push('before-styles')

@endpush
@push('after-scripts')
<script src="{{ asset('/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/daterangepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />
<script type="text/javascript" src="{{ asset('js/jquery_block_ui.js')}}"></script>
<script type="text/javascript">
  $('document').ready(function(){
    $('#companyname').html("Company : "+$('#omc_id option:selected').text());
    var start = moment().startOf('month');
    var end = moment().endOf('month');

    function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
     }

   }, cb);

    cb(start, end);
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {

      start=picker.startDate.format('YYYY-MM-DD');
      end=picker.endDate.format('YYYY-MM-DD');
      getData(start,end, $('#omc_id').val());
    });

    start1=start.format('YYYY-MM-DD');
    end1=end.format('YYYY-MM-DD');

    getData(start1,end1, $('#omc_id').val());
  });

  $('#omc_id').change(function(){
    $('#companyname').html("Company : "+$('#omc_id option:selected').text());
    let startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    let endDate=  $("#reportrange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    getData(startDate,endDate, $('#omc_id').val());
  });


  function getData( start, end, omc)
  {
    $.blockUI({
      centerX: true,
      centerY: true,
      message: '<h1> Just a moment...</h1>' ,
      css: { backgroundColor: '#e4e5e6', color: '#black'}
    });

    $('.exportlink'). attr("href", 'inventoryExcel?start='+start+'&end='+end+'&omc='+omc);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      method: 'POST',
      url: 'getInventoryData',
      data: {'start' : start,'end' : end,'omc':omc},
      success: function(response){

        $('#ordercontentDiv').html(response);
        $.unblockUI();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $.unblockUI();
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        alert("Please Try Again");
      }
    });
  }
</script>

@endpush

