@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card main_card">
    <div class="card-body main_card_body">
        <div class="row headerrow">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} 
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->
        
        <div class="row mt-4  mb-4" id="mainDiv">
            <div class="col" id="upliftingForm">
                <div class="card" >
                    <div class="card-body">
                        <div class="row">
                            @include('backend.includes.gampetrohead')
                        </div>
                        <div style="text-align: center;">
                            <h4> 
                                @lang('labels.backend.access.order.vehicleUplifting.uplifting_form') 
                            </h4>
                        </div>
                        <?php $totalProducts = count($order->OrderProducts); ?>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th> 
                                   Supplier 
                               </th>
                               <td colspan={{$totalProducts}}>
                                {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.release_number')
                            </th>
                            <td colspan={{$totalProducts}}>
                                {{$order->order_id}}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.release_date')
                            </th>
                            <td> {{$order->created_at}} </td>
                        </tr>
                    </table>
                    <table class="table table-bordered table-hover">
                        <tr>

                            <th colspan={{$totalProducts+1}} style="text-align: center;" >
                                @lang('labels.backend.access.order.chamberAssignment.quantity_ordered') 
                            </th>
                            <!-- <td  class="releasebalance"></td> -->
                           <!--  <th colspan={{$totalProducts}} style="text-align: center;" class="releasebalance"> 
                                @lang('labels.backend.access.order.vehicleUplifting.release_balance')
                            </th>
                            <td colspan=2></td> -->
                        </tr>
                        <tr>
                            <th>  </th>
                            @foreach($order->OrderProducts as $product)
                            <td >
                                {{$product->Product->name}}
                            </td>
                            @endforeach
                          <!--   <td  class="releasebalance"></td>
                            @foreach($order->OrderProducts as $product)
                            <td class="releasebalance">
                                {{$product->Product->name}}
                            </td>
                            @endforeach
                            <td colspan=2></td>  -->
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.balance')
                            </th>
                            @foreach($order->OrderProducts as $product)
                            <td >
                                @if(count($balances)) 
                                {{isset($balances[$product->product_id]) ? $balances[$product->product_id] : 0}} 
                                @else 
                                0 
                            @endif</td>
                            @endforeach
                           <!--  <td  class="releasebalance"></td>
                            @foreach($order->OrderProducts as $product)
                            <td class="releasebalance">
                                @if(count($balances)) 
                                {{isset($balances[$product->product_id]) ? $balances[$product->product_id] : 0}} 
                                @else 
                                0 
                            @endif</td>
                            @endforeach
                            <td colspan=2></td> -->
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.quantity_now')
                            </th>
                            @if($isHFO)
                            <td>{{$order->Truck->net_weight}}</td>
                            @else
                            @foreach($order->OrderProducts as $product)
                            <?php $totalOrdered = 0;?>
                            <td>@foreach($order->Chambers as $productChamber)
                                @if($productChamber->product_id == $product->product_id)
                                <?php $totalOrdered += $productChamber->quantity; ?>
                                @endif
                                @endforeach

                            {{$totalOrdered}}</td>
                            @endforeach
                            @endif
                            <!-- <td></td>
                            @foreach($order->OrderProducts as $product)
                            <td></td>
                            @endforeach
                            <td colspan=2></td> -->
                        </tr>
                    </table>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.driver_name')
                            </th>
                            <td colspan={{$totalProducts}}> {{$order->Driver->name}}
                            </td>
                            <th> 
                                @lang('labels.backend.access.order.chamberAssignment.vehicle_number')
                            </th>
                            <td colspan={{$totalProducts}}> {{$order->Vehicle->license_plate_number}}
                            </td>
                            <th> 
                                Signature
                            </th>
                            <td class="sign_td"></td>
                        </tr>
                        <tr>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.authorized_by')
                            </th>
                            <td colspan={{$totalProducts}}> 
                                {{$order->AuthorisedBy ? $order->AuthorisedBy->first_name.' '.$order->AuthorisedBy->last_name : ''}}
                            </td>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td colspan={{$totalProducts}}> 
                                {{$order->created_at}}
                            </td>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td class="sign_td"></td>
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.seen_by')
                            </th>
                            <td colspan={{$totalProducts}}>  {{$order->seen_by }}</td>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td colspan={{$totalProducts}}> {{ $order->created_at }} </td>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td class="sign_td"></td>
                        </tr>
                    </table>
                    @if(!$isHFO)
                    {{ html()->hidden('order_id',$order->id)
                    ->class('form-control')
                    ->attribute('id', 'orderId')
                    ->required() }}
                    <table class="table table-bordered table-hover" id="truckLoadingInformation">
                        <tr>
                            <th colspan="7" style="text-align: center;">
                                Truck Loading Information
                            </th>
                        </tr>
                        <tr>
                            <th  class="compartNo">
                                @lang('labels.backend.access.order.chamberAssignment.compartment_number')
                            </th>
                            <th class="compartCap" >
                                @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
                            </th>
                            <th class="assignedProd">
                                @lang('labels.backend.access.order.chamberAssignment.assigned_product')
                            </th>
                            <th class="assignedQuantity">
                                @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
                            </th>
                            <th class="loader">
                                @lang('labels.backend.access.order.vehicleUplifting.loader_name')
                            </th>
                            <th class="total_loaded">
                                @lang('labels.backend.access.order.vehicleUplifting.total_loaded')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.loader_signature')
                            </th>
                        </tr>
                        @foreach($order->Vehicle->vehicle_chamber as $vehicleChamber)
                        <?php 
                        $assignedchamberID=null;
                        $assignedchamberProductID =0;
                        $assignedchamberProductName="";
                        $assignedchamberQuantity=0;
                        $assignedchamberLoader="";
                        $assignedchamberTotalLoaded=0;
                        ?>
                        @foreach($order->Chambers as $assignedChamber)
                        @if($assignedChamber->vehicle_chamber_id == $vehicleChamber->id)
                        <?php 
                        $assignedchamberID=$assignedChamber->id;
                        $assignedchamberProductID=$assignedChamber->product_id;
                        $assignedchamberProductName=$assignedChamber->Product->name;
                        $assignedchamberQuantity=$assignedChamber->quantity;
                        $assignedchamberLoader=$assignedChamber->loader;
                        $assignedchamberTotalLoaded=$assignedChamber->total_loaded;
                        ?>
                        @endif
                        @endforeach
                        <tr>
                            <td>
                                {{$vehicleChamber->chamber_number}}
                            </td>
                            <td>
                                {{$vehicleChamber->capacity}}
                            </td>
                            <td> 
                                {{$assignedchamberProductName}}
                            </td>
                            <td>
                                {{$assignedchamberQuantity}}

                            </td>

                            <td class="loader">{{$assignedchamberLoader}}</td>
                            <td class="total_loaded"> 
                            {{$assignedchamberTotalLoaded}}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    </table>
                    @endif
                    @if($isHFO)
                    <table class="table" id="controlRoomDetail">
                        <tr>
                            <th colspan="12" style="text-align: center;">
                                @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                            </th>
                        </tr>
                        <tr>
                            <td>
                                @lang('labels.backend.access.order.vehicleUplifting.truck_loading_ticket_number')
                            </th>
                            <td>
                                {{$order->Truck->truck_loading_ticket_number}}
                            </td>
                            <th colspan="2"> 
                                Control Room Supervisor 
                            </th>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.weight_before_loading')
                            </th>
                            <td>
                                {{$order->Truck->weight_before_loading}}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.supervisor_name')
                            </th>
                            <td>
                                {{$order->AuthorisedBy  ? $order->AuthorisedBy->first_name." ".$order->AuthorisedBy->last_name : Auth::user()->fullname }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.weight_after_loading')
                            </th>
                            <td>
                                {{$order->Truck->weight_after_loading}}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td>
                                {{$order->created_at}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.net_weight')
                            </th>
                            <td> 
                                {{$order->Truck->net_weight}}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td>

                            </td>
                        </tr>
                    </table>
                    @else

                    <table class="table" id="controlRoomDetail">
                        <tr>
                            <th  colspan="12" style="text-align: center;">
                                @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                @lang('labels.backend.access.order.vehicleUplifting.supervisor_name'):
                            </th>
                            <td>
<!--                                     {{ $order->AuthorisedBy  ? $order->AuthorisedBy->first_name." ".$order->AuthorisedBy->last_name : Auth::user()->fullname }}
-->                                </td>
<th colspan="2">
    @lang('labels.backend.access.order.vehicleUplifting.date'):
</th>
<td>
    {{$order->created_at}}
</td>
<th colspan="2">
    @lang('labels.backend.access.order.vehicleUplifting.signature'):
</th>
<td>

</td>

</tr>
</table>  
@endif
</div>

</div>
@include('backend.order.includes.footer')

</div><!--col-->

</div><!--row-->

</div><!--card-body-->

<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            <button class="button btn btn-danger btn-sm" onclick="printpage();"> Print </button>

        </div><!--col-->
        @if(!$order->is_complete)
        <div class="col text-right">
            <a href="{{ route('order.assignchamber.index',$order->id ) }}"><button class="button btn btn-primary btn-sm pull-right">Update Chamber Detail</button></a> &nbsp;<a href="{{ route('order.assignchamber.upliftingVerification',$order->id ) }}"><button class="button btn btn-warning btn-sm pull-right">Edit Uplifting & Load Verification</button></a>&nbsp

        </div><!--col-->
        @endif
    </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-styles')
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;  /* this affects the margin in the printer settings */
    }
    
    @media print {

       header, footer { display: none !important; }
       .card {
        border: none !important;
        padding: 20px !important;
    }
    .table-bordered>tbody>tr>th{
        padding: 5px !important;
    }
    .table td ,
    .table th {
        padding: 2px !important;
    }
    .card-body { font-size: 16px !important; color:#23282c !important;}

    #truckLoadingInformation{
        text-align: center !important;
        min-height: 400px !important;
    }
    #truckLoadingInformation tr:not(:last-child){
        height: 20px !important;
    }
    .table#truckLoadingInformation td, .table-bordered#truckLoadingInformation td{
        border:none !important;
    }
    .table#truckLoadingInformation>tbody>tr>td{
        border-left: 1px solid lightgrey !important;
        border-right: 1px solid lightgrey !important;
        border-top: none !important;
        border-bottom-color: lightgray !important;
        border-bottom: 1px dashed lightgray !important;
    }
    .pagefooter{
        margin-left: 25px!important;
        margin-right: 30px!important;
        margin-top: 20px !important;
        background-color: white;
        position: fixed;
        bottom: 70px;
    }
    hr{
        display: none;
    }
    .managerDetail, .verificationDetail{
        width: auto;
    }
    .verificationDetail{
        margin-right: 0px;
        margin-left: auto;
    }
    .breadcrumb{
        display: none !important;
    }
    .card-title{
        display: none !important;
    }
    .alert { 
        display: none !important;
    }
    .main, .container-fluid {
        padding: 0px !important;
    }
    #form-errors{
        display: none !important;
    }
    .header-fixed .app-body {
        margin-top: 0px !important;
    }
    .headerrow{
        display: none !important;
    }

    .main_card{
        padding: 0px !important;
        margin: 0px !important;
    }
    .main_card_body{
        padding: 0px !important;
        margin: 0px !important;
    }
}
</style>
<style>
    #truckLoadingInformation th{
        text-align: center;
    }
    .total_loaded{
        width: 130px !important;
        /*display: none;*/
    }
    .compartNo{
        width: 130px !important;
    }
    .compartCap{
        width: 130px !important;
    }
    .assignedProd{
        width: 130px !important;
    }
    .assignedQuantity{
        width: 130px !important;
    }
    .loader{
        width: 250px !important;
        /*display: none;*/
    }
    .sign_td{
        width:170px !important;
    }
</style>
@endpush
@push('after-scripts')

<script>
    function printpage()
    {  
     $('.releasebalance').hide();
     $('.updatespan').attr('colspan',3);
     var element = document.getElementById("mainDiv");
     element.classList.remove("mt-4");
     var printContents = document.getElementById("upliftingForm").innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     $('.releasebalance').show();
     $('.updatespan').attr('colspan',1);
     document.body.innerHTML = originalContents;

 }

 $(window).on('afterprint', function () { 
    uri= window.location.href;
    window.location=uri;
})
</script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/order.js"></script>
@endpush

