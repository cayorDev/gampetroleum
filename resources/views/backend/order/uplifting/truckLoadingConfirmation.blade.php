@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} 
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4  mb-4">
            <div class="col"id="truckLoadConfirmationForm" >
                <div class="card">
                    <div id ="form-errors"></div>
                    {{ html()->form('POST', route('order.assignchamber.store'))->class('form-horizontal')->attribute('id', 'assignChamberForm')->open() }}
                    <div class="card-body">
                        <div class="row">
                            @include('backend.includes.gampetrohead')
                        </div>
                        <div style="text-align: center;">
                            <h4> 
                                @lang('labels.backend.access.order.chamberAssignment.truck_loading') 
                            </h4>
                        </div>
                        <?php $totalProducts = count($order->OrderProducts); ?>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.marketer') 
                                </th>
                                <td colspan={{$totalProducts}}> 
                                   {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}}
                               </td>
                               <th>
                                @lang('labels.backend.access.order.vehicleUplifting.release_number')
                            </th>
                            <td colspan={{$totalProducts}}></td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.release_date')
                            </th>
                            <td>
                                {{$order->created_at}}
                            </td>
                        </tr>

                        <tr>
                            <th> </th>
                            <th colspan={{$totalProducts}} style="text-align: center;">
                                @lang('labels.backend.access.order.chamberAssignment.quantity_ordered') 
                            </th>
                            <td></td>
                            <th colspan={{$totalProducts}} style="text-align: center;"> 
                                @lang('labels.backend.access.order.vehicleUplifting.release_balance')
                            </th>
                            <td colspan=2></td>
                        </tr>
                        <tr>
                            <th>  </th>
                            @foreach($order->OrderProducts as $product)
                            <td>
                                {{$product->Product->name}}
                            </td>
                            @endforeach
                            <td></td>
                            @foreach($order->OrderProducts as $product)
                            <td>
                                {{$product->Product->name}}
                            </td>
                            @endforeach
                            <td colspan=2></td> 
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.balance')
                            </th>
                            @foreach($order->OrderProducts as $product)
                            <td> 
                                @if(count($ibalances)) 
                                {{isset($ibalances[$product->product_id]) ? $ibalances[$product->product_id] : 0}} 
                                @else 
                                0 
                                @endif
                            </td>
                            @endforeach
                            <td></td>
                            @foreach($order->OrderProducts as $product)
                            <td>
                                @if(count($balances)) 
                                {{isset($balances[$product->product_id]) ? $balances[$product->product_id] : 0}} 
                                @else 
                                0 
                                @endif
                            </td>
                            @endforeach
                            <td colspan=2></td>
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.quantity_now')
                            </th>
                            @foreach($order->OrderProducts as $product)
                            <td>{{$product->quantity}}</td>
                            @endforeach
                            <td></td>
                            @foreach($order->OrderProducts as $product)
                            <td>{{$product->quantity}}</td>
                            @endforeach
                            <td colspan=2></td>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.driver_name')
                            </th>
                            <td colspan={{$totalProducts}}> {{$order->Driver->name}}
                            </td>
                            <th> 
                                @lang('labels.backend.access.order.chamberAssignment.vehicle_number')
                            </th>
                            <td colspan={{$totalProducts}}> {{$order->Vehicle->license_plate_number}}
                            </td>
                            <th width="50px"> 
                                Signature
                            </th>
                            <td width="100px"></td>
                        </tr>
                        <tr>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.authorized_by')
                            </th>
                            <td colspan={{$totalProducts}}>
                                {{$order->AuthorisedBy ? $order->AuthorisedBy->first_name.' '.$order->AuthorisedBy->last_name : ''}}
                            </td>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td colspan={{$totalProducts}}> 
                                {{$order->created_at}}
                            </td>
                            <th>  
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td></td>
                        </tr>
                        <tr>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.seen_by')
                            </th>
                            <td colspan={{$totalProducts}}> 
                                {{ html()->select('seen_by')
                                ->class('form-control')
                                ->options($user)
                                ->placeholder(__('validation.attributes.backend.access.users.select_user'))
                                ->attribute('name', 'seen_by')->value($order->seen_by?$order->seen_by:'') }}
                            </td>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td colspan={{$totalProducts}}> 
                                {{$order->created_at}}
                            </td>
                            <th> 
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td></td>
                        </tr>
                    </table>
                    {{ html()->hidden('order_id',$order->id)
                    ->class('form-control')
                    ->attribute('id', 'orderId')
                    ->required() }}
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th colspan="7" style="text-align: center;">
                                @lang('labels.backend.access.order.chamberAssignment.assign_chambers')
                            </th>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.compartment_number')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.assigned_product')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.total_loaded')
                            </th>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.loader_signature')
                            </th>
                        </tr>
                        @foreach($order->Chambers as $assignedChamber)
                        <tr>
                            <td>
                                {{$assignedChamber->vehicleChamber->chamber_number}}
                            </td>
                            <td>
                                {{$assignedChamber->vehicleChamber->capacity}}
                            </td>
                            {{ html()->hidden('chamber_id',$assignedChamber->vehicleChamber->id)
                            ->class('form-control')
                            ->attribute('name', 'products[product_'.$assignedChamber->vehicleChamber->id.'][chamberID]')
                            ->required() }}

                            {{ html()->hidden('id',$assignedChamber->id)->class('form-control')->attribute('name', 'products[product_'.$assignedChamber->vehicleChamber->id.'][id]')->required() }}

                            <td> 
                                {{ html()->hidden('product_id')
                                ->class('form-control')
                                ->value( $assignedChamber->product_id)
                                ->attribute('name', 'products[product_'.$assignedChamber->vehicleChamber->id.'][productID]')
                                ->attribute('readonly', true)
                                ->required() 
                            }}
                            {{$assignedChamber->Product->name}} 
                        </td>
                        <td>
                            {{ html()->text('quantity')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.order.quantity'))
                            ->attribute('name', 'products[product_'.$assignedChamber->vehicleChamber->id.'][quantity]')
                            ->attribute('value', $assignedChamber->quantity)
                            ->attribute('readonly', true)
                            ->required() }}
                        </td>
                        <td> {{ html()->text('total_loaded')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.order.placeholder.total_loaded'))
                            ->attribute('name', 'products[product_'.$assignedChamber->vehicleChamber->id.'][total_loaded]')
                            ->attribute('value', $assignedChamber->total_loaded)
                            ->required() }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    </table>
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.order.placeholder.loader'))->class('col-md-2 form-control-label')->for('loader') }}

                        <div class="col-md-5">
                            {{ html()->text('loader1')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader1'))
                            ->attribute('value', $order->loader1)
                            ->required() }}
                        </div><!--col-->
                        <div class="col-md-5">
                            {{ html()->text('loader2')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.order.placeholder.loader2'))
                            ->attribute('value', $order->loader2) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    @if($isHFO)
                    <table class="table">
                        <tr>
                            <th  colspan="4" style="text-align: center;">
                                @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                            </th>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.truck_loading_ticket_number')
                            </th>
                            <td>
                                {{ html()->text('ticket_number')
                                ->class('form-control')
                                ->attribute('name', 'truck[truck_loading_ticket_number]')
                                ->attribute('value',  $order->Truck ? $order->Truck->truck_loading_ticket_number:'0')
                                ->required() }}
                            </td>
                            <th colspan="2"> 
                                Control Room Supervisor 
                            </th>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.weight_before_loading')
                            </th>
                            <td>
                                {{ html()->text('weight_before_loading')
                                ->class('form-control')
                                ->attribute('name', 'truck[weight_before_loading]')
                                ->attribute('value',  $order->Truck ? $order->Truck->weight_before_loading :'0')
                                ->required() }}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.supervisor_name')
                            </th>
                            <td>
<!--                                 {{ $order->Truck && $order->Truck->Supervisor ? $order->Truck->Supervisor->first_name." ".$order->Truck->Supervisor->last_name : Auth::user()->fullname }}
 -->
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.weight_after_loading')
                            </th>
                            <td>
                                {{ html()->text('weight_after_loading')->class('form-control')->attribute('name', 'truck[weight_after_loading]') ->attribute('value', $order->Truck ? $order->Truck->weight_after_loading :'0')->required() }}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.date')
                            </th>
                            <td>
                                {{$order->created_at}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.net_weight')
                            </th>
                            <td> 
                                {{ html()->text('net_weight')
                                ->class('form-control')
                                ->attribute('name', 'truck[net_weight]')
                                ->attribute('value', $order->Truck ? $order->Truck->net_weight :'0')
                                ->required() }}
                            </td>
                            <th>
                                @lang('labels.backend.access.order.vehicleUplifting.signature')
                            </th>
                            <td>

                            </td>
                        </tr>
                    </table>
                    @else 
                    <table class="table">
                        <tr>
                            <th  colspan="6" style="text-align: center;">
                                @lang('labels.backend.access.order.vehicleUplifting.control_room_detail')
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                @lang('labels.backend.access.order.vehicleUplifting.supervisor_name'):
                            </th>
                            <td>
                                {{ $order->Truck && $order->Truck->Supervisor ? $order->Truck->Supervisor->first_name." ".$order->Truck->Supervisor->last_name : Auth::user()->fullname }}
                            </td>
                            <th colspan="2">
                                @lang('labels.backend.access.order.vehicleUplifting.date'):
                            </th>
                            <td>
                                {{$order->created_at}}
                            </td>
                            <th colspan="2">
                                @lang('labels.backend.access.order.vehicleUplifting.signature'):
                            </th>
                            <td>

                            </td>

                        </tr>
                    </table>    
                    @endif
                </div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div><!--card-body-->
<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}
            <button class="button btn btn-primary btn-sm" onclick="printpage();"> Print </button>
        </div>
        <div class="col text-right">
         {{ html()->button('Update')->class('btn btn-success btn-sm pull-right')->type('button')->attribute('id', 'assignChamberSubmit')}}

     </div><!--col-->
 </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
@endsection
@push('before-styles')
<style type="text/css" media="print">
    @media print {
        header, footer { display: none !important; }
    }
    @page {
        size: auto;   
        margin: 0; 
    }
    @media print {
        .table-bordered>tbody>tr>th{
            padding: inherit !important;
        }
        .table td ,
        .table th {
            padding: inherit !important;
        }
        .card-body { font-size: 16px; }
       
        .form-control::placeholder {
            color:transparent !important;
            -webkit-text-fill-color: transparent !important;
            text-shadow: none !important;
        }
    }

    
  

</style>
@endpush
@push('after-scripts')
<script>
    function printpage()
    {
        var printContents = document.getElementById("truckLoadConfirmationForm").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/order.js"></script>
@endpush

