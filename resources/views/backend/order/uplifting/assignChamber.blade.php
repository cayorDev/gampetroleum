@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.management') }} 
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4  mb-4" id="assignChamberOuter">
            <div class="col">
                <div class="card">
                    <div id ="form-errors"></div>
                    {{ html()->form('POST', route('order.assignchamber.store'))->class('form-horizontal')->attribute('id', 'assignChamberForm')->open() }}
                    <div class="card-body">
                        <div class="row">
                            @include('backend.includes.gampetrohead')
                        </div>
                        <div style="text-align: center;">
                            <h4> 
                                @lang('labels.backend.access.order.chamberAssignment.chamber_assignment') 
                            </h4>
                        </div>
                        <?php $totalProducts = count($order->OrderProducts); ?>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th> 
                                    Supplier 
                                </th>
                                <td colspan={{$totalProducts}}> 

                                    {{ $order->omc ? $order->omc->company_name.'/' : ''}}{{ $order->supplierOmc ? $order->supplierOmc->company_name.'/' : ''}}{{ $order->importer ? $order->importer->company_name : ''}} 
                                </td>
                            </tr>
                            <tr>
                                <th> </th>
                                <th colspan={{$totalProducts}} style="text-align: center;">
                                    @lang('labels.backend.access.order.chamberAssignment.products_orders') 
                                </th>
                            </tr>
                            <tr>
                                <th>  </th>
                                @foreach($order->OrderProducts as $product)
                                <td>
                                    {{$product->Product->name}}
                                </td>
                                @endforeach
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.vehicleUplifting.balance')
                                </th>
                                @foreach($order->OrderProducts as $product)
                                <td>
                                    @if(count($balances)) 
                                        {{isset($balances[$product->product_id]) ? $balances[$product->product_id] : 0}} 
                                    @else 
                                    0 
                                    @endif
                                </td>
                                @endforeach
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.chamberAssignment.total_quantity_ordered')
                                </th>
                                @foreach($order->OrderProducts as $product)
                                <td>{{$product->quantity}}</td>
                                @endforeach
                                
                            </tr>
                            <tr>
                                <th>
                                    @lang('labels.backend.access.order.chamberAssignment.driver_name')
                                </th>
                                <td colspan={{$totalProducts}}> {{$order->Driver->name}}
                                </td>
                            </tr>
                            <tr>
                                <th> 
                                    @lang('labels.backend.access.order.chamberAssignment.vehicle_number')
                                </th>
                                <td colspan={{$totalProducts}}> {{$order->Vehicle->license_plate_number}}
                                </td>                              
                            </tr>    
                        </table>
                        {{ html()->hidden('order_id',$order->id)
                        ->class('form-control')
                        ->attribute('id', 'orderId')
                        ->required() }}
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th colspan="7" style="text-align: center;">
                                    @lang('labels.backend.access.order.chamberAssignment.assign_chambers')
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    @lang('labels.backend.access.order.chamberAssignment.compartment_number')
                                </th>
                                <th>
                                    @lang('labels.backend.access.order.chamberAssignment.compartment_capacity')
                                </th>
                                <th>
                                    @lang('labels.backend.access.order.chamberAssignment.assigned_product')
                                </th>
                                <th>
                                    @lang('labels.backend.access.order.chamberAssignment.assigned_quantity')
                                </th>
                               
                            </tr>
                            @foreach($order->Vehicle->vehicle_chamber as $vehicleChamber)
                            <tr>
                                <td>
                                    {{$vehicleChamber->chamber_number}}
                                </td>
                                <td>
                                    {{$vehicleChamber->capacity}}
                                </td>
                                {{ html()->hidden('chamber_id',$vehicleChamber->id)
                                ->class('form-control')
                                ->attribute('name', 'products[product_'.$vehicleChamber->id.'][chamberID]')
                                ->required() }}

                                <?php $productID = ''; $quantity = 0 ; $totalLoaded = 0;?>
                                @if(count($order->Chambers))
                                @foreach($order->Chambers as $chambers)
                                @if($chambers->vehicle_chamber_id == $vehicleChamber->id)
                                {{ html()->hidden('id',$chambers->id)->class('form-control')->attribute('name', 'products[product_'.$vehicleChamber->id.'][id]')->required() }}

                                <?php $productID =  $chambers->product_id; 
                                $quantity =  $chambers->quantity; 
                                $totalLoaded =  $chambers->total_loaded; 
                                break;?>
                                @endif
                                @endforeach
                                @endif
                                <td> 
                                    {{ html()->select('product_id')
                                    ->class('form-control')
                                    ->options($products)
                                    ->value( $productID)
                                    ->placeholder(__('validation.attributes.backend.access.order.placeholder.product'))
                                    ->attribute('name', 'products[product_'.$vehicleChamber->id.'][productID]')->required() }} 
                                </td>
                                <td>
                                    {{ html()->text('quantity')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.order.quantity'))
                                    ->attribute('name', 'products[product_'.$vehicleChamber->id.'][quantity]')
                                    ->attribute('value', $quantity)
                                    ->required() }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('order.index'), __('buttons.general.cancel')) }}

                <button class="button btn btn-primary btn-sm" onclick="printpage();"> Print </button>

            </div><!--col-->
            <div class="col text-right">
               {{ html()->button(__('labels.backend.access.order.chamberAssignment.assign_chambers'))->class('btn btn-success btn-md pull-right')->type('button')->attribute('id', 'assignChamberSubmit')}}

           </div><!--col-->
       </div><!--row-->
   </div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-styles')
<style type="text/css" media="print">
@media print {
    header, footer { display: none !important; }
}
@page {
    size: auto;   
    margin: 0; 
}
@media print {
        .card {
            border: none !important;
            padding: 20px !important;
        }
        .table-bordered>tbody>tr>th{
            padding: 5px !important;
        }
        .form-control {
            border: none !important;
        }
        .table td ,
        .table th {
            padding: 5px !important;
        }
        .card-body { font-size: 16px };
        .form-control:placeholder {
            color:transparent !important;
            -webkit-text-fill-color: transparent !important;
            text-shadow: none !important;
        }

    }


</style>
@endpush
@push('after-scripts')

<script>
    // $('select').on('change', function() {
 //        var selectedvalue = this.value;
 //        var values = [];
 //        $('select option:selected').each(function() { 
 //            if($(this).attr('value')){
 //                values.push( $(this).attr('value') );
 //            }
 //        });
 //        var index = values.indexOf(selectedvalue);
 //        if (index > -1) {
 //         values.splice(index, 1);
 //     }
 //     if(values.includes(this.value)){
 //         errorsHtml = '<div class="alert alert-danger"><ul>';
 //         errorsHtml += '<li> Product Already Selected</li>';
 //         errorsHtml += '</ul></di>';
 //         $( '#form-errors' ).html( errorsHtml );
 //         this.value ="";
 //     }
 // });
function printpage()
    {
        var printContents = document.getElementById("assignChamberOuter").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
<script type="text/javascript" src="/js/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="/js/order.js"></script>
@endpush