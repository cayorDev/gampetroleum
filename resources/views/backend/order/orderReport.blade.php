@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.order.management'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.order.report') }} 
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                <div class="col-md-12" style="display: flex;justify-content: flex-end;">
                    {{ html()->select('supplier_id')
                    ->class("pull-right")
                    ->options($supplier)
                    ->attribute('readonly', true)
                    ->attribute('id', 'supplier_id')
                    ->required() }} &nbsp;&nbsp;
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                        <span></span> 
                        <b class="caret"></b>
                    </div>
                    &nbsp;&nbsp;
                    <a class="exportlink">
                        <button class="button btn btn-danger btn-md"> Export </button>
                    </a>
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4  mb-4" id="finalForm">
            <div class="col" >
                <div class="card">
                    <div class="card-body">
                       @include('backend.includes.gampetrohead')
                       <hr>
                       <div class="col"  id="ordercontentDiv">

                       </div>
                   </div>
               </div><!--col-->
           </div><!--row-->
       </div><!--card-body-->


   </div><!--card-->

   @endsection
   @push('before-styles')

   @endpush
   @push('after-scripts')
   <script src="{{ asset('/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/daterangepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />

   <script type="text/javascript">
    $('document').ready(function(){
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
         }

     }, cb);

        cb(start, end);
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {

            start=picker.startDate.format('YYYY-MM-DD');
            end=picker.endDate.format('YYYY-MM-DD');

            getData(start,end);

        });

        start1=start.format('YYYY-MM-DD');
        end1=end.format('YYYY-MM-DD');
        getData(start1,end1);

        

    });
$('#supplier_id').change(function(){
    let startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    let endDate=  $("#reportrange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    getData(startDate,endDate, $('#supplier_id').val());
  });


    function getData( start, end, supplier=0)
    {
     $('.exportlink'). attr("href", 'orderExcel?start='+start+'&end='+end+'&supplier='+supplier);
     $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: 'getYearData',
        data: {'start' : start,'end' : end,'supplier':supplier}, 
        success: function(response){
            $('#ordercontentDiv').html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            alert("Please Try Again");
        }
    });
 }


</script>

@endpush

