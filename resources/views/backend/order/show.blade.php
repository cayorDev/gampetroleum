@extends('backend.layouts.app')

@section('title', __('labels.backend.access.order.management') . ' | ' . __('labels.backend.access.order.view'))

@section('breadcrumb-links')
@include('backend.order.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.order.management')
                    <!-- <small class="text-muted">@lang('labels.backend.access.vehicle.view')</small> -->
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col" id="orderoverviewcol">
               <div class="tab-content" id="orderoverview">
                @include('backend.order.show.tab.overview')
            </div>
            <!--tab-content-->
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col"><button class="btn btn-primary" onclick="printpage();">Print Order</button></div>
    </div>
</div><!--card-body-->
<div class="card-footer">
    <div class="row">
        <div class="col">
            <small class="float-right text-muted">
                <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($order->created_at) }} ({{ $order->created_at->diffForHumans() }}),
                <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($order->updated_at) }} ({{ $order->updated_at->diffForHumans() }})
                @if($order->trashed())
                <strong>@lang('labels.backend.access.vehicle.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($order->deleted_at) }} ({{ $order->deleted_at->diffForHumans() }})
                @endif
            </small>
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-styles')
<style type="text/css" media="print">
    @page {
        size: auto;   
        margin: 0; 
    }
    @media print {
      header, footer { display: none !important; }
      .table>tbody>tr>th{
        padding: 5px !important;

    }
    .table td ,
    .table th {
        padding: inherit !important;

    }
    .pagefooter {
        margin: 0 auto !important;
    }
    .card-body { font-size: 16px };
    #orderoverviewcol {   
       flex-basis: unset !important;    
       flex-grow: unset !important;
   }
   .col {   
     flex-basis: unset !important;    
     flex-grow: unset !important;
 }
 #orderoverview{
    margin-top: 30px !important;
    padding: 20px !important;
    border:none !important;
}
 hr{
        display: none !important;
    }

    .table{
        border:1px solid #f4f4f4 !important;
    }
}

</style>
@endpush
@push('after-scripts')
<script>
    function printpage()
    {
        var printContents = document.getElementById("orderoverviewcol").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
@endpush
