<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.management.tabs.content.overview.position')</th>
                <td>{{ $management->position }}</td>
            </tr>
        	<tr>
                <th>@lang('labels.backend.access.management.tabs.content.overview.name')</th>
                <td>{{ $management->name }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->
