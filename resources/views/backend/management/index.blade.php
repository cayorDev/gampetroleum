@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.loader.management'))

@section('breadcrumb-links')
@include('backend.management.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.management.management') }} <small class="text-muted">{{ __('labels.backend.access.management.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.management.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="management-table">
                        <thead>
                            <tr>
                               <th>@lang('labels.backend.access.management.table.position')</th>
                               <th>@lang('labels.backend.access.management.table.name')</th>
                               <th>@lang('labels.general.actions')</th>
                           </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
               </div>
           </div><!--col-->
       </div><!--row-->
   </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

$('#management-table').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    ajax: '/admin/management/getManagementData',
    columns: [
    { data: 'position', name: 'position' },
    { data: 'name', name: 'name' },
    {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});
</script>
@endpush
