@extends('backend.layouts.app')

@section('title', __('labels.backend.access.management.management') . ' | ' . __('labels.backend.access.management.edit'))

@section('breadcrumb-links')
    @include('backend.management.includes.breadcrumb-links')
@endsection
@section('content')
{{ html()->modelForm($management, 'PATCH', route('admin.management.update', $management->id))->class('form-horizontal')->open() }}
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.management.management')
                        <small class="text-muted">@lang('labels.backend.access.management.edit')</small>
                    </h4>
                </div><!--col-->
            </div>
            <hr>
            <div class="row">
                @include('backend.includes.gampetrohead')
            </div>
			<div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.management.position'))->class('col-md-2 form-control-label')->for('position') }}

                            <div class="col-md-10">
                                {{ html()->text('position')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.management.position'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('readOnly')
                                    ->required() }}
                            </div><!--col-->
                        </div>

                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.management.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.management.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div>

                      {{ html()->hidden('id',$management->id)
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->required() }}
                </div>
            </div>
           </div>
            <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.management.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div>
       </div>

{{ html()->closeModelForm() }}
@endsection
