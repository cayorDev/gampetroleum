@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.omc.management'))

@section('breadcrumb-links')
@include('backend.Importer.includes.breadcrumb-links')
@endsection

@push('after-styles')
<style type="text/css">
.full{
    width: 100%;
}
.transactions {
    text-align: center; 
    max-height: 250px; 
    overflow-y: scroll;
}
</style>    
@endpush
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-6">
                <h4 class="card-title mb-0">
                    {{$importer->company_name}}
                </h4>
            </div><!--col-->
            <div class="col-md-6 col-sm-6 pull-right">
                <a href="{{ route('admin.importer.contacts',$importer->id) }}" class="btn btn-success ml-1 float-right"  >
                    <i class="fas fa-address-card" data-toggle="tooltip" title="@lang('buttons.omc.contacts')"></i>&nbsp;@lang('buttons.omc.contacts') </a>
                    <!-- <a href="{{ route('admin.importer.location',$importer->id) }}" class="btn btn-success ml-1 float-right" ><i class="fas fa-location-arrow" data-toggle="tooltip" title="@lang('buttons.omc.location')"></i>&nbsp;@lang('buttons.omc.location')</a> -->
                </div><!--btn-toolbar-->
            </div><!--row-->
        </div><!--card-header-->
        <div class="card-body">

            <div class='row'>
                @foreach($importerproducts as $key => $product)
                <div class="col-md-4 col-xs-2" style="margin:auto; ">
                    <div class="card" style="background: #b8ca84; border-radius: 4px;text-align: center;position: relative; padding: 10px;" >
                        <div class="icon" style="font-size: 76px;color: rgba(0,0,0,0.15);position: absolute; right:12px; bottom:12px;">
                            <i class="fa fa-gas-pump "></i>
                        </div>
                        <div class="inner"style="text-align: left; width:100%;  padding:10px; color:white;" > 
                            <h1 class="@if($product['balance'] > 0 ) text-green @else text-red @endif">{{$product['balance']}}</h1>
                            <h3 style="color:#6a6f5b;">{{$product['name']}}  </h3>  
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <hr>
            {{ html()->modelForm($importer,'PATCH',route('admin.importer.manageImporter',$importer->id))->class('form-horizontal')->open()}}
            @csrf
            <div class='row' style="margin-left: 2%; margin-right: 2%">
                <div class='col-md-6'>
                    <div class="card">
                        <div class="card-body">
                            <div class="row mt-4 mb-4">
                                <div class="col">
                                 {{ html()->hidden()
                                 ->id('manage')
                                 ->name('manage')
                                 ->class('form-control')
                                 ->attribute('maxlength', 191)
                                 ->value(0)
                                 ->required() }}

                                 {{ html()->hidden()
                                 ->id('inventory')
                                 ->name('inventory')
                                 ->class('form-control inventory')
                                 ->value(0) }}

                                 {{ html()->hidden()
                                 ->name('address_change')
                                 ->class('form-control address_change')
                                 ->value(0) }}


                                 <div class="form-group row">
                                  {{html()->label(__('validation.attributes.backend.access.omc.importers.company_name'))->class('col-md-4 col-sm-4  form-control-label')->for('company_name') }}

                                  <div class="col-md-8 col-sm-8">
                                      {{ html()->text("company_name")
                                      ->class('form-control Importer')
                                      ->placeholder(__('validation.attributes.backend.access.omc.importers.company_name'))
                                      ->attribute('maxlength', 191)
                                      ->required()
                                      ->readonly()
                                      ->autofocus() }}
                                  </div><!--col-->
                              </div><!--form-group-->

                              <div class="form-group row">
                                  {{html()->label(__('validation.attributes.backend.access.omc.importers.owner_name'))->class('col-md-4 col-sm-4  form-control-label')->for('owner_name') }}

                                  <div class="col-md-8 col-sm-8">
                                      {{ html()->text("owner_name")
                                      ->class('form-control Importer')
                                      ->placeholder(__('validation.attributes.backend.access.omc.importers.owner_name'))
                                      ->attribute('maxlength', 191)
                                      ->required()
                                      ->readonly()
                                      ->autofocus() }}
                                  </div><!--col-->
                              </div><!--form-group-->

                              <div class="form-group row">
                                  {{ html()->label(__('validation.attributes.backend.access.omc.importers.email'))->class('col-md-4 col-sm-4  form-control-label')->for('email') }}
                                  <div class="col-md-8 col-sm-8">
                                      {{ html()->text("email")
                                      ->class('form-control Importer')
                                      ->placeholder(__('validation.attributes.backend.access.omc.importers.email'))
                                      ->attribute('maxlength', 191)
                                      ->required()
                                      ->readonly()
                                      ->autofocus() }}
                                  </div><!--col-->
                              </div><!--form-group-->
                          </div><!-- column -->
                      </div><!-- row -->
                  </div><!--card-body--> 
                  <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            {{ html()->button('<i class="fas fa-edit "data-toggle="tooltip" title="'.__('buttons.omc.edit').'"></i>  '.__('buttons.omc.edit').'')->class('btn btn-success full')->type('button')->attribute('id', 'edit_Importer')}}
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-footer--> 
            </div><!--card-->
        </div><!--column-->
        @if(!is_null($address))
        <div class='col-md-6 col-sm-12 col-xs-12'>
            <div class='card' id="update_address">
                <div class='card-body'>
                    <div class="row mt-4 mb-4">
                        <div class="col">
                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.omc.address.street'))->class('col-md-4 form-control-label')->for('street') }}
                                <div class="col-md-8" name="address">
                                    @csrf
                                    {{ html()->text('address[street]')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.omc.address.street'))
                                    ->attribute('maxlength', 191)
                                    ->value($importer->Addresses->street)
                                    ->readonly()
                                    ->required() }}
                                </div><!--col-->
                            </div><!--form-group-->
                            <div class="form-group row" name="address">
                                {{ html()->label(__('validation.attributes.backend.access.omc.address.city'))->class('col-md-4 form-control-label')->for('city') }}
                                <div class="col-md-8">
                                   {{ html()->text('address[city]')
                                   ->class('form-control')
                                   ->placeholder(__('validation.attributes.backend.access.omc.address.city'))
                                   ->attribute('maxlength',91) 
                                   ->value($importer->Addresses->city)        ->readonly()
                                   ->required() }}
                               </div><!--col-->
                           </div><!--form-group--> 
                           <div class="form-group row" name="address">
                            {{ html()->label(__('validation.attributes.backend.access.omc.address.country'))->class('col-md-4 form-control-label')->for('country') }}
                            <div class="col-md-8">
                                {{ html()->text('address[country]')
                                ->class('form-control ')
                                ->placeholder(__('validation.attributes.backend.access.omc.address.country'))
                                ->attribute('maxlength', 191)
                                ->value($importer->Addresses->country)
                                ->readonly()
                                ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        {{ html()->hidden('addid')->value($importer->id) }}
                    </div><!-- column -->
                </div><!-- row -->
            </div><!-- body -->
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        {{ html()->button('<i class="fas fa-edit fa-lg" data-toggle="tooltip" title="'.__('buttons.omc.edit').'"></i> '.__('buttons.omc.edit').'')->class("btn btn-primary   col-md-12")->attribute('id', 'addressEdit')}}
                    </div>
                    <a href="{{route('admin.importer.addressdelete',$importer->id)}}" onclick="return confirm('do you want to Importer-Address ? ')" class="btn btn-danger btn col-md-6" role="button" ><i class="fas fa-trash fa-lg" data-toggle="tooltip" title="@lang('buttons.omc.delete')"></i> @lang('buttons.omc.delete')</a>
                </div><!--row-->
            </div><!--card-footer-->
        </div><!-- card -->
    </div><!-- col-->
    @else
    <div class='col-md-6 col-xs-12 col-xs-12' id="add_address_content" style="display: none">
        <div class='card'>
            <div class='card-body'>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.omc.address.street'))->class('col-md-4 form-control-label')->for('street') }}
                            <div class="col-md-8" name="address">
                                @csrf
                                {{ html()->text("address[street]")
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.omc.address.street'))
                                ->attribute('maxlength', 191)
                                ->attribute('disabled', true)
                            }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row" name="address">
                        {{ html()->label(__('validation.attributes.backend.access.omc.address.city'))->class('col-md-4 form-control-label')->for('city') }}
                        <div class="col-md-8">
                            {{ html()->text("address[city]")
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.omc.address.city'))
                            ->attribute('maxlength',91)
                            ->attribute('disabled', true) 
                        }}
                    </div><!--col-->
                </div><!--form-group--> 
                <div class="form-group row" name="address">
                    {{ html()->label(__('validation.attributes.backend.access.omc.address.country'))->class('col-md-4 form-control-label')->for('country') }}
                    <div class="col-md-8">
                        {{ html()->text("address[country]")
                        ->class('form-control country')
                        ->placeholder(__('validation.attributes.backend.access.omc.address.country'))
                        ->attribute('maxlength', 191)
                        ->attribute('disabled', true)
                    }}
                </div><!--col-->
            </div><!--form-group-->
        </div><!-- column -->
    </div><!-- row -->
</div><!-- body -->
<div class="card-footer">
    <div class="row">
        <div class="col-sm-12">
            {{ html()->button(__('<i class="fas fa-times-circle " data-toggle="tooltip" title="'.__('buttons.general.cancel'). '"></i>  '.__('buttons.general.cancel').''))->type('button')->class("btn btn-danger full address_cancel")->attribute('id', 'cancel')}}
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
</div><!-- card -->
</div><!-- col-->
@endif
@if(is_null($address))
<div class="col-md-6 col col-sm-12 col-xs-12 " id="add_address">
    {{ html()->button('<i class="fas fa-plus-circle" data-toggle="tooltip" title="'.__('buttons.omc.address.add').'"></i> '.__('buttons.omc.address.add').'')->class("btn btn-primary")->type('button')->attribute('id', 'add_address_data')}}
</div>
@endif
</div><!--row-->
<div class="card-footer">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('admin.importer.index'), __('buttons.general.cancel')) }}
        </div><!--col-->
        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.update')) }}
        </div><!--col-->
    </div><!--row-->
</div>
</div><!--card--> 
{{ html()->closeModelForm() }} 
</div> 
</div>
</div><!--card--> 
<div class= "card">
	<div class="card-header">
     <div class="row">
        <div class="col-sm-7">
            <h4 class="card-title mb-0">
                @lang('labels.backend.access.importer.inventory_manage')
            </h4>
        </div><!--col-->
    </div><!--row-->
</div><!--card-header--> 
<div class ="manage-form-errors"></div>

<div class="card-body">
    <div class="row text-center" style="padding-left:15px;">
        @foreach($importerproducts as $key => $product)
        <span class="label label-success" style="margin-right:10px;padding:5px;"> 
           <b>{{$product['name']}} : {{$product['balance']}}</b>
       </span>
       @endforeach
   </div> <br/>
   {{ html()->modelForm($importer,'POST',route('admin.importer.manageImporterInventory',$importer->id))->class('form-horizontal')->open()}}
   <div class="row">
    @csrf
    <div class="col-md-3">

        <select class="form-control products" name="product_id">
            @foreach( $products as $id => $name )
            <option value="{{$id}}">{{$name}}</option>
            @endforeach
        </select>

    </div><!--col-->
    <div class="col-md-2">
        {{ html()->select("transaction")
        ->name('operation')
        ->class('form-control transaction')
        ->options(['Add','Deduct'])
        ->value(0)
    }}
</div><!--col-->
<div class="col-md-3 col-sm-2">
    {{ html()->text("added")
    ->class('form-control append_quantity')
    ->name("append_quantity")
    ->placeholder(__('validation.attributes.backend.access.omc.importers.new_quantity'))
    ->attribute('maxlength', 191)
    ->attribute('id','append_quantity')
    ->required()
    ->autofocus() }}
</div><!--col-->
<div class="col-md-2">
    {{ form_submit('Update Inventory')->class('float-right')}}

</div>
</div> 
<hr>
{{ html()->closeModelForm() }} 

</div><!--card-body--> 
@if( count($transactions) ) 
<div class="card-body">
    <h6 class="text-center">
        <a href="{{route('admin.importer.transactions', $importer->id)}}" class="btn btn-primary btn-sm">Download Transactions</a>
    </h6>
    <h3 class="text-center">Transaction Activities</h3><hr>
    <div class="transactions">
        @foreach( $transactions as $transc )
        <p><b>{{$transc['first_name']}} {{$transc['last_name']}}</b> {{ $transc['transaction_type'] == 'add' ? 'added' : 'reduced' }} {{ $transc['product_name'] }} quantity by <b>{{ $transc['quantity'] }}</b> ltr {!! $transc['release_id'] ? 'via <a href="/admin/release/show/'.$transc['release_id'].'" target="_blank">R'.$transc['release_purchase_order'].'</a>': '' !!} {!! $transc['order_id'] ? 'via <a href="/order/view/'.$transc['order_id'].'" target="_blank">'.$transc['orders_purchase_order'].'</a>': '' !!}.</p>
        @endforeach
    </div>
</div>
@endif
</div>   	    
@endsection

@push('after-scripts')
<script type="text/javascript" src="/js/importer.js"></script>
@endpush