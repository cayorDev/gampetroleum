@extends('backend.layouts.app')

@section('title', __('labels.backend.access.loader.management') . ' | ' . __('labels.backend.access.loader.deleted'))

@section('breadcrumb-links')
    @include('backend.loader.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.loader.management')
                    <small class="text-muted">@lang('labels.backend.access.loader.deleted')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

         <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="loader-table">
                        <thead>
                            <tr>
                               <th>@lang('labels.backend.access.driver.table.name')</th>
                               <th>@lang('labels.general.actions')</th>
                           </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
               </div>
           </div><!--col-->
       </div><!--row-->
   </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
@endpush

@push('after-scripts')
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

$('#loader-table').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    ajax: '/admin/loader/getDeletedLoader',
    columns: [
    { data: 'name', name: 'name' },
    {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});
</script> 
@endpush
