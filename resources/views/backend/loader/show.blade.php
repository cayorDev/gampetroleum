@extends('backend.layouts.app')
@section('title', __('labels.backend.access.loader.view') . ' | ' . __('labels.backend.access.loader.view'))

@section('breadcrumb-links')
    @include('backend.loader.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.loader.management')
                    <small class="text-muted">@lang('labels.backend.access.loader.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->
         <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-user"></i> @lang('labels.backend.access.loader.tabs.titles.overview')</a>
                    </li>
                </ul>

                <!--tab-content-->  
                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.loader.show.tab.overview')
                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->


    
    </div><!--card-body-->

    
</div><!--card-->
@endsection
