<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
        	<tr>
                <th>@lang('labels.backend.access.driver.tabs.content.overview.name')</th>
                <td>{{ $driver->name }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->
