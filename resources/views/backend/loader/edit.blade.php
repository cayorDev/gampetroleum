@extends('backend.layouts.app')

@section('title', __('labels.backend.access.loader.management') . ' | ' . __('labels.backend.access.loader.edit'))

@section('breadcrumb-links')
    @include('backend.loader.includes.breadcrumb-links')
@endsection
@section('content')
{{ html()->modelForm($loader, 'PATCH', route('admin.loader.update', $loader->id))->class('form-horizontal')->open() }}
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.loader.management')
                        <small class="text-muted">@lang('labels.backend.access.loader.edit')</small>
                    </h4>
                </div><!--col-->
            </div>
            <hr>
            <div class="row">
                @include('backend.includes.gampetrohead')
            </div>
			<div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.loader.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.loader.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div>
                   
                      {{ html()->hidden('id',$loader->id)
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->required() }}
                </div>
            </div>
           </div>
            <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.loader.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div>
       </div>
 
{{ html()->closeModelForm() }}
@endsection