<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Gam Petroleum')">
    <meta name="author" content="@yield('meta_author', 'Zasya Solutions')">
    @yield('meta')

    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    <!-- {{ style(mix('css/backend.css')) }} -->
    {{ style(mix('css/frontend.css')) }}
    <!-- <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css')}}"> -->
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('css/frontend-header.css')}}"> -->
    <!-- {{ style(mix('css/backend.css')) }} -->
    @stack('after-styles')

</head>
<body class ="app header-fixed sidebar-fixed aside-menu-off-canvas sidebar-lg-show">
    <div id="app">
        @include('includes.partials.logged-in-as')
    
        @include('frontend.includes.nav')
        <div class="app-body">

            <div class="container container-fluid">
              <div class="animated fadeIn">
                <div class="content-header">
                    @yield('page-header')
                </div><!--content-header-->
                @include('includes.partials.messages')
                @yield('content')
            </div>
        </div><!-- container -->

    </div><!-- #app -->
</div>
<!-- Scripts -->
@stack('before-scripts')
{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/frontend.js')) !!}
@stack('after-scripts')

@include('includes.partials.ga')
</body>
</html>
