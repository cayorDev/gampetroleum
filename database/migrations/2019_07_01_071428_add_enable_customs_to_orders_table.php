<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableCustomsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('orders', 'enable_customs') ) {
            Schema::table('orders', function (Blueprint $table) {
            $table->boolean('enable_customs')->default(0)->after('is_complete');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       if ( Schema::hasColumn('orders', 'enable_customs') ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn( 'enable_customs' );
            });
        }
    }
}
