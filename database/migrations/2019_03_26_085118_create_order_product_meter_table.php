<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('order_product_meter') ) {
            Schema::create('order_product_meter', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('order_id');
                $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
                $table->unsignedInteger('order_product_id');
                $table->foreign('order_product_id')
                ->references('id')
                ->on('order_products')
                ->onDelete('cascade');
                $table->string('meter_id');
                $table->integer('opening_meter');
                $table->integer('closing_meter');
                $table->integer('quantity');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_meter');
    }
}
