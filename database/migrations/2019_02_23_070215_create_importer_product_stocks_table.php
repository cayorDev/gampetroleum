<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImporterProductStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('importer_product_stocks')) {
            Schema::create('importer_product_stocks', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('importer_id');
                $table->unsignedInteger('product_id');
                $table->integer('outstanding_balance')->default(0);
                $table->unsignedInteger('created_by');
                $table->foreign('importer_id')
                    ->references('id')
                    ->on('importers')
                    ->onDelete('cascade');
                $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
                $table->foreign('created_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('importer_product_stocks');
    }
}
