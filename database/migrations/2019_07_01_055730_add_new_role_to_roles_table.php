<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AddNewRoleToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            // Role::create(['name' => config('access.users.custom_officer_role')])->givePermissionTo('view backend');
            // Role::where('name', config('access.users.special_role'))->delete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            Role::where('name', config('access.users.custom_officer_role'))->delete();
            Role::create(['name' => config('access.users.special_role')])->givePermissionTo('view backend');

        });
    }
}
