<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransporterToVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('vehicles', 'active') ) {
            Schema::table('vehicles', function (Blueprint $table) {
            $table->string('status', 8)->default('active')->after('lower_seals');
            $table->string('transporter_name')->nullable()->after('license_plate_number');
            $table->string('transporter_tel_number',20)->nullable()->after('transporter_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('vehicles', 'active') ) {
            Schema::table('vehicles', function (Blueprint $table) {
                $table->dropColumn('status');
                $table->dropColumn( 'transporter_name' );
                $table->dropColumn( 'transporter_tel_number' );
            });
        }
    }
}
