<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Auth\User;

class AddCustomOfficerToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $user = User::where('email', 'customsofficer@gp.com')->get()->first();
            if(is_null( $user)){
               User::create([
                'first_name'        => 'GP',
                'last_name'         => 'Customs Officer',
                'email'             => 'customsofficer@gp.com',
                'password'          => 'secret',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
            ]);
           }
           

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            User::where('email', 'customsofficer@gp.com')->delete();
        });
    }
}
