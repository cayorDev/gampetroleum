<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('is_chamber_assigned')->default(0)->after('comments');   
            $table->boolean('is_vehicle_loaded')->default(0)->after('is_chamber_assigned');   
            $table->boolean('is_order_verified')->default(0)->after('is_vehicle_loaded');
            $table->boolean('is_complete')->default(0)->after('is_order_verified');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('is_chamber_assigned');   
            $table->dropColumn('is_vehicle_loaded')->default(0);   
            $table->dropColumn('is_order_verified')->default(0);
            $table->dropColumn('is_complete')->default(0);  
        });
    }
}
