<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\System\Order\Order;

class UpdateOrderIdForDuplicateOrderIdInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orders = Order::select('id','order_id')->get()->groupBY('order_id');
        foreach($orders as $order)
        {
            if(count($order)>1){
                $orderToUpdate=$order->last();
                $latestorder = Order::orderBy('order_id','DESC')->first();
                $orderToUpdate->order_id = $latestorder->order_id + 1;
                $orderToUpdate->save();
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
