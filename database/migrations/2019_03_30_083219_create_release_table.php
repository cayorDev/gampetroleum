<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('release') ) {
            Schema::create('release', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                        ->references('id')
                        ->on('omc')
                        ->onDelete('cascade');
                $table->unsignedInteger('importer_id');
                $table->foreign('importer_id')
                        ->references('id')
                        ->on('importers')
                        ->onDelete('cascade');
                $table->string('purchase_order', 50)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release');
    }
}
