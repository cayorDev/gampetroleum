<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImporterIdToOmcProductStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('omc_product_stocks', function (Blueprint $table) {
            $table->unsignedInteger('importer_id')->nullable()->after('product_id');
            $table->foreign('importer_id')
                        ->references('id')
                        ->on('importers')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('omc_product_stocks', function (Blueprint $table) {
            $table->dropColumn('importer_id');
        });
    }
}
