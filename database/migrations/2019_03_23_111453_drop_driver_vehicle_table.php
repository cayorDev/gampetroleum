<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDriverVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::dropIfExists('driver_vehicle');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if ( !Schema::hasTable('driver_vehicle') ) {
            Schema::create('driver_vehicle', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('vehicle_id');
                $table->foreign('vehicle_id')
                        ->references('id')
                        ->on('vehicles')
                        ->onDelete('cascade');
                $table->unsignedInteger('driver_id');
                $table->foreign('driver_id')
                        ->references('id')
                        ->on('drivers')
                        ->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
