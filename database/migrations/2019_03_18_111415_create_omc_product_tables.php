<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmcProductTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema for omc_product_stocks
        if ( !Schema::hasTable( 'omc_product_stocks' ) ) {
            Schema::create( 'omc_product_stocks', function ( Blueprint $table ) {
                $table->increments('id');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                        ->references('id')
                        ->on('omc')
                        ->onDelete('cascade');
                $table->unsignedInteger('product_id');
                $table->foreign('product_id')
                        ->references('id')
                        ->on('products')
                        ->onDelete('cascade');
                $table->integer('outstanding_balance')->default(0);
                $table->unsignedInteger('created_by');
                $table->foreign('created_by')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        //Schema for omc_product_transactions
        if ( !Schema::hasTable( 'omc_product_transactions' ) ) {
            Schema::create( 'omc_product_transactions', function ( Blueprint $table ) {
                $table->increments('id');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                            ->references('id')
                            ->on('omc')
                            ->onDelete('cascade');
                $table->unsignedInteger('product_id');
                $table->foreign('product_id')
                        ->references('id')
                        ->on('products')
                        ->onDelete('cascade');
                $table->integer('quantity')->default(0);
                $table->enum('transaction_type', [ 'add', 'deduct' ])->default('add');
                $table->unsignedInteger('created_by');
                $table->foreign('created_by')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('omc_product_stocks');
        Schema::dropIfExists('omc_product_transactions');
    }
}
