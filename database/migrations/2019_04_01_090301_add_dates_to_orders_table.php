<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatesToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->datetime('chamber_assigned_at')->nullable()->after('is_chamber_assigned');   
            $table->datetime('vehicle_loaded_at')->nullable()->after('chamber_assigned_at');   
            $table->datetime('order_verified_at')->nullable()->after('vehicle_loaded_at');
            $table->datetime('completed_at')->nullable()->after('order_verified_at');      
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
