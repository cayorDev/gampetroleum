<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOmcImporterInventoryTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('importer_product_transactions', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->nullable()->after('quantity');
        });

        Schema::table('omc_product_transactions', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->nullable()->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('abc', function (Blueprint $table) {
        //     //
        // });
    }
}
