<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if ( Schema::hasColumn( 'orders', 'purchase_order' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('purchase_order');
            });
        }
        if ( Schema::hasColumn( 'orders', 'importer_id' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropForeign('orders_importer_id_foreign');
                $table->dropColumn('importer_id');
            });
        }

        if (! Schema::hasColumn( 'orders', 'importer_id' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('importer_id')->nullable()->after('id');
            });
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
