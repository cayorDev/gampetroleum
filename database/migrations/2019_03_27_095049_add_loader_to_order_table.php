<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoaderToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn( 'orders', 'loader1' ) ) {
        Schema::table( 'orders', function( Blueprint $table ) {
            $table->string( 'loader1' )->after('order_id')->nullable();
        });
    }
    if ( !Schema::hasColumn( 'orders', 'loader2' ) ) {
        Schema::table( 'orders', function( Blueprint $table ) {
            $table->string( 'loader2' )->after('loader1')->nullable();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'orders', 'loader1' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn( 'loader1' );
            });
        }
        if ( Schema::hasColumn( 'orders', 'loader2' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn( 'loader2' );
            });
        }
    }
}
