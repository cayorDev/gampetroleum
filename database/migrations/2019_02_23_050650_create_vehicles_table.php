<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicles')) {
                Schema::create('vehicles', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('license_plate_number');
                    $table->integer('number_of_chambers');
                    $table->integer('chamber_capaciy');
                    $table->integer('upper_seals');
                    $table->integer('lower_seals');
                    $table->timestamps();
                    $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
