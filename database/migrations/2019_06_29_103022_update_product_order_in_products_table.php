<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductOrderInProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('products', 'product_order') ) {
            Schema::table('products', function (Blueprint $table) {
                 DB::table('products')->where('name', 'Gas Oil')
                ->update(['product_order' => 2]);

                DB::table('products')->where('name', 'PMS')
                ->update(['product_order' => 1]);

                DB::table('products')->where('name', 'HFO')
                ->update(['product_order' => 5]);

                DB::table('products')->where('name', 'Kerosene')
                ->update(['product_order' => 4]);

                DB::table('products')->where('name', 'Jet Fuel')
                ->update(['product_order' => 3]);

                DB::table('products')->where('name', 'LPG')
                ->update(['product_order' => 6]);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
