<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToContactsAndLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //Changes for omc_contacts table
        if ( Schema::hasTable('contacts') && !Schema::hasColumn('contacts', 'importer_id') ) {
            Schema::table('contacts', function (Blueprint $table) {
                $table->dropForeign('omc_contacts_omc_id_foreign');
                $table->unsignedInteger('omc_id')->nullable()->change();
                $table->unsignedInteger('importer_id')->nullable()->after('omc_id');
            });
        }
        
        //Changes for omc_locations table
        if ( Schema::hasTable('locations') && !Schema::hasColumn('locations', 'importer_id') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->dropForeign('omc_locations_omc_id_foreign');
                $table->unsignedInteger('omc_id')->nullable()->change();
                $table->unsignedInteger('importer_id')->nullable()->after('omc_id');
            });
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Changes for omc_contacts table
        if ( Schema::hasTable('contacts') && Schema::rename('contacts', 'omc_contacts') ) {
            Schema::table('omc_contacts', function (Blueprint $table) {
                $table->dropColumn('importer_id');
            });
        }
        
        //Changes for omc_locations table
        if ( Schema::hasTable('omc_locations') && Schema::rename('omc_locations', 'locations') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->dropColumn('importer_id');
            });
        }
    }
}
