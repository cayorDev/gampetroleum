<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoaderToChamberAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('chamber_assignment', 'loader') ) {
            Schema::table('chamber_assignment', function (Blueprint $table) {
                $table->string('loader')->nullable()->after('total_loaded');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('chamber_assignment', 'loader') ) {
            Schema::table('chamber_assignment', function (Blueprint $table) {
               $table->dropColumn('loader');
           });
        }
    }
}
