<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTableForHfoOmc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('omc_id')->nullable()->change();
            $table->unsignedInteger('supplier_omc_id')->nullable()->after('omc_id');
            $table->foreign('supplier_omc_id')
            ->references('id')
            ->on('omc')
            ->onDelete('cascade');
        });

        Schema::table('order_truck_details', function (Blueprint $table) {
            $table->dropForeign('order_truck_details_supervisor_foreign');
            $table->dropColumn('supervisor');
        });

        Schema::table('drivers', function (Blueprint $table) {
            $table->string('drivers_licence_number')->nullable()->change();
            $table->string('tel_number_1')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
