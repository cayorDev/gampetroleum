<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationColumnToOrderTruckDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('order_truck_details', 'location') ) {
            Schema::table('order_truck_details', function (Blueprint $table) {
                $table->string('location', 100)->nullable()->after('net_weight');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if ( Schema::hasColumn('order_truck_details', 'location') ) {
                Schema::table('order_truck_details', function (Blueprint $table) {
                    $table->dropColumn('location');
                });
            }
    }
}
