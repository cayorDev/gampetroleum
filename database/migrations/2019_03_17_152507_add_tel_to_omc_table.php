<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTelToOmcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('omc', 'tel') ) {
            Schema::table('omc', function (Blueprint $table) {
                $table->string('tel', 20)->nullable()->after('email');
            });
        }
        

        if ( !Schema::hasColumn('importers', 'tel') ) {
            Schema::table('importers', function (Blueprint $table) {
                $table->string('tel', 20)->nullable()->after('email');
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('omc', 'tel') ) {
            Schema::table('omc', function (Blueprint $table) {
                $table->dropColumn('tel');
            });
        }
        

        if ( Schema::hasColumn('importers', 'tel') ) {
            Schema::table('importers', function (Blueprint $table) {
                $table->dropColumn('tel');
            });
        }
    }
}
