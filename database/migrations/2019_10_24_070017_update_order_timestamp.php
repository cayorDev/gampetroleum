<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\System\Order\ChamberAssignment;
use App\Models\System\Order\OrderProductMeter;
use App\Models\System\Order\OrderProduct;
use App\Models\System\Order\TruckDetail;
use App\Models\System\Order\Order;
use App\Models\System\Importer\ImporterProductTransactions;
use App\Models\System\OMC\OmcProductTransactions;
use Carbon\Carbon;

class UpdateOrderTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           $orders=Order::whereTime('created_at', '=', '00:00:00')->withTrashed()->get();
           // dd($orders);
           foreach($orders as $order)
           {
            $date=Carbon::parse($order->created_at)->addHours(10);
            $products = OrderProduct::where('order_id',(int) $order->id)->withTrashed()->get();
             $product=$products->first()->product_id;
                    if( $product == 3 || $product == 6 ){
                        Order::where('id',(int) $order->id)->withTrashed()->update(['created_at'=>$date,'authorized_at'=>$date]);
                        TruckDetail::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                        if($product==3){
                            ImporterProductTransactions::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                        } else {
                            OmcProductTransactions::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                        }
                    } else {
                        $orderData=[
                            'created_at'=>!is_null( $order->created_at)?$date:null,
                            'chamber_assigned_at'=>!is_null( $order->chamber_assigned_at)?$date:null,
                            'vehicle_loaded_at'=>!is_null( $order->vehicle_loaded_at)?$date:null,
                            'order_verified_at'=>!is_null( $order->order_verified_at)?$date:null,
                            'completed_at'=>!is_null( $order->completed_at)?$date:null,
                            'authorized_at'=>!is_null( $order->authorized_at)?$date:null,
                        ];
                        Order::where('id',(int) $order->id)->withTrashed()->update($orderData);  
                        OrderProductMeter::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                        ChamberAssignment::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                        OmcProductTransactions::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
                    }
                    OrderProduct::where('order_id',(int) $order->id)->withTrashed()->update(['created_at'=>$date]);
           }
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
