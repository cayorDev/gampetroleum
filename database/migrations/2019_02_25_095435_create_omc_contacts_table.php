<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmcContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('omc_contacts') ) {
            Schema::create('omc_contacts', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                        ->references('id')
                        ->on('omc')
                        ->onDelete('cascade');
                $table->string('name', 100)->nullable();
                $table->string('email', 50)->nullable();
                $table->string('tel1', 20)->nullable();
                $table->string('tel2', 20)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index([ 'omc_id', 'name', 'email' ]);
            });
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('omc_contacts');
    }
}
