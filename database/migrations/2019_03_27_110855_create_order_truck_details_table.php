<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTruckDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     if ( !Schema::hasTable('order_truck_details') ) {
        Schema::create('order_truck_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')
            ->references('id')
            ->on('orders')
            ->onDelete('cascade');
            $table->unsignedInteger('supervisor');
            $table->foreign('supervisor')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->string('truck_loading_ticket_number');
            $table->string('weight_before_loading');
            $table->string('weight_after_loading');
            $table->string('net_weight');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_truck_details');
    }
}
