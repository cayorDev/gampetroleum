<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('orders') ) {
            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('importer_id');
                $table->foreign('importer_id')
                        ->references('id')
                        ->on('importers')
                        ->onDelete('cascade');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                        ->references('id')
                        ->on('omc')
                        ->onDelete('cascade');
                $table->unsignedInteger('vehicle_id');
                $table->foreign('vehicle_id')
                        ->references('id')
                        ->on('vehicles')
                        ->onDelete('cascade');
                $table->unsignedInteger('driver_id');
                $table->foreign('driver_id')
                        ->references('id')
                        ->on('drivers')
                        ->onDelete('cascade');
                $table->unsignedInteger('order_id');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
