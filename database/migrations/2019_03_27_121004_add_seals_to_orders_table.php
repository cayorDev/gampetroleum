<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSealsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if ( !Schema::hasColumn( 'orders', 'seals' ) ) {
            Schema::table( 'orders', function( Blueprint $table ) {
                $table->text( 'seals' )->after('loader2')->nullable();
            });
        }

        if ( !Schema::hasColumn( 'orders', 'comments' ) ) {
            Schema::table( 'orders', function( Blueprint $table ) {
                $table->text( 'comments' )->after('seals')->nullable();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'orders', 'seals' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn( 'seals' );

            });
        }
        if ( Schema::hasColumn( 'orders', 'comments' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn( 'comments' );

            });
        }
    }
}
