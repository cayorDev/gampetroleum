<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOmcImporterTransactionsAndOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Drop purchase order 
        if ( Schema::hasColumn('orders', 'purchase_order') ) {
            Schema::table('orders', function(Blueprint $table) {
                $table->dropColumn('purchase_order');
            });
        }

        //Adding release id to importer_product_transactions
        if ( !Schema::hasColumn('importer_product_transactions','release_id') ) {
            Schema::table('importer_product_transactions', function(Blueprint $table) {
                $table->unsignedInteger('release_id')->nullable()->after('created_by');
            });
        }

        //Adding release id to omc_product_transaction
        if ( !Schema::hasColumn('omc_product_transactions','release_id') ) {
            Schema::table('omc_product_transactions', function(Blueprint $table) {
                $table->unsignedInteger('release_id')->nullable()->after('created_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop purchase order 
        if ( !Schema::hasColumn('orders', 'purchase_order') ) {
            Schema::table('orders', function(Blueprint $table) {
                $table->string('purchase_order', 50)->nullable();
            });
        }

        //Adding release id to importer_product_transactions
        if ( Schema::hasColumn('importer_product_transactions','release_id') ) {
            Schema::table('importer_product_transactions', function(Blueprint $table) {
                $table->dropColumn('release_id');
            });
        }

        //Adding release id to omc_product_transaction
        if ( Schema::hasColumn('omc_product_transactions','release_id') ) {
            Schema::table('omc_product_transactions', function(Blueprint $table) {
                $table->dropColumn('release_id');
            });
        }
    }
}
