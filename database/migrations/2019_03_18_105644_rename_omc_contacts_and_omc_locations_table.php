<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOmcContactsAndOmcLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Changes for omc_contacts table
        if ( Schema::hasTable('omc_contacts') ) {
            Schema::rename('omc_contacts', 'contacts');
        }
        
        //Changes for omc_locations table
        if ( Schema::hasTable('omc_locations') ) {
            Schema::rename('omc_locations', 'locations');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Changes for contacts table
        if ( Schema::hasTable('contacts') ) {
            Schema::rename('contacts', 'omc_contacts');
        }
        
        //Changes for locations table
        if ( Schema::hasTable('locations') ) {
            Schema::rename('locations', 'omc_locations');
        }
    }
}
