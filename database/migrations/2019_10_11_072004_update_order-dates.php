<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use File as File;
use App\Models\System\Order\ChamberAssignment;
use App\Models\System\Order\OrderProductMeter;
use App\Models\System\Order\OrderProduct;
use App\Models\System\Order\TruckDetail;
use App\Models\System\Order\Order;
use App\Models\System\Importer\ImporterProductTransactions;
use App\Models\System\OMC\OmcProductTransactions;
use Carbon\Carbon;

class UpdateOrderDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date=new Carbon('2019-10-04');
        $file= File::get(public_path('orderIDFiles/4th.txt'));
        foreach (explode("\n", $file) as $key=>$line){
            $orderID=str_replace("\r", "", $line);
            if(is_numeric( $orderID)){
                $order=Order::where('id',(int) $orderID)->with('OrderProducts')->first();
                $diff = date_diff($order->created_at,$date);
                if ($diff->days != '0') {
                    $product=$order->OrderProducts->first()->product_id;
                    if( $product == 3 || $product == 6 ){
                        Order::where('id',(int) $orderID)->update(['created_at'=>$date,'authorized_at'=>$date]);
                        TruckDetail::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        if($order->OrderProducts->first()->product_id==3){
                            ImporterProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        } else {
                            OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        }
                    } else {
                        $orderData=[
                            'created_at'=>!is_null( $order->created_at)?$date:null,
                            'chamber_assigned_at'=>!is_null( $order->chamber_assigned_at)?$date:null,
                            'vehicle_loaded_at'=>!is_null( $order->vehicle_loaded_at)?$date:null,
                            'order_verified_at'=>!is_null( $order->order_verified_at)?$date:null,
                            'completed_at'=>!is_null( $order->completed_at)?$date:null,
                            'authorized_at'=>!is_null( $order->authorized_at)?$date:null,
                        ];
                        Order::where('id',(int) $orderID)->update($orderData);
                        OrderProductMeter::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        ChamberAssignment::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                    }
                    OrderProduct::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                }
            }
        }

        $date=new Carbon('2019-10-03');
        $file= File::get(public_path('orderIDFiles/3rd.txt'));
        foreach (explode("\n", $file) as $key=>$line){
            $orderID=str_replace("\r", "", $line);
            if(is_numeric( $orderID)){
                $order=Order::where('id',(int) $orderID)->with('OrderProducts')->first();
                $diff = date_diff($order->created_at,$date);
                if ($diff->days != '0') {
                    $product=$order->OrderProducts->first()->product_id;
                    if( $product == 3 || $product == 6 ){
                        Order::where('id',(int) $orderID)->update(['created_at'=>$date,'authorized_at'=>$date]);
                        TruckDetail::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        if($order->OrderProducts->first()->product_id==3){
                            ImporterProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        } else {
                            OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        }
                    } else {
                        $orderData=[
                            'created_at'=>!is_null( $order->created_at)?$date:null,
                            'chamber_assigned_at'=>!is_null( $order->chamber_assigned_at)?$date:null,
                            'vehicle_loaded_at'=>!is_null( $order->vehicle_loaded_at)?$date:null,
                            'order_verified_at'=>!is_null( $order->order_verified_at)?$date:null,
                            'completed_at'=>!is_null( $order->completed_at)?$date:null,
                            'authorized_at'=>!is_null( $order->authorized_at)?$date:null,
                        ];
                        Order::where('id',(int) $orderID)->update($orderData);
                        OrderProductMeter::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        ChamberAssignment::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                    }
                    OrderProduct::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                }
            }
        }

        $date=new Carbon('2019-10-02');
        $file= File::get(public_path('orderIDFiles/2nd.txt'));
        foreach (explode("\n", $file) as $key=>$line){
            $orderID=str_replace("\r", "", $line);
            if(is_numeric( $orderID)){
                $order=Order::withTrashed()->where('id',(int) $orderID)->with('OrderProducts')->first();
                $diff = date_diff($order->created_at,$date);
                if ($diff->days != '0') {
                    $product=$order->OrderProducts->first()->product_id;
                    if( $product == 3 || $product == 6 ){
                        Order::where('id',(int) $orderID)->update(['created_at'=>$date,'authorized_at'=>$date]);
                        TruckDetail::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        if($order->OrderProducts->first()->product_id==3){
                            ImporterProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        } else {
                            OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        }
                    } else {
                        $orderData=[
                            'created_at'=>!is_null( $order->created_at)?$date:null,
                            'chamber_assigned_at'=>!is_null( $order->chamber_assigned_at)?$date:null,
                            'vehicle_loaded_at'=>!is_null( $order->vehicle_loaded_at)?$date:null,
                            'order_verified_at'=>!is_null( $order->order_verified_at)?$date:null,
                            'completed_at'=>!is_null( $order->completed_at)?$date:null,
                            'authorized_at'=>!is_null( $order->authorized_at)?$date:null,
                        ];
                        Order::where('id',(int) $orderID)->update($orderData);
                        OrderProductMeter::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        ChamberAssignment::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                    }
                    OrderProduct::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                }
            }
        }

        $date=new Carbon('2019-10-01');
        $file= File::get(public_path('orderIDFiles/1st.txt'));
        foreach (explode("\n", $file) as $key=>$line){
            $orderID=str_replace("\r", "", $line);
            if(is_numeric( $orderID)){
                $order=Order::where('id',(int) $orderID)->with('OrderProducts')->first();
                $diff = date_diff($order->created_at,$date);
                if ($diff->days != '0') {
                    $product=$order->OrderProducts->first()->product_id;
                    if( $product == 3 || $product == 6 ){
                        Order::where('id',(int) $orderID)->update(['created_at'=>$date,'authorized_at'=>$date]);
                        TruckDetail::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        if($order->OrderProducts->first()->product_id==3){
                            ImporterProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        } else {
                            OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        }
                    } else {
                        $orderData=[
                            'created_at'=>!is_null( $order->created_at)?$date:null,
                            'chamber_assigned_at'=>!is_null( $order->chamber_assigned_at)?$date:null,
                            'vehicle_loaded_at'=>!is_null( $order->vehicle_loaded_at)?$date:null,
                            'order_verified_at'=>!is_null( $order->order_verified_at)?$date:null,
                            'completed_at'=>!is_null( $order->completed_at)?$date:null,
                            'authorized_at'=>!is_null( $order->authorized_at)?$date:null,
                        ];
                        Order::where('id',(int) $orderID)->update($orderData);
                        OrderProductMeter::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        ChamberAssignment::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                        OmcProductTransactions::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                    }
                    OrderProduct::where('order_id',(int) $orderID)->update(['created_at'=>$date]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
