<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmcLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('omc_locations') ) {
            Schema::create('omc_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('omc_id');
                $table->foreign('omc_id')
                        ->references('id')
                        ->on('omc')
                        ->onDelete('cascade');
                $table->string('name', 100)->nullable();
                $table->string('contact_name', 100)->nullable();
                $table->string('tel1', 20)->nullable();
                $table->string('tel2', 20)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('omc_locations');
    }
}
