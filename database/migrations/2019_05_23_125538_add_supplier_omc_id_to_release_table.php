<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierOmcIdToReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('release', 'supplier_omc_id') ) {
            Schema::table('release', function (Blueprint $table) {
                $table->unsignedInteger('supplier_omc_id')->nullable()->after('importer_id');
                $table->foreign('supplier_omc_id')
                ->references('id')
                ->on('omc')
                ->onDelete('cascade');
            });
        }

        if ( !Schema::hasColumn('omc_product_stocks', 'supplier_omc_id') ) {
            Schema::table('omc_product_stocks', function (Blueprint $table) {
                $table->unsignedInteger('supplier_omc_id')->nullable()->after('importer_id');
                $table->foreign('supplier_omc_id')
                ->references('id')
                ->on('omc')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('release', 'supplier_omc_id') ) {
            Schema::table('release', function (Blueprint $table) {
                $table->dropColumn('supplier_omc_id');
            });
        }

        if ( Schema::hasColumn('omc_product_stocks', 'supplier_omc_id') ) {
            Schema::table('release', function (Blueprint $table) {
                $table->dropColumn('supplier_omc_id');
            });
        }
    }
}
