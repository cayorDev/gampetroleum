<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('order_products') ) {
            Schema::create('order_products', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('order_id');
                $table->foreign('order_id')
                        ->references('id')
                        ->on('orders')
                        ->onDelete('cascade');
                $table->unsignedInteger('product_id');
                $table->foreign('product_id')
                        ->references('id')
                        ->on('products')
                        ->onDelete('cascade');
                $table->integer('quantity')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
