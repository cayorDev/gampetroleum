<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn( 'orders', 'created_by' ) ) {
            Schema::table( 'orders', function( Blueprint $table ) {
                $table->unsignedInteger('created_by')->after('deleted_at')->nullable();
                $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'orders', 'created_by' ) ) {
            Schema::table( 'orders', function( Blueprint $table ) {
                $table->dropColumn( 'created_by' );
            });
        }
    }
}
