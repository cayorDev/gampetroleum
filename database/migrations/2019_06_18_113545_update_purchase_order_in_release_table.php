<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchaseOrderInReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if ( Schema::hasColumn('release', 'purchase_order') ) {
        Schema::table('release', function (Blueprint $table) {
           $table->string('purchase_order', 225)->change();
       });
    }
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release', function (Blueprint $table) {
            //
        });
    }
}
