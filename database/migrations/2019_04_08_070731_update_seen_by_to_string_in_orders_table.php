<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeenByToStringInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn( 'orders', 'seen_by' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropForeign([ 'seen_by'] );
                $table->dropColumn( ['seen_by'] );
                
            });
        }
        Schema::table('orders', function (Blueprint $table) {
            $table->string('seen_by')->nullable()->after('approved_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'orders', 'seen_by' ) ) {
            Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('seen_by');
        });
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('seen_by')->nullable()->after('approved_by');
            $table->foreign('seen_by')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade');
        
        });
    }
}
