<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductOrderToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('products', 'product_order') ) {
            Schema::table('products', function (Blueprint $table) {
                $table->unsignedInteger('product_order')->after('measuring_unit')->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('products', 'product_order') ) {
            Schema::table('products', function (Blueprint $table) {
             $table->dropColumn('product_order')->after('measuring_unit');
         });
        }
    }
}
