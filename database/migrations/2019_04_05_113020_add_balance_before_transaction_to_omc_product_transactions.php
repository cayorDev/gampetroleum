<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceBeforeTransactionToOmcProductTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Adding balance_before_transaction column to omc_product_transactions table
        if ( Schema::hasTable('omc_product_transactions') && !Schema::hasColumn('omc_product_transactions', 'balance_before_transaction') ) {
            Schema::table('omc_product_transactions', function (Blueprint $table) {
                $table->integer('balance_before_transaction')->default(0)->after('quantity');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Removing balance_before_transaction column Form omc_product_transactions table
        if ( Schema::hasTable('omc_product_transactions') && Schema::hasColumn('omc_product_transactions', 'balance_before_transaction') ) {
            Schema::table('omc_product_transactions', function (Blueprint $table) {
                $table->dropColumn('balance_before_transaction');
            });
        }
    }
}
