<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuthorizedByToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn( 'orders', 'authorized_by' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('authorized_by')->nullable()->after('seen_at');
                $table->foreign('authorized_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            });
        }
        if ( !Schema::hasColumn( 'orders', 'authorized_at' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->datetime('authorized_at')->nullable()->after('authorized_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'orders', 'authorized_at' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('authorized_at');
            });
        }
        if ( Schema::hasColumn( 'orders', 'orders_authorized_by' ) ) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropForeign([ 'authorized_by'] );
                $table->dropColumn( ['authorized_by'] );
            });
        }
    }
    
}
