<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalLoadedToChamberAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     if ( !Schema::hasColumn( 'chamber_assignment', 'total_loaded' ) ) {
        Schema::table( 'chamber_assignment', function( Blueprint $table ) {
            $table->integer( 'total_loaded' )->after('quantity')->default(0)->nullable();
        });
    }
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn( 'chamber_assignment', 'total_loaded' ) ) {
            Schema::table('chamber_assignment', function (Blueprint $table) {
                $table->dropColumn( 'total_loaded' );
            });
        }
    }
}
