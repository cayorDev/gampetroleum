<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceBeforeTransactionToImporterProductTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Adding balance_before_transaction column to importer_product_transactions table
        if ( Schema::hasTable('importer_product_transactions') && !Schema::hasColumn('importer_product_transactions', 'balance_before_transaction') ) {
            Schema::table('importer_product_transactions', function (Blueprint $table) {
                $table->integer('balance_before_transaction')->default(0)->after('quantity');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Removing balance_before_transaction column from importer_product_transactions table
        if ( Schema::hasTable('importer_product_transactions') && Schema::hasColumn('importer_product_transactions', 'balance_before_transaction') ) {
            Schema::table('importer_product_transactions', function (Blueprint $table) {
                $table->dropColumn('balance_before_transaction');
            });
        }
    }
}
