<?php 

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->disableForeignKeys();

        Product::truncate();
        //Specify all products 
        $products = [
				[
					'name' 				=> 'Gas Oil',
					'sku'				=> 'ADO',
					'measuring_unit'	=> 'ltr'
				],
				[
					'name' 				=> 'PMS',
					'sku'				=> 'PMS',
					'measuring_unit'	=> 'ltr'
				],
				[
					'name' 				=> 'HFO',
					'sku'				=> 'HFO',
					'measuring_unit'	=> 'kgs'
				],	        	
				[
					'name' 				=> 'Kerosene',
					'sku'				=> 'KEROSENE',
					'measuring_unit'	=> 'ltr'
				],
				[
					'name' 				=> 'Jet Fuel',
					'sku'				=> 'JET',
					'measuring_unit'	=> 'ltr'
				],
				[
					'name' 				=> 'LPG',
					'sku'				=> 'LPG',
					'measuring_unit'	=> 'kgs'
				],
	        ];

        // Add the master administrator, user id of 1
        foreach ( $products as $product ) {
        	Product::create($product);
        }

        $this->enableForeignKeys();
    }
}
