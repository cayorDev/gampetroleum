<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $superadmin = Role::create(['name' => config('access.users.superadmin_role')]);
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $supervisor = Role::create(['name' => config('access.users.supervisor_role')]);
        $attendent = Role::create(['name' => config('access.users.attendent_role')]);
        $batman = Role::create(['name' => config('access.users.special_role')]);
        $operator = Role::create(['name' => config('access.users.operator_role')]);


        // Create Permissions
        $permissions = ['view backend'];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // ALWAYS GIVE ADMIN ROLE ALL PERMISSIONS
        $superadmin->givePermissionTo(Permission::all());
        // Assign Permissions to other Roles
        $admin->givePermissionTo('view backend');
        // Assign Permissions to other Roles
        $supervisor->givePermissionTo('view backend');
        // Assign Permissions to other Roles
        $attendent->givePermissionTo('view backend');
        // Assign Permissions to other Roles
        $batman->givePermissionTo('view backend');
        // Assign Permissions to other Roles
        $operator->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
