<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.superadmin_role'));
        User::find(2)->assignRole(config('access.users.admin_role'));
        User::find(3)->assignRole(config('access.users.supervisor_role'));
        User::find(4)->assignRole(config('access.users.attendent_role'));
        User::find(5)->assignRole(config('access.users.special_role'));
        User::find(6)->assignRole(config('access.users.operator_role'));

        $this->enableForeignKeys();
    }
}
