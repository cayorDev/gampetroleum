$(() => {
  loadStats()
});
$(document).ready( () => {
  console.info('Page ready to operate. You can now operate freely. Happy hunting.');

  $('#daterange-btn').daterangepicker(
  {
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  },
  function (start, end) {
          //Define dates to variables
          let start_date = start.format('MMMM D, YYYY');
          let end_date   = end.format('MMMM D, YYYY');
          //Start and end date span
          $('#daterange-btn span').html(start_date + ' - ' + end_date );
          //new request to get data with dates
          location.href = '/admin/dashboard?start='+start_date+'&end='+end_date;
        }
        );

  initializeDatepicker();
});

const loadStats = () => {
  $.ajax({
    url: '/admin/stats',
    type: 'get',
    dataType: 'json',
    success: result => {
      if ( result.status == 200 ) {
        showStatsUsingData(result.data);
      }
    },
    error: (xhr, response, err) => {
      console.log('error', err)
    },
    complete: data => {
      // console.log('complete data')
    }
  });
}

const showStatsUsingData = ({ omc, importers, orders }) => {

  loadOrdersMap(orders);
}


const loadOrdersMap = (values) => {
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
    {
      value: values.uplifting,
      color: "#f56954",
      highlight: "#f56954",
      label: "Pending For Uplifting"
    },
    {
      value: values.loadverify,
      color: "#00c0ef",
      highlight: "#00c0ef",
      label: "Order for Load Verification"
    },
    {
      value: values.orderforapproval,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Orders for Approval"
    },
    {
      value: values.released,                    
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Total Released Order"
    }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    // 
// new Chart(document.getElementById("pieChart"), {
//     type: 'doughnut',
//     data: {
//       datasets: PieData
//     },
//     options: pieOptions
// });
//   var myDoughnutChart = new Chart(document.getElementById("pieChart"), {
//     type: 'doughnut',
//     data: PieData,
//     options: pieOptions
// });

pieChart.Doughnut(PieData, pieOptions);
}

function initializeDatepicker(){
  var start = moment().startOf('month');
  var end =  moment().endOf('month');

  function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

  }
  $('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
     'Today': [moment(), moment()],
     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
     'This Month': [moment().startOf('month'), moment().endOf('month')],
     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
   }

 }, cb);

  cb(start, end);
  $('#reportrange').on('apply.daterangepicker', function(ev, picker) {

    start1=picker.startDate.format('YYYY-MM-DD');
    end1=picker.endDate.format('YYYY-MM-DD');

    getOrderTransactionData(start1,end1);

  });

  start1=start.format('YYYY-MM-DD');
  end1=end.format('YYYY-MM-DD');
  getOrderTransactionData(start1,end1);
}

function getOrderTransactionData(start, end)
{
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    method: 'POST',
    url: '/order/getOrderTransaction',
    data: {'start' : start,'end' : end}, 
    success: function(response){
      OrderGraph(response);
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(JSON.stringify(jqXHR));
      console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      alert("Please Try Again");
    }
  });
}


function OrderGraph(response)
{
  var order_datalabels = [];
  var order_datavalue = [];

  $.each(response.labels, function( index, value ) {
   order_datalabels.push(value);
 });
  $.each(response.chartData, function( index, value ) {
   var datavalue=[];
   $.each(value, function( index1, value1) {
    datavalue.push(value1);
  });

   order_datavalue.push({label: index,
     fillColor           : 'rgba(210, 214, 222, 1)',
     strokeColor         : 'rgba(210, 214, 222, 1)',
     pointColor          : 'rgba(210, 214, 222, 1)',
     pointStrokeColor    : '#c1c7d1',
     pointHighlightFill  : '#fff',
     pointHighlightStroke: 'rgba(220,220,220,1)',
     data                : datavalue
   });
 });


  var barChartDataValue = {
    labels  : order_datalabels,
    datasets: order_datavalue
  }

  var barChartCanvas                   = $('#order-transaction').get(0).getContext('2d')
  var barChart                         = new Chart(barChartCanvas)
  var barChartData                     = barChartDataValue
  barChartData.datasets[1].fillColor   = '#00a65a'
  barChartData.datasets[1].strokeColor = '#00a65a'
  barChartData.datasets[1].pointColor  = '#00a65a'
  barChartData.datasets[2].fillColor   = '#446d90'
  barChartData.datasets[2].strokeColor = '#446d90'
  barChartData.datasets[2].pointColor  = '#446d90'
  barChartData.datasets[3].fillColor   = '#6d3155'
  barChartData.datasets[3].strokeColor = '#6d3155'
  barChartData.datasets[3].pointColor  = '#6d3155'
  barChartData.datasets[4].fillColor   = '#8c3a1b'
  barChartData.datasets[4].strokeColor = '#8c3a1b'
  barChartData.datasets[4].pointColor  = '#8c3a1b'
  barChartData.datasets[5].fillColor   = '#f39c11'
  barChartData.datasets[5].strokeColor = '#f39c11'
  barChartData.datasets[5].pointColor  = '#f39c11'
  var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

  }