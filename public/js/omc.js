//Before page load
$(() => {
    $('.products_inventory_container').hide();
    setQuantityForImporterAndProduct();
});
//Process after page is ready to listen to events and handle
$(document).ready(() => {
    $('#replicateContacts').on('click', (e) => {
        let old_count  = parseInt($('#contact_count').val());
        let new_count  = old_count + 1;
        let content    = $('#main_contact').clone();
        $(content).find(':input').attr('value', "");
        content    = $(content).html()
        .replace(/[0]/g,new_count);
        setNewCount(new_count); 
        createNewContent(content, new_count);                                  
    });
    $('#main_container_for_contacts').on('click', '.removeBtn',function() {
        let container           = $(this);
        let parent_container    = container.closest('.contact');
        let hr                  = parent_container.closest('.contact_hr');

        parent_container.closest('.contact').remove();

    });

    $('#replicateLocations').on('click', (e) => {
        let old_count  = parseInt($('#locations_count').val());
        let new_count  = old_count + 1;
        let   content = $('#main_location').clone();
        $(content).find(':input').attr('value', "");
        $(content).find(':input').attr("disabled", false);
        content    = $(content).html()
        .replace(/[0]/g,new_count);

        setNewLocationsCount(new_count);  
        createNewLocationsContent(content, new_count);        
    });

    $('#main_container_for_locations').on('click', '.removeBtn',function() {
        let container           = $(this);
        let parent_container    = container.closest('.location'); 
        let hr                  = parent_container.closest('.contact_hr');
        parent_container.closest('.location').remove();
    });

    $('.selector_type').on('change', function() {
        let value = parseInt( $(this).val() );
        setContainers(value);
    });
    let value = parseInt( $(".selector_type").val() );
    setContainers(value);


    $('.products_select').on('change', function() {
        let products_content = getProductsContent( $(this).val() );
        $('.products_inventory_container').empty();
        $('.products_inventory_container').html(products_content);
        $('.products_inventory_container').show();
    });

    $('#addLocations').on('click', (e) => {
        let old_count  = parseInt($('#locations_count').val());
        let new_count  = old_count + 1;
        let   content = $('#mainlocations').clone();
        $(content).find(':input').attr("disabled", false);
        $(content).find(':input').attr("required", true);
        $(content).find(':input').attr("value", '');
        content    = $(content).html()
        .replace(/[0]/g,new_count);
        setNewLocationsCount(new_count);

        _createNewLocationsContent(content, new_count);
    });

    $('#main_container_locations').on('click', '.remove',function() {
       let container           = $(this);
       let old_count  = parseInt($('#locations_count').val());
       let new_count  = old_count - 1;
       let parent_container    = container.closest('.contact');
       let hr                  = parent_container.closest('.contact_hr');
       parent_container.closest('.contact').remove();
       setNewLocationsCount(new_count);
   });

    $('#main_container_locations').on('click', '#edit', function(e) {

        e.preventDefault();
        let div= $(this).attr('div-name');
        let id = $('.'+div).find('#loc_id').val();
        $('.'+div).find('input').attr('readonly', false);
        $('.'+div).find('.btnfirst').hide();
        $(this).parent().parent().find('.btnsecond').show();
    });

    $('#main_container_locations').on('click', '#back', function(e) {
        let div= $(this).attr('div-name');
        $('.'+div).find('input').attr("readonly", true);
        $('.'+div).find('.btnfirst').show();
        $('.'+div).find('.btnsecond').hide();
    });
    //js for omc products
    $(".products").on('change',function(){
        $select = $(this).val();
        switch($select) {
            case 'ADO':
            $value=$(".h1_0").text();
            $("#quantity").val($value); 
            $("#product_id").attr('value',1);
            break;   

            case 'PMS' :
            $value=$(".h1_1").text()
            $("#quantity").val($value);
            $("#product_id").attr('value',2);  
            break; 

            case 'HFO':
            $value=$(".h1_2").text()
            $("#quantity").val($value);
            $("#product_id").attr('value',3);  
            break;   

            case 'Kerosene':
            $value=$(".h1_3").text()
            $("#quantity").val($value);
            $("#product_id").attr('value',4);  
            break;   

            case 'Jet Fuel' :
            $value=$(".h1_4").text()
            $("#quantity").val($value);
            $("#product_id").attr('value',5);  
            break;   

            case 'LPG' :
            $value=$(".h1_5").text()
            $("#quantity").val($value);
            $("#product_id").attr('value',6);  
            break; 

            defualt: break;  
        }
    });


$(".add").click(function(e){
    e.preventDefault();
    $append_quantity= parseInt($("#append_quantity").val());
    $quantity=parseInt($("#quantity").val());
    $transaction=$("#transaction").val();

    if($transaction==0) {

        if($.isNumeric($append_quantity)){
            $new_quantity= $quantity + $append_quantity;
            $("#quantity").val($new_quantity);
        } 
        else {
            errors = '<div class="alert alert-danger"><ul><li>Enter Numeric Value in Add/Deduct</li></ul></div>';
            $(".manage-form-errors").append(errors) ;
            $(".manage-form-errors").fadeOut(7000);   
        }
    }
    else { 

        if($.isNumeric($append_quantity)){
            if($append_quantity>$quantity) {
                errors = '<div class="alert alert-danger"><ul><li>Not more than Quantity</li></ul></div>';
                $(".manage-form-errors").append(errors) ;  
                $(".manage-form-errors").fadeOut(3000);
            }
            else {
                $new_quantity= $quantity - $append_quantity;
                $("#quantity").val($new_quantity); 
            }      
        }
        else{
            errors = '<div class="alert alert-danger"><ul><li>Enter Numeric Value in Add/Deduct</li></ul></div>'; 
            $(".manage-form-errors").append(errors) ; 
            $(".manage-form-errors").fadeOut(3000);
        } 
    }
});

$("#edit_Importer").click(function(){
    $("input.Importer").attr("readonly",false);
    $("#manage").attr("value",1);  

});   
$("#transaction").on('change',function(){
    $transaction=$("#transaction").val();
    if($transaction==0) {
        $(".add").text("Add");
    }
    if($transaction==1) {
        $(".add").text("Deduct");
    }
});

    //On product or importer change
    $('select').change(function() {
        //get selected options importer id
        setQuantityForImporterAndProduct();
    });

});

//Get container for products accroding to selected ones
function getProductsContent( $products )
{   
    let content = '';
    let options = '';
    if ( typeof importers !== 'undefined') {
        $.each(importers, ( k,v ) => {
            options +='<option value="'+k+'">'+v+'</option>'; 
        });
    }

    if ( $.isArray( $products ) ) {
        for ( let prod of $products ) {
            content += '<div class="form-group row"><label class=" form-control-label col-md-2" for="product['+prod+']">'+prod+' Balance</label><div class="col-md-4"><input type="number" class="form-control" name="products['+prod+']" placeholder="Specify Quantity"></div>'

            if (typeof importers !== 'undefined') {
                content +='<label class=" form-control-label col-md-2" for="importers['+prod+']">Select Importer</label><div class="form-group col-md-4"><select class="form-control" name="importers['+prod+']">'+options+'</select></div>'
            }

            content += '</div>'
        }
    }

    return content;
}

//Set container hide and show properties
function setContainers( value )
{

    if ( value === 2 ) {
        $('.card-body').find(".hr").show();
        $('.products_container').find('input').attr('required', true);
        $("#main_container_for_locations").show();
        $('.products_container').show();

    } else if( value === 1 ) {
        $('.card-body').find(".hr").show();
        $('.products_container').find('input').attr('required', true);
        $("#main_container_for_locations").hide();
        $(".locations_hr").hide();
        $('.products_container').show();
        $('.location_field').attr('required', false);
    } else {
        $('.card-body').find(".hr").show();
        $('.products_container').find('input').attr('required', false);
        $('.products_container').hide();
        $("#main_container_for_locations").show();
    }

}

function setNewCount(count){
    $('#contact_count').val(count);
};

function createNewContent(content, count) {
    content = '<div class="contact" ><hr class=".contact_hr"><div class="btn-toolbar"><a href="javascript:void(0);" class="btn btn-danger ml-1 removeBtn" data-toggle="tooltip" title="remove"><i class="fas fa-minus-circle"></i>Remove</a></div><div class="col-md-12">'+content+'</div></div>';
    $('#main_container_for_contacts').append(content);
};

function setNewLocationsCount(count){
    $('#locations_count').val(count);
};

function createNewLocationsContent(content, count) {

    content = '<div class="location" ><hr class=".contact_hr"><div class="btn-toolbar "><a href="javascript:void(0);" class="btn btn-danger ml-1 removeBtn" data-toggle="tooltip" title="remove"><i class="fas fa-minus-circle"></i>Remove</a></div><div class="col-md-12" >'+content+'</div></div>';

    $('#main_container_for_locations').append(content);
};

//js to append the new location div
function _createNewLocationsContent(content, count) {
    content = '<div class="contact"><div class="row" ><div class="col-md-10" style="border-right: 1px solid rgba(0,0,0,.1)">'+content+'</div><div class="col-md-2"><a href="javascript:void(0);" class="btn btn-danger ml-1 remove" data-toggle="tooltip" title="remove"><i class="fas fa-minus-circle"></i>Remove</a></div></div><hr></div>';
    $('#main_container_locations').append(content);
};

$("#addressEdit").click(function(e){
  e.preventDefault();
  $(this).closest('#update_address').find('input').removeAttr("readonly");
  $(this).hide();
  $("#addressupdate").removeAttr('hidden');
  $(".country").attr('readonly', true);

});

$("#edit").click(function(e){
    e.preventDefault();
    $(this).closest('#omcform').find('input').removeAttr("readonly");
    $(this).hide();
    $("#update").removeAttr('hidden');
});

$("#add_address").click(function(e){
    e.preventDefault();
    $("#add_address_content").show();
    $("#add_address_data").hide();
});

$("#cancel").click(function(e){
    e.preventDefault();
    $("#add_address_data").show();
    $("#add_address_content").hide();
});

//js for omc contact page
$('#contacts_add').click(function(){
    let old_count=parseInt($("#contact_count").val());
    let new_count=old_count+1;
    let content =$("#clone").clone(); 
    $(content).find(":input").removeAttr("disabled");
    $(content).find(":input").attr('value','');
    content=$(content).html().replace(/[0]/g,new_count);
    setNewCountContact(new_count);
    createContentContact(content,new_count);

});

$(".card").on("click","#back",function(){
    $(this).parent().hide();
    $(this).parent().parent().find('.show_edit').show();
    $(this).parent().parent().find(':input').attr('readonly',true);
});

$(".card").on("click","#contacts_edit",function(){
    $(this).parent().hide();
    $(this).parent().parent().find('.hide_edit').show();
    $(this).parent().parent().find(':input').removeAttr('readonly');
});

$(".main_content").on("click",".remove",function(){
    let old_count  = parseInt($('#contact_count').val());
    let new_count  = old_count - 1;
    $(this).closest('.contact').remove();
    setNewCountContact(new_count);
});

function createContentContact(content,count){
    content = '<div class="contact"><div class="row" ><div class="col-md-9" style="border-right: 1px solid rgba(0,0,0,.1)">'+content+'</div><div class="col-md-2"><a href="javascript:void(0);" class="btn btn-sm btn-danger col-md-12 ml-1 remove" data-toggle="tooltip" title="remove"><i class="fas fa-minus-circle"></i>Remove</a></div></div><hr></div>';
    $( ".main_content" ).append(content);   
}

function setNewCountContact(count){
    $('#contact_count').val(count);
}

const setQuantityForImporterAndProduct = () => {
        let product_id      = $('.products option:selected').val();
        let importer_id     = $('.importers option:selected').val();
        let stock_id        = 0;
        let balance         = 0;
            $.each(prods, (k,v) => {
                    if ( v.product_id == product_id && v.importer == importer_id   ) {
                            balance     = v.balance;
                            stock_id    = v.id;
                    } 
            });
            //Set balance quantity
            $('#quantity').val(balance);
            $('#stock_id').val(stock_id)
    }

