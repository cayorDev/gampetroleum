var productId = 0;
$(document).ready(() => {
    $('.address_cancel').on('click', () => {
        changeAddressContent();
    });
    //Set default value 
    
    productId = parseInt($('.products').val());

    $('.products').on('change',function(){
        let id = parseInt($(this).val());
        let balance = parseInt( $('.product_'+id).text() );
        $('#quantity').val(balance);
        productId = id;
    });
     
    
   $(".add").click(function(e){
    e.preventDefault();
        $append_quantity= parseInt($("#append_quantity").val());
        $quantity=parseInt($("#quantity").val());
        $transaction=$("#transaction").val();
     
        if($transaction==0) {
            if($.isNumeric($append_quantity)){
                $new_quantity= $quantity + $append_quantity;
                $("#quantity").val($new_quantity);
                changeQuantityValues(productId, $new_quantity);
            } 
            else {
                errors = '<div class="alert alert-danger"><ul><li>Enter Numeric Value in Add/Deduct</li></ul></div>';
                $(".manage-form-errors").append(errors) ;
                $(".manage-form-errors").fadeOut(7000);   
            }
        }
        else { 

                if($.isNumeric($append_quantity)){
                    if($append_quantity>$quantity) {
                        errors = '<div class="alert alert-danger"><ul><li>Not more than Quantity</li></ul></div>';
                        $(".manage-form-errors").append(errors) ;  
                        $(".manage-form-errors").fadeOut(3000);
                    }
                    else {
                        $new_quantity= $quantity - $append_quantity;
                        $("#quantity").val($new_quantity); 
                    }      
                }
                else{
                    errors = '<div class="alert alert-danger"><ul><li>Enter Numeric Value in Add/Deduct</li></ul></div>'; 
                    $(".manage-form-errors").append(errors) ; 
                    $(".manage-form-errors").fadeOut(3000);
                } 
        }
    });

    $("#edit_Importer").click(function(){
        $("input.Importer").attr("readonly",false);
        $("#manage").attr("value",1);  

    });   
   $("#transaction").on('change',function(){
    $transaction=$("#transaction").val();
    if($transaction==0) {
        $(".add").text("Add");
    }
    if($transaction==1) {
        $(".add").text("Deduct");
    }
   });

   $("#add_address_data").click(function(e){
    e.preventDefault();
    $("#add_address_content").show();
    $("#add_address_content").find(':input').removeAttr('disabled');
    $("#add_address").hide();
    $('.address_change').val(1);
});
   $("#addressEdit").click(function(e){
  e.preventDefault();
  $(this).closest('#update_address').find('input').removeAttr("readonly");
  $(".country").attr('readonly', true);

});

});

const changeAddressContent = () => {
    $("#add_address_content").toggle();
    $("#add_address_content").find('input').val('');
    $("#add_address").toggle();
}

const changeQuantityValues = (id, quantity) => {
    let identifier = 'product_'+id;
    $('.'+identifier).text(quantity);
    $('#'+identifier).attr('value',quantity);
}
