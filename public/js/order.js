products = [];
importers = [];
suppliers = [];

$.noty.defaults.theme = 'metroui';
$.noty.defaults.type = 'error';
$.noty.defaults.layout = 'topRight';
$.noty.defaults.timeout = '10000';
$.noty.defaults.killer = true;
$.noty.defaults.modal = false;
var omcID = $('#omcSelect').val();
$('#inventoryContainer').hide();

if ( $('#omc_id').val() !== 'undefined' ) {
    omcID = $('#omc_id').val();
}

addProductList(omcID);

$('.closing-reading, .opening-reading').on('change keyup paste focusout', function(){
    setFinalReadingCount($(this).attr('id'))
});
// $('#productSelect').on('change', function(){
//     if (this.value && this.value == 3) {
//         $('.omcContainer').hide();
//     } else {
//         $('.omcContainer').show();
//     }
// });

$('#afterloading, #beforeloading').on('change keyup paste focusout', function(){
    setNetWeight();
});

$('#omcSelect').on('change', function() {
    $('.chamberProducts').empty();
    if( this.value ) {
        addProductList(this.value);
        $('.products_container').empty();
        $('.productQuantityAssigned').val(0);
       
    } 
});
$('#omcToOmc').on('change', function(){
    if (this.value){
        addOmcProductList(this.value);
        $('.products_containerOmc').empty();
        $('.productQuantityAssigned').val(0);
      
    }
})

$('#importerSelect').on('change', function() {
    if( this.value ) {
        setImporterProduct(importers[this.value]);
        $('.products_container').empty();
        $('.productQuantityAssigned').val(0);
     
    } 
});

$('#supplierOmc').on('change', function() {
    if( this.value ) {
        setSupplierOmcProduct(suppliers[this.value]);
        $('.products_containerOmc').empty();
        $('.productQuantityAssigned').val(0);
      
    } 
});


function addOmcProductList(omc_id) {

    $.ajax({
        url: '/order/getSpecifiedOmcProductList?omc_id='+omc_id,
        type: 'get',
        dataType: 'json',
        success: function(response) {
            if (response.status) {
                $('#createOmcOrderSubmit').attr('disabled',false);
                suppliers =  response.data;
                $("#supplierOmc").empty();
                $.each(suppliers, function(key, value) {
                    $('#supplierOmc')
                    .append($("<option></option>")
                        .attr("value",key)
                        .text(value.name));
                })
                //console.log(Object.keys(suppliers)[0]);
                setSupplierOmcProduct(suppliers[Object.keys(suppliers)[0]]);
            } else {
                $('.products_selectOmc').empty();
                $('#inventoryAvailableOmc').empty();
                $('.chamberProducts').empty();
                $('.products_containerOmc').empty();
                $('#createOmcOrderSubmit').attr('disabled',true);
                notyAlert('Inventory not available for OMC , Make sure user have Inventory before making any order');
            }
        },
        error: function(data) {
            notyAlert(data);
        }
    });
}

function addProductList(omcID) {
    if(omcID)
    {
        $.ajax({
            url: '/order/getOmcProductList/'+omcID,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                if (response.status) {
                    $('#createOrderSubmit').attr('disabled',false);
                    importers =  response.data.importers;
                    $("#importerSelect").empty();
                    $.each(importers, function(key, value) {
                        $('#importerSelect')
                        .append($("<option></option>")
                            .attr("value", key)
                            .text(value.importer_name)); 
                    })
                    setImporterProduct(importers[Object.keys(importers)[0]]);
                } else {
                    $('.products_select').empty();
                    $('#inventoryAvailable').empty();
                    $('#createOrderSubmit').attr('disabled',true);
                    notyAlert('Inventory not available for OMC , Make sure user have Inventory before making any order');
                }
            },
            error: function(data) {
                notyAlert(data);
            }
        });
    }
}

function setSupplierOmcProduct(supplier) {
    $('#omc_importer_id').val(supplier.importer_id);
    $('#supplier_omc_id').val(supplier.omc_id);
    $('.chamberProducts').empty();
    $('.products_selectOmc').empty();
    $('#inventoryAvailableOmc').empty();
    $('.chamberProducts').append('<option value="" selected>Select Products</option>');
    $('#quantity').val(0);
    $.each(supplier.products, function( index, value ) {
        $('.chamberProducts').append('<option value="' + index + '">' + value.product_name + '</option>');
        $('#inventoryContainerOmc').show();
        $('#inventoryAvailableOmc').prepend('<label class="badge">'+value.product_name+': '+value.balance+'</label>');
        $('.products_selectOmc').append('<option value="' + index + '">' + value.product_name + '</option>');

    });  
}

function setImporterProduct(importer) {
    // console.log(importer.product);
    products = importer.product;
    inventory = importer.inventory;
    $('.chamberProducts').empty();
    $('.products_select').empty();
    $('#inventoryAvailable').empty();
    $('.chamberProducts').append('<option value="" selected>Select Products</option>');
    $('#quantity').val(0);
    $.each(products, function( index, value ) {
        $('.chamberProducts').append('<option value="' + index + '">' + value + '</option>');
        $('#inventoryContainer').show();
        $('#inventoryAvailable').prepend('<label class="badge">'+value+': '+inventory[index]+'</label>');
        $('.products_select').append('<option value="' + index + '">' + value + '</option>');

    });  
}

$('.products_select').on('change', function() {
   $('.products_container').empty();
   let products_content = '';
   $(".products_select option:selected").each(function () {
       var $this = $(this);
       if ($this.length) {
           var selText = $this.text();
           var selVal = $this.val();
           products_content += '<div class="form-group row"><label class=" form-control-label col-md-2" for="product['+selVal+']">'+selText+' </label><div class="col-md-10"><input type="number" class="form-control" name="products['+selVal+']" placeholder="Select Quantity"></div></div>'
           $('.products_container').show();
       }
   });

   $('.products_container').html(products_content);
   $('.productQuantityAssigned').val(0);
   $('.selectOrderedProducts').val("");

});


$('.products_selectOmc').on('change', function() {
   $('.products_containerOmc').empty();
   let products_content = '';
   $(".products_selectOmc option:selected").each(function () {
       var $this = $(this);
       if ($this.length) {
           var selText = $this.text();
           var selVal = $this.val();
           products_content += '<div class="form-group row"><label class=" form-control-label col-md-2" for="product['+selVal+']">'+selText+' </label><div class="col-md-10"><input type="number" class="form-control" name="products['+selVal+']" placeholder="Select Quantity"></div></div>'
           $('.products_containerOmc').show();
       }
   });
   $('.products_containerOmc').html(products_content);
   $('.productQuantityAssigned').val(0);
   $('.selectOrderedProducts').val("");
  
});

$('#createOrderSubmit').click(function(){
    $.ajax({
        url: '/order/store',
        type: 'post',
        dataType: 'json',
        data: $('form#orderAddForm').serialize(),
        success: function(data) {
            if(data.success == true){
             window.location = "/order/assignChamber/upliftingVerification/"+data.orderId+"?isOrderCreated=1";
                // window.location = "/order/all";
            }

            if (data.success == false) {
                notyAlert(data.message);
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) 
            {
                errorsHtml += '<li>' + value[0] + '</li>';
                //notyError(value[0]);
            });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( '<div class="alert alert-danger">'+errorsHtml+'</div>' );
        }

    })
})

function notyError(errorsHtml){
    noty({
        text: errorsHtml,
        animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing'
        }
    });
}

function notyAlert(text){
    $.noty.defaults.layout = 'center';
    $.noty.defaults.modal = true;
    noty({
        text: text,
        buttons: [
        {
            addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                $noty.close();
            }
        }
        ]
    });
}

$('#createHfoOrderSubmit').click(function(){
    $.ajax({
        url: '/order/hfostore',
        type: 'post',
        dataType: 'json',
        data: $('form#hfoOrderAddForm').serialize(),
        success: function(data) {
            if(data.success == true){
                window.location = "/order/finalOrder/"+data.order_id+"?isRequestFromApproved=1";
            }

            if (data.success == false) {
                notyAlert(data.message);
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( errorsHtml );
        }
    });
})

$('#createOmcOrderSubmit').click(function(){
    $.ajax({
        url: '/order/store',
        type: 'post',
        dataType: 'json',
        data: $('form#omcOrderAddForm').serialize(),
        success: function(data) {
            if(data.success == true){
                window.location = "/order/all";
            }

            if (data.success == false) {
                notyAlert(data.message);
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( errorsHtml );
        }
    });
})


$('#updateOrderSubmit').click(function(){
    $.ajax({
        url: '/order/update/'+$('#orderId').val(),
        type: 'post',
        dataType: 'json',
        data: $('form#orderUpdateForm').serialize(),
        success: function(data) {
            if(data.success == true){
                window.location = "/order/all";
            } 
            if (data.success == false) {
                notyAlert(data.message);
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( errorsHtml );
        }
    });
});

$('#updateProduct').on('click', function(){
    var selected_products = [];
    $("#productdiv_1 select").each( function (){
        selected_products.push($(this).val());
    });
    var lastDiv = $('#productdiv_1').children('div').last().attr('id');
    var lastID=lastDiv.split('_')[1];
    var newID= parseInt(lastID)+1;

    var selectList = '<select name="products[product_'+newID+'][productID]" class="form-control">';
    $.each(products, function( index, value ) {
        if(!selected_products.includes(index)) {
            selectList +='<option value="' + index + '">' + value + '</option>';
        } 
    });
    selectList += "</select>";
    var productQuantity = '<div class="form-group row" id="productDiv_'+ newID +'"><div class="col-md-5"> '+selectList+'</div><div class="col-md-5 "> <input type="number" name="products[product_' +newID+'][quantity]" id="quantity' + newID + '" value="" class="form-control" placeholder="Quantity" min=0></div><div class="col-md-2"> <button type="button" class="btn-sm btn btn-danger" id="addMoreBtn" onclick="removeProductDiv(productDiv_'+newID +')">Remove </button></div></div>'     
    $('#productdiv_1').append(productQuantity);
    if($('#productdiv_1').children('div').length >= Object.keys(products).length) {
        $('#updateProduct').attr("disabled", "disabled");
    }
});

function removeProductDiv(divID){
    if($('#productdiv_1').children('div').length <= Object.keys(products).length) {
     $('#updateProduct').attr("disabled", false);
 } else {
    $(divID).remove();
}
}

$('#updateOrderProductsSubmit').click(function(){
    $.ajax({
        url: '/order/update_products',
        type: 'post',
        dataType: 'json',
        data: $('form#orderProductForm').serialize(),
        success: function(data) {
            if(data.success == true){
                $('#myModal').modal('hide');
                window.location = "/order/edit/"+$('#orderId').val();
            } else {
                errorsHtml = '<ul>';
                errorsHtml += '<li>' + data.message + '</li>';
                errorsHtml += '</ul>';
                notyError(errorsHtml);
                //$( '#modal-errors' ).html( errorsHtml );
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#modal-errors' ).html( errorsHtml );
        }
    });
});

$('#assignChamberSubmit').click(function(){
    $.ajax({
        url: '/order/assignChamber/store',
        type: 'post',
        dataType: 'json',
        data: $('form#assignChamberForm').serialize(),
        success: function(data) {
            if(data.success == true){
                window.location = "/order/all";
            } else {
                notyAlert(data.message);
                errorsHtml = '<ul>';
                errorsHtml += '<li>' + data.message + '</li>';
                errorsHtml += '</ul>';
                notyError(errorsHtml);
                //$( '#form-errors' ).html( errorsHtml );
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( errorsHtml );
        }
    });
});

$('#upliftVehicleSubmit').click(function(){
    $.ajax({
        url: '/order/assignChamber/uplift_vehicle',
        type: 'post',
        dataType: 'json',
        data: $('form#upliftVehicleForm').serialize(),
        success: function(data) {
            if(data.success == true){
                window.location = "/order/all";
            } else {
                errorsHtml = '<ul>';
                errorsHtml += '<li>' + data.message + '</li>';
                errorsHtml += '</ul>';
                notyError(errorsHtml);
                //$( '#form-errors' ).html( errorsHtml );
            }
        },
        error: function(data) {
            var errors = data.responseJSON.errors; 
            errorsHtml = '<ul>';
            $.each( errors , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
            errorsHtml += '</ul>';
            notyError(errorsHtml);
            //$( '#form-errors' ).html( errorsHtml );
        }
    });
})

function setFinalReadingCount(id){
    let start = parseInt( $("input[name='product["+id+"][opening_meter]']").val() );
    let final = parseInt( $("input[name='product["+id+"][closing_meter]']").val() );
    let finalClass= "input[name='product["+id+"][quantity]']";
    if ( parseInt(final-start) != NaN && parseInt(final-start) > 0 ) {
        $(finalClass).css('border-color', '#d2d6de');
        $(finalClass).val(parseInt(final-start));
    } else {
        $(finalClass).val(0);
        $(finalClass).css('border-color', 'indianred');
    }
}

function setNetWeight(){
    let beforeloading = parseInt( $('#beforeloading').val() );
    let afterloading = parseInt( $('#afterloading').val() );
    let finalClass= "#netweight";
    if ( parseInt(afterloading-beforeloading) != NaN && parseInt(afterloading-beforeloading) > 0 ) {
        $(finalClass).css('border-color', '#d2d6de');
        $(finalClass).val(parseInt(afterloading-beforeloading));
    } else {
        $(finalClass).val(0);
        $(finalClass).css('border-color', 'indianred');
    }
}



