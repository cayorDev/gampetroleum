products=[];
suppliers = [];

$.noty.defaults.theme = 'metroui';
$.noty.defaults.type = 'error';
$.noty.defaults.layout = 'topRight';
$.noty.defaults.timeout = '10000';
$.noty.defaults.killer = true;
$.noty.defaults.modal = false;

var importerID = $('#importerSelect').val();
addProductList(importerID);
$('#inventoryContainer').hide();
$('#inventoryContainerOmc').hide();

$('#importerSelect').on('change', function() {
  $('#orderProducts').hide();
  $('.products_select').empty();
  $('.products_container').empty();
  if( this.value ){
    addProductList(this.value);
  } 
});

//addOmcProductList();
function addOmcProductList(omcId) {
  $.ajax({
    url: '/order/getSpecifiedOmcProductListOmcBased?omc_id='+omcId,
    type: 'get',
    dataType: 'json',
    success: function(response) {
      if (response.status) {
        $('#createOmcOrderSubmit').attr('disabled',false);
        suppliers =  response.data;
        $("#omcImporterSelect").empty();
        $.each(suppliers, function(key, value) {
          $('#omcImporterSelect')
          .append($("<option></option>")
            .attr("value",key)
            .text(value.name));
        })
        setSupplierOmcProduct(suppliers[Object.keys(suppliers)[0]]);
      } else {
        $('.products_selectOmc').empty();
        $('#inventoryAvailableOMC').empty();
        $('.chamberProducts').empty();
        $('.products_containerOmc').empty();
        $('#createOmcOrderSubmit').attr('disabled',true);
        notyAlert('Inventory not available for OMC , Make sure user have Inventory before making any order');
      }
    },
    error: function(data) {
      notyAlert(data);
    }
  });
}

function addProductList(importerID) {
  if(importerID){
    $.ajax({
      url: '/admin/importer/getImporterProductList/'+importerID,
      type: 'get',
      dataType: 'json',
      success: function(response) {
        if (response.status) {
          products = response.data.product;
          inventory = response.data.inventory;
          $('.products_select').empty();
          $('#inventoryAvailable').empty();
          $.each(products, function( index, value ) {
            $('.products_select').append('<option value="' + index + '">' + value + '</option>');
            $('#inventoryContainer').show();
            $('#inventoryAvailable').prepend('<label class="badge">'+value+': '+inventory[index]+'</label>');
          }); 
        } else {
          $('#createOrderSubmit').attr('disabled',true);
          notyAlert('Inventory not available for OMC , Make sure user have Inventory before making any order');
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  }
}

function setSupplierOmcProduct(supplier) {
  $('#omc_importer_id').val(supplier.importer_id);
  $('#supplier_omc_id').val(supplier.omc_id);
  $('.products_selectOmc').empty();
  $('#inventoryAvailableOMC').empty();
  $.each(supplier.products, function( index, value ) {
    $('.chamberProducts').append('<option value="' + index + '">' + value.product_name + '</option>');
    $('#inventoryContainerOmc').show();
    $('#inventoryAvailableOMC').prepend('<label class="badge">'+value.product_name+': '+value.balance+'</label>');
    $('.products_selectOmc').append('<option value="' + index + '">' + value.product_name + '</option>');
  });  
}

$('.omcAsOmc').on('change', function(){
  var omcId =  $(this).val();
  addOmcProductList(omcId)
})

$('#omcImporterSelect').on('change', function(){
  console.log($(this).val());
  console.log(suppliers);
  setSupplierOmcProduct(suppliers[$(this).val()]);

})

$('.products_select').on('change', function() {
 $('.products_container').empty();
 let products_content = '';
 $(".products_select option:selected").each(function () {
   var $this = $(this);
   if ($this.length) {
     var selText = $this.text();
     var selVal = $this.val();
     products_content += '<div class="form-group row"><label class=" form-control-label col-md-2" for="product['+selVal+']">'+selText+' </label><div class="col-md-10"><input type="number" class="form-control" name="products['+selVal+']" placeholder="Select Quantity"></div></div>'
     $('.products_container').show();
   }
 });
 $('.products_container').html(products_content);
});

$('.products_selectOmc').on('change', function() {
  $('.products_container_omc').empty();
  let products_content = '';
  $(".products_selectOmc option:selected").each(function () {
   var $this = $(this);
   if ($this.length) {
     var selText = $this.text();
     var selVal = $this.val();
     products_content += '<div class="form-group row"><label class=" form-control-label col-md-2" for="product['+selVal+']">'+selText+' </label><div class="col-md-10"><input type="number" class="form-control" name="products['+selVal+']" placeholder="Select Quantity"></div></div>'
     $('.products_container_omc').show();
   }
 });
  $('.products_container_omc').html(products_content);
})

function notyError(errorsHtml){
  noty({
    text: errorsHtml,
    animation: {
      open: {height: 'toggle'},
      close: {height: 'toggle'},
      easing: 'swing'
    }
  });
}

function notyAlert(text){
  $.noty.defaults.layout = 'center';
  $.noty.defaults.modal = true;
  noty({
    text: text,
    buttons: [
    {
      addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
        $noty.close();
      }
    }
    ]
  });
}
