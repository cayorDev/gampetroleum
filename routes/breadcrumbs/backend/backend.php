<?php
Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.vehicle.add', function ($trail) {
    $trail->parent('admin.vehicle.index');
    $trail->push(__('strings.backend.vehicle.add'), route('admin.vehicle.add'));
});

Breadcrumbs::for('admin.vehicle.index', function ($trail) {
    $trail->push(__('strings.backend.vehicle.index'), route('admin.vehicle.index'));
});

Breadcrumbs::for('admin.driver.add', function ($trail) {
    $trail->parent('admin.driver.index');
    $trail->push(__('strings.backend.driver.add'), route('admin.driver.add'));
});

Breadcrumbs::for('admin.driver.index', function ($trail) {
    $trail->push(__('strings.backend.driver.index'), route('admin.driver.index'));
});

Breadcrumbs::for('admin.driver.show', function ($trail, $id) {
	$trail->parent('admin.driver.index');
    $trail->push(__('strings.backend.driver.show'), route('admin.driver.show', $id));
});

Breadcrumbs::for('admin.driver.edit', function ($trail, $id) {
	$trail->parent('admin.driver.index');
    $trail->push(__('strings.backend.driver.edit'), route('admin.driver.edit', $id));
});

Breadcrumbs::for('admin.driver.deleted', function ($trail) {
    $trail->parent('admin.driver.index');
    $trail->push(__('strings.backend.driver.deleted'), route('admin.driver.deleted'));
});

Breadcrumbs::for('admin.vehicle.show', function ($trail, $id) {
  $trail->parent('admin.vehicle.index');
  $trail->push(__('strings.backend.vehicle.show'), route('admin.vehicle.show',$id));
});

Breadcrumbs::for('admin.vehicle.edit', function ($trail, $id) {
  $trail->parent('admin.vehicle.index');
  $trail->push(__('strings.backend.vehicle.edit'), route('admin.vehicle.edit',$id));
});

Breadcrumbs::for('admin.vehicle.deleted', function ($trail) {
    $trail->parent('admin.vehicle.index');
    $trail->push(__('menus.backend.access.vehicle.deleted'), route('admin.vehicle.deleted'));
});
Breadcrumbs::for('admin.omc.index', function ($trail) {
    $trail->push(__('strings.backend.omc.index'), route('admin.omc.index'));
});


Breadcrumbs::for('admin.omc.store', function ($trail) {
    $trail->push(__('strings.backend.omc.store'), route('admin.omc.store'));
});



Breadcrumbs::for('admin.omc.delete', function ($trail,$id) {
    $trail->push(__('strings.backend.omc.delete'), route('admin.omc.delete',$id));
});

Breadcrumbs::for('admin.omc.contacts', function ($trail, $id) {
    $trail->parent('admin.omc.show',$id);

    $trail->push(__('strings.backend.omc.contacts'), route('admin.omc.contacts', $id));
});
Breadcrumbs::for('admin.omc.show', function ($trail,$id) {
	$trail->parent('admin.omc.index');
    $trail->push(__('strings.backend.omc.show'), route('admin.omc.show',$id));
});
Breadcrumbs::for('admin.omc.update', function ($trail,$id) {
    $trail->push(__('strings.backend.omc.update'), route('admin.omc.update',$id));
});
Breadcrumbs::for('admin.omc.addressdelete', function ($trail,$id) {
	$trail->push(__('strings.backend.omc.addressdelete'), route('admin.omc.addressdelete',$id));
});
Breadcrumbs::for('admin.omc.addressupdate', function ($trail,$id) {
    $trail->push(__('strings.backend.omc.addressupdate'), route('admin.omc.addressupdate',$id));
});
Breadcrumbs::for('admin.omc.address', function ($trail) {
    $trail->push(__('strings.backend.omc.address'), route('admin.omc.address'));
});
Breadcrumbs::for('admin.omc.location', function ($trail, $id) {
 $trail->parent('admin.omc.show',$id);
 $trail->push(__('strings.backend.omc.locations'), route('admin.omc.location', $id));
});
Breadcrumbs::for('admin.omc.add', function ($trail) {
    $trail->push(__('strings.backend.omc.add'), route('admin.omc.add'));
});
Breadcrumbs::for('admin.importer.add', function ($trail) {
    $trail->push(__('strings.backend.omc.importer.add'), route('admin.importer.add'));
});
Breadcrumbs::for('admin.omc.addressstore', function ($trail,$id) {
    $trail->push(__('strings.backend.omc.address.store'), route('admin.omc.addressstore',$id));
});
Breadcrumbs::for('admin.omc.edit', function ($trail) {
    $trail->push(__('strings.backend.omc.edit'), route('admin.omc.edit'));
});
Breadcrumbs::for('admin.omc.manageOmcProduct', function ($trail, $id) {
    $trail->push(__('strings.backend.omc.edit'), route('admin.omc.manageOmcProduct', $id));
});

Breadcrumbs::for('admin.omc.storeLoc', function ($trail, $id) {
    $trail->push(__('strings.backend.omc.index'), route('admin.omc.storeLoc', $id));
});

Breadcrumbs::for('admin.omc.manageOmcInventory', function ($trail, $id) {
    $trail->push(__('strings.backend.omc.edit'), route('admin.importer.manageOmcInventory', $id));
});
Breadcrumbs::for('admin.omc.transactions', function ($trail, $id) {
    $trail->parent('admin.dashboard');
    $trail->push(__('strings.backend.omc.transactions'), route('admin.omc.transactions', $id));
});
Breadcrumbs::for('admin.omc.excelTransactions', function ($trail, $id) {
    $trail->parent('admin.omc.transactions', $id);
    $trail->push(__('strings.backend.omc.excelTransactions'), route('admin.omc.excelTransactions', $id));
});

Breadcrumbs::for('admin.importer.index', function ($trail) {
    $trail->push(__('strings.backend.omc.importer.show'), route('admin.importer.index'));
});
Breadcrumbs::for('admin.importer.manage', function ($trail, $id) {
    $trail->parent('admin.importer.index');
    $trail->push(__('strings.backend.omc.importer.edit'), route('admin.importer.manage', $id));
});
Breadcrumbs::for('admin.importer.manageImporter', function ($trail, $id) {
    $trail->push(__('strings.backend.omc.importer.edit'), route('admin.importer.manageImporter', $id));
});
Breadcrumbs::for('admin.importer.manageImporterInventory', function ($trail, $id) {
    $trail->push(__('strings.backend.omc.importer.edit'), route('admin.importer.manageImporterInventory', $id));
});
Breadcrumbs::for('admin.importer.contacts', function ($trail, $id) {
    $trail->parent('admin.importer.manage',$id);
    $trail->push(__('strings.backend.omc.contacts'), route('admin.importer.contacts', $id));
});
Breadcrumbs::for('admin.importer.location', function ($trail, $id) {
    $trail->parent('admin.importer.index',$id);
    $trail->push(__('strings.backend.omc.locations'), route('admin.importer.location', $id));
});
Breadcrumbs::for('admin.importer.transactions', function ($trail, $id) {
    $trail->parent('admin.dashboard');
    $trail->push(__('strings.backend.omc.importer.transactions'), route('admin.importer.transactions', $id));
});
Breadcrumbs::for('admin.importer.excelTransactions', function ($trail, $id) {
    $trail->parent('admin.importer.transactions', $id);
    $trail->push(__('strings.backend.omc.importer.excelTransactions'), route('admin.importer.excelTransactions', $id));
});
Breadcrumbs::for('admin.audit.logs', function ($trail) {
    $trail->push(__('Audit Logs'), route('admin.audit.logs'));
});

Breadcrumbs::for('order.add', function ($trail) {
    $trail->push(__('strings.backend.order.add'), route('order.add'));
});

Breadcrumbs::for('admin.release.add', function ($trail) {
    $trail->push(__('menus.backend.access.order.release'), route('admin.release.add'));
});

Breadcrumbs::for('admin.release.index', function ($trail) {
    $trail->push(__('menus.backend.access.order.inventoryrelease'), route('admin.release.index'));
});


Breadcrumbs::for('admin.release.show', function ($trail, $id) {
    $trail->parent('admin.release.index');
    $trail->push(__('menus.backend.access.order.inventoryrelease'), route('admin.release.show', $id));
});

Breadcrumbs::for('order.index', function ($trail) {
    $trail->push(__('strings.backend.order.index'), route('order.index'));
});

Breadcrumbs::for('order.view', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.view'), route('order.view', $id));
});

Breadcrumbs::for('order.edit', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.update'), route('order.edit', $id));
});

Breadcrumbs::for('order.assignchamber.index', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.assignChamber'), route('order.assignchamber.index', $id));
});

Breadcrumbs::for('order.viewupliftingform', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.viewAssignedChamber'), route('order.viewupliftingform', $id));
});

Breadcrumbs::for('order.assignchamber.truckLoadingConfirmation', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.truckLoadingConfirmation'), route('order.assignchamber.truckLoadingConfirmation', $id));
});

Breadcrumbs::for('order.assignchamber.upliftingVerification', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.upliftingVerification'), route('order.assignchamber.upliftingVerification', $id));
});

Breadcrumbs::for('order.finalOrder', function ($trail, $id) {
    $trail->parent('order.index');
    $trail->push(__('strings.backend.order.finalOrder'), route('order.finalOrder', $id));
});

Breadcrumbs::for('order.orderReport', function ($trail) {
    $trail->push(__('strings.backend.order.orderReport'), route('order.orderReport'));
});

Breadcrumbs::for('order.dailyReport', function ($trail) {
    $trail->push(__('strings.backend.order.dailyReport'), route('order.dailyReport'));
});

Breadcrumbs::for('order.inventoryReport', function ($trail) {
    $trail->push(__('strings.backend.order.inventoryReport'), route('order.inventoryReport'));
});
Breadcrumbs::for('admin.loaderList', function ($trail) {
    $trail->push(__('strings.backend.order.loaderList'), route('admin.loaderList'));
});

Breadcrumbs::for('admin.loader.add', function ($trail) {
    $trail->parent('admin.loader.index');
    $trail->push(__('strings.backend.loader.add'), route('admin.loader.add'));
});

Breadcrumbs::for('admin.loader.index', function ($trail) {
    $trail->push(__('strings.backend.loader.index'), route('admin.loader.index'));
});

Breadcrumbs::for('admin.loader.show', function ($trail, $id) {
    $trail->parent('admin.loader.index');
    $trail->push(__('strings.backend.loader.show'), route('admin.loader.show', $id));
});

Breadcrumbs::for('admin.loader.edit', function ($trail, $id) {
    $trail->parent('admin.loader.index');
    $trail->push(__('strings.backend.loader.edit'), route('admin.loader.edit', $id));
});

Breadcrumbs::for('admin.loader.getAllDeleted', function ($trail) {
    $trail->parent('admin.loader.index');
    $trail->push(__('strings.backend.loader.deleted'), route('admin.loader.getAllDeleted'));
});

Breadcrumbs::for('admin.managementList', function ($trail) {
    $trail->push(__('strings.backend.order.managementList'), route('admin.managementList'));
});

Breadcrumbs::for('admin.management.index', function ($trail) {
    $trail->push(__('strings.backend.management.index'), route('admin.management.index'));
});

Breadcrumbs::for('admin.management.show', function ($trail, $id) {
    $trail->parent('admin.management.index');
    $trail->push(__('strings.backend.management.show'), route('admin.management.show', $id));
});

Breadcrumbs::for('admin.management.edit', function ($trail, $id) {
    $trail->parent('admin.management.index');
    $trail->push(__('strings.backend.management.edit'), route('admin.management.edit', $id));
});


Breadcrumbs::for('order.customOrders', function ($trail) {
    $trail->push(__('strings.backend.order.index'), route('order.customOrders'));
});


require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
