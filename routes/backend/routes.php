<?php

use App\Http\Controllers\Backend\VehicleController;
use App\Http\Controllers\Backend\DriverController;


Route::get('changepassword', function() {
    $user = App\User::where('email', 'superadmin@gp.com')->first();
    dd($user);
    $user->password = Hash::make('123456');
    $user->save();
 
    echo 'Password changed successfully.';
});
Route::group(['middleware' => ['auth'], 'prefix' => 'vehicle'], function () {
	Route::get('/add', [VehicleController::class, 'create'])->name('vehicle.add');
	Route::post('/store', [VehicleController::class, 'store'])->name('vehicle.store');
	Route::get('/index', [VehicleController::class, 'index'])->name('vehicle.index');
	Route::get('/show/{id}', [VehicleController::class, 'show'])->name('vehicle.show');
	Route::get('edit/{id}', [VehicleController::class, 'edit'])->name('vehicle.edit');
	Route::patch('/{id}', [VehicleController::class, 'update'])->name('vehicle.update');
	Route::delete('/{id}', [VehicleController::class, 'destroy'])->name('vehicle.destroy');
	Route::get('/deleted', [VehicleController::class, 'deleted_vehicles'])->name('vehicle.deleted');
	Route::get('delete/{id}', [VehicleController::class, 'delete'])->name('vehicle.delete-permanently');
	Route::get('restore/{id}/', [VehicleController::class, 'restore'])->name('vehicle.restore');
	Route::get('/getDriverList/{id}', [VehicleController::class, 'getDriverList'])->name('driver_vehicle.getList');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'driver'], function () {
	Route::get('/add', [DriverController::class, 'create'])->name('driver.add');
	Route::post('/store', [DriverController::class, 'store'])->name('driver.store');
	Route::get('/index', [DriverController::class, 'index'])->name('driver.index');
	Route::get('/show/{id}', [DriverController::class, 'show'])->name('driver.show');
	Route::get('edit/{id}', [DriverController::class, 'edit'])->name('driver.edit');
	Route::patch('/{id}', [DriverController::class, 'update'])->name('driver.update');
	Route::delete('/{id}', [DriverController::class, 'destroy'])->name('driver.destroy');
	Route::get('delete/{id}', [DriverController::class, 'delete'])->name('driver.delete-permanently');
	Route::get('restore/{id}', [DriverController::class, 'restore'])->name('driver.restore');
	Route::get('/deleted', [DriverController::class, 'getDeleted'])->name('driver.deleted');
});

Route::group([ 'namespace' => 'OMC', 'middleware' => ['auth'], 'prefix' => 'omc', 'as' => 'omc.' ], function(){

	Route::get('/index', 'OmcController@index')->name('index');
	Route::post('/addcontacts/{Omc_id}', 'OmcController@createContact')->name('addcontacts');

	Route::get('/add', 'OmcController@create')->name('add');
	Route::post('/store', 'OmcController@Omcstore')->name('store');
	Route::post('/storeLoc/{omc_id}', 'OmcController@storeLocations')->name('storeLoc');
	Route::get('/data', 'OmcController@data')->name('data');
	Route::get('/{id}/contacts', 'OmcController@contacts')->name('contacts');
	Route::get('/locations', 'OmcController@locations')->name('locations');
	Route::get('/show/{id}', 'OmcController@show')->name('show');
	Route::patch('/update/{id}', 'OmcController@update')->name('update');
	Route::patch('/addressupdate/{id}', 'OmcController@addressupdate')->name('addressupdate');
	Route::delete('/addressdelete/{id}', 'OmcController@addressdelete')->name('addressdelete');
	Route::post('/store/{id}', 'OmcController@addressStore')->name('addressstore');
	Route::get('/delete/{id}', 'OmcController@deleteOmc')->name('delete');
	Route::get('/address', 'OmcController@address')->name('address');
	Route::patch('/{id}/manageOmcProduct', 'OmcController@manageOmcProduct')->name('manageOmcProduct');
	Route::get('/{id}/location', 'OmcController@location')->name('location');
	Route::delete('/deleteLoc/{id}', 'OmcController@deleteLocation')->name('deleteLoc');
	Route::delete('/deleteContact/{id}', 'OmcController@deleteContact')->name('deleteContact');
	Route::post('/{id}/updateInventory','OmcController@manageOmcInventory')->name('manageOmcInventory');
});

Route::group([ 'namespace' => 'OMC', 'middleware' => ['auth'], 'prefix' => 'importer', 'as' => 'importer.' ], function(){
	Route::get('/', 'ImporterController@index')->name('index');
	Route::get('/add', 'ImporterController@create')->name('add');
	Route::get('/data', 'ImporterController@data')->name('data');
	Route::get('/delete/{id}','ImporterController@delete')->name('delete');
	Route::get('/{id}/manage','ImporterController@manage')->name('manage');
	Route::patch('/{id}/update','ImporterController@manageImporter')->name('manageImporter');
	Route::post('/{id}/updateInventory','ImporterController@manageImporterInventory')->name('manageImporterInventory');
	Route::get('/getImporterProductList/{id}', 'ImporterController@getImporterProductList')->name('getImporterProductList');
	Route::get('/{id}/location', 'ImporterController@location')->name('location');
	Route::post('/storeLoc/{omc_id}', 'ImporterController@storeLocations')->name('storeLoc');
	Route::delete('/deleteLoc/{id}', 'ImporterController@deleteLocation')->name('deleteLoc');
	Route::get('/{id}/contacts', 'ImporterController@contacts')->name('contacts');
	Route::post('/addcontacts/{id}', 'ImporterController@createContact')->name('addcontacts');
	Route::delete('/addressdelete/{id}', 'ImporterController@addressdelete')->name('addressdelete');
	Route::delete('/deleteContact/{id}', 'ImporterController@deleteContact')->name('deleteContact');
	Route::post('/getImporterTransactionData', 'ImporterController@getImporterTransactionData')->name('getImporterTransactionData');
});

Route::group([ 'namespace' => 'Audit', 'middleware' => ['auth'], 'prefix' => 'audit', 'as' => 'audit.' ], function(){
	Route::get('/logs', 'LogsController@index')->name('logs');
});

