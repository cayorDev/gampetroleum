<?php

use App\Http\Controllers\Backend\VehicleController;
use App\Http\Controllers\Backend\DriverController;
use App\Http\Controllers\Backend\LoaderController;
use App\Http\Controllers\Backend\ManagementController;

Route::group(['middleware' => ['auth'], 'prefix' => 'vehicle'], function () {
	Route::get('/add', [VehicleController::class, 'create'])->name('vehicle.add');
	Route::get('/getVehicleData', [VehicleController::class, 'getVehicleData'])->name('vehicle.data');
	Route::post('/store', [VehicleController::class, 'store'])->name('vehicle.store');
	Route::get('/index', [VehicleController::class, 'index'])->name('vehicle.index');
	Route::get('/show/{id}', [VehicleController::class, 'show'])->name('vehicle.show');
	Route::get('edit/{id}', [VehicleController::class, 'edit'])->name('vehicle.edit');
	Route::patch('/{id}', [VehicleController::class, 'update'])->name('vehicle.update');
	Route::delete('/destroy/{id}', [VehicleController::class, 'destroy'])->name('vehicle.destroy');
	Route::get('/destroy/{id}', [VehicleController::class, 'destroy'])->name('vehicle.destroy');
	Route::get('/deleted', [VehicleController::class, 'deleted_vehicles'])->name('vehicle.deleted');
	// Route::get('delete/{id}', [VehicleController::class, 'delete'])->name('vehicle.per-delete');
	Route::get('restore/{id}/', [VehicleController::class, 'restore'])->name('vehicle.restore');
	Route::get('/getDriverList/{id}', [VehicleController::class, 'getDriverList'])->name('driver_vehicle.getList');
	Route::get('/getDeletedVehicleData', [VehicleController::class, 'getDeletedVehicleData'])->name('vehicle.getDeletedVehicleData');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'driver'], function () {
	Route::get('/add', [DriverController::class, 'create'])->name('driver.add');
	Route::get('/getDriverData', [DriverController::class, 'getDriverData'])->name('driver.getDriverData');
	Route::post('/store', [DriverController::class, 'store'])->name('driver.store');
	Route::get('/index', [DriverController::class, 'index'])->name('driver.index');
	Route::get('/show/{id}', [DriverController::class, 'show'])->name('driver.show');
	Route::get('edit/{id}', [DriverController::class, 'edit'])->name('driver.edit');
	Route::patch('/{id}', [DriverController::class, 'update'])->name('driver.update');
	Route::delete('/destroy/{id}', [DriverController::class, 'destroy'])->name('driver.destroy');
	Route::get('/destroy/{id}', [DriverController::class, 'destroy'])->name('driver.destroy');
	// Route::get('delete/{id}', [DriverController::class, 'delete'])->name('driver.delete-permanently');
	Route::get('restore/{id}', [DriverController::class, 'restore'])->name('driver.restore');
	Route::get('/deleted', [DriverController::class, 'getDeleted'])->name('driver.deleted');
	Route::get('/getDeletedDriverData', [DriverController::class, 'getDeletedDriverData'])->name('driver.getDeletedDriverData');
});

Route::group([ 'namespace' => 'OMC', 'middleware' => ['auth'], 'prefix' => 'omc', 'as' => 'omc.' ], function(){

	Route::get('/index', 'OmcController@index')->name('index');
	Route::post('/addcontacts/{Omc_id}', 'OmcController@createContact')->name('addcontacts');

	Route::get('/add', 'OmcController@create')->name('add');
	Route::post('/store', 'OmcController@Omcstore')->name('store');
	Route::post('/storeLoc/{omc_id}', 'OmcController@storeLocations')->name('storeLoc');
	Route::get('/data', 'OmcController@data')->name('data');
	Route::get('/{id}/contacts', 'OmcController@contacts')->name('contacts');
	Route::get('/locations', 'OmcController@locations')->name('locations');
	Route::get('/show/{id}', 'OmcController@show')->name('show');
	Route::patch('/update/{id}', 'OmcController@update')->name('update');
	Route::patch('/addressupdate/{id}', 'OmcController@addressupdate')->name('addressupdate');
	Route::get('/addressdelete/{id}', 'OmcController@addressdelete')->name('addressdelete');
	Route::post('/store/{id}', 'OmcController@addressStore')->name('addressstore');
	Route::get('/delete/{id}', 'OmcController@deleteOmc')->name('delete');
	Route::get('/address', 'OmcController@address')->name('address');
	Route::get('/{id}/location', 'OmcController@location')->name('location');
	Route::get('/deleteLoc/{id}', 'OmcController@deleteLocation')->name('deleteLoc');
	Route::get('/deleteContact/{id}', 'OmcController@deleteContact')->name('deleteContact');
	Route::patch('/{id}/manageOmcProduct', 'OmcController@manageOmcProduct')->name('manageOmcProduct');
    Route::post('/{id}/updateInventory','OmcController@manageOmcInventory')->name('manageOmcInventory');
    Route::get('/{id}/viewTransactions', 'OmcController@viewTransactions')->name('transactions');
    Route::get('/{id}/excelTransactions', 'OmcController@excelTransactions')->name('excelTransactions');
     Route::post('/getOmcTransactionData', 'OmcController@getOmcTransactionData')->name('getOmcTransactionData');
});

Route::group([ 'namespace' => 'OMC', 'middleware' => ['auth'], 'prefix' => 'importer', 'as' => 'importer.' ], function(){
	Route::get('/', 'ImporterController@index')->name('index');
	Route::get('/add', 'ImporterController@create')->name('add');
	Route::get('/data', 'ImporterController@data')->name('data');
	Route::get('/delete/{id}','ImporterController@delete')->name('delete');
	Route::get('/{id}/manage','ImporterController@manage')->name('manage');
	Route::get('/{id}/contacts', 'ImporterController@contacts')->name('contacts');
	Route::get('/{id}/locations', 'ImporterController@location')->name('location');
	Route::patch('/{id}/update','ImporterController@manageImporter')->name('manageImporter');
	Route::post('/{id}/manageImporterInventory','ImporterController@manageImporterInventory')->name('manageImporterInventory');
	Route::get('/addressdelete/{id}', 'ImporterController@addressdelete')->name('addressdelete');
	Route::get('/getImporterProductList/{id}', 'ImporterController@getImporterProductList')->name('getImporterProductList');
	Route::post('/addcontacts/{id}', 'ImporterController@createContact')->name('addcontacts');
    Route::get('/deleteContact/{id}', 'ImporterController@deleteContact')->name('deleteContact');
    Route::get('/{id}/viewTransactions', 'ImporterController@viewTransactions')->name('transactions');
    Route::get('/{id}/excelTransactions', 'ImporterController@excelTransactions')->name('excelTransactions');
    Route::post('/getImporterTransactionData', 'ImporterController@getImporterTransactionData')->name('getImporterTransactionData');
});

Route::group([ 'namespace' => 'Audit', 'middleware' => ['auth'], 'prefix' => 'audit', 'as' => 'audit.' ], function(){
	Route::get('/logs', 'LogsController@index')->name('logs');
	Route::get('/log/{id}/remove', 'LogsController@remove')->name('remove');
});

Route::group([ 'namespace' => 'Order', 'middleware' => ['auth'], 'prefix' => 'release', 'as' => 'release.' ], function(){
	Route::get('/', 'ReleaseController@index')->name('index');
	Route::get('/show/{id}', 'ReleaseController@show')->name('show');
	Route::get('/getReleaseData', 'ReleaseController@getReleaseData')->name('releaseData');
	Route::get('/add', 'ReleaseController@releaseadd')->name('add');
	Route::post('/add', 'ReleaseController@addRelease')->name('store');
	Route::post('/addomc', 'ReleaseController@addReleaseOmc')->name('storeomc');

});
Route::group([ 'namespace' => 'Order', 'middleware' => ['auth'] , 'prefix' => 'loader'], function(){
Route::get('/loader_list', 'OrderController@loaderList')->name('loaderList');
// Route::get('/getLoaderData','OrderController@getLoaderData')->name('getLoaderData');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'loader'], function () {
	Route::get('/add', [LoaderController::class, 'create'])->name('loader.add');
	Route::get('/getLoaderData', [LoaderController::class, 'getLoaderData'])->name('loader.getLoaderData');
	Route::post('/store', [LoaderController::class, 'store'])->name('loader.store');
	Route::get('/index', [LoaderController::class, 'index'])->name('loader.index');
	Route::get('/show/{id}', [LoaderController::class, 'show'])->name('loader.show');
	Route::get('edit/{id}', [LoaderController::class, 'edit'])->name('loader.edit');
	Route::patch('/{id}', [LoaderController::class, 'update'])->name('loader.update');
	Route::delete('/destroy/{id}', [LoaderController::class, 'destroy'])->name('loader.destroy');
	Route::get('/destroy/{id}', [LoaderController::class, 'destroy'])->name('loader.destroy');
	// Route::get('delete/{id}', [LoaderController::class, 'delete'])->name('loader.delete-permanently');
	Route::get('restore/{id}', [LoaderController::class, 'restore'])->name('loader.restore');
	Route::get('/getDeletedLoader', [LoaderController::class, 'getDeletedLoader'])->name('loader.getDeletedLoader');
	Route::get('/getAllDeleted', [LoaderController::class, 'getAllDeleted'])->name('loader.getAllDeleted');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'management'], function () {
	Route::get('/getManagementData', [ManagementController::class, 'getManagementData'])->name('management.getManagementData');
	Route::get('/index', [ManagementController::class, 'index'])->name('management.index');
	Route::get('/show/{id}', [ManagementController::class, 'show'])->name('management.show');
	Route::get('edit/{id}', [ManagementController::class, 'edit'])->name('management.edit');
	Route::patch('/{id}', [ManagementController::class, 'update'])->name('management.update');
});





