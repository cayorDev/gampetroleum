<?php

Route::group([ 'namespace' => 'Order', 'middleware' => ['auth']], function(){
    Route::get('/getOmcProductList/{id}', 'OrderController@getOmcProductList')->name('omcproducts');
    Route::get('/getSpecifiedOmcProductList', 'OrderController@getSpecifiedOmcProductList')->name('getSpecifiedOmcProductList');
    Route::get('/getSpecifiedOmcProductListOmcBased', 'OrderController@getSpecifiedOmcProductListOmcBased')->name('getSpecifiedOmcProductListOmcBased');

    Route::get('/getVehicleChambers/{id}', 'OrderController@getVehicleChambers')->name('chambersview');
    Route::get('/all', 'OrderController@index')->name('index');
    Route::get('/add', 'OrderController@create')->name('add');
    Route::post('/store', 'OrderController@store')->name('store');
    Route::post('/hfostore', 'OrderController@hfostore')->name('hfostore');
    Route::post('/omcstore', 'OrderController@omcstore')->name('omcstore');
    Route::get('/supervisorIndex', 'OrderController@supervisorIndex')->name('supervisorIndex');
    Route::get('/supervisorData', 'OrderController@supervisorData')->name('supervisorData');
    
    Route::get('/getOrderData', 'OrderController@supervisorData')->name('data');
    

    Route::get('/delete/{id}','OrderController@delete')->name('delete');
    Route::get('/edit/{id}','OrderController@edit')->name('edit');
    Route::get('/view/{id}','OrderController@show')->name('view');
    Route::patch('/update/{id}','OrderController@update')->name('update');
    Route::post('/update_products', 'OrderController@update_products')->name('update_products');

    Route::get('/finalOrder/{id}', 'OrderController@finalOrder')->name('finalOrder');
    Route::get('approveorder/{id}','OrderController@approveOrder')->name('approveorder');
    Route::get('/orderExcel', 'OrderController@orderExcel')->name('orderExcel');
    Route::get('/orderReport', 'OrderController@orderReport')->name('orderReport');
    Route::post('/getYearData', 'OrderController@getYearData')->name('getYearData');
    Route::post('/getOrderTransaction', 'OrderController@getOrderTransaction')->name('getOrderTransaction');
    Route::get('/dailyReport', 'OrderController@dailyReport')->name('dailyReport');
    Route::post('/getDailyData', 'OrderController@getDailyData')->name('getDailyData');
    Route::get('/dailyExcel', 'OrderController@dailyExcel')->name('dailyExcel');
    Route::get('/inventoryReport', 'OrderController@inventoryReport')->name('inventoryReport');
    Route::post('/getInventoryData', 'OrderController@getInventoryData')->name('getInventoryData');
    Route::get('/inventoryExcel', 'OrderController@inventoryExcel')->name('inventoryExcel');

    Route::get('/orderTypeExcel', 'OrderController@orderTypeExcel')->name('orderTypeExcel');
    Route::get('/customOrders', 'OrderController@customOrders')->name('customOrders');
    Route::get('/getCustomsOrderData', 'OrderController@getCustomsOrderData')->name('getCustomsOrderData');
    Route::get('markCustomApproved/{id}','OrderController@markCustomApproved')->name('markCustomApproved');
     Route::get('/getMeterForProduct', 'OrderController@getMeterForProduct')->name('getMeterForProduct');

});

Route::group([ 'namespace' => 'Order', 'middleware' => ['auth'], 'prefix' => 'assignChamber', 'as' => 'assignchamber.' ], function(){
    Route::get('/{id}', 'ChamberAssignmentController@assignChamber')->name('index');
    Route::post('/store', 'ChamberAssignmentController@store')->name('store');
    Route::get('/truckLoadingConfirmation/{id}', 'ChamberAssignmentController@truckLoadingConfirmation')->name('truckLoadingConfirmation');
    Route::get('/upliftingVerification/{id}', 'ChamberAssignmentController@upliftingVerification')->name('upliftingVerification');
    Route::post('/uplift_vehicle', 'ChamberAssignmentController@uplift_vehicle')->name('uplift_vehicle');
});

Route::group([ 'namespace' => 'Order', 'middleware' => ['auth'],], function() {
    Route::get('/viewupliftingform/{id}', 'ChamberAssignmentController@show')->name('viewupliftingform');
});

?>